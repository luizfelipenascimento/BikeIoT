package android.scopus.com.bikeiot.routes;

import android.content.Context;
import android.content.Intent;
import android.scopus.com.bikeiot.FactoryFakeInputUtils;
import android.scopus.com.bikeiot.utils.Helper;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.data.local.BikeIotDbHelper;
import android.scopus.com.bikeiot.model.data.local.RouteDAO;
import android.scopus.com.bikeiot.routedetails.RouteDetailsActivity;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.ArrayList;

import static android.scopus.com.bikeiot.utils.CalculateUtils.metersToKm;
import static android.scopus.com.bikeiot.TestHelper.TestRecyclerViewHelper.nthChildOf;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.*;

/**
 *
 */
public class RoutesScreenTest {

    @Rule
    public ActivityTestRule<RoutesActivity> mRoutesActivityRule = new ActivityTestRule<>(
            RoutesActivity.class, true, false);

    private RouteDAO mRouteDAO;
    private Context mContext;
    private BikeIotDbHelper mDbHelper;

    @Before
    public void setupRoutesScreenTest() {
        mContext = InstrumentationRegistry.getTargetContext();
        mDbHelper = new BikeIotDbHelper(mContext);
        mRouteDAO = new RouteDAO(mContext);
        Intents.init();
    }

    @After
    public void tearDown() {
        mDbHelper.dropDatabaseTables(mDbHelper.getWritableDatabase());
        mDbHelper.onCreate(mDbHelper.getWritableDatabase());
        mDbHelper.close();
        Intents.release();
    }

    @Test
    public void listRoutesTest_whenThereAreRoutesToList() {

        ArrayList<Route> routes = new ArrayList<>();

        int maxSize = 4;
        for (int i = 0; i < maxSize; i++) {
            Route route = FactoryFakeInputUtils.createFakeRoute(mContext);
            route.setDescription("Route " + i);
            assertTrue(mRouteDAO.save(route));
            routes.add(route);
        }

        mRoutesActivityRule.launchActivity(new Intent());

        for (int i = 0; i < routes.size(); i++) {
            //asserting description
            onView(nthChildOf(withId(R.id.routes_recycler_view), i))
                    .check(matches(hasDescendant(withText(routes.get(i).getDescription()))));

            //asserting distance format
            String distance = Helper.formatKmDecimal(metersToKm(routes.get(i).getDistance()));
            onView(nthChildOf(withId(R.id.routes_recycler_view), i))
                    .check(matches(hasDescendant(withText(String.valueOf(distance + " " + mContext.getString(R.string.km_indicator))))));
            //asserting date info
            onView(nthChildOf(withId(R.id.routes_recycler_view), i))
                    .check(matches(hasDescendant(withText(Helper.formatDateToMyDateFormat(routes.get(i).getDate())))));
        }
    }

    @Test
    public void selectRouteOnList() {

        Route route = FactoryFakeInputUtils.createFakeRoute(mContext);
        mRouteDAO.save(route);

        mRoutesActivityRule.launchActivity(new Intent());

        onView(nthChildOf(withId(R.id.routes_recycler_view), 0))
                .perform(click());

        intended(hasComponent(RouteDetailsActivity.class.getName()));
    }
    //TODO: adicionar test para exibir somente rotas com status publicado:

    //TODO: adicionar test para exibir somente "minhas rotas"
}