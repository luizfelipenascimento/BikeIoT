package android.scopus.com.bikeiot;

import android.content.Context;
import android.scopus.com.bikeiot.model.beans.BikeDevice;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.filerelated.CSVFile;
import android.scopus.com.bikeiot.model.beans.Contact;
import android.scopus.com.bikeiot.model.beans.Result;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.utils.Helper;


import com.google.android.gms.maps.model.LatLng;

import java.util.Date;
import java.util.UUID;

import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.CSVFileRouteContract.CSV_ROUTE_COLUMN_DATETIME;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.CSVFileRouteContract.CSV_ROUTE_COLUMN_LATITUDE;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.CSVFileRouteContract.CSV_ROUTE_COLUMN_LONGITUDE;


public final class FactoryFakeInputUtils {

    private FactoryFakeInputUtils() {}

    public static Result createFakeResult(Context context) {
        Result result = new Result();
        result.setTotalTimeChronometer("00:20:00");
        result.setMaxVelocityMetersPerSec(20);
        result.setMeanVelocityMetersPerSec(10);
        result.setMeanHeartRate(80);
        result.setCaloriesBurned(200);
        result.setResultCSVFile(new CSVFile(context));
        result.setFileResultPath("test_path");
        return result;
    }

    public static BikeExercise createBikeExercise() {
        BikeExercise bikeExercise = new BikeExercise();
        bikeExercise.setExerciseDate(new Date());
        bikeExercise.setUser(new User());
        bikeExercise.setResult(new Result());
        bikeExercise.setRoute(new Route());
        bikeExercise.setBikeDevice(new BikeDevice());
        bikeExercise.setDescription("exercise");
        return bikeExercise;
    }

    public static BikeExercise createBikeExercise(User user, Result result, Route route, BikeDevice bikeDevice) {
        BikeExercise bikeExercise = new BikeExercise();
        bikeExercise.setExerciseDate(Helper.parseToDate(Helper.formatToDateString(new Date())));
        bikeExercise.setUser(user);
        bikeExercise.setResult(result);
        bikeExercise.setRoute(route);
        bikeExercise.setBikeDevice(bikeDevice);
        bikeExercise.setDescription("exercise");
        return bikeExercise;
    }

    public static CSVFile createFakeCsvFile(Context context, String fileName) {
        CSVFile file = new CSVFile(fileName,
                context,
                CSVFile.TEST_ROOT_FOLDER,
                new String[]{"testColumn", "testColumn2"});
        file.writeLine(new String[]{"sadasd","asdasd"});
        file.writeLine(new String[]{"asdad","asdasd"});
        file.writeLine(new String[]{"asdads","149"});
        return file;
    }

    public static CSVFile createFakeRouteCsvFile(Context context, String fileName) {
        String[] fileHeader = {CSV_ROUTE_COLUMN_LATITUDE,
                CSV_ROUTE_COLUMN_LONGITUDE,
                CSV_ROUTE_COLUMN_DATETIME};

        CSVFile file = new CSVFile(fileName,
                context,
                CSVFile.TEST_ROOT_FOLDER,
                fileHeader);

        file.writeLine(new String[]{"-35.2784167","149.1294692"});
        file.writeLine(new String[]{"-35.280321693840129","149.12908274880189"});
        file.writeLine(new String[]{"-35.2805167","149.1290879"});
        file.close();
        return file;
    }

    public static BikeDevice createFakeBikeDevice() {
        BikeDevice bikeDevice = new BikeDevice();
        bikeDevice.setRimValue(25);
        return bikeDevice;
    }

    public static User createFakeUser() {
        User user = new User();
        user.setPassword("123456");
        user.setName("luiz");
        user.setBirthday(Helper.parseToDate(Helper.formatToDateString(new Date())));
        user.setEmail("luiz@gmail.com");
        user.setHeight(1);
        user.setWeight(90);
        user.setUUID(UUID.randomUUID());
        return user;
    }

    public static Route createFakeRoute(Context context) {
        Route route = new Route();
        route.setDescription("Route test " + (int)Math.random() * 50);
        route.setDistance((1000 * (Math.random() * 10)));
        route.setEndLatLng(new LatLng((Math.random() * 50) + 0.222, (Math.random() * 50) + 0.6987));
        route.setStartLatLng(new LatLng((Math.random() * 50) + 0.555, (Math.random() * 50) + 0.7895));
        route.setRouteCSVFile(createFakeRouteCsvFile(context, "routeTestCSVFile_" + UUID.randomUUID()));
        route.setFileRoutePath(route.getRouteCSVFile().getPathFile());
        route.setImagePath("test_image_path");
        route.setDate(new Date());
        return route;
    }

    public static Contact createFakeContact() {
        Contact contact = new Contact();
        contact.setName("luiz test");
        contact.setPhoneNumber("+551122006688");
        contact.setUser(new User());
        return contact;
    }
}
