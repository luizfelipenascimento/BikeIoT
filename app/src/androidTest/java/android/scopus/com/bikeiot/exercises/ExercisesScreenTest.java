package android.scopus.com.bikeiot.exercises;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.scopus.com.bikeiot.FactoryFakeInputUtils;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.exercisedetails.ExerciseDetailsActivity;
import android.scopus.com.bikeiot.model.beans.BikeDevice;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.beans.Result;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.model.data.local.BikeExerciseDAO;
import android.scopus.com.bikeiot.model.data.local.BikeIotDbHelper;
import android.scopus.com.bikeiot.model.data.local.RouteDAO;
import android.scopus.com.bikeiot.model.data.local.UserDAO;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.Intents;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static android.scopus.com.bikeiot.TestHelper.TestRecyclerViewHelper.nthChildOf;

import static android.support.test.espresso.Espresso.onView;

import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 *
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ExercisesScreenTest {


    @Rule
    public ActivityTestRule<ExercisesActivity> mExercisesActivityRule = new ActivityTestRule<>(
            ExercisesActivity.class, true, false);

    private RouteDAO mRouteDAO;
    private BikeExerciseDAO mBikeExerciseDAO;
    private UserDAO mUserDAO;
    private BikeIotDbHelper mDbHelper;
    private SharedPreferences mSharedPreferences;
    private Context mContext;

    private void creatingUserSession(int id) {
        mSharedPreferences = mContext.getSharedPreferences(
                mContext.getString(R.string.shared_preferences_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(mContext.getString(R.string.shared_user_id), id);
        editor.commit();
    }

    private void clearSharedPreferences() {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.clear().commit();
    }

    @Before
    public void setupExercisesScreenTest() {
        mContext = InstrumentationRegistry.getTargetContext();
        mSharedPreferences = mContext.getSharedPreferences(
                mContext.getString(R.string.shared_preferences_key), Context.MODE_PRIVATE);

        mDbHelper = new BikeIotDbHelper(mContext);
        mUserDAO = new UserDAO(mContext);
        mBikeExerciseDAO = new BikeExerciseDAO(mContext);
        mRouteDAO = new RouteDAO(mContext);
        Intents.init();

    }

    @After
    public void tearDown() {
        clearSharedPreferences();
        mDbHelper.dropDatabaseTables(mDbHelper.getWritableDatabase());
        mDbHelper.onCreate(mDbHelper.getWritableDatabase());
        mDbHelper.close();
        Intents.release();
    }


    @Test
    public void testThereIsExerciseToList() {
        User user = FactoryFakeInputUtils.createFakeUser();
        Assert.assertTrue(mUserDAO.saveUser(user));

        Route route = FactoryFakeInputUtils.createFakeRoute(mContext);
        route.setRouteCSVFile(FactoryFakeInputUtils.createFakeCsvFile(mContext, "RouteTest"));
        route.setFileRoutePath(route.getRouteCSVFile().getPathFile());
        Assert.assertTrue(mRouteDAO.save(route));

        Result result = FactoryFakeInputUtils.createFakeResult(mContext);
        result.setResultCSVFile(FactoryFakeInputUtils.createFakeCsvFile(mContext, "testResult"));
        result.setFileResultPath(result.getResultCSVFile().getPathFile());


        ArrayList<BikeExercise> bikeExercises = new ArrayList<>();

        int maxSize = 4;
        for (int i = 0; i < maxSize; i++) {
            BikeExercise bikeExercise = FactoryFakeInputUtils.createBikeExercise();
            bikeExercise.setBikeDevice(new BikeDevice());
            bikeExercise.getBikeDevice().setRimValue(25);
            bikeExercise.setUser(user);
            bikeExercise.setResult(result);
            bikeExercise.setRoute(route);
            bikeExercise.setDescription("Exercicio " + i);
            bikeExercises.add(bikeExercise);
        }

        for (BikeExercise bte : bikeExercises) {
            Assert.assertTrue(mBikeExerciseDAO.saveBikeExercise(bte));
        }

        creatingUserSession(user.getId());


        mExercisesActivityRule.launchActivity(new Intent());

        for (int i = 0; i < bikeExercises.size(); i++) {
            onView(nthChildOf(withId(R.id.exercises_recycler_view), i))
                    .check(matches(hasDescendant(withText(bikeExercises.get(i).getDescription()))));
        }

    }

    @Test
    public void selectExercise_showExerciseDetailsActivity() {
        BikeExercise bikeExercise = FactoryFakeInputUtils.createBikeExercise(
                FactoryFakeInputUtils.createFakeUser(), FactoryFakeInputUtils.createFakeResult(mContext),
                FactoryFakeInputUtils.createFakeRoute(mContext), FactoryFakeInputUtils.createFakeBikeDevice());

        mUserDAO.saveUser(bikeExercise.getUser());
        mRouteDAO.save(bikeExercise.getRoute());
        mBikeExerciseDAO.saveBikeExercise(bikeExercise);

        creatingUserSession(bikeExercise.getUser().getId());

        mExercisesActivityRule.launchActivity(new Intent());

        onView(nthChildOf(withId(R.id.exercises_recycler_view), 0))
                .perform(click());

        intended(hasComponent(ExerciseDetailsActivity.class.getName()));
    }

    //TODO: implementar o teste para exibir o container de nenhum exercicio realizado1
    @Test
    public void testNoExerciseToBeDisplayed() {

    }
}