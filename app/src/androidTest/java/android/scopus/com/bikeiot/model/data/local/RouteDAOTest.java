package android.scopus.com.bikeiot.model.data.local;

import android.content.Context;
import android.scopus.com.bikeiot.FactoryFakeInputUtils;
import android.scopus.com.bikeiot.model.filerelated.CSVFile;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.beans.User;
import android.support.test.InstrumentationRegistry;

import com.google.android.gms.maps.model.LatLng;

import static android.scopus.com.bikeiot.utils.Helper.*;
import static junit.framework.Assert.*;
import static org.junit.Assert.assertNotEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 */
public class RouteDAOTest {


    private BikeIotDbHelper mDbHelper;
    private Context mContext;
    private RouteDAO mRouteDAO;
    private UserDAO mUserDAO;

    @Before
    public void setupRouteDAOTest() {
        mContext = InstrumentationRegistry.getTargetContext();
        mDbHelper = new BikeIotDbHelper(mContext);
        mRouteDAO = new RouteDAO(mContext);
        mUserDAO = new UserDAO(mContext);
    }

    @After
    public void tearDown() {
        mDbHelper.dropDatabaseTables(mDbHelper.getWritableDatabase());
        mDbHelper.onCreate(mDbHelper.getWritableDatabase());
        mDbHelper.close();
    }

    @Test
    public void saveAndSelectWithUser() {
        Route route = FactoryFakeInputUtils.createFakeRoute(mContext);
        ArrayList<LatLng> locations = new ArrayList<>();
        locations.add(new LatLng(45.555,48.6987));
        locations.add(new LatLng(42.555,43.5555));
        route.setEndLatLng(locations.get(locations.size() - 1));
        route.setStartLatLng(locations.get(0));
        route.setShared(BikeIoTContract.RouteEntry.TO_SHARE);
        User user = FactoryFakeInputUtils.createFakeUser();
        assertTrue(mUserDAO.saveUser(user));

        route.setUser(user);
        assertTrue(mRouteDAO.save(route));

        //select test
        Route savedRoute = mRouteDAO.getRoute(route.getId());

        assertNotNull(savedRoute);
        //asserting content
        assertEquals(route.getId(), savedRoute.getId());
        assertEquals(route.getDescription(), savedRoute.getDescription());
        assertEquals(route.getEndLatLng().latitude, savedRoute.getEndLatLng().latitude);
        assertEquals(route.getEndLatLng().longitude, savedRoute.getEndLatLng().longitude);
        assertEquals(route.getStartLatLng().latitude, savedRoute.getStartLatLng().latitude);
        assertEquals(route.getStartLatLng().longitude, savedRoute.getStartLatLng().longitude);
        assertEquals(locations.get(locations.size() - 1).latitude, savedRoute.getEndLatLng().latitude);
        assertEquals(locations.get(locations.size() - 1).longitude, savedRoute.getEndLatLng().longitude);
        assertEquals(locations.get(0).latitude, savedRoute.getStartLatLng().latitude);
        assertEquals(locations.get(0).longitude, savedRoute.getStartLatLng().longitude);
        assertEquals(route.getDistance(), savedRoute.getDistance());
        assertEquals(route.getRouteCsvFilePath(), savedRoute.getRouteCsvFilePath());
        assertEquals(formatToDateString(route.getDate()), formatToDateString(savedRoute.getDate()));
        assertEquals(route.getImagePath(), savedRoute.getImagePath());

        //asserting user id
        assertEquals(route.getUser().getId(), savedRoute.getUser().getId());

    }

    @Test
    public void saveAndSelectWithoutUser() {
        Route route = FactoryFakeInputUtils.createFakeRoute(mContext);
        assertTrue(mRouteDAO.save(route));

        Route selectedRoute = mRouteDAO.getRoute(route.getId());

        assertEquals(0, selectedRoute.getUser().getId());
    }

    @Test
    public void saveAndSelectWithImagePathNull() {
        Route route = FactoryFakeInputUtils.createFakeRoute(mContext);
        route.setImagePath(null);
        assertTrue(mRouteDAO.save(route));

        //select test
        Route savedRoute = mRouteDAO.getRoute(route.getId());

        assertNotNull(savedRoute);
        assertNull(savedRoute.getImagePath());
        assertEquals(route.getImagePath(), savedRoute.getImagePath());
    }

    @Test
    public void updateShareRoute() {
        Route route = FactoryFakeInputUtils.createFakeRoute(mContext);
        assertTrue(mRouteDAO.save(route));

        route.setShared(BikeIoTContract.RouteEntry.TO_SHARE);
        assertTrue(mRouteDAO.update(route));

        Route selectedRoute = mRouteDAO.getRoute(route.getId());
        assertEquals(route.getShared(), selectedRoute.getShared());
    }

    @Test
    public void renamingRouteFilePath() {
        Route route = new Route();
        route.setEndLatLng(new LatLng(45.555,48.6987));
        route.setStartLatLng(new LatLng(42.555,43.5555));
        route.setDistance(20);
        route.setDate(new Date());
        route.setFileRoutePath(FactoryFakeInputUtils.createFakeCsvFile(mContext, "testRouteFile").getPathFile());
        route.setDescription("Route 1");

        String oldRouteFileNamePath = route.getRouteCsvFilePath();
        assertTrue(mRouteDAO.save(route));


        CSVFile csvFile = new CSVFile(mContext);
        csvFile.renameFile(route.getRouteCsvFilePath(), "newFileNameRoute");
        route.setFileRoutePath(csvFile.getPathFile());
        String newRouteFilePathName = route.getRouteCsvFilePath();

        assertTrue(mRouteDAO.renameRouteFilePath(route));

        Route updatedRoute = mRouteDAO.getRoute(route.getId());

        assertNotNull(updatedRoute);
        assertNotEquals(oldRouteFileNamePath, updatedRoute.getRouteCsvFilePath());
        assertEquals(newRouteFilePathName, updatedRoute.getRouteCsvFilePath());
    }

    @Test
    public void renamingImagePath() {
        Route route = FactoryFakeInputUtils.createFakeRoute(mContext);
        CSVFile pathImage = FactoryFakeInputUtils.createFakeCsvFile(mContext, "testRouteImagePath");
        route.setImagePath(pathImage.getPathFile());
        String oldImagePath = route.getImagePath();
        assertTrue(mRouteDAO.save(route));

        pathImage.renameFile(route.getImagePath(), "newImagePathNameRoute");
        route.setImagePath(pathImage.getPathFile());
        String newRouteFilePathName = route.getImagePath();

        assertTrue(mRouteDAO.updateRenameImagePath(route.getImagePath(), route.getId()));

        Route updatedRoute = mRouteDAO.getRoute(route.getId());

        assertNotNull(updatedRoute);
        assertNotEquals(oldImagePath, updatedRoute.getImagePath());
        assertEquals(newRouteFilePathName, updatedRoute.getImagePath());
    }



    @Test
    public void SelectingManyRoutesWithUser() {
        ArrayList<Route> routes = new ArrayList<>();
        int maxSize = 10;

        for (int i = 0; i < maxSize; i++) {
            User user = FactoryFakeInputUtils.createFakeUser();
            mUserDAO.saveUser(user);
            Route route = FactoryFakeInputUtils.createFakeRoute(mContext);
            route.setUser(user);
            routes.add(route);
            assertTrue(mRouteDAO.save(route));
        }

        List<Route> savedRoutes = mRouteDAO.getRoutes();

        assertNotNull(savedRoutes);
        assertFalse(savedRoutes.isEmpty());
        assertEquals(maxSize, savedRoutes.size());

        //assert content
        for (int i = 0; i < savedRoutes.size(); i++) {
            assertEquals(routes.get(i).getId(), savedRoutes.get(i).getId());
            assertEquals(routes.get(i).getDescription(), savedRoutes.get(i).getDescription());
            assertEquals(routes.get(i).getEndLatLng().latitude, savedRoutes.get(i).getEndLatLng().latitude);
            assertEquals(routes.get(i).getEndLatLng().longitude, savedRoutes.get(i).getEndLatLng().longitude);
            assertEquals(routes.get(i).getStartLatLng().latitude, savedRoutes.get(i).getStartLatLng().latitude);
            assertEquals(routes.get(i).getStartLatLng().longitude, savedRoutes.get(i).getStartLatLng().longitude);
            assertEquals(routes.get(i).getDistance(), savedRoutes.get(i).getDistance());
            assertEquals(routes.get(i).getRouteCsvFilePath(), savedRoutes.get(i).getRouteCsvFilePath());
            assertEquals(formatToDateString(routes.get(i).getDate()), formatToDateString(savedRoutes.get(i).getDate()));
            assertEquals(routes.get(i).getImagePath(), savedRoutes.get(i).getImagePath());
            assertEquals(routes.get(i).getUser().getId(), savedRoutes.get(i).getUser().getId());
        }
    }


    @Test
    public void updateRouteWithUser() {
        User user = FactoryFakeInputUtils.createFakeUser();
        mUserDAO.saveUser(user);

        Route oldRoute = FactoryFakeInputUtils.createFakeRoute(mContext);
        oldRoute.setUser(user);

        assertTrue(mRouteDAO.save(oldRoute));

        Route newRouteInfo = new Route();

        newRouteInfo.setId(oldRoute.getId());
        newRouteInfo.setEndLatLng(new LatLng(21.222, 20.6987));
        newRouteInfo.setStartLatLng(new LatLng(25.555, 26.7895));
        newRouteInfo.setDistance(40);
        newRouteInfo.setDate(parseToDate("2018-02-25"));
        newRouteInfo.setFileRoutePath(FactoryFakeInputUtils.createFakeCsvFile(mContext, "TestRouteFile").getPathFile());
        newRouteInfo.setDescription("updated Route done");
        newRouteInfo.setImagePath("new_image_path");
        assertTrue(mRouteDAO.update(newRouteInfo));

        Route updatedRoute = mRouteDAO.getRoute(oldRoute.getId());

        assertNotNull(updatedRoute);

        assertEquals(newRouteInfo.getId(), updatedRoute.getId());
        assertEquals(newRouteInfo.getDescription(), updatedRoute.getDescription());
        assertEquals(newRouteInfo.getEndLatLng().latitude, updatedRoute.getEndLatLng().latitude);
        assertEquals(newRouteInfo.getEndLatLng().longitude, updatedRoute.getEndLatLng().longitude);
        assertEquals(newRouteInfo.getStartLatLng().latitude, updatedRoute.getStartLatLng().latitude);
        assertEquals(newRouteInfo.getStartLatLng().longitude, updatedRoute.getStartLatLng().longitude);
        assertEquals(newRouteInfo.getDistance(), updatedRoute.getDistance());
        assertEquals(newRouteInfo.getRouteCsvFilePath(), updatedRoute.getRouteCsvFilePath());
        assertEquals(formatToDateString(newRouteInfo.getDate()), formatToDateString(updatedRoute.getDate()));
        assertEquals(newRouteInfo.getImagePath(), updatedRoute.getImagePath());


        assertEquals(oldRoute.getId(), updatedRoute.getId());
        assertEquals(oldRoute.getUser().getId(), updatedRoute.getUser().getId());
        assertNotEquals(oldRoute.getDescription(), updatedRoute.getDescription());
        assertNotEquals(oldRoute.getEndLatLng().latitude, updatedRoute.getEndLatLng().latitude);
        assertNotEquals(oldRoute.getEndLatLng().longitude, updatedRoute.getEndLatLng().longitude);
        assertNotEquals(oldRoute.getStartLatLng().latitude, updatedRoute.getStartLatLng().latitude);
        assertNotEquals(oldRoute.getStartLatLng().longitude, updatedRoute.getStartLatLng().longitude);
        assertNotEquals(oldRoute.getDistance(), updatedRoute.getDistance());
        assertNotEquals(formatToDateString(oldRoute.getDate()), formatToDateString(updatedRoute.getDate()));
        assertNotEquals(oldRoute.getImagePath(), updatedRoute.getImagePath());
    }

    @Test
    public void deleteRoute() {
        ArrayList<Route> routes = new ArrayList<>();
        int maxSize = 4;
        int lastRegisteredId = 0;
        for (int i = 0; i < maxSize; i++) {
            Route route = FactoryFakeInputUtils.createFakeRoute(mContext);
            routes.add(route);
            assertTrue(mRouteDAO.save(route));
            lastRegisteredId = route.getId();
        }

        assertTrue(lastRegisteredId > 0);
        assertTrue(mRouteDAO.remove(lastRegisteredId));

        Route deletedRoute = mRouteDAO.getRoute(lastRegisteredId);
        assertNull(deletedRoute);

        List<Route> selectedRoutes = mRouteDAO.getRoutes();

        assertEquals(maxSize - 1, selectedRoutes.size());
    }

    @Test
    public void deleteRouteWithUser() {
        User user = FactoryFakeInputUtils.createFakeUser();
        mUserDAO.saveUser(user);

        Route route = FactoryFakeInputUtils.createFakeRoute(mContext);
        route.setUser(user);

        mRouteDAO.save(route);

        assertTrue(mRouteDAO.remove(route.getId()));
        assertNull(mRouteDAO.getRoute(route.getId()));
    }

}