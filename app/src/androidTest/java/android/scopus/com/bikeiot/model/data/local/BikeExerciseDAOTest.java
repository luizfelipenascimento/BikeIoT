package android.scopus.com.bikeiot.model.data.local;

import android.content.Context;
import android.scopus.com.bikeiot.model.beans.BikeDevice;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.filerelated.CSVFile;
import android.scopus.com.bikeiot.model.beans.Result;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.beans.User;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.google.android.gms.maps.model.LatLng;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.scopus.com.bikeiot.utils.Helper.formatDateToTimeString;
import static android.scopus.com.bikeiot.utils.Helper.formatToDateString;
import static org.junit.Assert.*;

/**
 *
 */
@RunWith(AndroidJUnit4.class)
public class BikeExerciseDAOTest {

    private BikeIotDbHelper mDbHelper;
    private UserDAO mUserDao;
    private BikeExerciseDAO mBikeExerciseDAO;
    private RouteDAO mRouteDAO;
    private Context mContext;


    public User createFakeUser() {
        User user = new User();
        user.setPassword("123456");
        user.setName("luiz");
        user.setBirthday(new Date());
        user.setEmail("luiz@gmail.com");
        user.setHeight(1);
        user.setWeight(90);
        return user;
    }

    public Route createFakeRoute() {
        Route route = new Route();
        route.setDistance(12);
        route.setDate(new Date());
        route.setEndLatLng(new LatLng(45.6456,48.5220));
        route.setStartLatLng(new LatLng(45.6456,48.5220));
        route.setRouteCSVFile(new CSVFile(mContext));
        return route;
    }

    public CSVFile createFakeCsvFile() {
        return new CSVFile("testFile",
                mContext,
                CSVFile.ROUTE_ROOT_FOLDER_NAME,
                new String[]{"testColumn"});
    }

    private Result createFakeResult() {
        Result result = new Result();
        result.setTotalTimeChronometer("00:50:00");
        result.setResultCSVFile(new CSVFile(mContext));
        result.setMaxVelocityMetersPerSec(20);
        result.setMeanVelocityMetersPerSec(20);
        result.setMeanHeartRate(10);
        result.setCaloriesBurned(200);
        return result;
    }
    private BikeExercise createBikeExercise() {
        User user = createFakeUser();
        mUserDao.saveUser(user);

        Route route = createFakeRoute();
        route.setFileRoutePath(createFakeCsvFile().getPathFile());
        mRouteDAO.save(route);

        Result result = createFakeResult();
        result.setFileResultPath(createFakeCsvFile().getPathFile());

        BikeExercise bikeExercise = new BikeExercise();
        BikeDevice bikeDevice = new BikeDevice();
        bikeDevice.setRimValue(26);
        bikeExercise.setBikeDevice(bikeDevice);
        bikeExercise.setExerciseDate(new Date());
        bikeExercise.setUser(user);
        bikeExercise.setResult(result);
        bikeExercise.setRoute(route);
        bikeExercise.setDescription("exercise 1");

        return bikeExercise;
    }

    @Before
    public void setupBikeExerciseDAOTest() {
        mContext = InstrumentationRegistry.getTargetContext();
        mDbHelper = new BikeIotDbHelper(mContext);
        mRouteDAO = new RouteDAO(mContext);
        mUserDao = new UserDAO(mContext);
        mBikeExerciseDAO = new BikeExerciseDAO(mContext);
        mDbHelper.onUpgrade(mDbHelper.getWritableDatabase(), 4, 5);
    }

    @After
    public void tearDown() {
        mDbHelper.dropDatabaseTables(mDbHelper.getWritableDatabase());
        mDbHelper.onCreate(mDbHelper.getWritableDatabase());
        mDbHelper.close();
    }

    @Test
    public void savingAndSelectingBikeExercise() {
        BikeExercise bikeExercise = createBikeExercise();
        bikeExercise.setDescription("exercicio 1");
        Assert.assertTrue(mBikeExerciseDAO.saveBikeExercise(bikeExercise));

        BikeExercise savedBikeExercise = mBikeExerciseDAO.getBikeExercise(bikeExercise.getId());

        assertNotNull(savedBikeExercise);
        assertEquals(bikeExercise.getId(), savedBikeExercise.getId());

        assertEquals(formatToDateString(bikeExercise.getExerciseDate()),
                formatToDateString(savedBikeExercise.getExerciseDate()));

        assertEquals(bikeExercise.getResult().getFileResultPath(),
                savedBikeExercise.getResult().getFileResultPath());

        assertEquals(formatDateToTimeString(bikeExercise.getResult().getTotalTimeChronometer()),
                formatDateToTimeString(savedBikeExercise.getResult().getTotalTimeChronometer()));

        assertEquals(bikeExercise.getDescription(), savedBikeExercise.getDescription());

        assertEquals(bikeExercise.getUser().getId(), savedBikeExercise.getUser().getId());
        assertEquals(bikeExercise.getRoute().getId(), savedBikeExercise.getRoute().getId());

        Assert.assertEquals(bikeExercise.getResult().getMaxVelocityMetersPerSec(),
                savedBikeExercise.getResult().getMaxVelocityMetersPerSec());
        Assert.assertEquals(bikeExercise.getResult().getMeanVelocityMetersPerSec(),
                savedBikeExercise.getResult().getMeanVelocityMetersPerSec());

        //asserting rimValue
        Assert.assertEquals(bikeExercise.getBikeDevice().getRimValue(), savedBikeExercise.getBikeDevice().getRimValue());

        //asserting meanHeartBeats and spentCalories
        Assert.assertEquals(bikeExercise.getResult().getMeanHeartRate(), savedBikeExercise.getResult().getMeanHeartRate());
        Assert.assertEquals(bikeExercise.getResult().getCaloriesBurned(), savedBikeExercise.getResult().getCaloriesBurned());

    }

    @Test
    public void updateBikeExercise() {
        BikeExercise bikeExercise = createBikeExercise();
        bikeExercise.setDescription("exercicio 1");
        Assert.assertTrue(mBikeExerciseDAO.saveBikeExercise(bikeExercise));

        bikeExercise.getResult().setMeanVelocityMetersPerSec(50);
        bikeExercise.getResult().setMaxVelocityMetersPerSec(55);
        bikeExercise.getResult().setFileResultPath("otherPath");
        bikeExercise.getBikeDevice().setRimValue(30);
        bikeExercise.getResult().setMeanHeartRate(110);
        bikeExercise.getResult().setCaloriesBurned(205);
        bikeExercise.setDescription("novo exercicio");

        mBikeExerciseDAO.updateBikeExercise(bikeExercise);
        BikeExercise updatedBikeExercise = mBikeExerciseDAO.getBikeExercise(bikeExercise.getId());

        assertEquals(bikeExercise.getId(), updatedBikeExercise.getId());

        assertEquals(formatToDateString(bikeExercise.getExerciseDate()),
                formatToDateString(updatedBikeExercise.getExerciseDate()));

        assertEquals(bikeExercise.getResult().getFileResultPath(),
                updatedBikeExercise.getResult().getFileResultPath());

        assertEquals(formatDateToTimeString(bikeExercise.getResult().getTotalTimeChronometer()),
                formatDateToTimeString(updatedBikeExercise.getResult().getTotalTimeChronometer()));

        assertEquals(bikeExercise.getDescription(), updatedBikeExercise.getDescription());

        assertEquals(bikeExercise.getUser().getId(), updatedBikeExercise.getUser().getId());
        assertEquals(bikeExercise.getRoute().getId(), updatedBikeExercise.getRoute().getId());

        Assert.assertEquals(bikeExercise.getResult().getMaxVelocityMetersPerSec(), updatedBikeExercise.getResult().getMaxVelocityMetersPerSec());
        Assert.assertEquals(bikeExercise.getResult().getMeanVelocityMetersPerSec(), updatedBikeExercise.getResult().getMeanVelocityMetersPerSec());

        Assert.assertEquals(bikeExercise.getBikeDevice().getRimValue(), updatedBikeExercise.getBikeDevice().getRimValue());

        Assert.assertEquals(bikeExercise.getResult().getMeanHeartRate(), updatedBikeExercise.getResult().getMeanHeartRate());
        Assert.assertEquals(bikeExercise.getResult().getCaloriesBurned(), updatedBikeExercise.getResult().getCaloriesBurned());
    }

    @Test
    public void renamingBikeExerciseResultFilePathName() {
        BikeExercise bikeExercise = createBikeExercise();
        String oldFilePath = bikeExercise.getResult().getFileResultPath();
        mBikeExerciseDAO.saveBikeExercise(bikeExercise);

        CSVFile csvFile = new CSVFile(mContext);
        csvFile.renameFile(oldFilePath, "newNameFileTest");
        String newFilepath = csvFile.getPathFile();
        bikeExercise.getResult().setFileResultPath(csvFile.getPathFile());

        mBikeExerciseDAO.renameResultFilePath(bikeExercise);
        BikeExercise updatedBikeExercise = mBikeExerciseDAO.getBikeExercise(bikeExercise.getId());

        assertNotNull(updatedBikeExercise);
        assertEquals(updatedBikeExercise.getResult().getFileResultPath(), newFilepath);
        assertNotEquals(updatedBikeExercise.getResult().getFileResultPath(), oldFilePath);
    }

    @Test
    public void removeBikeExercise() {
        BikeExercise bikeExercise = createBikeExercise();
        mBikeExerciseDAO.saveBikeExercise(bikeExercise);

        boolean status = mBikeExerciseDAO.removeBikeExercise(bikeExercise.getId());
        assertTrue(status);

        BikeExercise removedBikeExercise = mBikeExerciseDAO.getBikeExercise(bikeExercise.getId());
        assertNull(removedBikeExercise);
    }

    @Test
    public void selectBikeExercisesFromUser() {
        User user = createFakeUser();
        assertTrue(mUserDao.saveUser(user));

        User otherUser = createFakeUser();
        mUserDao.saveUser(otherUser);

        BikeExercise bikeExercise1 = createBikeExercise();
        BikeExercise bikeExercise2 = createBikeExercise();
        BikeExercise bikeExercise3 = createBikeExercise();
        bikeExercise1.setUser(user);
        bikeExercise2.setUser(user);
        bikeExercise3.setUser(user);

        ArrayList<BikeExercise> bikeExercises = new ArrayList<>();
        bikeExercises.add(bikeExercise1);
        bikeExercises.add(bikeExercise2);
        bikeExercises.add(bikeExercise3);


        BikeExercise otherBikeExercise = createBikeExercise();
        otherBikeExercise.setUser(otherUser);
        mBikeExerciseDAO.saveBikeExercise(otherBikeExercise);

        mBikeExerciseDAO.saveBikeExercise(bikeExercise1);
        mBikeExerciseDAO.saveBikeExercise(bikeExercise2);
        mBikeExerciseDAO.saveBikeExercise(bikeExercise3);

        List<BikeExercise> bikeExercisesFromUser = mBikeExerciseDAO.getBikeExercisesFromUser(user.getId());

        //testando se nao esta buscando de outros usuarios tambem
        for (BikeExercise bke : bikeExercisesFromUser) {
            assertNotEquals(otherBikeExercise.getId(), bke.getId());
        }


        assertNotNull(bikeExercisesFromUser);
        assertFalse(bikeExercisesFromUser.isEmpty());
        assertEquals(3,bikeExercisesFromUser.size());

        //asserting same content
        for (int i = 0; i < bikeExercisesFromUser.size(); i++) {
            assertEquals(bikeExercises.get(i).getId(), bikeExercisesFromUser.get(i).getId());

            assertEquals(formatToDateString(bikeExercises.get(i).getExerciseDate()),
                    formatToDateString(bikeExercisesFromUser.get(i).getExerciseDate()));

            assertEquals(bikeExercises.get(i).getResult().getFileResultPath(),
                    bikeExercisesFromUser.get(i).getResult().getFileResultPath());

            assertEquals(formatDateToTimeString(bikeExercises.get(i).getResult().getTotalTimeChronometer()),
                    formatDateToTimeString(bikeExercisesFromUser.get(i).getResult().getTotalTimeChronometer()));

            assertEquals(bikeExercises.get(i).getDescription(), bikeExercisesFromUser.get(i).getDescription());

            assertEquals(bikeExercises.get(i).getUser().getId(),
                    bikeExercisesFromUser.get(i).getUser().getId());

            assertEquals(bikeExercises.get(i).getRoute().getId(),
                    bikeExercisesFromUser.get(i).getRoute().getId());

            Assert.assertEquals(bikeExercises.get(i).getResult().getMaxVelocityMetersPerSec(),
                    bikeExercisesFromUser.get(i).getResult().getMaxVelocityMetersPerSec());

            Assert.assertEquals(bikeExercises.get(i).getResult().getMeanVelocityMetersPerSec(),
                    bikeExercisesFromUser.get(i).getResult().getMeanVelocityMetersPerSec());

            Assert.assertEquals(bikeExercises.get(i).getBikeDevice().getRimValue(),
                    bikeExercisesFromUser.get(i).getBikeDevice().getRimValue());

            Assert.assertEquals(bikeExercises.get(i).getResult().getMeanHeartRate(),
                    bikeExercisesFromUser.get(i).getResult().getMeanHeartRate());

            Assert.assertEquals(bikeExercises.get(i).getResult().getCaloriesBurned(),
                    bikeExercisesFromUser.get(i).getResult().getCaloriesBurned());
        }
    }
}