package android.scopus.com.bikeiot.model.data.local;

import android.content.Context;
import android.scopus.com.bikeiot.FactoryFakeInputUtils;
import android.scopus.com.bikeiot.model.beans.Contact;
import android.scopus.com.bikeiot.model.beans.User;
import android.support.test.InstrumentationRegistry;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */

public class ContactDAOTest {

    private Context mContext;
    private BikeIotDbHelper mDbHelper;
    private UserDAO mUserDAO;
    private ContactDAO mContactDao;

    @Before
    public void setupRouteDAOTest() {
        mContext = InstrumentationRegistry.getTargetContext();
        mDbHelper = new BikeIotDbHelper(mContext);
        mUserDAO = new UserDAO(mContext);
        mContactDao = new ContactDAO(mContext);
    }

    @After
    public void tearDown() {
        mDbHelper.dropDatabaseTables(mDbHelper.getWritableDatabase());
        mDbHelper.onCreate(mDbHelper.getWritableDatabase());
        mDbHelper.close();
    }


    @Test
    public void savingAndSelectingData() {
        Contact contact = FactoryFakeInputUtils.createFakeContact();
        Assert.assertTrue(mContactDao.save(contact));

        Contact contactSaved = mContactDao.getContact(contact.getId());
        Assert.assertNotNull(contactSaved);
        Assert.assertEquals(contactSaved.getName(), contact.getName());
        Assert.assertEquals(contactSaved.getPhoneNumber(), contact.getPhoneNumber());
    }

    @Test
    public void listFromUser() {
        ArrayList<Contact> contacts = new ArrayList<>();

        User user = FactoryFakeInputUtils.createFakeUser();
        mUserDAO.saveUser(user);

        for (int i = 0; i < 5; i++) {
            Contact c = FactoryFakeInputUtils.createFakeContact();
            c.setName("name : " + Math.random() * 3);
            c.setUser(user);
            contacts.add(c);
            Assert.assertTrue(mContactDao.save(c));
        }

        Assert.assertTrue(mContactDao.save(FactoryFakeInputUtils.createFakeContact()));

        List<Contact> userContactsSaved = mContactDao.getContactsFromUser(user.getId());

        Assert.assertEquals(5 , userContactsSaved.size());

        for (int i = 0 ; i < contacts.size(); i++) {
            Assert.assertEquals(contacts.get(i).getName(), userContactsSaved.get(i).getName());
            Assert.assertEquals(contacts.get(i).getPhoneNumber(), userContactsSaved.get(i).getPhoneNumber());
            Assert.assertEquals(user.getId(), userContactsSaved.get(i).getUser().getId());
        }

    }

    @Test
    public void remove() {
        Contact contact = FactoryFakeInputUtils.createFakeContact();
        Assert.assertTrue(mContactDao.save(contact));

        Assert.assertTrue(mContactDao.remove(contact.getId()));
        Assert.assertNull(mContactDao.getContact(contact.getId()));
    }

    @Test
    public void edit() {
        Contact contact = FactoryFakeInputUtils.createFakeContact();
        contact.setName("aaaaa");
        contact.setPhoneNumber("11111");
        Assert.assertTrue(mContactDao.save(contact));

        Contact changedContact = new Contact();
        changedContact.setId(contact.getId());
        changedContact.setPhoneNumber("sdasdasd");
        changedContact.setName("new name");

        mContactDao.edit(changedContact);

        Contact contactSavedAfterChange = mContactDao.getContact(contact.getId());

        Assert.assertNotEquals(contact.getName(), contactSavedAfterChange.getName());
        Assert.assertNotEquals(contact.getPhoneNumber(), contactSavedAfterChange.getPhoneNumber());
        Assert.assertEquals(changedContact.getName(), contactSavedAfterChange.getName());
        Assert.assertEquals(changedContact.getPhoneNumber(), contactSavedAfterChange.getPhoneNumber());
    }
}
