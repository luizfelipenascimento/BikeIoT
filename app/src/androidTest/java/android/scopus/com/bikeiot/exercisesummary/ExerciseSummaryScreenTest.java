package android.scopus.com.bikeiot.exercisesummary;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.scopus.com.bikeiot.utils.CalculateUtils;
import android.scopus.com.bikeiot.FactoryFakeInputUtils;
import android.scopus.com.bikeiot.utils.Helper;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.exercisedetails.ExerciseDetailsActivity;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.beans.Result;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.model.data.local.BikeExerciseDAO;
import android.scopus.com.bikeiot.model.data.local.BikeIotDbHelper;
import android.scopus.com.bikeiot.model.data.local.RouteDAO;
import android.scopus.com.bikeiot.model.data.local.UserDAO;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.scopus.com.bikeiot.utils.Helper.formatKmDecimal;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static android.support.test.espresso.Espresso.onView;

import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;

/**
 *
 */
public class ExerciseSummaryScreenTest {

    @Rule
    public ActivityTestRule<ExerciseSummaryActivity> mExerciseSummaryRule = new ActivityTestRule<>(
            ExerciseSummaryActivity.class, true, false);
    private Context mContext;
    private BikeIotDbHelper mDbHelper;
    private BikeExerciseDAO mBikeExerciseDAO;
    private RouteDAO mRouteDAO;
    private UserDAO mUserDao;


    @Before
    public void setupExercisesScreenTest() {
        mContext = InstrumentationRegistry.getTargetContext();
        mDbHelper = new BikeIotDbHelper(mContext);
        mBikeExerciseDAO = new BikeExerciseDAO(mContext);
        mUserDao = new UserDAO(mContext);
        mRouteDAO = new RouteDAO(mContext);
        Intents.init();
    }

    @After
    public void tearDown() {
        mDbHelper.dropDatabaseTables(mDbHelper.getWritableDatabase());
        mDbHelper.onCreate(mDbHelper.getWritableDatabase());
        mDbHelper.close();
        Intents.release();
    }

    private BikeExercise createBikeExercise() {
        User user = FactoryFakeInputUtils.createFakeUser();
        mUserDao.saveUser(user);

        Route route = FactoryFakeInputUtils.createFakeRoute(mContext);
        mRouteDAO.save(route);

        Result result = FactoryFakeInputUtils.createFakeResult(mContext);
        BikeExercise bikeExercise = FactoryFakeInputUtils.createBikeExercise();
        bikeExercise.setUser(user);
        bikeExercise.setRoute(route);
        bikeExercise.setResult(result);
        bikeExercise.setBikeDevice(FactoryFakeInputUtils.createFakeBikeDevice());
        return  bikeExercise;
    }

    @Test
    public void displayBikeExerciseInfo_whenThereIsNoInformationForCardiacRateAndCaloriesBurned() {
        User user = FactoryFakeInputUtils.createFakeUser();
        mUserDao.saveUser(user);

        Route route = FactoryFakeInputUtils.createFakeRoute(mContext);
        mRouteDAO.save(route);

        Result result = FactoryFakeInputUtils.createFakeResult(mContext);
        BikeExercise bikeExercise = FactoryFakeInputUtils.createBikeExercise();
        bikeExercise.setUser(user);
        bikeExercise.setRoute(route);
        bikeExercise.setResult(result);

        bikeExercise.getResult().setCaloriesBurned(0);
        bikeExercise.getResult().setMeanHeartRate(0);
        bikeExercise.setBikeDevice(FactoryFakeInputUtils.createFakeBikeDevice());

        mBikeExerciseDAO.saveBikeExercise(bikeExercise);

        Intent intent = new Intent();
        intent.putExtra(ExerciseSummaryActivity.EXTRA_BIKE_EXERCISE_ID, bikeExercise.getId());
        mExerciseSummaryRule.launchActivity(intent);

        String kmIndicator = mContext.getResources().getString(R.string.km_indicator);
        String kmHIndicator = mContext.getResources().getString(R.string.km_h_indicator);
        String infoNotAvailable = mContext.getResources().getString(R.string.information_not_available);

        String duration = Helper.formatDateToTimeString(bikeExercise.getResult().getTotalTimeChronometer());
        onView(withId(R.id.exercise_resume_duration_time_text_view))
                .check(matches(withText(duration)));

        String distanceKm = formatKmDecimal(CalculateUtils.metersToKm(bikeExercise.getRoute().getDistance())) + " " + kmIndicator;
        onView(withId(R.id.exercise_resume_route_distance_text_view))
                .check(matches(withText(distanceKm)));

        String meanVelocityKmH = formatKmDecimal(CalculateUtils.metersPerSecToKmPerH(
                bikeExercise.getResult().getMeanVelocityMetersPerSec())) + " " + kmHIndicator;

        onView(withId(R.id.exercise_resume_mean_velocity_text_view))
                .check(matches(withText(meanVelocityKmH)));

        String maxVelocityKmH = formatKmDecimal(CalculateUtils.metersPerSecToKmPerH(
                bikeExercise.getResult().getMaxVelocityMetersPerSec())) + " " + kmHIndicator;

        onView(withId(R.id.exercise_resume_max_velocity_text_view))
                .check(matches(withText(maxVelocityKmH)));

        onView(withId(R.id.exercise_resume_calories_burned_text_view))
                .check(matches(withText(mContext.getResources().getString(R.string.information_not_available))));

        onView(withId(R.id.exercise_resume_mean_heart_rate_text_view))
                .check(matches(withText(infoNotAvailable)));

    }

    @Test
    public void displayBikeExerciseInfo_whenThereIsInformationForCardiacRateAndCaloriesBurned() {
        User user = FactoryFakeInputUtils.createFakeUser();
        mUserDao.saveUser(user);

        Route route = FactoryFakeInputUtils.createFakeRoute(mContext);
        mRouteDAO.save(route);

        Result result = FactoryFakeInputUtils.createFakeResult(mContext);
        BikeExercise bikeExercise = FactoryFakeInputUtils.createBikeExercise();
        bikeExercise.setUser(user);
        bikeExercise.setRoute(route);
        bikeExercise.setResult(result);
        bikeExercise.setBikeDevice(FactoryFakeInputUtils.createFakeBikeDevice());

        mBikeExerciseDAO.saveBikeExercise(bikeExercise);

        Intent intent = new Intent();
        intent.putExtra(ExerciseSummaryActivity.EXTRA_BIKE_EXERCISE_ID, bikeExercise.getId());
        mExerciseSummaryRule.launchActivity(intent);

        String kmIndicator = mContext.getResources().getString(R.string.km_indicator);
        String kmHIndicator = mContext.getResources().getString(R.string.km_h_indicator);

        String duration = Helper.formatDateToTimeString(bikeExercise.getResult().getTotalTimeChronometer());
        onView(withId(R.id.exercise_resume_duration_time_text_view))
                .check(matches(withText(duration)));

        String distanceKm = formatKmDecimal(CalculateUtils.metersToKm(bikeExercise.getRoute().getDistance())) + " " + kmIndicator;
        onView(withId(R.id.exercise_resume_route_distance_text_view))
                .check(matches(withText(distanceKm)));

        String meanVelocityKmH = formatKmDecimal(CalculateUtils.metersPerSecToKmPerH(
                bikeExercise.getResult().getMeanVelocityMetersPerSec())) + " " + kmHIndicator;
        onView(withId(R.id.exercise_resume_mean_velocity_text_view))
                .check(matches(withText(meanVelocityKmH)));

        String maxVelocityKmH = formatKmDecimal(CalculateUtils.metersPerSecToKmPerH(
                bikeExercise.getResult().getMaxVelocityMetersPerSec())) + " " + kmHIndicator;
        onView(withId(R.id.exercise_resume_max_velocity_text_view))
                .check(matches(withText(maxVelocityKmH)));

        double caloriesBurned = bikeExercise.getResult().getCaloriesBurned();
        onView(withId(R.id.exercise_resume_calories_burned_text_view))
                .check(matches(withText(String.valueOf(caloriesBurned))));

        double meanHeartRate = bikeExercise.getResult().getMeanHeartRate();
        onView(withId(R.id.exercise_resume_mean_heart_rate_text_view))
                .check(matches(withText(String.valueOf(meanHeartRate))));
    }

    @Test
    public void confirmInformationWithDescriptionEmpty_showMsgError() {
        User user = FactoryFakeInputUtils.createFakeUser();
        mUserDao.saveUser(user);

        Route route = FactoryFakeInputUtils.createFakeRoute(mContext);
        mRouteDAO.save(route);

        Result result = FactoryFakeInputUtils.createFakeResult(mContext);
        BikeExercise bikeExercise = FactoryFakeInputUtils.createBikeExercise();
        bikeExercise.setUser(user);
        bikeExercise.setRoute(route);
        bikeExercise.setResult(result);
        bikeExercise.setBikeDevice(FactoryFakeInputUtils.createFakeBikeDevice());

        mBikeExerciseDAO.saveBikeExercise(bikeExercise);

        Intent intent = new Intent();
        intent.putExtra(ExerciseSummaryActivity.EXTRA_BIKE_EXERCISE_ID, bikeExercise.getId());
        mExerciseSummaryRule.launchActivity(intent);

        onView(withId(R.id.confirm_exercise_menu_item)).perform(click());

        onView(withId(R.id.exercise_resume_general_error_text_view))
                .check(matches(withText(R.string.empty_description_error_msg)));

    }

    @Test
    public void confirmInformationWithDescription_showExerciseDetailsUi() {
        BikeExercise bikeExercise = createBikeExercise();
        mBikeExerciseDAO.saveBikeExercise(bikeExercise);

        Intent intent = new Intent();
        intent.putExtra(ExerciseSummaryActivity.EXTRA_BIKE_EXERCISE_ID, bikeExercise.getId());
        mExerciseSummaryRule.launchActivity(intent);

        onView(withId(R.id.exercise_resume_description_edit_text)).perform(typeText("Escola"));
        onView(withId(R.id.exercise_resume_description_edit_text)).perform(closeSoftKeyboard());
        onView(withId(R.id.confirm_exercise_menu_item)).perform(click());

        intended(hasComponent(ExerciseDetailsActivity.class.getName()));
    }

    @Test
    public void displayBikeExerciseInfo_whenActivityIsRotate() {
        BikeExercise bikeExercise = createBikeExercise();
        mBikeExerciseDAO.saveBikeExercise(bikeExercise);

        Intent intent = new Intent();
        intent.putExtra(ExerciseSummaryActivity.EXTRA_BIKE_EXERCISE_ID, bikeExercise.getId());
        mExerciseSummaryRule.launchActivity(intent);

        String kmIndicator = mContext.getResources().getString(R.string.km_indicator);
        String kmHIndicator = mContext.getResources().getString(R.string.km_h_indicator);

        Activity activity = mExerciseSummaryRule.getActivity();
        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        String duration = Helper.formatDateToTimeString(bikeExercise
                .getResult().getTotalTimeChronometer());
        onView(withId(R.id.exercise_resume_duration_time_text_view))
                .check(matches(withText(duration)));

        String distanceKm = formatKmDecimal(CalculateUtils.metersToKm(bikeExercise.getRoute()
                .getDistance())) + " " + kmIndicator;
        onView(withId(R.id.exercise_resume_route_distance_text_view))
                .check(matches(withText(distanceKm)));

        String meanVelocityKmH = formatKmDecimal(CalculateUtils.metersPerSecToKmPerH(
                bikeExercise.getResult().getMeanVelocityMetersPerSec())) + " " + kmHIndicator;
        onView(withId(R.id.exercise_resume_mean_velocity_text_view))
                .check(matches(withText(meanVelocityKmH)));

        String maxVelocityKmH = formatKmDecimal(CalculateUtils.metersPerSecToKmPerH(
                bikeExercise.getResult().getMaxVelocityMetersPerSec())) + " " + kmHIndicator;
        onView(withId(R.id.exercise_resume_max_velocity_text_view))
                .check(matches(withText(maxVelocityKmH)));

        double caloriesBurned = bikeExercise.getResult().getCaloriesBurned();
        onView(withId(R.id.exercise_resume_calories_burned_text_view))
                .check(matches(withText(String.valueOf(caloriesBurned))));

        double meanHeartRate = bikeExercise.getResult().getMeanHeartRate();
        onView(withId(R.id.exercise_resume_mean_heart_rate_text_view))
                .check(matches(withText(String.valueOf(meanHeartRate))));

        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    //TODO: test imagem nao exibida
    //TODO: test resultados nao exibidos quando é passado um id que nao existe
}