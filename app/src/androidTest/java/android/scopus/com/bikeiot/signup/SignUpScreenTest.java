package android.scopus.com.bikeiot.signup;

import android.scopus.com.bikeiot.R;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class SignUpScreenTest {

    @Rule
    public ActivityTestRule<SignUpActivity> mSignUpActivityRule = new ActivityTestRule<>(
            SignUpActivity.class);

    @Test
    public void onSubmitClick_ShowEmptyEmailError() {
        onView(withId(R.id.sign_up_email_edit_text)).perform(clearText());
        onView(withId(R.id.sign_up_password_edit_text)).perform(typeText("123456"));
        onView(withId(R.id.sign_up_submit_button)).perform(click());
        onView(withId(R.id.sign_up_password_edit_text)).perform(closeSoftKeyboard());
        onView(withId(R.id.email_msg_error_text_view)).check(matches(withText(R.string.empty_email_error)));
    }
}