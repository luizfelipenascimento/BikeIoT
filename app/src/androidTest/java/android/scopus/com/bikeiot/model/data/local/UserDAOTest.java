package android.scopus.com.bikeiot.model.data.local;

import android.content.Context;
import android.scopus.com.bikeiot.FactoryFakeInputUtils;
import android.scopus.com.bikeiot.model.beans.User;
import android.support.test.InstrumentationRegistry;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

/**
 *
 */
public class UserDAOTest {

    private BikeIotDbHelper mDbHelper;
    private Context mContext;
    private UserDAO mUserDAO;

    @Before
    public void setupRouteDAOTest() {
        mContext = InstrumentationRegistry.getTargetContext();
        mDbHelper = new BikeIotDbHelper(mContext);
        mUserDAO = new UserDAO(mContext);
    }

    @After
    public void tearDown() {
        mDbHelper.dropDatabaseTables(mDbHelper.getWritableDatabase());
        mDbHelper.onCreate(mDbHelper.getWritableDatabase());
        mDbHelper.close();
    }

    @Test
    public void saveAndSelect() {
        User user = FactoryFakeInputUtils.createFakeUser();
        user.setUUID(UUID.randomUUID());

        assertTrue(mUserDAO.saveUser(user));

        User selectedUser = mUserDAO.getUser(user.getId());
        assertNotNull(selectedUser);

        assertUserInfo(user, selectedUser);
    }

    @Test
    public void edit() {
        User user = FactoryFakeInputUtils.createFakeUser();
        user.setName("old name");

        assertTrue(mUserDAO.saveUser(user));

        User selectUser = mUserDAO.getUser(user.getId());
        selectUser.setName("other name");
        assertTrue(mUserDAO.update(selectUser));

        User selectChanged = mUserDAO.getUser(selectUser.getId());
        assertNotEquals(user.getName(), selectChanged.getName());

        assertUserInfo(selectUser, selectChanged);

    }

    private void assertUserInfo(User userA, User userB) {
        assertEquals(userA.getId(), userB.getId());
        assertEquals(userA.getBirthday(), userB.getBirthday());
        assertEquals(userA.getEmail(), userB.getEmail());
        junit.framework.Assert.assertEquals(userA.getHeight(), userB.getHeight());
        assertEquals(userA.getWeight(), userB.getWeight());
        assertEquals(userA.getName(), userB.getName());
        assertEquals(userA.getPassword(), userB.getPassword());
        assertEquals(userA.getUUID().toString(), userB.getUUID().toString());
    }



}