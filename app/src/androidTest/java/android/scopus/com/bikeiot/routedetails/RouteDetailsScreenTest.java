package android.scopus.com.bikeiot.routedetails;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.utils.SharedPreferencesUtils;
import android.scopus.com.bikeiot.model.data.RouteRepository;
import android.scopus.com.bikeiot.model.data.local.BikeIotDbHelper;
import android.scopus.com.bikeiot.model.data.local.RouteDAO;
import android.scopus.com.bikeiot.model.data.local.UserDAO;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;

/**
 *
 */
public class RouteDetailsScreenTest {

    @Rule
    public ActivityTestRule<RouteDetailsActivity> mDetailsActivityTestRule = new ActivityTestRule<>(
            RouteDetailsActivity.class, true, false);

    private RouteRepository mRouteRepository;
    private SharedPreferences mSharedPreferences;
    private UserDAO mUserDAO;
    private Context mContext;
    private BikeIotDbHelper mDbHelper;


    @Before
    public void setupRouteDetailsScreenTest() {
        mContext = InstrumentationRegistry.getTargetContext();
        mDbHelper = new BikeIotDbHelper(mContext);
        mRouteRepository = new RouteRepository(new RouteDAO(mContext));
        mUserDAO = new UserDAO(mContext);
        mSharedPreferences = mContext.getSharedPreferences(SharedPreferencesUtils.SHARED_PREFERENCES_DEFAULT_FILE_KEY, Context.MODE_PRIVATE);
        Intents.init();
    }

    @After
    public void tearDown() {
        mDbHelper.dropDatabaseTables(mDbHelper.getWritableDatabase());
        mDbHelper.onCreate(mDbHelper.getWritableDatabase());
        mDbHelper.close();
        SharedPreferencesUtils.clearSharedPreferences(mSharedPreferences);
        Intents.release();
    }

    @Test
    public void showNoRouteError_whenRouteIdDoesNotExist() {

        Intent intent = new Intent();
        intent.putExtra(RouteDetailsActivity.EXTRA_ROUTE_ID, 0);
        mDetailsActivityTestRule.launchActivity(intent);

        onView(withId(R.id.no_route_error_container_frame_layout)).check(matches(isDisplayed()));
        onView(withId(R.id.no_route_error_text_view)).check(matches(withText(R.string.no_route_error_msg)));
        onView(withId(R.id.route_info_container_linear_layout)).check(matches(not(isDisplayed())));

    }

}