package android.scopus.com.bikeiot.model;

import android.content.Context;
import android.os.Environment;
import android.scopus.com.bikeiot.model.filerelated.FileContractConstants;
import android.scopus.com.bikeiot.model.filerelated.FileRepository;
import android.support.test.InstrumentationRegistry;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.ROUTE_IMAGE_FOLDER;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.TEST_FOLDER;
import static org.junit.Assert.*;

/**
 *
 */
public class FileRepositoryTest {


    private Context mContext;
    private FileRepository mFileRepository;

    @Before
    public void setupFileRepositoryTest() {
        mContext = InstrumentationRegistry.getTargetContext();
        mFileRepository = new FileRepository(mContext);
    }

    @Test
    public void createAndDeleteFile() {
        File resultFile = mFileRepository.createFile("testRota", ROUTE_IMAGE_FOLDER + File.separator + TEST_FOLDER,
                FileContractConstants.PNG_FILE_EXTENSION, Environment.getExternalStorageDirectory());
        Assert.assertNotNull(resultFile);

        resultFile.delete();
        mFileRepository.scanFile(resultFile, mContext);
        Assert.assertTrue(!resultFile.exists());
    }

    @Test
    public void renameFile() {
        String fileName = "testRotaOldName";
        File resultFile = mFileRepository.createFile(fileName, ROUTE_IMAGE_FOLDER,
                FileContractConstants.PNG_FILE_EXTENSION, Environment.getExternalStorageDirectory());

        String fileNewName = "newFileName";

        File updatedFile = mFileRepository.renameFile(resultFile.getPath(),
                fileNewName, FileContractConstants.PNG_FILE_EXTENSION);

        assertNotEquals(resultFile.getAbsoluteFile(), updatedFile.getAbsoluteFile());

    }
}