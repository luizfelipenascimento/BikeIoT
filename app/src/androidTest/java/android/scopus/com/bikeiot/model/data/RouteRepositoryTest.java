package android.scopus.com.bikeiot.model.data;

import android.content.Context;
import android.scopus.com.bikeiot.FactoryFakeInputUtils;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.data.local.BikeIotDbHelper;
import android.scopus.com.bikeiot.model.data.local.RouteDAO;
import android.support.test.InstrumentationRegistry;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;

/**
 *
 */
public class RouteRepositoryTest {

    private BikeIotDbHelper mDbHelper;
    private Context mContext;
    private RouteRepository mRouteRepository;

    @Before
    public void RouteRepositoryTest() {
        mContext = InstrumentationRegistry.getTargetContext();
        mDbHelper = new BikeIotDbHelper(mContext);
        RouteDAO routeDAO = new RouteDAO(mContext);
        mRouteRepository = new RouteRepository(routeDAO);
    }

    @After
    public void tearDown() {
        mDbHelper.dropDatabaseTables(mDbHelper.getWritableDatabase());
        mDbHelper.onCreate(mDbHelper.getWritableDatabase());
        mDbHelper.close();
    }

    @Test
    public void testRouteSelectionCache() {
        Route route = FactoryFakeInputUtils.createFakeRoute(mContext);
        boolean saveResult = mRouteRepository.save(route);
        Assert.assertTrue(saveResult);
        Assert.assertTrue(mRouteRepository.isCacheBroken());

        Route savedRoute = mRouteRepository.getRoute(route.getId());
        Assert.assertNotNull(savedRoute);
        Assert.assertFalse(mRouteRepository.isCacheBroken());
        Route cachedRoute = mRouteRepository.getRoute(route.getId());
        assertEquals(savedRoute, cachedRoute);
    }

}