package android.scopus.com.bikeiot.model;

import android.content.Context;
import android.scopus.com.bikeiot.model.filerelated.CSVFile;
import android.scopus.com.bikeiot.model.filerelated.CSVFileContentManager;
import android.support.test.InstrumentationRegistry;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

/**
 *
 */
public class CSVFileContentManagerTest {


    private Context mContext;

    @Before
    public void setupCSVFileContentManagerTest() {
        mContext = InstrumentationRegistry.getTargetContext();
    }

    @Test
    public void loadFileContent() throws CSVFileContentManager.NoCSVColumnException {
        String column1 = "testColumn1";
        String column2 = "testColumn2";
        String header[] = {column1, column2};
        String fileName = "testContentFile_" + UUID.randomUUID();
        String[] line1 = {"111","1212"};
        String[] line2 = {"222","2323"};

        CSVFile file = new CSVFile(fileName, mContext, CSVFile.TEST_ROOT_FOLDER, header);
        file.writeLine(line1);
        file.writeLine(line2);
        file.close();

        CSVFileContentManager fileContentManager = CSVFile.readAllFileContent(file.getPathFile());

        Assert.assertNotNull(fileContentManager);

        Assert.assertEquals(line1[0], fileContentManager.getColumnValue(column1, 0));
        Assert.assertEquals(line1[1], fileContentManager.getColumnValue(column2, 0));

        Assert.assertEquals(line2[0], fileContentManager.getColumnValue(column1, 1));
        Assert.assertEquals(line2[1], fileContentManager.getColumnValue(column2, 1));
    }
}