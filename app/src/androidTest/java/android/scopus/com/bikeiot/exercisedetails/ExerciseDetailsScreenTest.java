package android.scopus.com.bikeiot.exercisedetails;

import android.content.Context;
import android.content.Intent;
import android.scopus.com.bikeiot.utils.CalculateUtils;
import android.scopus.com.bikeiot.FactoryFakeInputUtils;
import android.scopus.com.bikeiot.utils.Helper;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.utils.SharedPreferencesUtils;
import android.scopus.com.bikeiot.exercises.ExercisesActivity;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.data.local.BikeExerciseDAO;
import android.scopus.com.bikeiot.model.data.local.BikeIotDbHelper;
import android.scopus.com.bikeiot.model.data.local.RouteDAO;
import android.scopus.com.bikeiot.model.data.local.UserDAO;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.scopus.com.bikeiot.utils.Helper.formatKmDecimal;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 *
 */
public class ExerciseDetailsScreenTest {


    @Rule
    public ActivityTestRule<ExerciseDetailsActivity> mExerciseDetailsRule = new ActivityTestRule<>(
            ExerciseDetailsActivity.class, true, false);

    private Context mContext;
    private BikeIotDbHelper mDbHelper;
    private BikeExerciseDAO mBikeExerciseDAO;
    private UserDAO mUserDao;
    private RouteDAO mRouteDAO;
    private String mPreferencesFile;
    private String mUserKeySharedPreferences;

    @Before
    public void setupExercisesScreenTest() {
        mContext = InstrumentationRegistry.getTargetContext();
        mDbHelper = new BikeIotDbHelper(mContext);
        mBikeExerciseDAO = new BikeExerciseDAO(mContext);
        mUserDao = new UserDAO(mContext);
        mRouteDAO = new RouteDAO(mContext);
        mPreferencesFile = mContext.getResources().getString(R.string.shared_preferences_key);
        mUserKeySharedPreferences = mContext.getResources().getString(R.string.shared_user_id);
        Intents.init();
    }

    @After
    public void tearDown() {
        mDbHelper.dropDatabaseTables(mDbHelper.getWritableDatabase());
        mDbHelper.onCreate(mDbHelper.getWritableDatabase());
        mDbHelper.close();
        SharedPreferencesUtils.clearSharedPreferences(mContext, mPreferencesFile);
        Intents.release();
    }

    @Test
    public void displayBikeExerciseInfo_whenThereIsInformationForCardiacRateAndCaloriesBurned() {

        BikeExercise bikeExercise = FactoryFakeInputUtils.createBikeExercise(FactoryFakeInputUtils.createFakeUser(),
                FactoryFakeInputUtils.createFakeResult(mContext), FactoryFakeInputUtils.createFakeRoute(mContext),
                FactoryFakeInputUtils.createFakeBikeDevice());

        mUserDao.saveUser(bikeExercise.getUser());
        mRouteDAO.save(bikeExercise.getRoute());
        mBikeExerciseDAO.saveBikeExercise(bikeExercise);

        SharedPreferencesUtils.saveIntToSharedPreferences(mContext, mPreferencesFile,
                mUserKeySharedPreferences, bikeExercise.getUser().getId());

        Intent intent = new Intent();
        intent.putExtra(ExerciseDetailsActivity.EXTRA_BIKE_EXERCISE_ID, bikeExercise.getId());
        mExerciseDetailsRule.launchActivity(intent);

        String kmIndicator = mContext.getResources().getString(R.string.km_indicator);
        String kmHIndicator = mContext.getResources().getString(R.string.km_h_indicator);

        String userName = bikeExercise.getUser().getName();
        onView(withId(R.id.user_name_exercise_details)).check(matches(withText(userName)));

        String exerciseDescription = bikeExercise.getDescription();
        onView(withId(R.id.exercise_details_description)).check(matches(withText(exerciseDescription)));

        String duration = Helper.formatDateToTimeString(bikeExercise.getResult().getTotalTimeChronometer());
        onView(withId(R.id.exercise_duration_time_text_view))
                .check(matches(withText(duration)));

        String distanceKm = formatKmDecimal(CalculateUtils.metersToKm(bikeExercise.getRoute().getDistance()))
                + " " + kmIndicator;
        onView(withId(R.id.exercise_route_distance_text_view))
                .check(matches(withText(distanceKm)));

        String meanVelocityKmH = formatKmDecimal(CalculateUtils.metersPerSecToKmPerH(
                bikeExercise.getResult().getMeanVelocityMetersPerSec())) + " " + kmHIndicator;
        onView(withId(R.id.exercise_mean_velocity_text_view))
                .check(matches(withText(meanVelocityKmH)));

        String maxVelocityKmH = formatKmDecimal(CalculateUtils.metersPerSecToKmPerH(
                bikeExercise.getResult().getMaxVelocityMetersPerSec())) + " " + kmHIndicator;
        onView(withId(R.id.exercise_max_velocity_text_view))
                .check(matches(withText(maxVelocityKmH)));

        double caloriesBurned = bikeExercise.getResult().getCaloriesBurned();
        onView(withId(R.id.exercise_calories_burned_text_view))
                .check(matches(withText(String.valueOf(caloriesBurned))));

        double meanHeartRate = bikeExercise.getResult().getMeanHeartRate();
        onView(withId(R.id.exercise_mean_heart_rate_text_view))
                .check(matches(withText(String.valueOf(meanHeartRate))));
    }

    @Test
    public void clickOnDelete_showListExercisesWhenDeleteIsSucceed() {

        BikeExercise bikeExercise = FactoryFakeInputUtils.createBikeExercise(FactoryFakeInputUtils.createFakeUser(),
                FactoryFakeInputUtils.createFakeResult(mContext), FactoryFakeInputUtils.createFakeRoute(mContext),
                FactoryFakeInputUtils.createFakeBikeDevice());

        mUserDao.saveUser(bikeExercise.getUser());
        mRouteDAO.save(bikeExercise.getRoute());
        mBikeExerciseDAO.saveBikeExercise(bikeExercise);

        SharedPreferencesUtils.saveIntToSharedPreferences(mContext, mPreferencesFile,
                mUserKeySharedPreferences, bikeExercise.getUser().getId());

        Intent intent = new Intent();
        intent.putExtra(ExerciseDetailsActivity.EXTRA_BIKE_EXERCISE_ID, bikeExercise.getId());
        mExerciseDetailsRule.launchActivity(intent);

        //TODO: adptar test para o alert dialog

        onView(withId(R.id.delete_menu_item)).perform(click());

        intended(hasComponent(ExercisesActivity.class.getName()));

    }
}