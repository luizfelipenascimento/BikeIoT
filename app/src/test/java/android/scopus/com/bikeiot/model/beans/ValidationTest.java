package android.scopus.com.bikeiot.model.beans;

import android.content.Context;
import android.scopus.com.bikeiot.BuildConfig;
import android.scopus.com.bikeiot.model.InputHandler;
//import android.scopus.com.bikeiot.FactoryFakeInput;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.annotation.internal.DoNotInstrument;


public class ValidationTest {

    @Test
    public void testTelephoneValidation() {
        String telephoneRegex = "^([1-2][1-9] (9\\d{4})-\\d{4})|((\\(1[2-9]{1}\\)|\\([2-9]{1}\\d{1}\\)) [5-9]\\d{3}-\\d{4})";
        String telephone = "11 97284-2060";
        Assert.assertTrue(telephone.matches(telephoneRegex));
    }

    @Test
    public void testCellphoneValidationNoMask() {
        String cellphone = "11972842060";
        Assert.assertTrue(InputHandler.isPhoneNumberValid(cellphone));
    }

}
