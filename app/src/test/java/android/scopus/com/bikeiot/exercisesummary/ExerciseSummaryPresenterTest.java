package android.scopus.com.bikeiot.exercisesummary;

import android.content.Context;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.model.filerelated.FileRepository;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.data.BikeExerciseDataSource;
import android.scopus.com.bikeiot.model.data.RouteDataSource;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.never;

/**
 *
 */
public class ExerciseSummaryPresenterTest {

    @Mock
    private BikeExerciseDataSource mBikeExerciseDataSource;

    @Mock
    private RouteDataSource mRouteDataSource;

    @Mock
    private Context mContext;

    @Mock
    private ExerciseSummaryContract.View mView;

    private ExerciseSummaryPresenter mPresenter;
    private int mBikeExerciseId;

    @Before
    public void ExerciseResumePresenterTest() {
        MockitoAnnotations.initMocks(this);
        mBikeExerciseId = 1;
        mPresenter = new ExerciseSummaryPresenter(mBikeExerciseId, mBikeExerciseDataSource, mRouteDataSource, mView, new FileRepository(mContext));
    }

    @Test
    public void testLoadBikeExerciseAndInitialize() {
        int routeId = 1;
        BikeExercise bikeExercise = new BikeExercise();
        bikeExercise.setId(mBikeExerciseId);
        bikeExercise.setRoute(new Route());
        bikeExercise.getRoute().setId(routeId);

        Mockito.when(mBikeExerciseDataSource.getBikeExercise(mBikeExerciseId)).thenReturn(bikeExercise);
        Mockito.when(mRouteDataSource.getRoute(routeId)).thenReturn(new Route());
        mPresenter.initialize();

        Mockito.verify(mView).displayExerciseResume(bikeExercise);
    }

    @Test
    public void testLoadBikeExerciseAndInitialize_whenCantLoadBikeExercise() {
        int routeId = 1;
        BikeExercise bikeExercise = new BikeExercise();
        bikeExercise.setRoute(new Route());
        bikeExercise.getRoute().setId(routeId);

        Mockito.when(mBikeExerciseDataSource.getBikeExercise(mBikeExerciseId)).thenReturn(null);
        Mockito.when(mRouteDataSource.getRoute(routeId)).thenReturn(new Route());

        mPresenter.loadBikeExercise(mBikeExerciseId);

        Mockito.verify(mView).showNoBikeExerciseError();
        Mockito.verify(mView, never()).displayExerciseResume(null);
    }

    @Test
    public void testUpdateBikeExerciseDescription_whenDescriptionIsOk() {
        int routeId = 1;
        BikeExercise bikeExercise = new BikeExercise();
        bikeExercise.setId(1);
        bikeExercise.setRoute(new Route());
        bikeExercise.getRoute().setId(routeId);
        Route route = new Route();
        route.setId(routeId);

        Mockito.when(mBikeExerciseDataSource.getBikeExercise(mBikeExerciseId)).thenReturn(bikeExercise);
        Mockito.when(mRouteDataSource.getRoute(routeId)).thenReturn(route);

        Mockito.when(mBikeExerciseDataSource.updateBikeExercise(bikeExercise)).thenReturn(true);
        Mockito.when(mRouteDataSource.update(route)).thenReturn(true);

        mPresenter.updateBikeExerciseAndRouteDescription("escola");
        Mockito.verify(mView).showBikeExerciseDetailsUi(mBikeExerciseId);
    }

    @Test
    public void testUpdateBikeExerciseDescription_whenClickOnConfirmAndDescriptionIsEmpty() {

        int routeId = 1;
        BikeExercise bikeExercise = new BikeExercise();
        bikeExercise.setRoute(new Route());
        bikeExercise.getRoute().setId(routeId);

        Mockito.when(mBikeExerciseDataSource.getBikeExercise(mBikeExerciseId)).thenReturn(bikeExercise);
        Mockito.when(mRouteDataSource.getRoute(routeId)).thenReturn(new Route());

        mPresenter.updateBikeExerciseAndRouteDescription("");
        Mockito.verify(mView).showGeneralErrorMsg(R.string.empty_description_error_msg);
        Mockito.verify(mView, never()).showBikeExerciseDetailsUi(mBikeExerciseId);
    }

}