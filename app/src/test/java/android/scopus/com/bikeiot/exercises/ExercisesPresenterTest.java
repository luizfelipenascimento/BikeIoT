package android.scopus.com.bikeiot.exercises;

import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.data.BikeExerciseRepository;

import com.google.common.collect.Lists;

import org.junit.Test;
import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

/**
 *
 */
public class ExercisesPresenterTest {



    @Mock
    private ExercisesContract.View mView;

    @Mock
    private BikeExerciseRepository mBikeExerciseRepository;


    @InjectMocks
    private ExercisesPresenter mExercisesPresenter;

    @Before
    public void setupExercisesPresenterTest() {
        MockitoAnnotations.initMocks(this);
        mExercisesPresenter = new ExercisesPresenter(mView, mBikeExerciseRepository, 2);
    }

    @Test
    public void testListExercises_whenThereIsNoUserExercisesToList() {
        int userId = 1;
        Mockito.when(mBikeExerciseRepository.getBikeExercisesFromUser(userId)).thenReturn(Collections.EMPTY_LIST);
        mExercisesPresenter.loadUserExercises(userId);
        Mockito.verify(mView).showNoExercisesDone();
    }

    @Test
    public void testListExercises_whenThereIsUserExercisesToList() {
        int userId = 1;
        List<BikeExercise> bikeExercises =  Lists.newArrayList(new BikeExercise(), new BikeExercise(), new BikeExercise());

        Mockito.when(mBikeExerciseRepository.getBikeExercisesFromUser(userId)).thenReturn(bikeExercises);
        mExercisesPresenter.loadUserExercises(userId);

        Mockito.verify(mView).showExercises(bikeExercises);
    }

    @Test
    public void testSelectExerciseItem_whenSelectExerciseItemShowExerciseDetails() {
        BikeExercise bikeExercise = new BikeExercise();
        bikeExercise.setId(1);

        mExercisesPresenter.openBikeExercise(bikeExercise);

        Mockito.verify(mView).showExerciseDetailsUi(bikeExercise.getId());
    }


}