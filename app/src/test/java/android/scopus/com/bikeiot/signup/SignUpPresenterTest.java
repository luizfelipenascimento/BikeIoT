package android.scopus.com.bikeiot.signup;

import android.content.Context;
import android.scopus.com.bikeiot.BuildConfig;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.model.data.UserDataSource;
import android.scopus.com.bikeiot.model.data.local.BikeIotDbHelper;
import android.scopus.com.bikeiot.model.data.local.UserDAO;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.annotation.internal.DoNotInstrument;

import static org.mockito.ArgumentMatchers.any;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
@DoNotInstrument
public class SignUpPresenterTest {


    @Mock
    private SignUpContract.View mView;

    @Mock
    private UserDataSource mUserDataSource;

    @Mock
    private User mUser;

    Context mContext;

    private SignUpPresenter mSignUpPresenter;

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    private BikeIotDbHelper mDbHelper;
    private UserDAO mUserDao;
    //private FactoryFakeInput mFactoryFakeInput;

    @Before
    public void setupSingUpPresenterTest() {
        MockitoAnnotations.initMocks(this);
        mSignUpPresenter = new SignUpPresenter(mView, mUserDataSource);
        mContext = RuntimeEnvironment.application;
        mDbHelper = new BikeIotDbHelper(RuntimeEnvironment.application);
        mSignUpPresenter.setContext(RuntimeEnvironment.application);
        mUserDao = new UserDAO(RuntimeEnvironment.application);
        //mFactoryFakeInput = new FactoryFakeInput(mDbHelper);
    }

    @After
    public void tearDown() {
        mDbHelper.close();
    }

    @Test
    public void whenEmailIsEmptyShowError() {
        mSignUpPresenter.signUp("", "123456", "luiz", "1995-02-26");
        Mockito.verify(mView).showEmailMessageError(R.string.empty_email_error);
    }

    @Test
    public void whenPasswordIsEmptyShowError() {
        mSignUpPresenter.signUp("luiz@gmail.com", "", "luiz", "1995-02-26");
        Mockito.verify(mView).showPasswordMessageError(R.string.empty_password_error);
    }

    @Test
    public void whenEmailIsInvalidShowError() {
        mSignUpPresenter.signUp("adasdasd@", "1234", "luiz", "1995-02-26");
        Mockito.verify(mView).showEmailMessageError(R.string.invalid_email);
    }

    @Test
    public void whenEmailAnsPasswordIsInvalidOrEmptyShowBothError() {
        mSignUpPresenter.signUp("", "", "luiz", "1995-02-26");
        Mockito.verify(mView).showEmailMessageError(R.string.empty_email_error);
        Mockito.verify(mView).showPasswordMessageError(R.string.empty_password_error);
    }


    @Test
    public void whenNameIsEmptyShowError() {
        mSignUpPresenter.signUp("luiz@gmail.com","123456","", "1995-02-26");
        Mockito.verify(mView).showNameMessageError(R.string.empty_name_error);
    }

    @Test
    public void whenSignUpSucceed() {
        String email = "luiz@gmail.com";
        Mockito.when(mUserDataSource.saveUser(any(User.class))).thenReturn(true);

        mSignUpPresenter.signUp(email,"123456", "luiz", "1995-02-26");
        Mockito.verify(mView).showLogin();
    }

    @Test
    public void whenEmailIsNoUniqueShowError() {

        User user = new User("luiz","luiz@gmail.com","123456", "1995-02-26");
        mUserDao.saveUser(user);

        mSignUpPresenter.signUp(user.getEmail(),"123456", "luiz", "1995-02-26");
        Mockito.verify(mView).showEmailMessageError(R.string.email_already_used);
    }


    @Test
    public void whenSignUpFailShowError() {
        Mockito.when(mUserDataSource.saveUser(new User())).thenReturn(false);
        mSignUpPresenter.signUp("luiz@gmail.com", "12345678", "luiz", "1995-02-26");
        Mockito.verify(mView).showGeneralError(R.string.general_process_error_msg);
    }

    @Test
    public void whenBirthdayIsEmptyShowError() {
        mSignUpPresenter.signUp("luizfelipe@gmail.com", "12345678", "luiz", "");
        Mockito.verify(mView).showBirthdayMessageError(R.string.empty_birthday_error);
    }

    @Test
    public void whenBirthdayInInvalidShowError() {
        mSignUpPresenter.signUp("luizfelipe@gmail.com", "12345678", "luiz", "123121-5-5");
        Mockito.verify(mView).showBirthdayMessageError(R.string.invalid_birthday_error);
    }

    //TODO: colocar o edit text no formato de data


}