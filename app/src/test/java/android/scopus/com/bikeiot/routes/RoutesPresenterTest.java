package android.scopus.com.bikeiot.routes;

import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.data.RouteRepository;

import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

/**
 *
 */
public class RoutesPresenterTest {


    private RoutesPresenter mRoutesPresenter;

    @Mock
    private RoutesContract.View mView;

    @Mock
    private RouteRepository mRouteRepository;

    @Before
    public void setupRoutesPresenterTest() {
        MockitoAnnotations.initMocks(this);
        mRoutesPresenter = new RoutesPresenter(mView, mRouteRepository);
    }

    @Test
    public void testListRoutes_whenThereAreRoutesShowRoutes() {

        List<Route> routes = Lists.newArrayList(new Route(), new Route(), new Route());

        Mockito.when(mRouteRepository.getRoutes()).thenReturn(routes);

        mRoutesPresenter.loadRoutes();
        Mockito.verify(mView).showRoutes(routes);
        Mockito.verify(mView, Mockito.never()).showNoRoutes();
    }

    @Test
    public void testNoRouteToList_whenThereIsNoRouteShowNoRoutes() {
        Mockito.when(mRouteRepository.getRoutes()).thenReturn(Collections.EMPTY_LIST);

        mRoutesPresenter.loadRoutes();

        Mockito.verify(mView).showNoRoutes();
        Mockito.verify(mView, Mockito.never()).showRoutes(Collections.EMPTY_LIST);
    }

    @Test
    public void testRouteSelection_whenRouteIsSelectedShowSelectRouteDetails(){
        int routeId = 1;
        Route route = new Route();
        Mockito.when(mRouteRepository.getRoute(routeId)).thenReturn(route);

        mRoutesPresenter.selectRoute(routeId);
        Mockito.verify(mView).showRouteDetailsUi(routeId);
    }

    @Test
    public void testRouteSelectionError_whenRouteIsSelectedButNoReturnShowSelectRouteDetails() {
        int routeId = 1;
        Mockito.when(mRouteRepository.getRoute(routeId)).thenReturn(null);

        mRoutesPresenter.selectRoute(routeId);
        Mockito.verify(mView).showErrorInSelectProcess(R.string.error_route_selection);
        Mockito.verify(mView, Mockito.never()).showRouteDetailsUi(routeId);
    }

}