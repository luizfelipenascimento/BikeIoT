package android.scopus.com.bikeiot.routedetails;

import android.content.Context;
import android.content.SharedPreferences;
import android.scopus.com.bikeiot.model.location.LocationUpdater;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.model.data.RouteRepository;
import android.scopus.com.bikeiot.model.data.UserRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 *
 */
public class RouteDetailsPresenterTest {


    @Mock
    private RouteRepository mRouteRepository;

    @Mock
    private UserRepository mUserRepository;

    @Mock
    private RouteDetailsContract.View mView;

    @Mock
    private LocationUpdater mLocationUpdater;

    @Mock
    private Context mContext;

    @Mock
    private SharedPreferences mSharedPreferences;

    private RouteDetailsPresenter mPresenter;
    private int routeId;

    @Before
    public void setupRouteDetailsPresenterTest() {
        MockitoAnnotations.initMocks(this);
        routeId = 1;

        mPresenter = new RouteDetailsPresenter(mRouteRepository, mLocationUpdater, mUserRepository,
                mView, routeId, mSharedPreferences);
    }

    @Test
    public void loadRoute_whenLoadIsSuccessfulShowRouteDetails() {

        Route route = new Route();
        route.setUser(new User());
        Mockito.when(mRouteRepository.getRoute(routeId)).thenReturn(route);

        mPresenter.initialize();

        Mockito.verify(mView).showRouteDetails(route);
    }

    @Test
    public void loadRoute_whenLoadIsNotSuccessfulShowRouteDetails() {
        Mockito.when(mRouteRepository.getRoute(routeId)).thenReturn(null);

        mPresenter.initialize();

        Mockito.verify(mView).showNoRouteError();
    }

    @Test
    public void loadRouteWithUser_showUserDetails() {
        User user = new User();
        user.setId(1);
        Route route = new Route();
        route.setUser(user);

        Mockito.when(mRouteRepository.getRoute(routeId)).thenReturn(route);
        Mockito.when(mUserRepository.getUser(route.getUser().getId())).thenReturn(user);

        mPresenter.initialize();

        Mockito.verify(mView).showUserDetails(user);
    }

    @Test
    public void loadRouteWithoutUser_showNoUserDetails() {
        User user = new User();
        user.setId(0);
        Route route = new Route();
        route.setUser(user);

        Mockito.when(mRouteRepository.getRoute(routeId)).thenReturn(route);
        Mockito.when(mUserRepository.getUser(route.getUser().getId())).thenReturn(null);

        mPresenter.initialize();

        Mockito.verify(mView).showNoUserDetails();
    }

    @Test
    public void removeRouteSuccess_showRemoveSuccessAndRoutesUi() {

        Mockito.when(mRouteRepository.remove(routeId)).thenReturn(true);
        mPresenter.removeRoute();

        Mockito.verify(mView).showRemoveSuccess();
        Mockito.verify(mView).showRoutesUi();
    }

    @Test
    public void removeRouteFails_showRemoveRouteError() {

        Mockito.when(mRouteRepository.remove(routeId)).thenReturn(false);
        mPresenter.removeRoute();

        Mockito.verify(mView).showRemoveRouteError();
        Mockito.verify(mView, Mockito.never()).showRemoveSuccess();
        Mockito.verify(mView, Mockito.never()).showRoutesUi();
    }

}