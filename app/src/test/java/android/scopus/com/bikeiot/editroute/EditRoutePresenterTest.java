package android.scopus.com.bikeiot.editroute;

import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.data.BikeExerciseDataSource;
import android.scopus.com.bikeiot.model.data.RouteRepository;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;

/**
 *
 */
public class EditRoutePresenterTest {


    @Mock
    private RouteRepository mRouteRepository;

    @Mock
    private EditRouteContract.View mView;

    private int routeId;

    private EditRoutePresenter mPresenter;
    private Route mReturnedRoute;

    @Before
    public void ExerciseResumePresenterTest() {
        MockitoAnnotations.initMocks(this);
        routeId = 1;
        mPresenter = new EditRoutePresenter(mRouteRepository, routeId, mView);

        mReturnedRoute = new Route();
        mReturnedRoute.setShared(0);
        mReturnedRoute.setDescription("old");

    }

    @Test
    public void testLoadRouteSuccess_showRouteDetails() {
        Route route = new Route();
        Mockito.when(mRouteRepository.getRoute(routeId)).thenReturn(route);
        mPresenter.loadRoute();
        Mockito.verify(mView).showRouteDetails(route);
    }

    @Test
    public void testLoadRouteFail_showRouteDetails() {
        Mockito.when(mRouteRepository.getRoute(routeId)).thenReturn(null);
        mPresenter.loadRoute();
        Mockito.verify(mView).showNoRouteError();
    }

    @Test
    public void updateSuccess_whenDataHasChanged() {
        mReturnedRoute.setShared(0);
        mReturnedRoute.setDescription("old");

        Mockito.when(mRouteRepository.getRoute(routeId)).thenReturn(mReturnedRoute);
        Mockito.when(mRouteRepository.update(mReturnedRoute)).thenReturn(true);
        mPresenter.update("novo", true);
        Mockito.verify(mView).showUpdateSuccess();
        Mockito.verify(mView).sendResponseForRouteDetailsUi(routeId);
    }

    @Test
    public void updateWhenNothingIsChanged_dontShowUpdateSuccess() {
        mReturnedRoute.setShared(0);
        mReturnedRoute.setDescription("old");

        Mockito.when(mRouteRepository.getRoute(routeId)).thenReturn(mReturnedRoute);
        Mockito.when(mRouteRepository.update(mReturnedRoute)).thenReturn(true);
        mPresenter.update("old", false);
        Mockito.verify(mView, Mockito.never()).showUpdateSuccess();
        Mockito.verify(mView).sendResponseForRouteDetailsUi(routeId);
    }

    @Test
    public void updateFails_showUpdateError() {
        Mockito.when(mRouteRepository.getRoute(routeId)).thenReturn(mReturnedRoute);
        Mockito.when(mRouteRepository.update(mReturnedRoute)).thenReturn(false);
        mPresenter.update("novo", true);
        Mockito.verify(mView).showUpdateError();
    }

    @Test
    public void updateWhenDescriptionIsEmpty_showEmptyDescriptionErrors() {
        mPresenter.update("", true);
        Mockito.verify(mView).showEmptyDescriptionError(R.string.empty_description_error_msg);
    }
}