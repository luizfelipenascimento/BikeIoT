package android.scopus.com.bikeiot.exercisedetails;

import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.model.data.BikeExerciseDataSource;
import android.scopus.com.bikeiot.model.data.RouteDataSource;
import android.scopus.com.bikeiot.model.data.UserDataSource;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 *
 */
public class ExerciseDetailsPresenterTest {
    //talvez seja necessario exibir as informacoes de bikeExercise e route em metodos independentes
    //ja que a rota pode ser deleta mesmo que já tenha um relacionamento com bikeExercise.
    //TODO: testar a chamada para exibicao das informacoes da rota e do bikeExercise

    private int mBikeExerciseId;

    @Mock
    private RouteDataSource mRouteDataSource;

    @Mock
    private BikeExerciseDataSource mBikeExerciseDataSource;

    @Mock
    private UserDataSource mUserDataSource;

    @Mock
    private ExerciseDetailsContract.View mView;

    private ExerciseDetailsPresenter mPresenter;
    private int mOwnerUserId;

    @Before
    public void ExerciseResumePresenterTest() {
        MockitoAnnotations.initMocks(this);
        mBikeExerciseId = 1;
        mOwnerUserId = 1;
        mPresenter = new ExerciseDetailsPresenter(mBikeExerciseDataSource, mUserDataSource,
                mRouteDataSource, mView, mBikeExerciseId, mOwnerUserId);
    }

    private BikeExercise createSimpleBikeExercise() {
        int userId = 1;
        int routeId = 1;
        BikeExercise bikeExercise = new BikeExercise();
        bikeExercise.setId(mBikeExerciseId);
        bikeExercise.setUser(new User());
        bikeExercise.getUser().setId(userId);
        bikeExercise.setRoute(new Route());
        bikeExercise.getRoute().setId(routeId);

        return bikeExercise;
    }

    @Test
    public void testShowBikeExerciseAndInitialize_whenLoadBikeExerciseWasSucceed() {
        BikeExercise bikeExercise = createSimpleBikeExercise();

        Mockito.when(mBikeExerciseDataSource.getBikeExercise(mBikeExerciseId)).thenReturn(bikeExercise);
        Mockito.when(mRouteDataSource.getRoute(bikeExercise.getRoute().getId())).thenReturn(bikeExercise.getRoute());
        Mockito.when(mUserDataSource.getUser(bikeExercise.getUser().getId())).thenReturn(bikeExercise.getUser());
        mPresenter.initialize();

        //TODO: testar a chamada para cada um dos metodos que irao exibir infomacoes de rota e user
        Mockito.verify(mView).showExerciseDetails(bikeExercise);
        Mockito.verify(mView).showRouteInfo(bikeExercise.getRoute());
        Mockito.verify(mView).showUserInfo(bikeExercise.getUser());
    }

    @Test
    public void testInitializeShowErrorMessage_whenLoadBikeExerciseWasNotSucceed() {
        BikeExercise bikeExercise = createSimpleBikeExercise();

        Mockito.when(mBikeExerciseDataSource.getBikeExercise(mBikeExerciseId)).thenReturn(null);

        mPresenter.initialize();

        Mockito.verify(mView).showExerciseDetailsLoadError(R.string.cant_load_exercise_details_error);
        Mockito.verify(mView, Mockito.never()).showExerciseDetails(bikeExercise);
    }

    @Test
    public void testShowEditBikeExerciseUi() {
        mPresenter.openBikeExerciseEdit();
        Mockito.verify(mView).showBikeExerciseEditUi(mBikeExerciseId);
    }

    @Test
    public void showRouteDetailsUi() {
        BikeExercise bikeExercise = createSimpleBikeExercise();
        Mockito.when(mBikeExerciseDataSource.getBikeExercise(mBikeExerciseId)).thenReturn(bikeExercise);
        mPresenter.openRouteDetails();
        Mockito.verify(mView).showRouteDetailsUi(bikeExercise.getId());
    }

    @Test
    public void routeDetailsUiShowGeneralError_whenCantLoadBikeExercise() {
        Mockito.when(mBikeExerciseDataSource.getBikeExercise(mBikeExerciseId)).thenReturn(null);
        mPresenter.openRouteDetails();
        Mockito.verify(mView).showGeneralError(R.string.general_process_error_msg);
    }

    @Test
    public void showUserProfileUi() {
        BikeExercise bikeExercise = createSimpleBikeExercise();
        Mockito.when(mBikeExerciseDataSource.getBikeExercise(mBikeExerciseId)).thenReturn(bikeExercise);
        mPresenter.openUserProfile();
        Mockito.verify(mView).showRouteDetailsUi(bikeExercise.getUser().getId());
    }

    @Test
    public void userProfileUiShowGeneralError_whenCantLoadBikeExercise() {
        Mockito.when(mBikeExerciseDataSource.getBikeExercise(mBikeExerciseId)).thenReturn(null);
        mPresenter.openRouteDetails();
        Mockito.verify(mView).showGeneralError(R.string.general_process_error_msg);
    }

    @Test
    public void testDeleteBikeExerciseSucceed_showBikeExercisesList() {
        Mockito.when(mBikeExerciseDataSource.removeBikeExercise(1)).thenReturn(true);
        mPresenter.removeBikeExercise();
        Mockito.verify(mView).showDeleteStatusMessage(R.string.success_delete_bike_exercise_msg);
        Mockito.verify(mView).showExercisesListUi();
    }

    @Test
    public void testDeleteBikeExerciseNotSucceed_showGeneralError() {
        Mockito.when(mBikeExerciseDataSource.removeBikeExercise(1)).thenReturn(false);
        mPresenter.removeBikeExercise();
        Mockito.verify(mView).showGeneralError(R.string.general_process_error_msg);
        Mockito.verify(mView, Mockito.never()).showExercisesListUi();
    }

}