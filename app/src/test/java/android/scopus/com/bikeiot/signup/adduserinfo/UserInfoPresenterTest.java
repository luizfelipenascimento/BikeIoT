package android.scopus.com.bikeiot.signup.adduserinfo;

import android.scopus.com.bikeiot.R;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;


public class UserInfoPresenterTest {

    @Mock
    private UserInfoContract.View mUserInfoView;

    private UserInfoPresenter mUserInfoPresenter;

    public void setupUserInfoPresenterTest() {
        MockitoAnnotations.initMocks(this);
        mUserInfoPresenter = new UserInfoPresenter(mUserInfoView);
    }

    @Test
    public void whenNameIsEmptyShowMessageError() {
    }
}