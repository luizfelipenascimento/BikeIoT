package android.scopus.com.bikeiot.common;

import android.content.Context;
import android.scopus.com.bikeiot.utils.Helper;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;

/**
 * Created by Luiz Felipe on 18/05/2018.
 */

public class NavigationDrawerHelper {

    public void setupDrawerNavigationContent(final Context context, NavigationView navigationView,
                                             final int currentMenuId, final DrawerLayout drawerLayout) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Helper.Menu.selectDrawerItem(context, item, currentMenuId);
                drawerLayout.closeDrawers();
                return true;
            }
        });
    }
}
