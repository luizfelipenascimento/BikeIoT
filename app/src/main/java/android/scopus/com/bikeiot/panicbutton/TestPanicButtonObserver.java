package android.scopus.com.bikeiot.panicbutton;

import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;


/**
 * Created by Luiz Felipe on 22/04/2018.
 */

public class TestPanicButtonObserver extends ContentObserver {

    private static final String TAG = "TestPanicButtonObserver";
    private Context mContext;



    public TestPanicButtonObserver(Context context, Handler handler) {
        super(handler);
        mContext = context;
    }

    @Override
    public boolean deliverSelfNotifications() {
        return super.deliverSelfNotifications();
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        super.onChange(selfChange, uri);

        Log.d(TAG, "onChange uri: " + uri.toString());
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);

        Log.d(TAG, "onChange");
    }
}
