package android.scopus.com.bikeiot.usercontact;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.common.ConfirmationDialog;
import android.scopus.com.bikeiot.model.beans.Contact;
import android.scopus.com.bikeiot.model.data.ContactRepository;
import android.scopus.com.bikeiot.model.data.UserRepository;
import android.scopus.com.bikeiot.model.data.local.ContactDAO;
import android.scopus.com.bikeiot.model.data.local.UserDAO;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static android.scopus.com.bikeiot.Constants.EXTRA_CONTACT_ID;
import static android.scopus.com.bikeiot.Constants.REQUEST_RESULT_EDIT_REGISTER_CONTACT_ID;

public class UserContactEditCreateActivity extends AppCompatActivity
        implements UserContactEditCreateContract.View {

    private Toolbar mToolbar;
    private Button mSaveButton;
    private EditText mPhoneEditText;
    private EditText mNameEditText;
    private UserContactEditCreatePresenter mPresenter;
    private boolean isAbleToDelete = false;
    private TextView mPhoneNumberErrorTextView;
    private TextView mNameErrorTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_helper_contact);

        //setting up toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.edit_create_contact_title);
        mNameEditText = (EditText) findViewById(R.id.name_edit_text);
        mPhoneEditText = (EditText) findViewById(R.id.phone_number_edit_text);
        mPhoneNumberErrorTextView = (TextView) findViewById(R.id.phone_number_error_text_view);
        mNameErrorTextView = (TextView) findViewById(R.id.name_error_text_view);

        PhoneNumberFormattingTextWatcher phoneNumberFormattingTextWatcher;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            phoneNumberFormattingTextWatcher = new PhoneNumberFormattingTextWatcher("BR");
        } else {
            phoneNumberFormattingTextWatcher = new PhoneNumberFormattingTextWatcher();
        }

        mPhoneEditText.addTextChangedListener(phoneNumberFormattingTextWatcher);

        //verificando que a activity foi instanciada para result para exibir home no toolbar
        if (getCallingActivity() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        SharedPreferences preferences = this.getSharedPreferences
                (getString(R.string.shared_preferences_key), Context.MODE_PRIVATE);

        int contactId = getIntent().getIntExtra(EXTRA_CONTACT_ID, 0);

        ContactRepository repository = new ContactRepository(new ContactDAO(this));
        UserRepository userRepository = new UserRepository(new UserDAO(this));
        mPresenter = new UserContactEditCreatePresenter(repository, userRepository, contactId,
                preferences, this);

        mSaveButton = (Button) findViewById(R.id.save_button);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearErrors();
                String name = mNameEditText.getText().toString();
                String phone = mPhoneEditText.getText().toString();
                mPresenter.persist(name, phone);
            }
        });
    }

    private void clearErrors() {
        mPhoneNumberErrorTextView.setText("");
        mNameErrorTextView.setText("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_menu_item:
                showDeleteConfirmationDialog();
                return true;
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showDeleteConfirmationDialog() {
        FragmentManager fm = getFragmentManager();
        String content = getResources().getString(R.string.remove_contact_confirmation);
        String title = getResources().getString(R.string.dialog_remove_title);
        ConfirmationDialog confirmationDialog = ConfirmationDialog.newInstance(content, title, new ConfirmationDialog.ConfirmationDialogListener() {
            @Override
            public void onConfirmationDialogPositiveChoose() {
                mPresenter.removeContact();
            }

            @Override
            public void onConfirmationDialogNegativeChoose() {

            }
        });
        confirmationDialog.show(fm, ConfirmationDialog.DIALOG_TAG);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.initialize();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (isAbleToDelete) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.delete_menu, menu);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void showNameError(int msgError) {
        mNameErrorTextView.setText(msgError);
    }

    @Override
    public void showPhoneError(int msgError) {
        mPhoneNumberErrorTextView.setText(msgError);
    }

    @Override
    public void showContactInfo(Contact contact) {
        mNameEditText.setText(contact.getName());
        mPhoneEditText.setText(contact.getPhoneNumber());
    }

    @Override
    public void showError(int msgId) {
        Toast.makeText(this, msgId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showContactsFromResponseUi() {
        setResult(REQUEST_RESULT_EDIT_REGISTER_CONTACT_ID);
        finish();
    }

    @Override
    public void showSuccessMsg(int msgId) {
        Toast.makeText(this, msgId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void forceMenuShowDeleteOption() {
        isAbleToDelete = true;
        invalidateOptionsMenu();
    }

}
