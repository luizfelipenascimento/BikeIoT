package android.scopus.com.bikeiot.signup;

import android.content.Context;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.model.InputHandler;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.model.data.UserDataSource;
import android.scopus.com.bikeiot.model.data.local.BikeIoTContract.UserEntry;

/**
 * Created by Felipe on 2/14/2018.
 */

public class SignUpPresenter implements SignUpContract.Presenter {

    private static final String TAG = SignUpPresenter.class.getSimpleName();

    private SignUpContract.View mView;

    private UserDataSource mUserDataSource;
    private Context mContext;


    public SignUpPresenter(SignUpContract.View view, UserDataSource userDataSource) {
        mView = view;
        mUserDataSource = userDataSource;
    }


    @Override
    public void signUp(String email, String password, String name, String birthday) {
        if (isInputCorrect(email, password, name, birthday)) {
            User user = new User(name, email, password, birthday);

            if (mUserDataSource.saveUser(user)) {
                mView.showLogin();
            } else {
                mView.showGeneralError(R.string.general_process_error_msg);
            }
        }
    }

    @Override
    public void setContext(Context context) {
        mContext = context;
    }

    private boolean isInputCorrect(String email, String password, String name, String birthday) {
        boolean isEmailCorrect = validEmail(email);
        boolean isPasswordCorrect = validPassword(password);
        boolean isNameCorrect = validName(name);
        boolean isBirthdayCorrect = validBirthday(birthday);

        return  isEmailCorrect && isPasswordCorrect && isNameCorrect && isBirthdayCorrect;
    }

    private boolean validBirthday(String birthday) {

        if(InputHandler.isEmptyOrNull(birthday)) {
            mView.showBirthdayMessageError(R.string.empty_birthday_error);
            return false;
        }

        if (!InputHandler.isValidDate(birthday)) {
            mView.showBirthdayMessageError(R.string.invalid_birthday_error);
            return false;
        }

        return true;
    }

    private boolean validName(String name) {
        if (InputHandler.isEmptyOrNull(name)) {
            mView.showNameMessageError(R.string.empty_name_error);
            return false;
        }
        return true;
    }

    private boolean validPassword(String password) {
        if (InputHandler.isEmptyOrNull(password)) {
            mView.showPasswordMessageError(R.string.empty_password_error);
            return false;
        }

        return true;
    }

    private boolean validEmail(String email) {
        if (InputHandler.isEmptyOrNull(email)) {
            mView.showEmailMessageError(R.string.empty_email_error);
            return false;
        }

        if (!InputHandler.isValidEmail(email)) {
            mView.showEmailMessageError(R.string.invalid_email);
            return false;
        }

        if (!InputHandler.isUnique(UserEntry.TABLE_NAME, UserEntry.COLUMN_USER_EMAIL, email, mContext)) {
            mView.showEmailMessageError(R.string.email_already_used);
            return false;
        }

        return true;
    }
}
