package android.scopus.com.bikeiot.model.beans;

import android.scopus.com.bikeiot.utils.Helper;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 *
 */

public class User implements Serializable {

    private int mId;
    private ArrayList<BikeExercise> mExercises;
    private String mName;
    private Date mBirthday;
    private String mEmail;
    private int mWeight;
    private String mPassword;
    private double mHeight;
    private List<Contact> mContacts;
    private UUID mUUID;

    public User(String name, String email, String password, int weight, double height, String birthday, UUID uuid) {
        mName = name;
        mBirthday = Helper.parseToDate(birthday);
        mEmail = email;
        mWeight = weight;
        mPassword = password;
        mHeight = height;
        mUUID = uuid;
    }


    public User() {
    }

    public User(String name, String email, String password, String birthday) {
        mName = name;
        mEmail = email;
        mPassword = password;
        try {
            mBirthday = new SimpleDateFormat("yyyy-MM-dd").parse(birthday);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void setId(int id) {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public void setExercises(ArrayList<BikeExercise> exercises) {
        mExercises = exercises;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setWeight(int weight) {
        mWeight = weight;
    }

    public void setBirthday(Date birthday) {
        mBirthday = birthday;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public void setHeight(double height) {
        mHeight = height;
    }

    public void setContacts(List<Contact> contacts) {
        mContacts = contacts;
    }

    public List<Contact> getContacts() {
        return mContacts;
    }

    public double getHeight() {
        return mHeight;
    }

    public ArrayList<BikeExercise> getExercises() {
        return mExercises;
    }

    public String getName() {
        return mName;
    }

    public Date getBirthday() {
        return mBirthday;
    }

    public String getEmail() {
        return mEmail;
    }

    public String getPassword() {
        return mPassword;
    }

    public int getWeight() {
        return mWeight;
    }

    public UUID getUUID() {
        return mUUID;
    }

    public void setUUID(UUID UUID) {
        mUUID = UUID;
    }
}
