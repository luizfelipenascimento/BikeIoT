package android.scopus.com.bikeiot.model.beans;

import com.google.android.gms.maps.model.LatLng;

/**
 *
 */

public class Place {

    private LatLng location;
    private String name;
    private double distanceToPlace;
    private boolean isOpenedNow;

    public LatLng getLocationLatLng() {
        return location;
    }

    public String getName() {
        return name;
    }

    public double getDistanceToPlace() {
        return distanceToPlace;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDistanceToPlace(double distanceToPlace) {
        this.distanceToPlace = distanceToPlace;
    }

    public void setOpenedNow(boolean openedNow) {
        isOpenedNow = openedNow;
    }

    public boolean isOpenedNow() {
        return isOpenedNow;
    }
}
