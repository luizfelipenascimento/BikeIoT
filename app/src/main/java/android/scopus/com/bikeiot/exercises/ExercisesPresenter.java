package android.scopus.com.bikeiot.exercises;

import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.data.BikeExerciseDataSource;
import android.scopus.com.bikeiot.model.data.BikeExerciseRepository;
import android.util.Log;

import java.util.List;

/**
 *
 */

public class ExercisesPresenter implements ExercisesContract.Presenter {

    private static final String TAG = "ExercisesPresenter";

    private int mUserId;
    private ExercisesContract.View mView;
    private BikeExerciseRepository mBikeExerciseRepository;

    public ExercisesPresenter(ExercisesContract.View view, BikeExerciseRepository bikeExerciseRepository,
                              int userId) {
        mView = view;
        mBikeExerciseRepository = bikeExerciseRepository;
        mUserId = userId;
    }

    @Override
    public void loadUserExercises(int userId) {
        List<BikeExercise> bikeExercises = mBikeExerciseRepository.getBikeExercisesFromUser(userId);
        if (!bikeExercises.isEmpty()) {
            Log.d(TAG, "bikeExercises is not empty");
            mView.showExercises(bikeExercises);
        } else {
            Log.d(TAG, "bikeExercises is empty");
            mView.showNoExercisesDone();
        }
    }

    @Override
    public void loadExercises() {

    }

    @Override
    public void initialize() {
        mBikeExerciseRepository.forceCacheBroke();
        loadUserExercises(mUserId);
    }

    @Override
    public void openBikeExercise(BikeExercise bikeExercise) {
        if (bikeExercise != null) {
            mView.showExerciseDetailsUi(bikeExercise.getId());
        }
    }
}
