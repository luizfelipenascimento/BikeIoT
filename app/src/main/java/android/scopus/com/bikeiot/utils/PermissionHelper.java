package android.scopus.com.bikeiot.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

/**
 * Created by Luiz Felipe on 17/04/2018.
 */

public class PermissionHelper {

    public static boolean isPermissionGranted(Context context, String permission) {
        if (isMApi()) {
            return context.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        }
        return true;
    }

    public static boolean isPermissionsGranted(Context context, String[] permissions) {
        for (String permission : permissions) {
            if (!isPermissionGranted(context, permission)) {
                return false;
            }
        }
        return true;
    }

    public static void requestPermissions(Activity activity, int permissionCode, String[] permissions) {
        if (isMApi()) {
            activity.requestPermissions(permissions, permissionCode);
        }
    }

    private static boolean isMApi() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

}
