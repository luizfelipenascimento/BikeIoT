package android.scopus.com.bikeiot.usercontacts;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.common.BaseListAdapterOnClickListener;
import android.scopus.com.bikeiot.common.NavigationDrawerHelper;
import android.scopus.com.bikeiot.model.beans.Contact;
import android.scopus.com.bikeiot.model.data.ContactRepository;
import android.scopus.com.bikeiot.model.data.local.ContactDAO;
import android.scopus.com.bikeiot.usercontact.UserContactEditCreateActivity;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import static android.scopus.com.bikeiot.Constants.EXTRA_CONTACT_ID;
import static android.scopus.com.bikeiot.Constants.REQUEST_RESULT_EDIT_REGISTER_CONTACT_ID;

public class UserContactsActivity extends AppCompatActivity implements UserContactsContract.View,
        BaseListAdapterOnClickListener<Contact> {

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private NavigationView mNavigationView;
    private UserContactsPresenter mPresenter;
    private RecyclerView mContactsRecyclerView;
    private ImageView mNoContactRegisteredImageView;
    private TextView mNoContactRegisteredTextView;
    private LinearLayout mWarningContainerLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_contacts);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.contacts_activity_title);

        //setting up navigation
        mNavigationView = (NavigationView) findViewById(R.id.main_navigation_view);
        new NavigationDrawerHelper().setupDrawerNavigationContent(this, mNavigationView,
                R.id.contacts_list_menu_item, mDrawerLayout);

        mContactsRecyclerView = (RecyclerView) findViewById(R.id.contacts_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mContactsRecyclerView.setLayoutManager(linearLayoutManager);

        mNoContactRegisteredTextView = (TextView) findViewById(R.id.warning_status_text_view);
        mNoContactRegisteredImageView = (ImageView) findViewById(R.id.warning_status_image_view);
        mWarningContainerLinearLayout = (LinearLayout) findViewById(R.id.warning_status_container_linear_layout);
        mWarningContainerLinearLayout.setVisibility(View.GONE);

        ContactRepository repository = new ContactRepository(new ContactDAO(this));
        SharedPreferences sharedPreferences = this.getSharedPreferences(getString(R.string.shared_preferences_key), Context.MODE_PRIVATE);
        mPresenter = new UserContactsPresenter(repository, sharedPreferences, this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.initialize();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.add_contact_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mActionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        if (item.getItemId() == R.id.add_contact_menu_item) {
            showRegisterContactUi();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showContacts(List<Contact> contacts) {
        mWarningContainerLinearLayout.setVisibility(View.GONE);
        UserContactsListAdapter adapter = new UserContactsListAdapter(contacts, this, this);
        mContactsRecyclerView.setAdapter(adapter);
    }

    @Override
    public void showNoContactRegistered() {
        mContactsRecyclerView.setAdapter(null);
        mWarningContainerLinearLayout.setVisibility(View.VISIBLE);
        mNoContactRegisteredTextView.setText(R.string.no_contact_registered);
    }

    @Override
    public void showContactUi(int id) {
        Intent intent = new Intent(this, UserContactEditCreateActivity.class);
        intent.putExtra(EXTRA_CONTACT_ID, id);
        startActivityForResult(intent, REQUEST_RESULT_EDIT_REGISTER_CONTACT_ID);
    }

    @Override
    public void showErrorToSelectContact() {
        Toast.makeText(this, R.string.error_to_select_contact, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showRegisterContactUi() {
        Intent intent = new Intent(this, UserContactEditCreateActivity.class);
        startActivityForResult(intent, REQUEST_RESULT_EDIT_REGISTER_CONTACT_ID);
    }

    @Override
    public void onClickItem(Contact contact) {
        mPresenter.selectContact(contact.getId());
    }
}
