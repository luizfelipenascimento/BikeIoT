package android.scopus.com.bikeiot.hospitals;

import android.location.Location;
import android.net.Uri;
import android.scopus.com.bikeiot.model.location.LocationUpdater;
import android.scopus.com.bikeiot.model.beans.Place;
import android.scopus.com.bikeiot.model.network.NetworkHelper;
import android.scopus.com.bikeiot.model.network.googleplacesAPI.GooglePlacesAsyncTask;
import android.scopus.com.bikeiot.model.network.googleplacesAPI.GooglePlacesQueryUtils;
import android.util.Log;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static android.scopus.com.bikeiot.model.network.googleplacesAPI.GooglePlacesQueryUtils.HOSPITAL_TYPE;

/**
 *
 */

public class HospitalsPresenter implements HospitalsContract.Presenter {

    private static final String TAG = HospitalsPresenter.class.getSimpleName();
    private final HospitalsContract.View mView;
    private final NetworkHelper mNetworkHelper;
    private GooglePlacesAsyncTask.CallbackOnResponseListener mGooglePlaceApiCallback;
    private LocationUpdater mLocationUpdater;
    private String mGooglePlaceApiKey;
    private Location mLocation;
    private LocationUpdater.Callback.OnLocationReceived mLocationUpdaterCallback;
    private LocationUpdater.Callback.OnLastKnownLocation mLastLocationCallback;
    private boolean requestLocationForGooglePlace;
    private List<LatLng> mHospitalLocations;
    private LocationCallback mLocationCallback;

    public HospitalsPresenter(LocationUpdater locationUpdater, String googlePlaceApiKey,
                              HospitalsContract.View view, NetworkHelper networkHelper,
                              Location location) {

        mLocation = location;
        mGooglePlaceApiKey = googlePlaceApiKey;
        mLocationUpdater = locationUpdater;
        mView = view;

        createLocationCallback();
        mLocationUpdater.setupLocationCallback(mLocationCallback);

        createGooglePlaceApiCallback();
        mNetworkHelper = networkHelper;
        mHospitalLocations = new ArrayList<>();
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                mLocation = locationResult.getLastLocation();

                if (requestLocationForGooglePlace) {
                    requestClosestHospitals();
                    requestLocationForGooglePlace = false;
                    mLocationUpdater.stopLocationUpdates();
                }
            }
        };
    }

    private void createGooglePlaceApiCallback() {
        mGooglePlaceApiCallback = new GooglePlacesAsyncTask.CallbackOnResponseListener() {

            @Override
            public void onNetworkProblem() {
                mView.hideHospitalSearchProgress();
                mView.showNetworkConnectionError();
            }

            @Override
            public void onResponse(List<Place> places) {
                Log.d(TAG, "HospitalsPresenter onResponse called");

                mView.hideHospitalSearchProgress();

                if (places.isEmpty()) {
                    mView.showNoCloseHospitalWarning();
                    return;
                }

                places = filterOnlyOpenedPlaces(places);

                places = filterMinimumPlacesQuantity(places);
                setDistanceToEachPlace(places);

                mView.showClosestHospitalsPlaces(places);
                //mView.showMarkerInMap(mHospitalLocationLatLng, R.string.hospital);
            }

            @Override
            public void onFailResponse() {
                mView.hideHospitalSearchProgress();
                mView.showNoCloseHospitalWarning();
            }
        };
    }

    private List<Place> filterOnlyOpenedPlaces(List<Place> places) {
        Iterator<Place> placesIterator = places.iterator();

        while (placesIterator.hasNext()) {
            if (!placesIterator.next().isOpenedNow()) {
                placesIterator.remove();
            }
        }

        return places;
    }

    private void setDistanceToEachPlace(List<Place> places) {

        for (Place place: places) {
            LatLng placeLatLng = place.getLocationLatLng();

            Location placeLocation = new Location("");
            placeLocation.setLatitude(placeLatLng.latitude);
            placeLocation.setLongitude(placeLatLng.longitude);

            place.setDistanceToPlace(mLocation.distanceTo(placeLocation));
        }
    }

    private List<Place> filterMinimumPlacesQuantity(List<Place> places) {
        int maxAllowed = 10;

        if (places.size() >= maxAllowed) {
            places = places.subList(0, maxAllowed - 1);
        }

        return places;
    }

    @Override
    public void initialize() {
        getMyCurrentLocationAndRequestClosestHospitals();
    }

    public void getMyCurrentLocationAndRequestClosestHospitals() {
        mLocationUpdater.startLocationUpdates();
        requestLocationForGooglePlace = true;
    }

    private void requestClosestHospitals() {
        Log.d(TAG, "requestClosestHospitals called");

        String builtUrlString = GooglePlacesQueryUtils.buildUrlGetClosePlacesRankByDistance(
                GooglePlacesQueryUtils.HOSPITAL_TYPE, mLocation, HOSPITAL_TYPE, mGooglePlaceApiKey);

        Log.d(TAG, "requestClosestHospitals generated url: " + builtUrlString);

        //objects[0] = string url / objects[1] = Location
        new GooglePlacesAsyncTask(mNetworkHelper, mGooglePlaceApiCallback)
                .execute(builtUrlString);

    }

    @Override
    public void finalize() {
        mLocationUpdater.stopLocationUpdates();
    }

    @Override
    public void selectPlace(Place hospitalPlace) {
        LatLng placeLocation = hospitalPlace.getLocationLatLng();

        String uriBaseString = "google.navigation:q=";
        uriBaseString += placeLocation.latitude + "," + placeLocation.longitude;

        Uri uriGoogleMaps = Uri.parse(uriBaseString);

        mView.showGoogleMapsNavigation(uriGoogleMaps);
    }

}
