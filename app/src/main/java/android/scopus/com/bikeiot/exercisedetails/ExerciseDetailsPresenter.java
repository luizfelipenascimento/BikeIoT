package android.scopus.com.bikeiot.exercisedetails;

import android.net.Uri;
import android.scopus.com.bikeiot.utils.CalculateUtils;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.utils.UriHelper;
import android.scopus.com.bikeiot.model.filerelated.CSVFileContentManager;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.filerelated.CSVFile;
import com.github.mikephil.charting.data.Entry;

import android.scopus.com.bikeiot.model.data.BikeExerciseDataSource;
import android.scopus.com.bikeiot.model.data.RouteDataSource;
import android.scopus.com.bikeiot.model.data.UserDataSource;
import android.util.Log;

import java.util.ArrayList;

import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.CSVFileResultsContract.CSV_RESULT_COLUMN_DATE_TIME;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.CSVFileResultsContract.CSV_RESULT_COLUMN_PITCH;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.CSVFileResultsContract.CSV_RESULT_COLUMN_VELOCITY;

/**
 *
 */

public class ExerciseDetailsPresenter implements ExerciseDetailsContract.Presenter {

    private static final String TAG = ExerciseDetailsPresenter.class.getSimpleName();
    private UserDataSource mUserDataSource;
    private RouteDataSource mRouteDataSource;
    private BikeExerciseDataSource mBikeExerciseDataSource;
    private ExerciseDetailsContract.View mView;
    private int mBikeExerciseId;
    private int mOwnerUserId;
    private BikeExercise mBikeExercise;

    public ExerciseDetailsPresenter(BikeExerciseDataSource bikeExerciseDataSource,
                                    UserDataSource userDataSource, RouteDataSource routeDataSource,
                                    ExerciseDetailsContract.View view, int bikeExerciseId,
                                    int ownerUserId) {

        mUserDataSource = userDataSource;
        mRouteDataSource = routeDataSource;
        mBikeExerciseDataSource = bikeExerciseDataSource;
        mView = view;
        mBikeExerciseId = bikeExerciseId;
        mOwnerUserId = ownerUserId;
    }

    @Override
    public void initialize() {
        loadBikeExercise(mBikeExerciseId);
    }

    @Override
    public void loadBikeExercise(int bikeExerciseId) {
        mBikeExercise = mBikeExerciseDataSource.getBikeExercise(bikeExerciseId);

        if (mBikeExercise == null) {
            mView.showExerciseDetailsLoadError(R.string.cant_load_exercise_details_error);
            return;
        }

        mBikeExercise.setUser(mUserDataSource.getUser(mBikeExercise.getUser().getId()));
        mBikeExercise.setRoute(mRouteDataSource.getRoute(mBikeExercise.getRoute().getId()));

        if (mBikeExercise.getUser() == null) {
            mView.showNoUserInfo();
        } else {
            mView.showUserInfo(mBikeExercise.getUser());
        }

        if (mBikeExercise.getRoute() == null) {
            mView.showNoRouteInfo();
        } else {
            mView.showRouteInfo(mBikeExercise.getRoute());
            Log.d(TAG, " m route image path :"  + mBikeExercise.getRoute().getImagePath());

            Uri imageUri = null;

            try {
                imageUri = UriHelper.getUriFromFile(mBikeExercise.getRoute().getImagePath());
            } catch (Exception e) {
                Log.e(TAG, "UriHelper.getUriFromFile exception " + e);
            }

            if (imageUri != null) {
                mView.showRouteImage(imageUri);
            } else {
                mView.showNoRouteImage();
            }
        }

        mView.showExerciseDetails(mBikeExercise);
        prepareChartData(mBikeExercise);
    }

    @Override
    public void openUserProfile() {
        if (mBikeExercise == null) {
            mBikeExercise = mBikeExerciseDataSource.getBikeExercise(mBikeExerciseId);
            if (mBikeExercise == null) {
                mView.showGeneralError(R.string.general_process_error_msg);
                return;
            }
        }

        mView.showRouteDetailsUi(mBikeExercise.getUser().getId());
    }

    @Override
    public void openRouteDetails() {
        if (mBikeExercise == null) {
            mBikeExercise = mBikeExerciseDataSource.getBikeExercise(mBikeExerciseId);
            if (mBikeExercise == null) {
                mView.showGeneralError(R.string.general_process_error_msg);
                return;
            }
        }

        mView.showRouteDetailsUi(mBikeExercise.getRoute().getId());
    }

    @Override
    public void openBikeExerciseEdit() {
        mView.showBikeExerciseEditUi(mBikeExerciseId);
    }

    @Override
    public void removeBikeExercise() {
        if (mBikeExerciseDataSource.removeBikeExercise(mBikeExerciseId)) {
            mView.showDeleteStatusMessage(R.string.success_delete_bike_exercise_msg);
            mView.showExercisesListUi();
        } else {
            mView.showGeneralError(R.string.general_process_error_msg);
        }
    }

    private void prepareChartData(BikeExercise bikeExercise) {
        float velocity, pitch;
        String date;

        ArrayList<Entry> velocityEntries = new ArrayList<>();
        ArrayList<Entry> pitchEntries = new ArrayList<>();

        CSVFileContentManager contentManager = CSVFile.readAllFileContent(bikeExercise.getResult().getFileResultPath());

        if (contentManager == null) {
            mView.showErrorToReadResultFile();
            return;
        }

        try {
            for (int line = 0; line < contentManager.getNumberOfLines(); line++) {
                date = contentManager.getColumnValue(CSV_RESULT_COLUMN_DATE_TIME, line);

                velocity = Float.parseFloat(contentManager.getColumnValue(CSV_RESULT_COLUMN_VELOCITY, line));
                velocity = (float) CalculateUtils.metersPerSecToKmPerH((double) velocity);
                velocityEntries.add(new Entry(line, velocity));

                pitch = Float.parseFloat(contentManager.getColumnValue(CSV_RESULT_COLUMN_PITCH, line));
                pitchEntries.add(new Entry(line, pitch));
            }
        } catch (CSVFileContentManager.NoCSVColumnException e) {
            Log.e(TAG, "error: " + e);
            mView.showErrorToReadResultFile();
            return;
        }

        if (velocityEntries.size() > 0)
            mView.showVelocityChart(velocityEntries);

        if (pitchEntries.size() > 0)
            mView.showPitchChart(pitchEntries);
    }
}
