package android.scopus.com.bikeiot.signup.adduserinfo;



public interface UserInfoContract {

    interface View {
        void showNameMessageError(int msgId);

    }

    interface Presenter {
        void setUserInformation(String name, String birthday, String weight, String height);
    }

}
