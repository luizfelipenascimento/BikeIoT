package android.scopus.com.bikeiot.common;

import android.app.Activity;
import android.scopus.com.bikeiot.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static android.scopus.com.bikeiot.Constants.EXTRA_CONTENT;

public class WarningDialogActivity extends Activity {

    private Button mConfirmButton;
    private TextView mContentTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFinishOnTouchOutside(false);
        setContentView(R.layout.activity_warning_dialog);

        String content = getIntent().getStringExtra(EXTRA_CONTENT);

        mContentTextView = (TextView) findViewById(R.id.content_text_view);
        mConfirmButton = (Button) findViewById(R.id.confirm_button);

        if (content != null) {
            mContentTextView.setText(content);
        } else {
            mContentTextView.setText(R.string.default_warning_msg);
        }


        mConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WarningDialogActivity.this.finish();
            }
        });
    }


}
