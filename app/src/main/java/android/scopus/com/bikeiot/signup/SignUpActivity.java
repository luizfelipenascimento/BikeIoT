package android.scopus.com.bikeiot.signup;

import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.model.data.local.UserDAO;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SignUpActivity extends AppCompatActivity implements SignUpContract.View {

    private Button mSubmitButton;

    private EditText mEmailEditText;
    private EditText mPasswordEditText;
    private TextView mBirthdayEditText;
    private EditText mNameEditText;

    private TextView mPasswordMgsError;
    private TextView mEmailMgsError;
    private TextView mBirthdayMsgErrorTextView;
    private TextView mNameMsgErrorTextView;


    private TextView mGeneralMsgError;
    private SignUpContract.Presenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mPresenter = new SignUpPresenter(this, new UserDAO(this));
        mPresenter.setContext(this);

        mSubmitButton = (Button) findViewById(R.id.sign_up_submit_button);

        mEmailEditText = (EditText) findViewById(R.id.sign_up_email_edit_text);
        mPasswordEditText = (EditText) findViewById(R.id.sign_up_password_edit_text);
        mBirthdayEditText = (EditText) findViewById(R.id.birthday_edit_text);
        mNameEditText = (EditText) findViewById(R.id.name_edit_text);

        mPasswordMgsError = (TextView) findViewById(R.id.password_msg_error_text_view);
        mEmailMgsError = (TextView) findViewById(R.id.email_msg_error_text_view);
        mBirthdayMsgErrorTextView = (TextView) findViewById(R.id.birthday_msg_error_text_view);
        mNameMsgErrorTextView = (TextView) findViewById(R.id.name_msg_error_text_view);
        mGeneralMsgError = (TextView) findViewById(R.id.general_error_text_view);


        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEmailEditText.getText().toString();
                String password = mPasswordEditText.getText().toString();
                String name = mNameEditText.getText().toString();
                String birthday = mBirthdayEditText.getText().toString();
                mPresenter.signUp(email, password, name, birthday);
            }
        });
    }

    @Override
    public void showEmailMessageError(int msgId) {
        mEmailMgsError.setText(getResources().getText(msgId));
    }

    @Override
    public void showPasswordMessageError(int msgId) {
        mPasswordMgsError.setText(getResources().getText(msgId));
    }

    @Override
    public void showLogin() {

    }

    @Override
    public void showGeneralError(int msgId) {
        mGeneralMsgError.setText(getResources().getText(msgId));
    }

    @Override
    public void showNameMessageError(int msgId) {
        mNameMsgErrorTextView.setText(msgId);
    }

    @Override
    public void showBirthdayMessageError(int msgId) {
        mBirthdayMsgErrorTextView.setText(msgId);
    }
}
