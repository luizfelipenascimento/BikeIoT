package android.scopus.com.bikeiot.model.network.googleplacesAPI;

import android.location.Location;
import android.scopus.com.bikeiot.model.beans.Place;
import android.scopus.com.bikeiot.model.network.ApiQueryUtils;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */

public final class GooglePlacesQueryUtils {

    public static final String HOSPITAL_TYPE = "hospital";
    public static final String[] HOSPITAL_RELATE_KEY_WORDS = {"hospital","clinica","ubs","upa","socorro"};

    private static final String TAG = GooglePlacesQueryUtils.class.getSimpleName();

    private final static String BASE_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";

    private ApiQueryUtils mApiQueryUtils;

    public static String buildUrlGetClosePlacesInRadius(String type, Location location, int radius, String keyword, String key) {
        String url = BASE_URL;
        url += "location=" + location.getLatitude() + "," + location.getLongitude();
        url += "&radius=" + String.valueOf(radius);
        url += "&type=" + type;
        url += "&keyword=" + keyword;
        url += "&key=" + key;
        return url;
    }

    private static String configureKeywords(String[] keywords) {
        String keys = "";
        for (int i = 0; i < keywords.length; i++) {
            keys += (i == keywords.length - 1) ? keywords[i] : keywords[i] + " or ";
        }
        return keys;
    }

    //https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&rankby=distance&type=food&key=YOUR_API_KEY
    public static String buildUrlGetClosePlacesRankByDistance(String type, Location location, String keyword,String key) {
        String url = BASE_URL;
        url += "location=" + location.getLatitude() + "," + location.getLongitude();
        url += "&rankby=distance";
        url += "&type=" + type;
        url += "&keyword=" + keyword;
        url += "&key=" + key;
        return url;
    }

    public GooglePlacesQueryUtils() {
        mApiQueryUtils = new ApiQueryUtils();
    }

    public List<Place> fetchDataClosestPlaces(String builtUrl) {
        String response = mApiQueryUtils.getJsonResponse(builtUrl);
        List<Place> places = parseJasonToClosestLocationData(response);
        return places;
    }

    public LatLng getClosestPlace(List<LatLng> locationsLatLng, Location currentLocation) {
        if (locationsLatLng.isEmpty()) return null;

        Location closestLocation = null;
        for (LatLng latLng : locationsLatLng) {

            Location location = buildLocationFromLatLng(latLng);

            if (closestLocation == null) {
                closestLocation = location;
            } else if (currentLocation.distanceTo(location) < currentLocation.distanceTo(closestLocation)) {
                closestLocation = location;
            }

        }

        return new LatLng(closestLocation.getLatitude(), closestLocation.getLongitude());
    }

    private Location buildLocationFromLatLng(LatLng latLng) {
        Location location = new Location("");
        location.setLatitude(latLng.latitude);
        location.setLongitude(latLng.longitude);
        return location;
    }

    private List<Place> parseJasonToClosestLocationData(String response) {
        ArrayList<Place> places = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray resultsJson = jsonObject.getJSONArray("results");

            for (int i = 0; i < resultsJson.length(); i++) {
                Place place = new Place();

                JSONObject jsonObjectPlace = resultsJson.getJSONObject(i);

                String name = jsonObjectPlace.getString("name");
                place.setName(name);

                if (jsonObjectPlace.has("opening_hours")) {
                    JSONObject openingHourJson = jsonObjectPlace.getJSONObject("opening_hours");
                    boolean openedNow = openingHourJson.getBoolean("open_now");
                    place.setOpenedNow(openedNow);
                }

                JSONObject geometryJson = jsonObjectPlace.getJSONObject("geometry");
                JSONObject locationJson = geometryJson.getJSONObject("location");

                double latitude = locationJson.getDouble("lat");
                double longitude = locationJson.getDouble("lng");

                LatLng latLng = new LatLng(latitude, longitude);

                place.setLocation(latLng);

                places.add(place);

            }

        } catch (JSONException e) {
            Log.e(TAG, "parseJasonToClosestLocationData: " + e);
        }

        return places;
    }
}
