package android.scopus.com.bikeiot.routedetails;

import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.beans.User;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

/**
 *
 */

public interface RouteDetailsContract {

    interface View {
        void showRoutePolylineOnMap(PolylineOptions polylineOptions);
        void showDistanceToRoute(double distanceToRoute) ;
        void showRouteDetails(Route route);
        void showDeleteUpdateMenu();
        void showNoRouteError();
        void showCenterMapButton();
        void centerRouteOnMap(LatLngBounds locationBounds);
        void showNoRoutePolylineLoaded();
        void sendSelectedRouteToExerciseRecorderUi(ArrayList<LatLng> routePoints);
        void showSelectRouteDialogConfirmation(double distance);
        void showUserDetails(User user);
        void showNoUserDetails();
        void showEditRouteUi(int routeId);
        void showRoutesUi();
        void showDeleteDialogConfirmation();
        void showRemoveSuccess();
        void showRemoveRouteError();
        void showStartEndMarkers(LatLng startLatLng, LatLng endLatLng);
        void showImpossibleToSelect();
    }

    interface Presenter {
        void initialize();
        void loadRoute(int RouteId);
        void loadLocation();
        void loadRoutePointLocationBounds();
        void loadDistanceToRoad();
        void loadRoutePolyline();
        void selectRoute();
        void confirmRouteSelect();
        void removeRoute();
        void prepareToEditRouteUi();
    }
}
