package android.scopus.com.bikeiot.exercisedetails;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.scopus.com.bikeiot.BaseNavigation;
import android.scopus.com.bikeiot.utils.Helper;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.utils.SharedPreferencesUtils;
import android.scopus.com.bikeiot.exercises.ExercisesActivity;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.model.data.local.BikeExerciseDAO;
import android.scopus.com.bikeiot.model.data.local.RouteDAO;
import android.scopus.com.bikeiot.model.data.local.UserDAO;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;

import static android.scopus.com.bikeiot.utils.CalculateUtils.metersPerSecToKmPerH;
import static android.scopus.com.bikeiot.utils.CalculateUtils.metersToKm;
import static android.scopus.com.bikeiot.utils.Helper.formatDateToTimeString;
import static android.scopus.com.bikeiot.utils.Helper.formatKmDecimal;

public class ExerciseDetailsActivity extends AppCompatActivity
        implements ExerciseDetailsContract.View, BaseNavigation {


    public static final String EXTRA_BIKE_EXERCISE_ID =
            "android.scopus.com.bikeiot.ExerciseDetailsActivity.EXTRA_BIKE_EXERCISE_ID";

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private NavigationView mNavigationView;
    private ExerciseDetailsPresenter mPresenter;
    private TextView mUserNameTextView;
    private TextView mExerciseDescriptionTextView;
    private TextView mExerciseDurationTextView;
    private TextView mExerciseRouteDistanceTextView;
    private TextView mExerciseMeanVelocityTextView;
    private TextView mExerciseMaxVelocityTextView;
    private TextView mExerciseCaloriesTextView;
    private TextView mExerciseMeanHeartRateTextView;
    private ImageView mExerciseRouteImageView;
    private LinearLayout mNoRouteImageErrorContainer;
    private LineChart mPitchLineChart;
    private LineChart mVelocityLineChart;
    private TextView mErrorReadDataFileTextView;
    private LinearLayout mChartContainerLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_details);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(R.string.exercise_details_activity_title);

        //setting up navigation
        mNavigationView = (NavigationView) findViewById(R.id.main_navigation_view);
        setupDrawerNavigationContent(mNavigationView);

        mUserNameTextView = (TextView) findViewById(R.id.user_name_exercise_details);
        mExerciseDescriptionTextView = (TextView) findViewById(R.id.exercise_details_description);
        mExerciseDurationTextView = (TextView) findViewById(R.id.exercise_duration_time_text_view);
        mExerciseRouteDistanceTextView = (TextView) findViewById(R.id.exercise_route_distance_text_view);
        mExerciseMeanVelocityTextView = (TextView) findViewById(R.id.exercise_mean_velocity_text_view);
        mExerciseMaxVelocityTextView = (TextView) findViewById(R.id.exercise_max_velocity_text_view);
        mExerciseCaloriesTextView = (TextView) findViewById(R.id.exercise_calories_burned_text_view);
        mExerciseMeanHeartRateTextView = (TextView) findViewById(R.id.exercise_mean_heart_rate_text_view);
        mExerciseRouteImageView = (ImageView) findViewById(R.id.exercise_route_image_view);
        mNoRouteImageErrorContainer = (LinearLayout) findViewById(R.id.no_route_image_error_container_linear_layout);

        mVelocityLineChart = (LineChart) findViewById(R.id.velocity_line_chart);
        mPitchLineChart = (LineChart) findViewById(R.id.pitch_line_chart);
        mErrorReadDataFileTextView = (TextView) findViewById(R.id.error_read_data_file_chart_text_view);
        mChartContainerLinearLayout = (LinearLayout) findViewById(R.id.chart_container_linear_layout);

        int bikeExerciseId = getIntent().getIntExtra(EXTRA_BIKE_EXERCISE_ID, 0);

        int OwnerUserId = SharedPreferencesUtils.getIntSharedPreferences(this,
                getString(R.string.shared_preferences_key), getString(R.string.shared_user_id),0);

        mPresenter = new ExerciseDetailsPresenter(new BikeExerciseDAO(this),
                new UserDAO(this), new RouteDAO(this), this,
                bikeExerciseId, OwnerUserId);
    }

    @Override
    public void setupDrawerNavigationContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Helper.Menu.selectDrawerItem(ExerciseDetailsActivity.this, item, -1);
                mDrawerLayout.closeDrawers();
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.initialize();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mActionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.delete_menu_item:
                //TODO: criar alert dialog para confirmar a acao
                mPresenter.removeBikeExercise();
                return true;
            case R.id.edit_menu_item:
                mPresenter.openBikeExerciseEdit();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.delete_edit_menu, menu);
        return true;
    }

    @Override
    public void showExerciseDetails(BikeExercise bikeExercise) {
        String km_hIndicator = getString(R.string.km_h_indicator);

        String duration = formatDateToTimeString(bikeExercise.getResult().getTotalTimeChronometer());
        String meanVelocity = formatKmDecimal(metersPerSecToKmPerH(bikeExercise.getResult()
                .getMeanVelocityMetersPerSec()));
        String maxVelocity = formatKmDecimal(metersPerSecToKmPerH(bikeExercise.getResult().getMaxVelocityMetersPerSec()));

        mExerciseDurationTextView.setText(duration);
        mExerciseMeanVelocityTextView.setText(meanVelocity + " " + km_hIndicator);
        mExerciseMaxVelocityTextView.setText(maxVelocity + " " + km_hIndicator);
        mExerciseDescriptionTextView.setText(bikeExercise.getDescription());
        mExerciseCaloriesTextView.setText(String.valueOf(bikeExercise.getResult().getCaloriesBurned()));
        mExerciseMeanHeartRateTextView.setText(String.valueOf(bikeExercise.getResult().getMeanHeartRate()));

    }

    @Override
    public void showBikeExerciseEditUi(int bikeExerciseId) {

    }

    @Override
    public void showRouteDetailsUi(int routeId) {

    }

    @Override
    public void showUserProfileUi(int userId) {

    }

    @Override
    public void showExercisesListUi() {
        Intent intent = new Intent(this, ExercisesActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void showExerciseDetailsLoadError(int errorMsgId) {
        //TODO: implementar o error do exerciseDetails
    }

    @Override
    public void showRouteInfo(Route route) {
        String kmIndicator = getString(R.string.km_indicator);
        String distance = formatKmDecimal(metersToKm(route.getDistance()));
        mExerciseRouteDistanceTextView.setText(distance + " " + kmIndicator);
    }

    @Override
    public void showUserInfo(User user) {
        mUserNameTextView.setText(user.getName());
    }

    @Override
    public void showNoRouteInfo() {
        mExerciseRouteDistanceTextView.setText(R.string.information_not_available);
    }

    @Override
    public void showNoUserInfo() {
        mUserNameTextView.setText(R.string.information_not_available);
    }

    @Override
    public void showDeleteStatusMessage(int messageId) {
        Toast toast = Toast.makeText(this, messageId, Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void showGeneralError(int errorMessageId) {

    }

    @Override
    public void showRouteImage(Uri uri) {
        Glide.with(this)
                .load(uri)
                .into(mExerciseRouteImageView);
    }

    @Override
    public void showNoRouteImage() {
        mNoRouteImageErrorContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showErrorToReadResultFile() {
        mErrorReadDataFileTextView.setVisibility(View.VISIBLE);
        mChartContainerLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void showVelocityChart(ArrayList<Entry> velocityEntries) {
        mErrorReadDataFileTextView.setVisibility(View.GONE);
        mChartContainerLinearLayout.setVisibility(View.VISIBLE);
        LineDataSet lineDataSet = new LineDataSet(velocityEntries, getString(R.string.velocity_chart));
        lineDataSet.setCircleColor(Color.RED);
        lineDataSet.setDrawCircles(false);
        lineDataSet.setColor(getColor(R.color.danger));
        LineData lineData = new LineData(lineDataSet);
        mVelocityLineChart.setData(lineData);
    }

    @Override
    public void showPitchChart(ArrayList<Entry> pitchEntries) {
        mErrorReadDataFileTextView.setVisibility(View.GONE);
        mChartContainerLinearLayout.setVisibility(View.VISIBLE);
        LineDataSet lineDataSet = new LineDataSet(pitchEntries, getString(R.string.pitch_chart_description));
        lineDataSet.setDrawCircles(false);
        LineData lineData = new LineData(lineDataSet);
        mPitchLineChart.setData(lineData);
    }

}
