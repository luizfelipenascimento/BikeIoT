package android.scopus.com.bikeiot.model.filerelated;

import android.content.Context;
import android.scopus.com.bikeiot.utils.Helper;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 */

public class CSVPartTimerFactory {

    private final static String TAG = CSVPartTimerFactory.class.getSimpleName();
    private final long CREATION_INTERVAL;

    public interface OnCreateFilePartListener {
        void onCreatedFile(File file);
    }

    private final String[] mHeader;
    private ArrayList<String[]> contentQueue;
    private CSVFile mCSVFile;
    private Context mContext;

    private long startTimer;
    private boolean restart = true;

    private Timer mTimer;

    private TimerTask mTimerTask;

    private String mBaseName;

    private OnCreateFilePartListener mListener;

    public CSVPartTimerFactory(Context context, String baseName, String[] header,
                               OnCreateFilePartListener listener, long creationInterval) {
        mContext = context;
        mBaseName = baseName;
        mListener = listener;
        mHeader = header;
        CREATION_INTERVAL = creationInterval;
        contentQueue = new ArrayList<>();
    }

    public void appendInfo(String[] values) {
        Log.d(TAG, "appendInfo called");
        contentQueue.add(values);

        if (restart) {
            createFile();
            startTimer = System.currentTimeMillis();
            restart = false;
        }

        if (System.currentTimeMillis() - startTimer >= CREATION_INTERVAL) {
            restart = true;
            mCSVFile.close();
            mListener.onCreatedFile(mCSVFile.getFile());
        } else {
            writeToFile();
        }


        /*if (mCSVFile == null) {
            createFile();
            setupTimer();
            Log.d(TAG, "csv was null or closed - " + mCSVFile.getFile().getName());
        }*/

    }

    private void writeToFile() {
        Log.d(TAG, "writeToFile called");
        if (contentQueue.size() > 1) {
            Log.d(TAG, "writeLine more than one content on buffer");
            writeRemainingContentToFile(contentQueue);
        } else {
            Log.d(TAG, "writeLine normal buffer");
            try {
                Log.d(TAG, "writing on : " + mCSVFile.getFileName());
                mCSVFile.writeLine(contentQueue.get(0));
                contentQueue.remove(0);
                Log.d(TAG, "content is empty: " + contentQueue.isEmpty());
            } catch (Exception e) {
                Log.e(TAG, "error while writing to file " + e);
            }

        }
    }

    private void writeRemainingContentToFile(ArrayList<String[]> contentQueue) {
        Log.d(TAG, "writeRemainingContentToFile called");

        Iterator<String[]> infoIterator = contentQueue.iterator();

        while (infoIterator.hasNext()) {
            mCSVFile.writeLine(infoIterator.next());
            infoIterator.remove();
        }
    }

    private void stopTimer() {
        if (mTimer != null) {
            Log.d(TAG, "Timer cancel");
            mTimer.cancel();
        }
    }

    private void setupTimer() {
        Log.d(TAG, "setupTimer called");
        mTimer = new Timer();
        mTimerTask = new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "mTimerTask run called" + mCSVFile.getFile().getName());
                mCSVFile.close();
                File filePartCreated = mCSVFile.getFile();
                mListener.onCreatedFile(filePartCreated);
                mCSVFile = null;
                stopTimer();
            }
        };
        mTimer.schedule(mTimerTask, CREATION_INTERVAL);
    }

    private void createFile() {
        Log.d(TAG, "createFile called");
        String filename = mBaseName + Helper.formatDateTimeSimpleSeparator(new Date());
        mCSVFile = new CSVFile(filename, mContext, FileContractConstants.CSV_PART_FOLDER, mHeader);
    }
}
