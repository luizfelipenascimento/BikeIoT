package android.scopus.com.bikeiot.exercisesummary;

import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.scopus.com.bikeiot.utils.Helper;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.common.ConfirmationDialog;
import android.scopus.com.bikeiot.exercisedetails.ExerciseDetailsActivity;
import android.scopus.com.bikeiot.model.filerelated.FileRepository;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.data.local.BikeExerciseDAO;
import android.scopus.com.bikeiot.model.data.local.RouteDAO;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import static android.scopus.com.bikeiot.utils.CalculateUtils.metersPerSecToKmPerH;
import static android.scopus.com.bikeiot.utils.CalculateUtils.metersToKm;
import static android.scopus.com.bikeiot.utils.Helper.formatKmDecimal;

public class ExerciseSummaryActivity extends AppCompatActivity implements ExerciseSummaryContract.View {

    public static final String EXTRA_BIKE_EXERCISE_ID =
            "android.scopus.com.bikeiot.EXTRA_BIKE_EXERCISE_ID";
    private static final String TAG = ExerciseSummaryActivity.class.getSimpleName();

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private NavigationView mNavigationView;
    private Toast mToast;

    private LinearLayout mResultInfoContainerLinearLayout;
    private TextView mMeanVelocityTextView;
    private TextView mDistanceTextView;
    private TextView mMaxVelocityTextView;
    private TextView mCaloriesBurnedTextView;
    private TextView mMeanHeartRateTextView;
    private ImageView mExerciseRouteImageView;
    private ExerciseSummaryPresenter mPresenter;
    private TextView mTimeDurationTextView;
    private TextView mGeneralErrorTextView;
    private EditText mDescriptionEditText;
    private TextView mNoRouteImageTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_summary);

        //setting up toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.summary_exercise_activity_title);

        //setting up navigation
        mNavigationView = (NavigationView) findViewById(R.id.main_navigation_view);
        setupDrawerNavigationContent(mNavigationView);

        mResultInfoContainerLinearLayout = (LinearLayout) findViewById(R.id.results_info_table_container);

        mDistanceTextView = (TextView) findViewById(R.id.exercise_resume_route_distance_text_view);
        mMeanVelocityTextView = (TextView) findViewById(R.id.exercise_resume_mean_velocity_text_view);
        mMaxVelocityTextView = (TextView) findViewById(R.id.exercise_resume_max_velocity_text_view);
        mCaloriesBurnedTextView = (TextView) findViewById(R.id.exercise_resume_calories_burned_text_view);
        mMeanHeartRateTextView = (TextView) findViewById(R.id.exercise_resume_mean_heart_rate_text_view);
        mTimeDurationTextView = (TextView) findViewById(R.id.exercise_resume_duration_time_text_view);
        mGeneralErrorTextView = (TextView) findViewById(R.id.exercise_resume_general_error_text_view);

        mDescriptionEditText = (EditText) findViewById(R.id.exercise_resume_description_edit_text);
        mDescriptionEditText.clearFocus();
        mExerciseRouteImageView = (ImageView) findViewById(R.id.exercise_route_image_view);
        mNoRouteImageTextView = (TextView) findViewById(R.id.no_route_image_error);

        int bikeExerciseId = getIntent().getIntExtra(EXTRA_BIKE_EXERCISE_ID, 0);

        mPresenter = new ExerciseSummaryPresenter(bikeExerciseId, new BikeExerciseDAO(this),
                new RouteDAO(this), this, new FileRepository(this));

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.initialize();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mActionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.delete_exercise_menu_item:
                showRemoveConfirmationDialog();
                return true;
            case R.id.confirm_exercise_menu_item:
                Log.d(TAG, "updating description:");
                String description = mDescriptionEditText.getText().toString();
                mPresenter.updateBikeExerciseAndRouteDescription(description);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showRemoveConfirmationDialog() {
        FragmentManager fm = getFragmentManager();

        String content = getString(R.string.exercise_remove_confirmation_msg);
        String title = getString(R.string.dialog_remove_title);
        ConfirmationDialog confirmationDialog = ConfirmationDialog
                .newInstance(content, title, new ConfirmationDialog.ConfirmationDialogListener() {
            @Override
            public void onConfirmationDialogPositiveChoose() {
                mPresenter.removeBikeExercise();
            }

            @Override
            public void onConfirmationDialogNegativeChoose() {
                //do nothing
            }
        });

        confirmationDialog.show(fm, ConfirmationDialog.DIALOG_TAG);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.exercise_summary_menu, menu);
        return true;
    }

    private void setupDrawerNavigationContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Helper.Menu.selectDrawerItem(ExerciseSummaryActivity.this, item, -1);
                mDrawerLayout.closeDrawers();
                return true;
            }
        });
    }

    @Override
    public void displayExerciseResume(BikeExercise bikeExercise) {
        Log.d(TAG, "displayExerciseResume called");

        String kmIndicator = getResources().getString(R.string.km_indicator);
        String kmHIndicator = getResources().getString(R.string.km_h_indicator);

        double distanceKm = metersToKm(bikeExercise.getRoute().getDistance());
        double meanVelocityKm_h = metersPerSecToKmPerH(bikeExercise.getResult().getMeanVelocityMetersPerSec());
        double maxVelocityKm_h = metersPerSecToKmPerH(bikeExercise.getResult().getMaxVelocityMetersPerSec());
        double heartRate = bikeExercise.getResult().getMeanHeartRate();
        double caloriesBurned = bikeExercise.getResult().getCaloriesBurned();
        String timeDuration = Helper.formatDateToTimeString(bikeExercise.getResult().getTotalTimeChronometer());

        mDistanceTextView.setText(formatKmDecimal(distanceKm )+ " " + kmIndicator);
        mMeanVelocityTextView.setText(formatKmDecimal(meanVelocityKm_h) + " " + kmHIndicator);
        mMaxVelocityTextView.setText(formatKmDecimal(maxVelocityKm_h) + " " + kmHIndicator);
        mTimeDurationTextView.setText(timeDuration);

        if (caloriesBurned != 0) {
            mCaloriesBurnedTextView.setText(String.valueOf(caloriesBurned));
        } else mCaloriesBurnedTextView.setText(R.string.information_not_available);

        if (heartRate != 0) {
            mMeanHeartRateTextView.setText(String.valueOf(heartRate));
        } else mMeanHeartRateTextView.setText(R.string.information_not_available);

    }

    @Override
    public void showBikeExerciseDetailsUi(int bikeExerciseId) {
        Intent intent = new Intent(this, ExerciseDetailsActivity.class);
        intent.putExtra(ExerciseDetailsActivity.EXTRA_BIKE_EXERCISE_ID, bikeExerciseId);
        startActivity(intent);
        finish();
    }


    @Override
    public void showNoBikeExerciseError() {
        //TODO: definir como será a mensagem de erro
    }

    @Override
    public void showGeneralErrorMsg(int msgId) {
        mGeneralErrorTextView.setText(msgId);
    }

    @Override
    public void showRouteImage(Uri uri) {
        Log.d(TAG, "showRouteImage called");
        Log.d(TAG, "imagePath : " + uri.toString());
        Glide.with(this)
                .load(uri)
                .into(mExerciseRouteImageView);

    }

    @Override
    public void showNoRouteImage() {
        mNoRouteImageTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showRemoveSuccess() {
        Toast.makeText(this, R.string.remove_success, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void endActivity() {
        finish();
    }

    private void clearDescriptionMessageError() {
        mGeneralErrorTextView.setText("");
    }
}
