package android.scopus.com.bikeiot.model.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.scopus.com.bikeiot.model.bluetooth.BluetoothProtocol;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

/**
 *
 */

public class BluetoothConnectionService {
    //TODO: criar uma flag indicando se a conexao foi perdida.

    private  static final String TAG = "BluetoothConnectServ";
    private  static final String appName = "MYAPP";

    private static final String RECONNECT_MOTIVATION_TIME_OUT =  "TIME_EXCEEDED";
    private static final String RECONNECT_MOTIVATION_LOST_CONNECTION = "LOST_CONNECTION";

    //UUID e basicamente um endereco que um aparelho utiliza para conectar com outro;
    private static final UUID MY_UUID_INSECURE =
            UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    public static final String BT_SERVICE_CONNECTION_LOST_FEEDBACK = "BT_SERVICE_CONNECTION_LOST_FEEDBACK";
    public static final String EXTRA_USED_DEVICE_ADDRESS = "EXTRA_USED_DEVICE_ADDRESS";

    private int startId;

    private final BluetoothAdapter mBluetoothAdapter;
    Context mContext;

    private ConnectThread mConnectThread;
    private BluetoothDevice mDevice;
    private ConnectedThread mConnectedThread;
    private boolean running = false;
    private int mTimeOutProblemCount = 0;
    private final static int MAX_ALLOWED_TIME_OUT_PROBLEMS = 5;
    private boolean firstConnection;
    private boolean mIsPaused = false;

    private ConnectionListener mListenerCallback;


    //TODO: implementar o callback para conexao com o bluetooth
    public interface ConnectionListener {
        void onConnectionSucceed(BluetoothDevice bluetoothDevice);
        void onConnectionFailed(BluetoothDevice bluetoothDevice);
    }

    public BluetoothConnectionService(Context context, int startId, ConnectionListener listenerCallback) {
        this.startId = startId;
        mContext = context;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        firstConnection = true;
        mListenerCallback = listenerCallback;
    }

    public void stopConnection() {
        Log.d(TAG, "stopping connection with : " + mDevice.getName() + " " + mDevice.getAddress());
        running = false;
        if(mConnectedThread != null) {
            //mConnectedThread.interrupt();
            mConnectedThread.cancel();
            mConnectedThread  = null;
        }

        if(mConnectThread != null) {
            //mConnectThread.interrupt();
            mConnectThread.cancel();
            mConnectThread = null;
        }
    }

    public BluetoothConnectionService(Context context) {
        mContext = context;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    public int getStartId() {
        return startId;
    }

    /**
     *  Essa thread fica rodando enquanto tenta estabelecer uma conexao de saida
     *  com um dispositivo
     */
    private class ConnectThread extends Thread {
        private BluetoothSocket mmSocket;

        public ConnectThread(BluetoothDevice device) {
            Log.d(TAG, "ConnectThread: started");
            mDevice = device;
        }

        public void run(){

            mmSocket = wrapperSocketConnection(mDevice);

            if (mmSocket != null) {
                connected(mmSocket, mDevice);
                mListenerCallback.onConnectionSucceed(mDevice);
            } else {
                mListenerCallback.onConnectionFailed(mDevice);
            }
        }

        public BluetoothSocket wrapperSocketConnection(BluetoothDevice device) {
            BluetoothSocket socket = null;
            Log.i(TAG, "mConnectThread: run");

            //retorna um bluetoothSocket para uma conexao com um dado dispositivo
            //ira utilizar o socket temporario (tmp) e criar um RF socket
            try {
                Log.d(TAG, "ConnectThread: tentando criar InsecureRfcommSocketToService com UUID: "
                        + MY_UUID_INSECURE);
                socket = device.createRfcommSocketToServiceRecord(MY_UUID_INSECURE);
            } catch (IOException e) {
                Log.e(TAG, "ConnectThread: nao foi possivel criar InsecureRfcommSocketToService " + e.getMessage());
            }

            //sempre apos criar o socket cancelar o discovery mode porque consome muitos recursos
            // e deixa a conexao lenta
            mBluetoothAdapter.cancelDiscovery();

            try {

                socket.connect();

                Log.d(TAG, "run: ConnectThread connected");

                return socket;

            } catch (IOException e) {
                try {
                    socket.close();
                    Log.e(TAG, "run: Closed socket");

                } catch (IOException e1) {

                    Log.e(TAG, "run: mConnectThread: nao foi possivel fechar a conexao do socket "
                            + e1.getMessage());

                }
                Log.d(TAG, "run: ConnectThread: Nao foi possivel connectar ao UUID: " + MY_UUID_INSECURE + " e: " + e);
            }

            Log.d(TAG, "run: ConnectThread: conexao estabelecida com UUID: " + MY_UUID_INSECURE);
            return null;
        }

        public void cancel() {
            try {
                Log.d(TAG, "cancel: Fechando o socket do lado do cliente");
                if (mmSocket!= null) {
                    mmSocket.close();
                }
            } catch (IOException e) {
                Log.e(TAG, "cancel: close() do mmSocket no ConnectThread falhou: " + e.getMessage());
            }
        }
    }

    /**
     * Basicamente inica o ConnectThread (client side)
     * Tenta criar uma conexao com um dispositivo
     *
     * Then ConnectThread starts and attempts to make a connection with other devices AcceptThread
     * @param device
     */
    public void startClient(BluetoothDevice device) {
        Log.d(TAG, "startClient: started");
        running = true;
        mConnectThread = new ConnectThread(device);
        mConnectThread.start();
    }

    private void resetTimeOutCount() {
        Log.d(TAG, "resetTimeOutCount called");
        Log.d(TAG, "valor antes de resetar mTimeOutProblemCount: " + mTimeOutProblemCount);

        mTimeOutProblemCount = 0;
    }

    //TODO: definir um contador de tentativas de conexao geral.
    private void reconnect(String motivation) {
        Log.d(TAG, "reconnect called");
        if (running) {
            Log.d(TAG, "reconnect called bluetooth is running");
            if (mConnectedThread != null) {
                mConnectedThread.interrupt();
            }
            mConnectedThread = null;

            switch (motivation) {

                case RECONNECT_MOTIVATION_TIME_OUT:
                    Log.d(TAG, "reconnect RECONNECT_MOTIVATION_TIME_OUT called");

                    if (mTimeOutProblemCount < MAX_ALLOWED_TIME_OUT_PROBLEMS) {

                        mTimeOutProblemCount++;
                        Log.d(TAG, "reconnect mTimeOutProblemCount after call:" + mTimeOutProblemCount);

                        tryToReconnect();
                    } else {
                        //TODO: () enviar mensagem para activity que a conexao foi cancelada parou
                        Log.d(TAG, "reconnect muitos erros com TIME_OUT a conexao foi cancelada!");

                        sendWarningConnectionLost();
                        //this.stopConnection();
                    }

                    break;

                case RECONNECT_MOTIVATION_LOST_CONNECTION:
                    Log.d(TAG, "reconnect RECONNECT_MOTIVATION_LOST_CONNECTION called");

                    tryToReconnect();

                    break;
            }
        }
    }

    private void tryToReconnect() {
        Log.d(TAG, "tryToReconnect called");

        new Thread(new Runnable() {

            private int connectionTries = 0;
            private final int MAX_ALLOWED_TRIES = 5;
            private final long CONNECTIONS_TRIES_INTERVAL = 1000;

            @Override
            public void run() {
                BluetoothSocket socket = null;
                while (socket == null && connectionTries < MAX_ALLOWED_TRIES) {
                    Log.d(TAG, "tryToReconnect tentativa de conexao numero : " + connectionTries);
                    try {
                        try {
                            socket = mConnectThread.wrapperSocketConnection(mDevice);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        connectionTries++;

                        if (socket != null) {
                            Log.d(TAG, "tryToReconnect conexao foi estabelecida");
                            connected(socket, mDevice);
                            break;
                        }

                        Thread.sleep(CONNECTIONS_TRIES_INTERVAL);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }

                if (socket == null) {
                    Log.d(TAG, "tryToReconnect conexao nao estabelecida apos " + connectionTries + " tentativas");
                    stopConnection();
                    sendWarningConnectionLost();
                }
            }

        }).run();
    }

    private void sendWarningConnectionLost() {
        Intent intent = new Intent(BT_SERVICE_CONNECTION_LOST_FEEDBACK);
        intent.putExtra(EXTRA_USED_DEVICE_ADDRESS, mDevice.getAddress());
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }

    //TODO: desenvolver uma meneira de conseguir informações na thread com protocolo e sem protocolo
    private interface ConnectedThreadInterface {
        void manageConnection();
        void write(byte[] bytes);
        void cancel();
    }

    private class ConnectedThread extends Thread implements ConnectedThreadInterface {

        private final BluetoothSocket mmSocket;
        private final OutputStream mmOutputStream;
        private final InputStream mmInputStream;
        private BluetoothProtocol mmBtProtocol;
        private Thread mmReadInputThread;
        private static final long TIMEOUT = 2500;
        private boolean timeExceeded = false;
        private boolean lostConnection = false;

        public ConnectedThread(BluetoothSocket socket) {
            Log.d(TAG, "ConnectedThread: Starting");

            mmBtProtocol = new BluetoothProtocol("DATA:", "}");

            mmSocket = socket;
            InputStream  tmpIn = null;
            OutputStream tmpOut = null;

            try {
                tmpIn = mmSocket.getInputStream();
                tmpOut = mmSocket.getOutputStream();

            } catch (IOException e) {
                e.printStackTrace();
            }

            mmInputStream = tmpIn;
            mmOutputStream = tmpOut;

        }

        private Thread createReadInputThread() {
            Thread readInputThread = new Thread(new Runnable() {

                @Override
                public void run() {
                    Log.d(TAG, "readInputThread run:");
                    byte[] buffer = new byte[1024];
                    int bytes;
                    mmBtProtocol.reset();
                    try {
                        while (!mmBtProtocol.isMessageCompleted() && mmBtProtocol.isReadyToReceive()) {

                            Log.d(TAG, "readInputThread: while message not completed");

                            bytes = mmInputStream.read(buffer);
                            Log.d(TAG, "readInputThread bytes value comming: " + bytes);

                            String incomingMessage = new String(buffer, 0, bytes);

                            Log.d(TAG, "readInputThread: run: InputStream " + incomingMessage);

                            //ocorreu algum erro com a primeira requisicao
                            if (incomingMessage.equals("R")) {
                                makeRequest();
                            }

                            mmBtProtocol.handleInputMessage(incomingMessage);
                        }

                    } catch (IOException e) {
                        Log.e(TAG, "readInputThread run: erro ao ler o input : " + e.getMessage());
                        if (running) {
                            Log.e(TAG, "readInputThread run: conexao perdida");
                            lostConnection = true;
                        }
                    }
                }

            });

            return readInputThread;

        }


        @Override
        public void run() {
            write("FIRSTRESET".getBytes());
            while (mmSocket.isConnected()) {

                if (!mIsPaused) {

                    makeRequest();

                    mmReadInputThread = createReadInputThread();

                    synchronized (mmReadInputThread) {
                        Log.d(TAG, "ConnectedThread synchronized (mmReadInputThread)");

                        Log.d(TAG, "ConnectedThread starting mmReadInputThread");
                        mmReadInputThread.start();

                        try {
                            Log.d(TAG, "ConnectedThread synchronized mmReadInputThread wait");
                            mmReadInputThread.wait(TIMEOUT);

                            Log.d(TAG, "ConnectedThread synchronized mmReadInputThread " + TIMEOUT + " millis waited");

                            if (mmReadInputThread.isAlive()) {

                                timeExceeded = true;
                                Log.d(TAG, "ConnectedThread synchronized mmReadInputThread is alive");
                                mmInputStream.close();

                            } else {
                                Log.d(TAG, "ConnectedThread synchronized mmReadInputThread is not alive");
                                resetTimeOutCount();
                            }

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    try {

                        mmReadInputThread.join();

                        if (mmBtProtocol.isMessageCompleted()) {
                            Log.d(TAG, "ConnectedThread run: mmBtProtocol message is complete");
                            sendInputInfoToService();
                        } else {
                            Log.d(TAG, "ConnectedThread run: mmBtProtocol message is not complete");
                        }

                        if (!timeExceeded && lostConnection) {
                            Log.d(TAG, "ConnectedThread run: conexao foi perdida, tentando reconectar");
                            reconnect(RECONNECT_MOTIVATION_LOST_CONNECTION);
                        }

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }

            if (timeExceeded) {
                Log.d(TAG, "ConnectedThread run: time has exceeded");
                reconnect(RECONNECT_MOTIVATION_TIME_OUT);
            }
        }

        private void makeRequest() {
            write("SENDIT".getBytes());
        }

        private void sendInputInfoToService() {
            String completeMessage = mmBtProtocol.getCompleteData();
            Intent incomingMessageIntent = new Intent("inputMessage");
            incomingMessageIntent.putExtra("data", completeMessage);
            incomingMessageIntent.putExtra("device", mDevice);
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(incomingMessageIntent);
        }

        @Override
        public void manageConnection() {

        }

        @Override
        public void write(byte[] bytes) {
            try {
                Log.d(TAG, "ConnectedThread: write: escrevendo");
                mmBtProtocol.setReadyToReceive(true);
                mmOutputStream.write(bytes);
            } catch (IOException e) {
                Log.d(TAG, "ConnectedThread: write: erro ao escrever a informacao: " + e);
                e.printStackTrace();
            }
        }

        @Override
        public void cancel() {
            try {
                mmInputStream.close();
                mmOutputStream.close();
                mmSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public BluetoothDevice getConnectedDevice() {
        return mDevice;
    }

    public void write(String message) {
        Log.d(TAG, "write: informacao que sera enviada " + message);
        mConnectedThread.write(message.getBytes());
    }

    public void pauseCommunication() {
        Log.d(TAG, "pauseCommunication called");
        mIsPaused = true;
    }

    public void continueCommunication() {
        Log.d(TAG, "continueCommunication called");
        mIsPaused = false;
    }

    public void connected(BluetoothSocket socket, String firstMessage) {
        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.write(firstMessage.getBytes());
        mConnectedThread.start();
    }

    private void connected(BluetoothSocket socket, BluetoothDevice device) {
        Log.d(TAG, "connected: started");
        mConnectedThread = new ConnectedThread(socket);
        mConnectedThread.start();
    }
}
