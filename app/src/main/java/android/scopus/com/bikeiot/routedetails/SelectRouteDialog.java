package android.scopus.com.bikeiot.routedetails;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.scopus.com.bikeiot.R;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Luiz Felipe on 09/04/2018.
 */

public class SelectRouteDialog extends DialogFragment {


    private static final String DISTANCE_EXTRA = "distance";

    private TextView mDescription;

    public static SelectRouteDialog newInstance(String distance) {
        SelectRouteDialog dialog = new SelectRouteDialog();

        Bundle args = new Bundle();
        args.putString(DISTANCE_EXTRA, distance);
        dialog.setArguments(args);
        return dialog;
    }

    private String mDistance;

    public interface SelectRouteDialogListener {
        void onSelectRouteDialogPositiveChoose();
        void onSelectRouteDialogNegativeChoose();
    }

    private SelectRouteDialogListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String distanceFormatted = getArguments().getString(DISTANCE_EXTRA);

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialogView  = inflater.inflate(R.layout.dialog_select_route_confirmation, null);
        mDescription = (TextView) dialogView.findViewById(R.id.route_confirmation_description);

        String message = getResources().getString(R.string.dialog_description_distance_to_route);
        message += " " + distanceFormatted + ". " + getResources().getString(R.string.confirmation_continue_question);

        mDescription.setText(message);

        alertBuilder.setTitle(R.string.select_route_confirmation);

        alertBuilder.setView(dialogView)
                .setPositiveButton(R.string.dialog_continue, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onSelectRouteDialogPositiveChoose();
                    }
                })
                .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onSelectRouteDialogNegativeChoose();
                    }
                });

        return alertBuilder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (SelectRouteDialogListener) activity;
        } catch (ClassCastException e) {
          throw new ClassCastException("class is not implementing SelectRouteDialogListener" + e);
        }
    }
}
