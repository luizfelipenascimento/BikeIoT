package android.scopus.com.bikeiot.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.bikebluetoothdevice.BikeDevicesActivity;
import android.scopus.com.bikeiot.exerciserecoder.ExerciseRecorderActivity;
import android.scopus.com.bikeiot.exercises.ExercisesActivity;
import android.scopus.com.bikeiot.routes.RoutesActivity;
import android.scopus.com.bikeiot.usercontacts.UserContactsActivity;
import android.view.MenuItem;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Luiz Felipe on 08/01/2018.
 */

public class Helper {

    private static final String TAG = Helper.class.getSimpleName();

    public static Date parseToDate(String date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String formatDateToMyDateFormat(Date date) {
        return new SimpleDateFormat("dd/MM/yyyy").format(date);
    }

    public static String formatToDateTimeString(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS").format(date);
    }

    public static String formatDateTimeSimpleSeparator(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss-SSS").format(date);
    }

    public static String formatToDateString(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public static String formatDateToTimeString(Date date) {
        return new SimpleDateFormat("HH:mm:ss").format(date);
    }

    public static String formatKmDecimal(double km) {
        return new DecimalFormat("0.0").format(km);
    }

    public static String formatDistanceToKmOrMetersWithIndicator(double distanceToRoad, Context context) {
        int indicatorId = R.string.meter_indicator;

        if (distanceToRoad > 1000) {
            distanceToRoad = CalculateUtils.metersToKm(distanceToRoad);
            indicatorId = R.string.km_indicator;
        }

        String indicator = context.getResources().getString(indicatorId);

        //TODO: refatorar o nome formatKmDecimal para formatDistanceDecimal
        String distanceToRoadString = Helper.formatKmDecimal(distanceToRoad) + " " + indicator;

        return distanceToRoadString;
    }

    public static class Menu {
        private Menu() {}

        private static final int[] NO_CLOSE_MENU_ITEMS = {R.id.exercise_recorder_menu_item};

        public static void selectDrawerItem(Context context, MenuItem menuItem, int currentItemMenuId) {
            int itemId = menuItem.getItemId();
            switch (itemId) {
                case R.id.exercise_recorder_menu_item:
                    if(itemId != currentItemMenuId) {
                        Intent intent = new Intent(context, ExerciseRecorderActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        context.startActivity(intent);
                        closeActivity(context, currentItemMenuId);
                    }
                    break;
                case R.id.bike_selection_menu_item:
                    if(itemId != currentItemMenuId) {
                        Intent intent = new Intent(context, BikeDevicesActivity.class);
                        context.startActivity(intent);
                        closeActivity(context, currentItemMenuId);
                    }
                    break;
                case R.id.exercises_list_menu_item:
                    if (itemId != currentItemMenuId) {
                        Intent intent = new Intent(context, ExercisesActivity.class);
                        context.startActivity(intent);
                        closeActivity(context, currentItemMenuId);
                    }
                    break;
                case R.id.routes_list_menu_item:
                    if (itemId != currentItemMenuId) {
                        Intent intent = new Intent(context, RoutesActivity.class);
                        context.startActivity(intent);
                        closeActivity(context, currentItemMenuId);
                    }
                    break;
                case R.id.contacts_list_menu_item:
                    if (itemId != currentItemMenuId) {
                        Intent intent = new Intent(context, UserContactsActivity.class);
                        context.startActivity(intent);
                        closeActivity(context, currentItemMenuId);
                    }
                    break;
                /*case R.id.test_call_help_menu_item:
                    if (itemId != currentItemMenuId) {
                        Intent intent = new Intent(context, CallHelpTestActivity.class);
                        context.startActivity(intent);
                    }
                    break;
                case R.id.test_bikeIot_menu_item:
                    if(itemId != currentItemMenuId) {
                        Intent intent = new Intent(context, MainActivity.class);
                        context.startActivity(intent);
                    }
                    break;*/
                default:
                    break;
            }
        }

        private static void closeActivity(Context context, int menuId) {
            if (!isOnExceptionItems(menuId)) {
                ((Activity) context).finish();
            }
        }

        private static boolean isOnExceptionItems(int menuId) {
            for (int menuItemId : NO_CLOSE_MENU_ITEMS) {
                if (menuItemId == menuId) return true;
            }
            return false;
        }

    }
}
