package android.scopus.com.bikeiot.model.network.googledirectionAPI;

import android.location.Location;

/**
 * Created by Luiz Felipe on 18/04/2018.
 */

public class GoogleDirectionsQueryUtils {

    public static final String WALKING_MODE = "walking";
    public static final String DRIVING_MODE = "driving";
    public static final String BIKE_MODE = "bicycling";
    public static final String TRANSIT_MODE = "transit";

    //exemplo de request
    //https://maps.googleapis.com/maps/api/directions/json?origin=Brooklyn&destination=Queens&mode=transit&key=YOUR_API_KEY
    private final static String BASE_URL = "https://maps.googleapis.com/maps/api/directions/json?";

    public static String buildUrlRequest(Location locationA, Location locationB, String key, String mode) {
        String buildUrlRequest = BASE_URL;
        buildUrlRequest += "origin=" + locationA.getLatitude() + "," + locationA.getLongitude();
        buildUrlRequest += "&destination=" + locationB.getLatitude() + "," + locationB.getLongitude();
        buildUrlRequest += "&mode=" + mode;
        buildUrlRequest += "&key=" + key;
        return buildUrlRequest;
    }




}
