package android.scopus.com.bikeiot.model.data;

import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.beans.Route;

import java.util.List;

/**
 * Created by Luiz Felipe on 22/02/2018.
 */

public interface RouteDataSource {

    List<Route> getRoutes();
    boolean save(Route route);
    Route getRoute(int id);
    boolean update(Route route);
    boolean renameRouteFilePath(Route route);
    boolean updateRenameImagePath(String newImagePath, int routeId);
    boolean remove(int routeId);
    boolean updateShare(int id ,int share);
}
