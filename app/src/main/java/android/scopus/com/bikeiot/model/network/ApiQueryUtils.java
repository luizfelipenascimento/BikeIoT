package android.scopus.com.bikeiot.model.network;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 */

public class ApiQueryUtils {

    private static final String TAG = ApiQueryUtils.class.getSimpleName();

    public URL createURL(String urlString) {
        URL url = null;
        try {
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            Log.e(TAG, "createURL error : " + e);
            e.printStackTrace();
        }
        return url;
    }

    private String makeRequest(URL url) throws IOException {
        String jsonResponse = "";

        if (url == null) {
            Log.d(TAG, "makeRequest url is null");
            return jsonResponse;
        }

        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;

        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setConnectTimeout(1000);
            httpURLConnection.setReadTimeout(1500);
            httpURLConnection.connect();

            if (httpURLConnection.getResponseCode() == 200) {
                inputStream = httpURLConnection.getInputStream();
                jsonResponse = readInputStream(inputStream);
                Log.d(TAG, "makeRequest jsonResponse: " + jsonResponse);
            }

        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "makeRequest error: " + e);
        }

        return jsonResponse;
    }

    private String readInputStream(InputStream inputStream) throws IOException {
        StringBuilder jsonResponse = new StringBuilder();
        if (inputStream != null) {
            Log.d(TAG, "readInputStream inputStream is not null");
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                jsonResponse.append(line);
                line = reader.readLine();
            }
        }
        return jsonResponse.toString();
    }

    public String getJsonResponse(String builtUrl) {
        String jsonResponse = "";
        URL url = createURL(builtUrl);
        try {
            jsonResponse = makeRequest(url);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonResponse;
    }

}
