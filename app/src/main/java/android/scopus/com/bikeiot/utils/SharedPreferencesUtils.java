package android.scopus.com.bikeiot.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 *
 */

public final class SharedPreferencesUtils {

    //TODO: mudar SharedPreferencesUtils para SharedPreferencesHelper com um construtor que recebe Context

    public final static String LOGGED_USER_ID_SHARED_PREFERENCES_KEY = "shared_user_id";
    public final static String SHARED_PREFERENCES_DEFAULT_FILE_KEY = "my_shared_preferences_key";

    private static final String TAG = SharedPreferencesUtils.class.getSimpleName();


    public SharedPreferencesUtils() {}

    public static int getIntSharedPreferences(Context context, String fileSource, String key, int defaultValue) {
        int id = defaultValue;
        SharedPreferences preferences = context.getSharedPreferences(fileSource, Context.MODE_PRIVATE);
        if (preferences != null) {
            id = preferences.getInt(key, defaultValue);
        }
        return id;
    }

    public static String getStringSharedPreferences(Context context, String fileSource, String key, String defaultValue) {
        String content = defaultValue;
        SharedPreferences preferences = context.getSharedPreferences(fileSource, Context.MODE_PRIVATE);
        if (preferences != null) {
            content = preferences.getString(key, defaultValue);
        }
        return content;
    }

    public static int getIntSharedPreferences(SharedPreferences preferences, String key, int defaultValue) {
        int id = defaultValue;
        if (preferences != null) {
            id = preferences.getInt(key, defaultValue);
        }
        return id;
    }

    public static boolean saveIntToSharedPreferences(Context context, String fileSource,
                                                     String key, int value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(fileSource, Context.MODE_PRIVATE);;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        return editor.commit();
    }

    public static boolean saveIntToSharedPreferences(SharedPreferences sharedPreferences, String key,
                                                     int value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        return editor.commit();
    }

    public static void clearSharedPreferences(SharedPreferences sharedPreferences) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear().commit();
    }

    public static void clearSharedPreferences(Context context, String fileSource) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(fileSource, Context.MODE_PRIVATE);;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear().commit();
    }
}
