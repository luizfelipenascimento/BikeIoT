package android.scopus.com.bikeiot.bikebluetoothdevice;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.scopus.com.bikeiot.R;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Luiz Felipe on 31/10/2017.
 */

public class ListDeviceAdapter extends ArrayAdapter<BluetoothDevice> {

    private LayoutInflater mInflater;
    private TextView macAddressTextView;
    private TextView nameTextView;


    private ArrayList<BluetoothDevice> mDevices;

    public ListDeviceAdapter(@NonNull Context context, int resourceId, ArrayList<BluetoothDevice> devices) {
        super(context, resourceId, devices);
        mDevices = devices;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        BluetoothDevice device = mDevices.get(position);
        mInflater = LayoutInflater.from(getContext());
        View customView = mInflater.inflate(R.layout.device_custom_row, parent,false);

        macAddressTextView = (TextView) customView.findViewById(R.id.device_mac_address);
        nameTextView = (TextView) customView.findViewById(R.id.device_name);

        macAddressTextView.setText(device.getAddress());
        nameTextView.setText(device.getName());

        return customView;
    }
}
