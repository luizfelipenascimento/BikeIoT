package android.scopus.com.bikeiot.hospitals;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.scopus.com.bikeiot.utils.PermissionHelper;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.exerciserecoder.ExerciseRecorderActivity;
import android.scopus.com.bikeiot.model.location.LocationUpdater;
import android.scopus.com.bikeiot.model.beans.Place;
import android.scopus.com.bikeiot.model.network.NetworkHelper;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

public class HospitalsActivity extends AppCompatActivity implements HospitalsContract.View,
        OnMapReadyCallback, HospitalsLocationsListAdapter.HospitalClickListener {

    private static final String TAG = HospitalsActivity.class.getSimpleName();
    private Toolbar mToolbar;
    private GoogleMap mGoogleMap;

    private static final int MY_LOCATION_REQUEST_CODE = 1010;
    private HospitalsPresenter mPresenter;
    private RecyclerView mHospitalsRecyclerView;
    private ImageView mWarningStatusImageView;
    private TextView mWarningStatusTextView;
    private LinearLayout mWarningStatusLinearLayout;
    private LinearLayout mGeneralStatusProgressLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getTheme().applyStyle(R.style.helpTheme, true);
        setContentView(R.layout.activity_help_to_hospital);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setBackgroundColor(getColor(R.color.helpModePrimary));
        setSupportActionBar(mToolbar);

        /*SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);*/

        setTitle(getString(R.string.help_to_hospital_activity_atitle));

        FusedLocationProviderClient fusedClient = LocationServices.getFusedLocationProviderClient(this);
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);

        mWarningStatusLinearLayout = (LinearLayout) findViewById(R.id.warning_status_container_linear_layout);
        mWarningStatusImageView = (ImageView) findViewById(R.id.warning_status_image_view);
        mWarningStatusTextView = (TextView) findViewById(R.id.warning_status_text_view);

        mHospitalsRecyclerView = (RecyclerView) findViewById(R.id.hospitals_recycler_view);
        mHospitalsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mGeneralStatusProgressLinearLayout =
                (LinearLayout) findViewById(R.id.general_status_progress_container_linear_layout);
        hideHospitalSearchProgress();

        String googlePlaceKey = getString(R.string.google_places_api_key);

        LocationUpdater locationUpdater = new LocationUpdater(fusedClient, settingsClient, LocationUpdater
                .locationRequestHighAccuracy(500,1000,1));

        Location location = getIntent().getParcelableExtra(ExerciseRecorderActivity.EXTRA_LAST_KNOWN_LOCATION);

        mPresenter = new HospitalsPresenter(locationUpdater, googlePlaceKey, this,
                new NetworkHelper(this), location);

    }

    @Override
    protected void onResume() {
        super.onResume();

        mWarningStatusLinearLayout.setVisibility(View.GONE);

        if (!PermissionHelper.isPermissionGranted(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            PermissionHelper.requestPermissions(this, MY_LOCATION_REQUEST_CODE,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION});
        } else {
            showHospitalSearchProgress();
            mHospitalsRecyclerView.setAdapter(null);
            mPresenter.initialize();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.refresh_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.refresh_menu_item) {
            mWarningStatusLinearLayout.setVisibility(View.GONE);
            clearHospitalsList();
            showHospitalSearchProgress();
            mPresenter.getMyCurrentLocationAndRequestClosestHospitals();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void clearHospitalsList() {
        mHospitalsRecyclerView.setAdapter(null);
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_LOCATION_REQUEST_CODE:
                if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    PermissionHelper.requestPermissions(this, MY_LOCATION_REQUEST_CODE,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION});
                } else {
                    /*if (!mGoogleMap.isMyLocationEnabled()) {
                        mGoogleMap.setMyLocationEnabled(true);
                    }*/
                }
                break;
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMyLocationEnabled(true);
        mPresenter.getMyCurrentLocationAndRequestClosestHospitals();
    }

    @Override
    public void showNetworkConnectionError() {
        mWarningStatusLinearLayout.setVisibility(View.VISIBLE);
        mWarningStatusImageView.setImageDrawable(getDrawable(R.drawable
                .ic_signal_cellular_connected_no_internet_2_bar_black_24dp));
        mWarningStatusTextView.setText(R.string.no_internet_connection_warning);
    }

    @Override
    public void showNoCloseHospitalWarning() {
        mWarningStatusLinearLayout.setVisibility(View.VISIBLE);
        mWarningStatusImageView.setImageDrawable(getDrawable(R.drawable.ic_local_hospital_red_24dp));
        mWarningStatusTextView.setText(R.string.no_hospital_found_warning);


        //TODO: criar um botao de refresh e pesquisar novamente o hospital mais proximo
    }

    @Override
    public void showMarkerInMap(LatLng locationLatLng, int titleId) {
        Log.d(TAG, "showMarkerInMap called");
        mGoogleMap.addMarker(new MarkerOptions()
                .position(locationLatLng)
                .title(getString(titleId)));
    }

    @Override
    public void showRouteToHospital(PolylineOptions polylineOptions) {

    }

    @Override
    public void showRouteTraceOnCenter(LatLngBounds latLngBounds) {
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 0));
    }

    @Override
    public void showClosestHospitalsPlaces(List<Place> hospitalsLocations) {

        HospitalsLocationsListAdapter adapter = new HospitalsLocationsListAdapter(
                hospitalsLocations, this, this);

        mHospitalsRecyclerView.setAdapter(adapter);
    }

    @Override
    public void showGoogleMapsNavigation(Uri uriGoogleMaps) {
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, uriGoogleMaps);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    @Override
    public void showHospitalSearchProgress() {
        mGeneralStatusProgressLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideHospitalSearchProgress() {
        mGeneralStatusProgressLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void onClickListItem(Place hospitalPlace) {
        mPresenter.selectPlace(hospitalPlace);
    }

}
