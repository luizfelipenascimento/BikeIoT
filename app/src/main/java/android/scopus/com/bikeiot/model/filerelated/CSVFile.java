package android.scopus.com.bikeiot.model.filerelated;


import android.content.Context;
import android.media.MediaScannerConnection;
import android.os.Environment;
import android.scopus.com.bikeiot.model.filerelated.CSVFileContentManager;
import android.scopus.com.bikeiot.model.filerelated.FileRepository;
import android.util.Log;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;


//TODO: (CSVFile) decidir se os objetos terao relacionamento com o CSVFile ou somente o pathName
public class CSVFile {

    //TODO: remover os seguintes atibutos statics da classe e extender de FileRepository
    //TODO: utilizar somente openCSV para manipular csv https://www.callicoder.com/java-read-write-csv-file-opencsv/

    public static final String RESULT_ROOT_FOLDER_NAME = "Results";
    public static final String ROUTE_ROOT_FOLDER_NAME = "Routes";
    public static final String UNDERLINE_SEPARATOR = "_";
    public static final String TEST_ROOT_FOLDER = "test";

    private static final String TAG = "CSVFile";
    private static final char SEPARATOR = ';';
    private final String ROOT_FOLDER = "BikeIot";
    public static final String EXTENSION = ".csv";

    private String mDirectory;
    private FileOutputStream mFileOutputStream;
    private File mFile;
    private File mFolder;
    private String mFileName;
    private String[] mFileHeader;
    private String[] mInfoLineArray;
    private Context mContext;
    private int indexInfoLine = 0;
    private boolean mIsClosed = false;

    public CSVFile(Context context) {
        mContext = context;
    }

    public CSVFile(String fileName, Context context, String rootFolder, String[] fileHeader) {
        Log.d(TAG, "CSVFile called");
        setFileName(fileName);
        mContext = context;
        mFileHeader = fileHeader;
        mDirectory = configureDirectory(rootFolder);

        mFolder = new File(Environment.getExternalStorageDirectory(), mDirectory);
        if(!mFolder.exists()) {
            mFolder.mkdirs();
            mFolder.setReadable(true);
            mFolder.setExecutable(true);
            mFolder.setWritable(true);
        }

        mFile = new File(mFolder, mFileName);
        MediaScannerConnection.scanFile(mContext, new String[] {mFolder.toString()}, null, null);
        MediaScannerConnection.scanFile(mContext, new String[] {mFile.toString()}, null, null);
        FileRepository.scanFile(mFolder, mContext);
        FileRepository.scanFile(mFile, mContext);


        Log.d(TAG, "file path: " + mFile.getPath());
        try {
            Log.d(TAG, "CSVFile criando FileOutputStream");
            mFileOutputStream = new FileOutputStream(mFile);
            writeLine(mFileHeader);
        } catch (FileNotFoundException e) {
            Log.e(TAG, "CSVFile erro ao criar novo input stream");
            e.printStackTrace();
        }
    }

    private String configureDirectory(String directory) {
        if (directory.charAt(0) != '/') {
            return ROOT_FOLDER + File.separator + directory;
        } else {
            return  ROOT_FOLDER + directory;
        }
    }

    public String setFileName(String fileName) {
        return mFileName = fileName + EXTENSION;
    }

    public void writeLine(String[] data) {
        StringBuilder lineInfo = new StringBuilder();
        for(int i = 0; i < data.length; i++) {
            lineInfo.append(data[i] + SEPARATOR);
        }
        lineInfo.append("\n");

        Log.d(TAG, "writeLine : info line " + lineInfo);
        try {
            mFileOutputStream.write(lineInfo.toString().getBytes());
            MediaScannerConnection.scanFile(mContext, new String[] {mFile.getAbsolutePath()}, null, null);
            MediaScannerConnection.scanFile(mContext, new String[]{mFile.toString()}, null, null);
            FileRepository.scanFile(mFile, mContext);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            mFileOutputStream.flush();
            mFileOutputStream.close();
            mIsClosed = true;
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "close() fail to close fileOutput of " + mFile.getName());
        }
    }

    public boolean isClosed() {
        return mIsClosed;
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public void appendInfoInLineArray(String info) {
        Log.d(TAG, "appendInfoInLineArray info: " + info);
        mInfoLineArray[indexInfoLine] = info;
        indexInfoLine++;
        if(indexInfoLine >= mInfoLineArray.length) {
            indexInfoLine = 0;
        }
    }

    public String extractDirectory(String filePathName) {
        if (filePathName.lastIndexOf(File.separator) + 1 == filePathName.length()) {
            return filePathName;
        } else {
            return filePathName.substring(0, filePathName.lastIndexOf(File.separator) + 1);
        }
    }

    public void renameFile(String oldFilePathName, String newName) {
        Log.d(TAG, "renameFile called");
        mDirectory = extractDirectory(oldFilePathName);
        mFile = new File(oldFilePathName);
        File newFile = new File(mDirectory, setFileName(newName));
        makeFileAvailable(newFile);
        if (mFile.renameTo(newFile)) {
            MediaScannerConnection.scanFile(mContext, new String[] {mFile.getAbsolutePath()}, null, null);
            Log.d(TAG, "renameFile: succeed");
            mFile = newFile;
            MediaScannerConnection.scanFile(mContext, new String[] {mFile.getAbsolutePath()}, null, null);
            FileRepository.scanFile(mFile, mContext);
        }
    }

    private void makeFileAvailable(File file) {
        file.setReadable(true);
        file.setExecutable(true);
        file.setWritable(true);
    }

    public static CSVFileContentManager readAllFileContent(String filePathName) {
        try {
            CSVParser parser = new CSVParserBuilder().withSeparator(SEPARATOR).build();
            CSVReader reader = new CSVReaderBuilder(new FileReader(filePathName))
                    .withCSVParser(parser).build();

            return new CSVFileContentManager(reader.readAll());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String[] getInfoLineArray() {
        return mInfoLineArray;
    }

    public void createInfoLineArray(int size) {
        mInfoLineArray = new String[size];
    }

    public void clearInfoLineArray() {
        mInfoLineArray = null;
        indexInfoLine = 0;
    }

    public String[] getFileHeader() {
        return mFileHeader;
    }

    public String getPathFile() {
        return mFile.getPath();
    }

    public String getFileName() {
        return mFileName;
    }

    public File getFile() {
        return mFile;
    }
}
