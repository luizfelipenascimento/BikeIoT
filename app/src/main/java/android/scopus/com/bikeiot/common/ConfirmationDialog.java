package android.scopus.com.bikeiot.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.scopus.com.bikeiot.R;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

/**
 *
 */

public class ConfirmationDialog extends DialogFragment {

    public static final String DIALOG_TAG = "ConfirmationDialog";
    private static final String CONFIRMATION_CONTENT = "warning_info";
    private static final String TITLE_INFO = "title";

    public static ConfirmationDialog newInstance(String confirmationContent, String title,
                                                 ConfirmationDialogListener confirmationDialogListener) {
        ConfirmationDialog dialog = new ConfirmationDialog();
        dialog.setListener(confirmationDialogListener);
        Bundle args = new Bundle();
        args.putString(CONFIRMATION_CONTENT, confirmationContent);
        args.putString(TITLE_INFO, title);
        dialog.setArguments(args);
        return dialog;
    }

    private void setListener(ConfirmationDialogListener confirmationDialogListener) {
        mListener = confirmationDialogListener;
    }

    public interface ConfirmationDialogListener {
        void onConfirmationDialogPositiveChoose();
        void onConfirmationDialogNegativeChoose();
    }

    private ConfirmationDialogListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo_Light_Dialog);
        String content = getArguments().getString(CONFIRMATION_CONTENT);
        String title = getArguments().getString(TITLE_INFO);

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialogView  = inflater.inflate(R.layout.dialog_confirmation_action, null);

        alertBuilder.setTitle(title);
        alertBuilder.setMessage(content);
        alertBuilder.setView(dialogView)
                .setPositiveButton(R.string.dialog_continue, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onConfirmationDialogPositiveChoose();
                    }
                })
                .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onConfirmationDialogNegativeChoose();
                    }
                });

        return alertBuilder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        /*try {
            mListener = (ConfirmationDialogListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("class is not implementing SelectRouteDialogListener" + e);
        }*/
    }
}
