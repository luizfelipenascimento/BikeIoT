package android.scopus.com.bikeiot.panicbutton;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.IBinder;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.utils.SharedPreferencesUtils;
import android.scopus.com.bikeiot.exerciserecoder.ExerciseRecorderActivity;
import android.scopus.com.bikeiot.exerciserecoder.ExerciseRecorderService;
import android.scopus.com.bikeiot.model.location.LocationUpdater;
import android.scopus.com.bikeiot.model.beans.Contact;
import android.scopus.com.bikeiot.model.data.ContactRepository;
import android.scopus.com.bikeiot.model.data.UserRepository;
import android.scopus.com.bikeiot.model.data.local.ContactDAO;
import android.scopus.com.bikeiot.model.data.local.UserDAO;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.SettingsClient;

import java.util.ArrayList;
import java.util.List;

import static android.scopus.com.bikeiot.Constants.BRAZIL_PHONE_CODE;

/**
 *
 */

public class CallHelpService extends Service implements CallHelpController.EventListener {

    public static final String ACTION_START_WARNING_EVENT = "ACTION_START_WARNING_EVENT";
    public static final String EXTRA_PHONE_NUMBERS = "EXTRA_PHONE_NUMBERS";
    public static final String ACTION_CANCEL_WARNING_EVENT = "ACTION_CANCEL_WARNING_EVENT";
    public static final String ACTION_ACCIDENT_DETECTED = "ACTION_ACCIDENT_DETECTED";

    private static final String TAG = "CallHelpService";
    private SensorManager mSensorManager;
    private Sensor mSensorAccelerometer;

    private SensorEventListener mSensorListener;
    private LocationUpdater mLocationUpdater;
    private static boolean mServiceAlive = false;
    private LocationCallback mLocationCallback;
    private CallHelpController mCallHelpController;

    private ContactRepository mContactRepository;
    private int userId;
    private UserRepository mUserRepository;

    @Override
    public void onCreate() {
        super.onCreate();

        mServiceAlive = true;

        FusedLocationProviderClient fusedClient = LocationServices.getFusedLocationProviderClient(this);
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);

        userId = SharedPreferencesUtils.getIntSharedPreferences(this,
                getString(R.string.shared_preferences_key), getString(R.string.shared_user_id),0);

        mContactRepository = new ContactRepository(new ContactDAO(this));
        mUserRepository = new UserRepository(new UserDAO(this));
        mLocationUpdater = new LocationUpdater(fusedClient, settingsClient, LocationUpdater
                .locationRequestHighAccuracy(0,0,0));

        createLocationCallback();
        mLocationUpdater.setupLocationCallback(mLocationCallback);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand called");
        if (intent != null) {

            String action = intent.getAction();

            if (action.equals(ACTION_START_WARNING_EVENT)) {
                Log.d(TAG, "ACTION_START_WARNING_EVENT called");
                if (mCallHelpController == null || !mCallHelpController.eventAlive()) {
                    Log.d(TAG, "ACTION_START_WARNING_EVENT starting");
                    //TODO: refatorar e remover o numero de telefone fixo.

                    mUserRepository.forceBreakCache();
                    String userName = mUserRepository.getUser(userId).getName();

                    mContactRepository.forceBreakCache();
                    List<Contact> contacts = mContactRepository.getContactsFromUser(userId);

                    ArrayList<String> phoneNumbers = extractPhoneNumbers(contacts);

                    mCallHelpController = new CallHelpController(this, phoneNumbers ,
                            this, userName);

                    showDialogActivityAccident();

                    //inicializando requesicao de locations updates
                    mLocationUpdater.startLocationUpdates();

                    mCallHelpController.startCallEvent();
                }
            }

            if (action.equals(ACTION_CANCEL_WARNING_EVENT)) {

                mCallHelpController.stopTimer();

                Toast.makeText(this, "Aviso cancelado", Toast.LENGTH_SHORT).show();
            }

        }

        return START_STICKY;
    }

    private ArrayList<String> extractPhoneNumbers(List<Contact> contacts) {
        ArrayList<String> phoneNumbers = new ArrayList<>();
        for (Contact contact : contacts) {
            String phoneNumber = contact.getPhoneNumber();
            if (!phoneNumber.contains(BRAZIL_PHONE_CODE)) {
                phoneNumbers.add(BRAZIL_PHONE_CODE + phoneNumber);
            } else {
                phoneNumbers.add(phoneNumber);
            }
        }
        return phoneNumbers;
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                Log.d(TAG, "LocationCallback onLocationResult called");

                Location location = locationResult.getLastLocation();

                if (mCallHelpController != null) {
                    mCallHelpController.setLocation(location);
                }

                mLocationUpdater.stopLocationUpdates();
            }
        };
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy called");
        mServiceAlive = false;
    }

    private void showDialogActivityAccident() {
        Intent dialogIntent = new Intent(this, CallHelpDialogActivity.class);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_USER_ACTION);
        startActivity(dialogIntent);
    }

    public static boolean isAlive() {
        return mServiceAlive;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onEventCompleted() {
        Log.d(TAG, "onEventCompleted called");
        //parando a service ExerciseRecorderService
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(ExerciseRecorderActivity.ACTION_RESET_INFO));
        sendBroadcast(new Intent(CallHelpDialogActivity.ACTION_CLOSE));
        stopService(new Intent(this, ExerciseRecorderService.class));
    }

    @Override
    public void onEventCanceled() {
        Log.d(TAG, "onEventCanceled called");
    }

    @Override
    public void onError(int errorId) {
        Log.d(TAG, "onError called " + getString(errorId));
    }

    @Override
    public void endStatus(int statusId) {
        Log.d(TAG, "endStatus called " + getString(statusId));
    }
}
