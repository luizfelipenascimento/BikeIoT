package android.scopus.com.bikeiot.panicbutton;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.provider.Telephony;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.common.WarningDialog;
import android.scopus.com.bikeiot.common.WarningDialogActivity;
import android.telephony.SmsManager;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.scopus.com.bikeiot.Constants.EXTRA_CONTENT;
import static android.scopus.com.bikeiot.Constants.GOOGLE_MAPS_HTTP_BASE_LINK;

/**
 *
 */

public class CallHelpController {

    private static final String TAG = "CallHelpController";

    private final long TIMER_FOR_EVENT = 1000 * 10; //1 minute dor wait
    private final String mUserName;

    private Context mContext;

    private List<String> mPhoneNumbers;

    private ArrayList<String> mPhoneNumbersFailed;

    private Location mLocation;

    private Thread mCallHelperTimerThread;

    private EventListener mEventListener;

    interface EventListener {
        void onEventCompleted();
        void onEventCanceled();
        void onError(int errorId);
        void endStatus(int statusId);
    }

    public CallHelpController(Context context, ArrayList<String> phoneNumbers, EventListener eventListener,
                              String userName) {
        mPhoneNumbers = phoneNumbers;
        mContext = context;
        mEventListener = eventListener;
        mUserName = userName;
        mPhoneNumbersFailed = new ArrayList<>();
    }

    private Address mAddress;

    private Runnable CallHelperTimerRunnable = new Runnable() {

        @Override
        public void run() {
            try {

                Thread.sleep(TIMER_FOR_EVENT);

                Log.d(TAG, "sending sms");
                mAddress = loadAddressInfo();

                if (mPhoneNumbers.isEmpty()) {
                    mEventListener.onError(R.string.no_contact_registered);
                }

                for (String phone : mPhoneNumbers) {
                    if (!sendSms(phone, formatHelpWarningMassage(mUserName))) {
                        mPhoneNumbersFailed.add(phone);
                    }
                }

                generateEndStatus();
                mEventListener.onEventCompleted();
                return;
            } catch (InterruptedException e) {
                Log.d(TAG, "CallHelperTimerRunnable interrupted");
                mEventListener.onEventCanceled();
                return;
            }
        }
    };

    private void generateEndStatus() {
        if (mPhoneNumbersFailed.size() == mPhoneNumbers.size()) {
            mEventListener.endStatus(R.string.error_to_send_sms);
        } else {
            mEventListener.endStatus(R.string.send_sms_success);
        }
    }

    private Address loadAddressInfo() {
        if (mLocation == null) {
            return null;
        }

        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(mLocation.getLatitude(),
                    mLocation.getLongitude(), 1);

            return addresses.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean sendSms(String phoneNumber, String smsMessage) {
        try {
            Log.d(TAG, "sendSms phoneNumber:" + phoneNumber);
            Log.d(TAG, "sendSms message: " + smsMessage);

            SmsManager smsManager = SmsManager.getDefault();

            ArrayList<String> messageParts = smsManager.divideMessage(smsMessage);

            smsManager.sendMultipartTextMessage(phoneNumber, null, messageParts,
                    null, null);

            Log.d(TAG, "sendSms: mesagem enviada");

            //TODO: remover esse debug
            /*Intent dialogIntent = new Intent(mContext, WarningDialogActivity.class);

            dialogIntent.putExtra(EXTRA_CONTENT,
                    "mensagem possivelmente enviada para o numero: " + phoneNumber);

            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_USER_ACTION);
            mContext.startActivity(dialogIntent);*/

            return true;
        } catch (Exception e) {
            Log.e(TAG, "sendSms: erro ao enviar: " + e);
            //TODO: remover esse debug
            Intent dialogIntent = new Intent(mContext, WarningDialogActivity.class);
            dialogIntent.putExtra(EXTRA_CONTENT, "error: " + e);
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_USER_ACTION);
            mContext.startActivity(dialogIntent);

            return false;
        }
    }

    private String formatHelpWarningMassage(String name) {
        String contentMessage = "";
        String locationDetails = "";

        locationDetails = formatLocationDetails(mAddress, mLocation);

        String headerInfo = mContext.getString(R.string.sms_warning_help_warning_header) + "\n";
        String infoBody = mContext.getString(R.string.sms_info_body_accident) + "\n";
        infoBody = infoBody.replaceFirst("%s", name);
        String endSmsInfo = mContext.getString(R.string.sms_end_info);

        contentMessage += headerInfo;
        contentMessage += infoBody;
        contentMessage += locationDetails;
        contentMessage += endSmsInfo;

        return contentMessage;
    }

    private String formatLocationDetails(Address address, Location location) {
        if (location == null) {
            return "";
        }

        StringBuilder completeAddressInfo = new StringBuilder();
        String header =  mContext.getString(R.string.sms_user_location_header) + "\n";
        completeAddressInfo.append(header);

        //pegando a informacao presente no objeto address e colocando no String Builder completeAddressInfo
        if (address != null) {
            if (address.getMaxAddressLineIndex() == 0) {
                completeAddressInfo.append(address.getAddressLine(0));
            } else {
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    completeAddressInfo.append(address.getAddressLine(i));
                }
            }
        }

        //adicionando latitude e longitude ao String builder
        completeAddressInfo.append("\n" + mContext.getString(R.string.latitude )+ String.valueOf(mLocation.getLatitude()))
                .append(" - " + mContext.getString(R.string.longitude) + String.valueOf(mLocation.getLongitude()) + "\n");

        //adicionando link para o google maps da localizacao do usuario
        completeAddressInfo.append(GOOGLE_MAPS_HTTP_BASE_LINK + mLocation.getLatitude() + ","
                + mLocation.getLongitude() + "\n");

        return completeAddressInfo.toString();
    }

    public void startCallEvent() {
        mCallHelperTimerThread = new Thread(CallHelperTimerRunnable);
        mCallHelperTimerThread.start();
    }

    public void stopTimer() {
        if (mCallHelperTimerThread != null && mCallHelperTimerThread.isAlive()) {
            mCallHelperTimerThread.interrupt();
        }
    }

    public void setLocation(Location location) {
        mLocation = location;
    }

    public boolean eventAlive() {
        if (mCallHelperTimerThread == null) {
            return false;
        }

        return mCallHelperTimerThread.isAlive();
    }
}
