package android.scopus.com.bikeiot.editroute;

import android.scopus.com.bikeiot.model.beans.Route;

/**
 * Created by Luiz Felipe on 10/04/2018.
 */

interface EditRouteContract {

    interface View {
        void showRouteDetails(Route route);
        void showNoRouteError();
        void sendResponseForRouteDetailsUi(int routeId);
        void showUpdateError();
        void showEmptyDescriptionError(int errorMsgId);
        void showUpdateSuccess();
        void hideDescriptionEmptyError();
    }

    interface Presenter {
        void initialize();
        void loadRoute();
        void update(String description, boolean toShare);
    }

}
