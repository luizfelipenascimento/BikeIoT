package android.scopus.com.bikeiot.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.scopus.com.bikeiot.model.data.local.BikeIotDbHelper;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Felipe on 2/18/2018.
 */

public class InputHandler {

    private static final String TAG = InputHandler.class.getSimpleName();
    private Context mContext;
    private static final String EMAIL_CHECK_REGEX = "((([^\\.:;,<>()@\\s\\[\\]])+(\\.[^\\.:;,<>()@\\s\\[\\]]+)*)|(\".+\"))@[a-zA-Z\\-0-9]+\\.([a-zA-Z]+)((\\.[a-zA-Z]+)*)";
    private static final String PHONE_WITH_MASK_CHECK_REGEX = "^([1-2][1-9] (9\\d{4})-\\d{4})|((\\(1[2-9]{1}\\)|\\([2-9]{1}\\d{1}\\)) [5-9]\\d{3}-\\d{4})";
    private static final String CELLPHONE_CHECK_REGEX_NO_MASK = "([1-2][1-9](9\\d{4})\\d{4})";

    private  InputHandler() {}

    public static boolean isEmptyOrNull(String value) {
        return value.isEmpty() || value == null;
    }

    public static boolean isValidEmail(String email) {
        return email.matches(EMAIL_CHECK_REGEX);
    }

    public static boolean isPhoneNumberValidMask(String phoneNumber) {
        return phoneNumber.matches(PHONE_WITH_MASK_CHECK_REGEX);
    }

    public static boolean isPhoneNumberValid(String phoneNumber) {
        return phoneNumber.matches(CELLPHONE_CHECK_REGEX_NO_MASK);
    }

    public static boolean isOutOfBound(int min, int max, int value) {
        return value < min || value > max;
    }

    public static boolean isOutOfBound(double min, double max, double value) {
        return value < min || value > max;
    }

    public static boolean isUnique(String tableName, String columnName, String value, Context context) {
        BikeIotDbHelper mDbHelper = new BikeIotDbHelper(context);
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        String[] project = {columnName};
        String[] args = {value};

        Cursor cursor = db.query(tableName,
                project,
                columnName + "=?",
                args,
                null,
                null,
                null);

        try {
            Log.d(TAG, "cursor.getCount()");
            return cursor.getCount() == 0;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }

        return false;
    }

    public static boolean isValidDate(String date) {
        boolean isFormatCorrect = date.matches("[0-9][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]");
        if(isFormatCorrect) {
            try {
                Date newDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
                Calendar calendar = Calendar.getInstance();
                calendar.setLenient(false);
                calendar.setTime(newDate);
                return true;
            } catch (ParseException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }
}
