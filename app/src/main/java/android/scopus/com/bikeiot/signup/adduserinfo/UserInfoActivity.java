package android.scopus.com.bikeiot.signup.adduserinfo;

import android.scopus.com.bikeiot.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class UserInfoActivity extends AppCompatActivity implements UserInfoContract.View {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
    }

    @Override
    public void showNameMessageError(int msgId) {

    }
}
