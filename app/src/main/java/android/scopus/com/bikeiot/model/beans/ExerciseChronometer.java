package android.scopus.com.bikeiot.model.beans;

import android.content.Context;
import android.content.Intent;
import android.scopus.com.bikeiot.exerciserecoder.ExerciseRecorderActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class ExerciseChronometer implements Runnable {
    //TODO: arrumar o reset quando fechar
    private static final String TAG = "ExerciseChronometer";
    private static final long SECOND_IN_MILLIS = 1000;
    private static final long MINUTE_IN_MILLIS = SECOND_IN_MILLIS * 60;
    private static final long HOURS_IN_MILLIS = MINUTE_IN_MILLIS * 60;

    private long startTime;
    private long pausedTime;
    private int hour;
    private int minutes;
    private int seconds;
    private boolean isRunning;
    private boolean isPaused;
    private long distanceTime;
    private Context mContext;

    public ExerciseChronometer(Context context) {
        mContext = context;
    }

    public void start() {
        startTime = System.currentTimeMillis();
        isRunning = true;
    }

    public void stop() {
        isRunning = false;
        //resetTime();
    }

    public void pause() {
        pausedTime = System.currentTimeMillis();
        isPaused = true;
    }

    public void play() {
        isPaused = false;
    }

    private String getFormattedTime(int hour, int minutes, int seconds) {
        return String.format("%02d:%02d:%02d", hour,minutes,seconds);
    }

    public void resetTime() {
        seconds = 0;
        minutes = 0;
        hour = 0;
    }

    public String getTime() {
        return String.format("%02d:%02d:%02d", hour,minutes,seconds);
    }




    @Override
    public void run() {

        while (isRunning) {
            if (!isPaused) {
                if (pausedTime != 0) {
                    long currentTime = System.currentTimeMillis();
                    startTime += currentTime - pausedTime;
                    distanceTime = currentTime - startTime;
                    pausedTime = 0;
                } else {
                    distanceTime = System.currentTimeMillis() - startTime;
                }

                seconds = (int) (distanceTime / SECOND_IN_MILLIS) % 60;
                minutes = (int) (distanceTime / MINUTE_IN_MILLIS) % 60;
                hour = (int) (distanceTime / HOURS_IN_MILLIS);
            }
            Intent intentSendMessage = new Intent(ExerciseRecorderActivity.CHRONOMETER_KEY);
            intentSendMessage.putExtra("data", getFormattedTime(hour, minutes, seconds));
            LocalBroadcastManager.getInstance(mContext).sendBroadcast(intentSendMessage);

            try {
                Thread.sleep(80);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Log.d(TAG, "Chronometer stopped");
        Intent intentSendMessage = new Intent(ExerciseRecorderActivity.CHRONOMETER_KEY);
        intentSendMessage.putExtra("data", getFormattedTime(0, 0,0));
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intentSendMessage);
    }
}
