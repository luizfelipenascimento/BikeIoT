package android.scopus.com.bikeiot.model.network.googledirectionAPI;

import android.content.Context;
import android.os.AsyncTask;
import android.scopus.com.bikeiot.model.network.ApiListenerBasic;
import android.scopus.com.bikeiot.model.network.NetworkHelper;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 *
 */

public class GoogleDirectionsAsyncTask extends AsyncTask<String, Void, List<LatLng>> {


    private final NetworkHelper mNetworkHelper;

    interface ListenerResult extends ApiListenerBasic {
        void onSuccess(List<LatLng> locationsLatLng);
        void onFail();
    }

    GoogleDirectionsAsyncTask(NetworkHelper networkHelper) {
        mNetworkHelper = networkHelper;
    }

    @Override
    protected List<LatLng> doInBackground(String... strings) {
        if (!mNetworkHelper.isNetworkConnected()) {

            return null;
        }



        return null;
    }

}
