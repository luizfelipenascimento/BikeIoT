package android.scopus.com.bikeiot.routes;

import android.content.Context;
import android.net.Uri;
import android.scopus.com.bikeiot.utils.Helper;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.model.beans.Route;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;
import java.util.List;

import static android.scopus.com.bikeiot.utils.CalculateUtils.metersToKm;
import static android.scopus.com.bikeiot.utils.Helper.formatKmDecimal;

/**
 * Created by Luiz Felipe on 05/03/2018.
 */

public class RouteListAdapter  extends RecyclerView.Adapter<RouteListAdapter.RouteViewHolder>{

    private static final String TAG = RouteListAdapter.class.getSimpleName();

    private List<Route> mRoutes;

    private RouteListClickListener mRouteListClickListener;
    private Context mContext;
    private RequestManager mRequestManagerGlide;

    public RouteListAdapter(List<Route> routes, RouteListClickListener clickListener, Context context
            , RequestManager requestManagerGlide) {
        mRoutes = routes;
        mRouteListClickListener = clickListener;
        mContext = context;
        mRequestManagerGlide = requestManagerGlide;
    }

    interface RouteListClickListener {
        void onClickListItem(Route route);
    }

    @Override
    public RouteViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        int layoutIdForRouteItemRow = R.layout.routes_item_row;
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(layoutIdForRouteItemRow, viewGroup, false);

        return new RouteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RouteViewHolder holder, int position) {
        Route route = mRoutes.get(position);

        String distanceText = formatKmDecimal(metersToKm(route.getDistance()));
        String kmIndicator = mContext.getString(R.string.km_indicator);

        holder.mRouteDistance.setText(distanceText + " " + kmIndicator);
        holder.mRouteDateTextView.setText(Helper.formatDateToMyDateFormat(route.getDate()));
        holder.mRouteDescriptionTextView.setText(route.getId() + " " + route.getDescription());


        Uri uri = null;
        try {
            File imageFile = new File(route.getImagePath());

            uri = Uri.fromFile(imageFile);

        } catch (Exception e) {
            Log.e(TAG, "error to load image: " + e);
            Log.d(TAG, "description:" + route.getDescription() +
            " id: " + route.getId());
        }

        if (uri != null) {
            mRequestManagerGlide.load(uri)
                    .apply(RequestOptions.circleCropTransform())
                    .into(holder.mRouteImageView);
        }

        holder.itemView.setTag(route.getId());
    }

    @Override
    public int getItemCount() {
        return mRoutes.size();
    }


    class RouteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView mNoImageTextView;
        TextView mRouteDistance;
        TextView mRouteDescriptionTextView;
        TextView mRouteDateTextView;
        ImageView mRouteImageView;

        public RouteViewHolder(View itemView) {
            super(itemView);
            mRouteImageView = (ImageView) itemView.findViewById(R.id.route_image_view);
            mRouteDateTextView = (TextView) itemView.findViewById(R.id.route_date_text_view);
            mRouteDescriptionTextView = (TextView) itemView.findViewById(R.id.route_description_text_view);
            mRouteDistance = (TextView) itemView.findViewById(R.id.route_distance_text_view);
            mNoImageTextView = (TextView) itemView.findViewById(R.id.no_image_loaded);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();

            Log.d(TAG, "onClick position of selected item: " + position);

            Log.d(TAG, "onClick route item clicked id: "+ v.getTag());

            mRouteListClickListener.onClickListItem(mRoutes.get(position));
        }
    }
}
