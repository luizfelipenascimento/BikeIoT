package android.scopus.com.bikeiot.model.data;

import android.scopus.com.bikeiot.model.filerelated.CSVFileContentManager;
import android.scopus.com.bikeiot.model.filerelated.CSVFile;
import android.scopus.com.bikeiot.model.beans.Route;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.CSVFileRouteContract.CSV_ROUTE_COLUMN_LATITUDE;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.CSVFileRouteContract.CSV_ROUTE_COLUMN_LONGITUDE;

/**
 * Created by Luiz Felipe on 04/04/2018.
 */

public class RouteRepository implements RouteDataSource {

    private static final String TAG = RouteRepository.class.getSimpleName();
    private RouteDataSource mRouteDataSource;

    public RouteRepository(RouteDataSource routeDataSource) {
        mRouteDataSource = routeDataSource;
    }

    private boolean cacheBroken = true;
    private Route mCachedRoute;
    private List<Route> mRoutesCached;

    @Override
    public List<Route> getRoutes() {
        if (!cacheBroken && mRoutesCached != null) {
            return mRoutesCached;
        } else {
            mRoutesCached = mRouteDataSource.getRoutes();
            return mRoutesCached;
        }
    }

    @Override
    public boolean save(Route route) {
        forceBreakCache();
        return mRouteDataSource.save(route);
    }

    @Override
    public Route getRoute(int id) {
        if (!cacheBroken && mCachedRoute != null) {
            return mCachedRoute;
        } else {
            Route route = mRouteDataSource.getRoute(id);
            if (route == null) {
                return null;
            }
            route.setLocationPoints(loadLocationPointsFromFile(route.getRouteCsvFilePath()));
            refreshRouteCache(route);
            return route;
        }
    }

    public Route getRoute(int id, boolean refresh) {
        if (!cacheBroken && mCachedRoute != null && !refresh) {
            return mCachedRoute;
        } else {
            Route route = mRouteDataSource.getRoute(id);
            if (route == null) {
                return null;
            }
            route.setLocationPoints(loadLocationPointsFromFile(route.getRouteCsvFilePath()));
            refreshRouteCache(route);
            return route;
        }
    }

    @Override
    public boolean update(Route route) {
        forceBreakCache();
        return mRouteDataSource.update(route);
    }

    @Override
    public boolean renameRouteFilePath(Route route) {
        forceBreakCache();
        return mRouteDataSource.renameRouteFilePath(route);
    }

    @Override
    public boolean updateRenameImagePath(String newImagePath, int routeId) {
        forceBreakCache();
        return mRouteDataSource.updateRenameImagePath(newImagePath, routeId);
    }

    @Override
    public boolean remove(int routeId) {
        forceBreakCache();
        return mRouteDataSource.remove(routeId);
    }

    @Override
    public boolean updateShare(int id, int share) {
        forceBreakCache();
        return mRouteDataSource.updateShare(id, share);
    }

    private List<LatLng> loadLocationPointsFromFile(String filepath) {

        ArrayList<LatLng> locationPoints = new ArrayList<>();
        try {
            CSVFileContentManager contentManager = CSVFile.readAllFileContent(filepath);
            if (contentManager != null) {
                for (int line = 0; line < contentManager.getNumberOfLines(); line++) {

                    double lat = Double.parseDouble(contentManager.getColumnValue
                            (CSV_ROUTE_COLUMN_LATITUDE , line));

                    double lng = Double.parseDouble(contentManager.getColumnValue
                            (CSV_ROUTE_COLUMN_LONGITUDE, line));

                    LatLng latLng = new LatLng(lat, lng);

                    locationPoints.add(latLng);
                }
            }
        } catch (CSVFileContentManager.NoCSVColumnException e) {
            Log.e(TAG, "loadLocationPointsFromFile " + e);
            e.printStackTrace();
        }
        return locationPoints;
    }

    public void forceBreakCache() {
        cacheBroken = true;
    }

    private void refreshRouteCache(Route route) {
        mCachedRoute = route;
        cacheBroken = false;
    }

    private void refreshRoutesCache(List<Route> routes) {
        mRoutesCached = routes;
        cacheBroken = false;
    }

    public boolean isCacheBroken() {
        return cacheBroken;
    }
}
