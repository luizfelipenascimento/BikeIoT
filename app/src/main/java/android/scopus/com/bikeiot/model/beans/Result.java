package android.scopus.com.bikeiot.model.beans;

import android.scopus.com.bikeiot.model.filerelated.CSVFile;
import android.util.Log;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.scopus.com.bikeiot.utils.CalculateUtils.*;

public class Result implements Serializable {

    private static final String TAG = "Result";

    private static final int DELTA_TIME_INTERVAL = 2; //seconds
    public static final String ACCIDENT_HAPPENED = "Y";

    private Date mTotalTimeChronometer;
    private String mFileResultPath;
    private BikeExercise mBikeExercise;
    private String mResultString;
    private double[] mAccelerationInfo;
    private double[] mGyroscopeInfo;
    private CSVFile mResultCSVFile;
    private double mVelocityMetersPerSec = 0;
    private double mTotalVelocity = 0;
    private double mMeanVelocityMetersPerSec = 0;
    private double mNumberOfResults = 0;
    private double mMaxVelocityMetersPerSec = 0;
    private double mDistanceBasedOnRotationsMeters = 0;
    private int mRotations;
    private double mMeanHeartRate = 0;
    private double mCaloriesBurned = 0;
    private double mPitch = 0;
    private double mRoll = 0;
    private double mLinearAccelerationAcc = 0;
    private double mAverageAccZ = 0;
    private double mAverageAccY = 0;
    private String mAccidentStatus = "N";


    public void setTotalTimeChronometer(String totalTimeChronometer) {
        try {
            mTotalTimeChronometer = new SimpleDateFormat("HH:mm:ss").parse(totalTimeChronometer);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Date getTotalTimeChronometer() {
        return mTotalTimeChronometer;
    }

    public double[] getAccelerationInfo() {
        return mAccelerationInfo;
    }

    public double[] getGyroscopeInfo() {
        return mGyroscopeInfo;
    }

    public Result() {
        super();
        mAccelerationInfo = new double[3];
        mGyroscopeInfo = new double[3];
        mVelocityMetersPerSec = 0;
    }

    public void setResultString(String resultString) {
        Log.d(TAG, "setResultString() called");
        resetAccelerometerInfoArray();
        resetGyroscopeInfoArray();
        resetRotations();
        mResultString = resultString;
        mNumberOfResults++;
        organizeInformation();
        cleanIdentifiers(resultString);
    }

    public void resetRotations() {
        mRotations = 0;
    }

    private void resetAccelerometerInfoArray() {
        Log.d(TAG, "resetAccelerometerInfoArray called");
        mAccelerationInfo = new double[3];
    }

    private void resetGyroscopeInfoArray() {
        Log.d(TAG, "resetGyroscopeInfoArray called");
        mGyroscopeInfo = new double[3];
    }

    private void organizeInformation() {
        String[] infos = mResultString.split(";");
        Log.d(TAG, "organizeInformation mResultString: " + mResultString);
        for(int i = 0; i < infos.length; i++) {
            String infoType = infos[i].substring(0, infos[i].indexOf(":"));
            Log.d(TAG, "organizeInformation() infoType: " + infoType);
            switch (infoType) {
                case "A": setAccelerometerInfo(infos[i]);
                    break;
                case "G": setGyroscopeInfo(infos[i]);
                    break;
                case "H": setStringRotationValuesToVelocity(infos[i]);
                    break;
                case "O": configRollInfo(infos[i]);
                    break;
                case "P": configPitchInfo(infos[i]);
                    break;
                case "LAC": configLinearAccelerationAccInfo(infos[i]);
                    break;
                case "MAZ": configAverageAccelerometerZ(infos[i]);
                    break;
                case "MAY": configAverageAccelerometerY(infos[i]);
                    break;
                case "ACDNT": configAccidentInfo(infos[i]);
                    break;
            }
        }
    }

    private void configAccidentInfo(String info) {
        info = cleanIdentifiers(info);
        Log.d(TAG, "configAverageAccelerometerZ: " + info);
        mAccidentStatus = info;
    }

    private void configAverageAccelerometerY(String info) {
        info = cleanIdentifiers(info);
        Log.d(TAG, "configAverageAccelerometerZ: " + info);
        mAverageAccY = Double.parseDouble(info);
    }

    private void configAverageAccelerometerZ(String info) {
        info = cleanIdentifiers(info);
        Log.d(TAG, "configAverageAccelerometerZ: " + info);
        mAverageAccZ = Double.parseDouble(info);
    }

    private void configLinearAccelerationAccInfo(String info) {
        info = cleanIdentifiers(info);
        Log.d(TAG, "configLinearAccelerationAccInfo: " + info);
        mLinearAccelerationAcc = Double.parseDouble(info);
    }

    private void configPitchInfo(String info) {
        info = cleanIdentifiers(info);
        Log.d(TAG , "configPitchInfo : pitch: " + info);
        mPitch = Double.parseDouble(info);
    }

    public void setAccelerometerInfo(String accelerometerInfo) {

        String onlyNumericValues = cleanIdentifiers(accelerometerInfo);

        Log.d(TAG, "setAccelerometerInfo() - informacao sem identificadores " + onlyNumericValues);
        String[] accelerometerXYZ = onlyNumericValues.split(",");

        Log.d(TAG, "setAccelerometerInfo: colocando info no array de inteiros");
        for(int i = 0; i < accelerometerXYZ.length; i++) {

            String indexType = accelerometerXYZ [i].substring(0, accelerometerXYZ [i].indexOf(":"));
            Log.d(TAG, "setGyroscopeInfo indexType: " + indexType);

            switch (indexType) {
                case "x":
                        double x = Double.parseDouble(cleanIdentifiers(accelerometerXYZ[i]));
                        Log.d(TAG, "setGyroscopeInfo value x : " + x);
                        mAccelerationInfo[0] = x;
                    break;
                case "y":
                        double y = Double.parseDouble(cleanIdentifiers(accelerometerXYZ[i]));
                        Log.d(TAG, "setGyroscopeInfo value y : " + y);
                        mAccelerationInfo[1] = y;
                    break;
                case "z":
                        double z = Double.parseDouble(cleanIdentifiers(accelerometerXYZ[i]));
                        Log.d(TAG, "setGyroscopeInfo value z : " + z);
                        mAccelerationInfo[2] = z;
                    break;
                default: Log.e(TAG, "setGyroscopeInfo - nenhum index foi passado");
            }
        }
    }

    private void setGyroscopeInfo(String gyroscopeInfo) {

        String onlyNumericValues = cleanIdentifiers(gyroscopeInfo);
        Log.d(TAG, "setGyroscopeInfo() - informacao sem identificadores " + onlyNumericValues);

        String[] gyroscopeXYZ = onlyNumericValues.split(",");

        Log.d(TAG, "setGyroscopeInfo: colocando info no array de inteiros");
        for(int i = 0; i < gyroscopeXYZ.length; i++) {

            String indexType = gyroscopeXYZ[i].substring(0, gyroscopeXYZ[i].indexOf(":"));
            Log.d(TAG, "setGyroscopeInfo indexType: " + indexType);

            switch (indexType) {
                case "x":
                    double x = Double.parseDouble(cleanIdentifiers(gyroscopeXYZ[i]));
                    Log.d(TAG, "setGyroscopeInfo value x : " + x);
                    mGyroscopeInfo[0] = x;
                    break;
                case "y":
                    double y = Double.parseDouble(cleanIdentifiers(gyroscopeXYZ[i]));
                    Log.d(TAG, "setGyroscopeInfo value y : " + y);
                    mGyroscopeInfo[1] = y;
                    break;
                case "z":
                    double z = Double.parseDouble(cleanIdentifiers(gyroscopeXYZ[i]));
                    Log.d(TAG, "setGyroscopeInfo value z : " + cleanIdentifiers(gyroscopeXYZ[i]));
                    mGyroscopeInfo[2] = z;
                    break;
                default: Log.e(TAG, "setGyroscopeInfo - nenhum index foi passado");
            }
            //mGyroscopeInfo[i] = Integer.parseInt(gyroscopeXYZ[i]);
        }
    }

    public void setStringRotationValuesToVelocity(String rotationString) {
        rotationString = cleanIdentifiers(rotationString);
        Log.d(TAG, "setStringRotationValuesToVelocity: rotationString result: " + rotationString);

        mRotations = Integer.parseInt(rotationString);
        //TODO: refatorar - remover esse relacionamento
        int rimValue = mBikeExercise.getBikeDevice().getRimValue();

        mVelocityMetersPerSec = calculateVelocityBasedOnRotation(mRotations, rimValue);
        Log.d(TAG, "rim value: " + rimValue + " rotaions: " + mRotations);
        
        Log.d(TAG, "mVelocityMetersPerSec: " + mVelocityMetersPerSec);
        mDistanceBasedOnRotationsMeters += calculateDistanceBasedOnRotation(mRotations, rimValue);
        Log.d(TAG, "mDistanceBasedOnRotationsMeters: " + mDistanceBasedOnRotationsMeters);
        mMaxVelocityMetersPerSec = calculateMaxVelocity(mVelocityMetersPerSec, mMaxVelocityMetersPerSec);
        mTotalVelocity += mVelocityMetersPerSec;
        mMeanVelocityMetersPerSec = calculateMeanVelocity(mTotalVelocity, mNumberOfResults);
    }

    private String cleanIdentifiers(String info) {
        return info.substring(info.indexOf(":") + 1, info.length());
    }

    public double calculateMaxVelocity(double velocity, double maxVelocity) {
        if (velocity >= maxVelocity) {
            maxVelocity = velocity;
        }
        return maxVelocity;
    }

    public double calculateMaxVelocity(double[] velocities) {
        for (double velocity : velocities) {
            if (velocity >= mMaxVelocityMetersPerSec) {
                mMaxVelocityMetersPerSec = velocity;
            }
        }
        return mMaxVelocityMetersPerSec;
    }

    public double calculateMeanVelocity(double totalVelocity, double numberOfResults) {
        double meanVelocity = 0;
        if (numberOfResults != 0) {
            meanVelocity = totalVelocity / numberOfResults;
        }
        return meanVelocity;
    }

    public double calculateMeanVelocity(double[] velocities) {
        for (double velocity : velocities) {
            mTotalVelocity += velocity;
        }
        mMeanVelocityMetersPerSec = mTotalVelocity / velocities.length;
        return mMeanVelocityMetersPerSec;
    }

    /**
     * Calcula a velocidade beseado no numero de rotacoes realizadas e
     * no aro da bicicleta.
     * @param rotation
     * @param rimValue
     * @return valor em metros por segundo (m/s)
     */
    public double calculateVelocityBasedOnRotation(int rotation, int rimValue) {
        Log.d(TAG, " rinInMeters: " + rimValue);
        double rinInMeters = centimeterToMeter(millimetersToCentimeters(inchToMillimeters(rimValue)));
        double r = rinInMeters / 2; //divisao do diametro para o raio
        return 2 * Math.PI * r * (rotation/DELTA_TIME_INTERVAL); //Sistema internacional m/s
    }

    /**
     * Calcula a velocidade beseado no numero de rotacoes realizadas e
     * no aro da bicicleta.
     * @param rotation
     * @param rimValue
     * @return valor em metros(m)
     */
    public double calculateDistanceBasedOnRotation(int rotation, int rimValue) {
        double rinInMeters = centimeterToMeter(millimetersToCentimeters(inchToMillimeters(rimValue)));
        Log.d(TAG, " rinInMeters: " + rimValue);
        double r = rinInMeters / 2;
        return 2 * Math.PI * r * (rotation); //retorna valor em metros
    }

    public void setVelocityMetersPerSec(double velocityMetersPerSec) {
        mVelocityMetersPerSec = velocityMetersPerSec;
    }

    public void setResultCSVFile(CSVFile resultCSVFile) {
        mResultCSVFile = resultCSVFile;
    }

    public void setBikeExercise(BikeExercise bikeExercise) {
        mBikeExercise = bikeExercise;
    }

    public void setFileResultPath(String fileResultPath) {
        mFileResultPath = fileResultPath;
    }

    public void setMeanVelocityMetersPerSec(double meanVelocityMetersPerSec) {
        mMeanVelocityMetersPerSec = meanVelocityMetersPerSec;
    }

    public void setMaxVelocityMetersPerSec(double maxVelocityMetersPerSec) {
        mMaxVelocityMetersPerSec = maxVelocityMetersPerSec;
    }

    public void setRotations(int rotations) {
        mRotations = rotations;
    }

    public void setPitch(double pitch) {
        mPitch = pitch;
    }

    public void setRoll(double roll) {
        mRoll = roll;
    }

    public void setLinearAccelerationAcc(double linearAccelerationAcc) {
        mLinearAccelerationAcc = linearAccelerationAcc;
    }

    public BikeExercise getBikeExercise() {
        return mBikeExercise;
    }

    public int getRotations() {
        return mRotations;
    }

    public double getPitch() {
        return mPitch;
    }

    public double getRoll() {
        return mRoll;
    }

    public double getLinearAccelerationAcc() {
        return mLinearAccelerationAcc;
    }

    /**
     * Get para velocidade em metros por segundos
     * @return
     */
    public double getVelocityMetersPerSec() {
        return mVelocityMetersPerSec;
    }

    public void setDistanceBasedOnRotationsMeters(double distanceBasedOnRotationsMeters) {
        mDistanceBasedOnRotationsMeters = distanceBasedOnRotationsMeters;
    }

    public void setMeanHeartRate(double meanHeartRate) {
        mMeanHeartRate = meanHeartRate;
    }

    public void setCaloriesBurned(double caloriesBurned) {
        mCaloriesBurned = caloriesBurned;
    }


    public void setAverageAccZ(double averageAccZ) {
        mAverageAccZ = averageAccZ;
    }

    public void setAverageAccY(double averageAccY) {
        mAverageAccY = averageAccY;
    }

    public double getAverageAccZ() {
        return mAverageAccZ;
    }

    public double getAverageAccY() {
        return mAverageAccY;
    }

    public double getMeanHeartRate() {
        return mMeanHeartRate;
    }

    public double getCaloriesBurned() {
        return mCaloriesBurned;
    }

    public double getDistanceBasedOnRotationsMeters() {
        return mDistanceBasedOnRotationsMeters;
    }

    public double getMeanVelocityMetersPerSec() {
        return mMeanVelocityMetersPerSec;
    }

    public double getMaxVelocityMetersPerSec() {
        return mMaxVelocityMetersPerSec;
    }

    public String getFileResultPath() {
        return mFileResultPath;
    }

    public CSVFile getResultCSVFile() {
        return mResultCSVFile;
    }

    public void configRollInfo(String roll) {
        roll = cleanIdentifiers(roll);
        Log.d(TAG, "configRollInfo : " + roll);
        mRoll = Double.parseDouble(roll);
    }

    public String getAccidentStatus() {
        return mAccidentStatus;
    }

    public void setAccidentStatus(String accidentStatus) {
        mAccidentStatus = accidentStatus;
    }

}
