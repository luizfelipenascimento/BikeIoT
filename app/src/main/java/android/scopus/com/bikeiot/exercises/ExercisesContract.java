package android.scopus.com.bikeiot.exercises;

import android.scopus.com.bikeiot.model.beans.BikeExercise;

import java.util.List;

/**
 *
 */

public interface ExercisesContract {

    interface View {
        void showExercises(List<BikeExercise> bikeExercises);
        void showExerciseDetailsUi(int bikeExerciseId);
        void showNoExercisesDone();
    }

    interface Presenter {
        void loadUserExercises(int userId);
        void loadExercises();
        void initialize();
        void openBikeExercise(BikeExercise bikeExercise);
    }
}
