package android.scopus.com.bikeiot.usercontacts;

import android.content.SharedPreferences;
import android.scopus.com.bikeiot.utils.SharedPreferencesUtils;
import android.scopus.com.bikeiot.model.beans.Contact;
import android.scopus.com.bikeiot.model.data.ContactRepository;

import java.util.List;

import static android.scopus.com.bikeiot.Constants.SHARED_USER_ID;

/**
 *
 */

public class UserContactsPresenter implements UserContactsContract.Presenter {

    private ContactRepository mContactRepository;
    private int mUserId;
    private UserContactsContract.View mView;

    public UserContactsPresenter(ContactRepository contactRepository, SharedPreferences sharedPreferences,
                                 UserContactsContract.View view) {
        mContactRepository = contactRepository;
        mUserId = SharedPreferencesUtils.getIntSharedPreferences(sharedPreferences,
                SHARED_USER_ID, 0);
        mView = view;
    }

    @Override
    public void loadUserContacts() {
        List<Contact> contacts = mContactRepository.getContactsFromUser(mUserId);
        if (!contacts.isEmpty()) {
            mView.showContacts(contacts);
        } else {
            mView.showNoContactRegistered();
        }
    }

    @Override
    public void selectContact(int id) {
        mContactRepository.forceBreakCache();
        Contact contact = mContactRepository.getContact(id);
        if (contact != null) {
            mView.showContactUi(contact.getId());
        } else {
            mView.showErrorToSelectContact();
        }
    }

    @Override
    public void initialize() {
        mContactRepository.forceBreakCache();
        loadUserContacts();
    }
}
