package android.scopus.com.bikeiot.routedetails;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.scopus.com.bikeiot.model.location.LocationUpdater;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.model.data.RouteRepository;
import android.scopus.com.bikeiot.model.MapsHelper;
import android.scopus.com.bikeiot.model.data.UserRepository;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

import static android.scopus.com.bikeiot.utils.SharedPreferencesUtils.LOGGED_USER_ID_SHARED_PREFERENCES_KEY;

/**
 *
 */

public class RouteDetailsPresenter implements RouteDetailsContract.Presenter {

    //TODO: mudar para a rota que esta sendo apresentada para route.getLocationsToExhibition();

    private static final String TAG = RouteDetailsPresenter.class.getSimpleName();
    private final int mLoggedUserId;
    private int mRouteId;
    private RouteRepository mRouteRepository;
    private LocationUpdater mLocationUpdater;
    private UserRepository mUserRepository;
    private RouteDetailsContract.View mView;
    private Location mLastLocation;

    private static final double MIN_DISTANCE_METERS_TO_WARNING = 1000; //1 km

    private LocationUpdater.Callback.OnLastKnownLocation mLastLocationCallback = new LocationUpdater
            .Callback.OnLastKnownLocation() {
            @Override
            public void lastKnownLocationReceived(Location location) {
                Log.d(TAG, "callback lastKnownLocationReceived called");
                mLastLocation = location;
            }
    };
    private LocationUpdater.Callback.OnLocationReceived mLocationReceivedCallback = new LocationUpdater
            .Callback.OnLocationReceived() {
            @Override
            public void locationReceived(Location location) {
                Log.d(TAG, "callback mLocationReceivedCallback  called");

                mLastLocation = location;
                Route route = mRouteRepository.getRoute(mRouteId);
                if (route != null) {
                    double distanceToRoad  = getDistanceToRoute(mLastLocation, route.getStartLatLng());
                    sendDistanceToRouteToUi(distanceToRoad);
                }
                //stop location updates depois de conseguir uma localizacao
                mLocationUpdater.stopLocationUpdates();
            }
    };


    public RouteDetailsPresenter(RouteRepository routeRepository, LocationUpdater locationUpdater,
                                 UserRepository userRepository, RouteDetailsContract.View view,
                                 int routeId, SharedPreferences sharedPreferences) {

        mRouteRepository = routeRepository;
        mUserRepository = userRepository;
        mRouteId = routeId;
        mView = view;
        mLoggedUserId = sharedPreferences.getInt(LOGGED_USER_ID_SHARED_PREFERENCES_KEY, 0);
        mLocationUpdater = locationUpdater;
        //configurando location update
        mLocationUpdater.setupLocationUpdateCallback(mLocationReceivedCallback);
    }

    private void sendDistanceToRouteToUi(double distanceToRoad) {
        Log.d(TAG, "sendDistanceToRouteToUi called");

        mView.showDistanceToRoute(distanceToRoad);
    }

    @Override
    public void initialize() {
        Log.d(TAG, "initialize called");
        loadRoute(mRouteId);
    }

    @Override
    public void loadRoute(int routeId) {
        Log.d(TAG, "loadRoute called");

        mRouteRepository.forceBreakCache();
        Route route = mRouteRepository.getRoute(routeId);

        if (route == null) {
            mView.showNoRouteError();
        } else {
            mView.showRouteDetails(route);

            User user = mUserRepository.getUser(route.getUser().getId());
            if (user != null) {
                mView.showUserDetails(user);

                if (mLoggedUserId == user.getId()) {
                    mView.showDeleteUpdateMenu();
                }

            } else {
                mView.showNoUserDetails();
            }

            loadDistanceToRoad();
        }
    }

    @Override
    public void loadLocation() {
        Log.d(TAG, "loadLocation called");
        mLocationUpdater.startLocationUpdates();
    }

    private double getDistanceToRoute(Location location, LatLng startLocationPoint) {
        Log.d(TAG, "getDistanceToRoute called");
        if (location == null) {
            return 0;
        }

        Location startRouteLocation = new Location("");
        startRouteLocation.setLatitude(startLocationPoint.latitude);
        startRouteLocation.setLongitude(startLocationPoint.longitude);
        return location.distanceTo(startRouteLocation);
    }

    @Override
    public void loadRoutePointLocationBounds() {

        Log.d(TAG, "loadRoutePointLocationBounds called");
        Route route = mRouteRepository.getRoute(mRouteId);
        if (route != null) {
            if (!route.getLocationPoints().isEmpty()) {
                LatLngBounds bounds = prepareLatLngBounds(route.getLocationPoints());
                mView.centerRouteOnMap(bounds);
            }
        }
    }

    @Override
    public void loadDistanceToRoad() {
        Log.d(TAG, "loadDistanceToRoad called");
        //inicializa o locationUpdates e calcula a distancia no mLocationReceivedCallback
        mLocationUpdater.startLocationUpdates();
    }

    @Override
    public void loadRoutePolyline() {
        Log.d(TAG, "loadRoutePolyline called");
        Route route = mRouteRepository.getRoute(mRouteId);
        if (route != null && !route.getLocationPoints().isEmpty()) {
            mView.showRoutePolylineOnMap(createRoutePolyline(route.getLocationPoints()));
            mView.showStartEndMarkers(route.getStartLatLng(), route.getEndLatLng());
        } else {
            Log.d(TAG, "loadRoutePolyline locationPoint is null");
            mView.showNoRoutePolylineLoaded();
        }
    }

    @Override
    public void selectRoute() {
        Route route = mRouteRepository.getRoute(mRouteId);
        if (!route.getLocationPoints().isEmpty()) {
            ArrayList<LatLng> routeLocations = (ArrayList<LatLng>) route.getLocationPoints();

            double distanceToRoute = getDistanceToRoute(mLastLocation, route.getStartLatLng());

            if (distanceToRoute < MIN_DISTANCE_METERS_TO_WARNING) {
                mView.sendSelectedRouteToExerciseRecorderUi(routeLocations);

            } else {
                mView.showSelectRouteDialogConfirmation(distanceToRoute);
            }
        } else {
            mView.showImpossibleToSelect();
        }
    }

    @Override
    public void confirmRouteSelect() {
        Route route = mRouteRepository.getRoute(mRouteId);
        if (!route.getLocationPoints().isEmpty()) {
            ArrayList<LatLng> locationPoints = (ArrayList<LatLng>) route.getLocationPoints();
            mView.sendSelectedRouteToExerciseRecorderUi(locationPoints);
        }
    }

    @Override
    public void removeRoute() {
        if (mRouteRepository.remove(mRouteId)) {
            mView.showRemoveSuccess();
            mView.showRoutesUi();
        } else {
            mView.showRemoveRouteError();
        }
    }

    @Override
    public void prepareToEditRouteUi() {
        mView.showEditRouteUi(mRouteId);
    }


    private LatLngBounds prepareLatLngBounds(List<LatLng> latLngs) {
        return MapsHelper.calculateLatLngBoundsMinCenterDisplacement(latLngs, 200,
                -200, 200, -200);
    }

    private PolylineOptions createRoutePolyline(List<LatLng> locationPoints) {
        PolylineOptions polylineOptions = new PolylineOptions();

        for(LatLng point : locationPoints) {
            polylineOptions.add(point);
            Log.d(TAG, "drawOnMap points lat:" + point.latitude + " long:" + point.longitude);
        }

        polylineOptions.color(Color.BLACK);
        return polylineOptions;
    }
}
