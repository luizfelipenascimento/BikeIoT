package android.scopus.com.bikeiot.exercises;

import android.content.Context;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import static android.scopus.com.bikeiot.utils.Helper.formatDateToTimeString;
import static android.scopus.com.bikeiot.utils.Helper.formatDateToMyDateFormat;

/**
 *
 */

public class ExercisesListAdapter extends RecyclerView.Adapter<ExercisesListAdapter.ExerciseViewHolder> {

    private List<BikeExercise> mBikeExercises;

    private static final String TAG = ExercisesListAdapter.class.getSimpleName();

    private ExercisesListClickListener mExercisesListClickListener;

    interface ExercisesListClickListener {
        void onClickListItem(BikeExercise bikeExercise);
    }

    public ExercisesListAdapter(List<BikeExercise> bikeExercises, ExercisesListClickListener clickListener) {
        mBikeExercises = bikeExercises;
        mExercisesListClickListener = clickListener;
    }

    @Override
    public ExerciseViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForExercisesItemRow = R.layout.exercises_item_row;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForExercisesItemRow, viewGroup, shouldAttachToParentImmediately);

        return new ExerciseViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ExerciseViewHolder holder, int position) {
        BikeExercise bikeExercise = mBikeExercises.get(position);

        holder.mExerciseTimeDurationTextView.setText(formatDateToTimeString(bikeExercise.getResult().getTotalTimeChronometer()));
        holder.mExerciseDescriptionTextView.setText(bikeExercise.getId() + " " + bikeExercise.getDescription());
        holder.mExerciseDateTextView.setText(formatDateToMyDateFormat(bikeExercise.getExerciseDate()));
        holder.itemView.setTag(bikeExercise.getId());
    }

    @Override
    public int getItemCount() {
        return mBikeExercises.size();
    }


    class ExerciseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView mExerciseDateTextView;
        TextView mExerciseDescriptionTextView;
        TextView mExerciseTimeDurationTextView;

        public ExerciseViewHolder(View itemView) {
            super(itemView);

            mExerciseDateTextView = (TextView) itemView.findViewById(R.id.exercise_date_text_view);
            mExerciseDescriptionTextView = (TextView) itemView.findViewById(R.id.exercise_description_text_view);
            mExerciseTimeDurationTextView = (TextView) itemView.findViewById(R.id.exercise_duration_time_text_view);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Log.d(TAG, "ExerciseViewHolder onclick: #" + position);
            mExercisesListClickListener.onClickListItem(mBikeExercises.get(position));
        }
    }



}
