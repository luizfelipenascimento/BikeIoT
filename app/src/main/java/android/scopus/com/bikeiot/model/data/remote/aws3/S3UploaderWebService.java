package android.scopus.com.bikeiot.model.data.remote.aws3;

import android.content.Context;
import android.scopus.com.bikeiot.Constants;
import android.scopus.com.bikeiot.model.network.NetworkHelper;
import android.util.Log;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;

import java.io.File;
import java.io.FileNotFoundException;

import static android.scopus.com.bikeiot.Constants.KEY_S3;
import static android.scopus.com.bikeiot.Constants.SECRET_S3;

/**
 *
 */

public class S3UploaderWebService {

    private static final String TAG = S3UploaderWebService.class.getSimpleName();
    private static final int CONNECTION_TIMEOUT = 5000;
    private static final int SOCKET_TIMEOUT = 10000;

    private AmazonS3Client s3Client;
    private BasicAWSCredentials credentials;
    private NetworkHelper mNetworkHelper;

    private S3Service.OnUploadedFileListener mOnUploadListener;

    S3UploaderWebService() {
        credentials = new BasicAWSCredentials(KEY_S3, SECRET_S3);
        ClientConfiguration clientConfiguration = new ClientConfiguration()
                .withConnectionTimeout(CONNECTION_TIMEOUT)
                .withSocketTimeout(SOCKET_TIMEOUT);
        s3Client = new AmazonS3Client(credentials, clientConfiguration);
    }

    public boolean uploadFileBlocking(File file) throws FileNotFoundException {
        Log.d(TAG, "uploadFile called");

        if (!file.exists() || file.isDirectory()) {
            throw new FileNotFoundException();
        }

        try {
            PutObjectRequest putRequest = new PutObjectRequest(Constants.S3_BUCKET_NAME, file.getName(), file);
            s3Client.putObject(putRequest);
            return true;
        } catch (AmazonServiceException e) {
            Log.e(TAG, "AmazonClientException " + e);
        } catch (AmazonClientException e) {
            Log.e(TAG, "AmazonClientException " + e);
        }

        return false;
    }
}
