package android.scopus.com.bikeiot.model.network.googleplacesAPI;

import android.location.Location;
import android.os.AsyncTask;
import android.scopus.com.bikeiot.model.beans.Place;
import android.scopus.com.bikeiot.model.network.ApiListenerBasic;
import android.scopus.com.bikeiot.model.network.NetworkHelper;
import android.util.Log;

import java.util.List;

/**
 *
 */

public class GooglePlacesAsyncTask extends AsyncTask<String, Void, List<Place>> {

    private static final String TAG = GooglePlacesAsyncTask.class.getSimpleName();

    private NetworkHelper mNetWorkHelper;
    private boolean networkProblem;

    private CallbackOnResponseListener mCallbackListener;

    public interface CallbackOnResponseListener extends ApiListenerBasic {
        void onResponse(List<Place> places);
        void onFailResponse();

    }

    public GooglePlacesAsyncTask(NetworkHelper networkHelper, CallbackOnResponseListener callback) {
        Log.d(TAG, "GooglePlacesAsyncTask called");
        mNetWorkHelper = networkHelper;
        mCallbackListener = callback;
    }

    @Override
    protected List<Place> doInBackground(String... strings) {
        Log.d(TAG, "doInBackground called");
        if (strings.length < 0 && (strings[0] == null)) {
            return null;
        }

        String url = strings[0];

        if (!mNetWorkHelper.isNetworkConnected()) {
            Log.d(TAG, "doInBackground mNetWorkHelper called");
            networkProblem = true;
            return null;
        }

        List<Place> places = new GooglePlacesQueryUtils().fetchDataClosestPlaces(url);
        return places;
    }

    @Override
    protected void onPostExecute(List<Place> places) {
        if (places != null) {
            mCallbackListener.onResponse(places);
            return;
        }

        if (networkProblem) {
            mCallbackListener.onNetworkProblem();
        }

        if (!networkProblem) {
            mCallbackListener.onFailResponse();
        }
    }
}
