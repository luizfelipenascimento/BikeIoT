package android.scopus.com.bikeiot.model.data;

import android.scopus.com.bikeiot.model.beans.User;

/**
 * Created by Felipe on 2/18/2018.
 */

public interface UserDataSource {

    User getUser(int userId);

    User getUserByEmail(String email);

    boolean saveUser(User user);

    boolean update(User user);
}
