package android.scopus.com.bikeiot.exerciserecoder;

/**
 * Created by Luiz Felipe on 09/01/2018.
 */

public interface ExerciseRecorderContract {

    interface View {
        void showExerciseResumeUi(int bikeExerciseId);
        void showStartExercise();
        void showExerciseControl();
        void requestBikeDeviceUiForResult();
        void showHelpToHospitalUi();
        void showPanicButtonConfirmationDialog();
        void stopExerciseRecorderService();
        void showSelectedBikeDeviceIcon();
        void showNoBikeDeviceSelectedIcon();
    }

    interface Presenter {

    }
}
