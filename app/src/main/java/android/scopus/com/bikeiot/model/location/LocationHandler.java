package android.scopus.com.bikeiot.model.location;

import android.location.Location;
import android.util.Log;

public class LocationHandler {

    private static final String TAG = "LocationHandler";
    public static final long ONE_MINUTE = 1000 * 60;

    public boolean isLocationBetter(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            Log.d(TAG, "currentBestLocation is null");
            return true;
        }

        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > ONE_MINUTE;
        boolean isSignificantlyOlder = timeDelta < - ONE_MINUTE;
        boolean isNewer = timeDelta > 0;

        if (isSignificantlyNewer) {
            Log.d(TAG, "isSignificantlyNewer");
            return true;
        } else if (isSignificantlyOlder) {
            Log.d(TAG, "isSignificantlyOlder");
            return false;
        }

        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        if (isMoreAccurate) {
            Log.d(TAG, "isMoreAccurate");
            return true;
        } else if (isNewer && !isLessAccurate) {
            Log.d(TAG, "isNewer and !isLessAccurate");
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate) {
            Log.d(TAG, "isNewer and !isSignificantlyLessAccurate");
            return true;
        }

        Log.d(TAG, "is not a better location");
        return false;
    }

}
