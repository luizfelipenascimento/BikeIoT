package android.scopus.com.bikeiot.editroute;

import android.content.Intent;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.data.RouteRepository;
import android.scopus.com.bikeiot.model.data.local.RouteDAO;
import android.scopus.com.bikeiot.routedetails.RouteDetailsActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class EditRouteActivity extends AppCompatActivity implements EditRouteContract.View {

    public static final int REQUEST_ROUTE_UPDATE_ID = 1011;

    private Toolbar mToolbar;
    private EditRoutePresenter mPresenter;
    private EditText mRouteDescriptionEditText;
    private Switch mShareSwitch;
    private TextView mDescriptionErrorTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_route);

        //setting up toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mRouteDescriptionEditText = (EditText) findViewById(R.id.route_description_text_view);
        mShareSwitch = (Switch) findViewById(R.id.route_shared_switch);
        mDescriptionErrorTextView = (TextView) findViewById(R.id.route_description_error_text_view);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.edit_route_activity_title);

        int routeId = getIntent().getIntExtra(RouteDetailsActivity.EXTRA_ROUTE_ID, 0);

        mPresenter = new EditRoutePresenter(new RouteRepository(new RouteDAO(this)), routeId, this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.confirm_menu_item:
                hideDescriptionEmptyError();
                String description = mRouteDescriptionEditText.getText().toString();
                boolean shareChecked = mShareSwitch.isChecked();
                mPresenter.update(description, shareChecked);
                return true;
            case android.R.id.home:
                Intent home = NavUtils.getParentActivityIntent(this);
                home.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                NavUtils.navigateUpTo(this, home);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.confirm_menu, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.initialize();
    }

    @Override
    public void showRouteDetails(Route route) {
        mRouteDescriptionEditText.setText(route.getDescription());
        mShareSwitch.setChecked(route.getShared() == 0 ? false : true);
    }

    @Override
    public void showNoRouteError() {
        //TODO: colocar tela de erro quando nao carregar a rota
    }

    @Override
    public void sendResponseForRouteDetailsUi(int routeId) {
        Intent intent = new Intent();
        intent.putExtra(RouteDetailsActivity.EXTRA_ROUTE_ID, routeId);
        setResult(REQUEST_ROUTE_UPDATE_ID);
        finish();
    }

    @Override
    public void showUpdateError() {
        Toast.makeText(this, R.string.update_fail, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmptyDescriptionError(int errorMsgId) {
        mDescriptionErrorTextView.setVisibility(View.VISIBLE);
        mDescriptionErrorTextView.setText(errorMsgId);
    }

    @Override
    public void showUpdateSuccess() {
        Toast.makeText(this, R.string.update_success, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideDescriptionEmptyError() {
        mDescriptionErrorTextView.setVisibility(View.GONE);
    }

}
