package android.scopus.com.bikeiot.panicbutton;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.util.Log;


/**
 *
 */

public class PanicButtonReceiver extends BroadcastReceiver {

    private static final String TAG = "PanicButtonReceiver";

    public PanicButtonReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive called");
        if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
            Log.d(TAG, "VOLUME_CHANGED_ACTION called");
        }
    }
}
