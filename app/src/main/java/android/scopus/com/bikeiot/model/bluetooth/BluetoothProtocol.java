package android.scopus.com.bikeiot.model.bluetooth;

import android.util.Log;


public class BluetoothProtocol {

    private static final String TAG = "BluetoothProtocol";

    public static final String FIRST_RESET_KEY_WORD = "FIRSTRESET";

    protected final String START_INDICATOR;
    protected final String END_INDICATOR;
    private StringBuilder mStringBuilder;
    private String mCompleteData;
    private boolean mMessageCompleted = false;
    private boolean mReadyToReceive = true;

    public BluetoothProtocol(String start, String end) {
        START_INDICATOR = start;
        END_INDICATOR = end;
        mStringBuilder = new StringBuilder();
    }

    public void setMessageCompleted(boolean completed) {
        mMessageCompleted = completed;
    }

    public void reset() {
        Log.d(TAG, "reset() called");
        mMessageCompleted = false;
        mReadyToReceive = true;
    }

    public void handleInputMessage(String message) {
        Log.d(TAG, "handleInputMessage message received: " + message);
        mStringBuilder.append(message);
        if(mStringBuilder.indexOf(END_INDICATOR) > 0) {
            if(mStringBuilder.indexOf(START_INDICATOR) >= 0) {
                mCompleteData = mStringBuilder.substring(mStringBuilder.indexOf(START_INDICATOR)
                        + START_INDICATOR.length(), mStringBuilder.indexOf(END_INDICATOR));
                mMessageCompleted = true;
                Log.d(TAG, "handleInputMessage: complete data " + mCompleteData);
            }
            mStringBuilder.delete(0, mStringBuilder.length());
        }
    }

    public boolean isMessageCompleted() {
        return mMessageCompleted;
    }

    public String getCompleteData() {
        return mCompleteData;
    }

    public void setReadyToReceive(boolean ready) {
        mReadyToReceive = ready;
    }

    public boolean isReadyToReceive() {
        return mReadyToReceive;
    }

}
