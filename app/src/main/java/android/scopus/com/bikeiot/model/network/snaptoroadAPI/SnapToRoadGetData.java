package android.scopus.com.bikeiot.model.network.snaptoroadAPI;

import android.content.Context;
import android.content.Intent;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.exerciserecoder.ExerciseRecorderService;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luiz Felipe on 16/03/2018.
 */

public class SnapToRoadGetData implements Runnable {

    public static final String ACTION_SEND_SNAPPED_LOCATION_POINTS =
            "android.scopus.com.bikeiot.model.ACTION_SEND_SNAPPED_LOCATION_POINTS";
    public static final String EXTRA_LOCATIONS = "EXTRA_LOCATIONS";

    private static final String BASE_URL_REQUEST = "https://roads.googleapis.com/v1/snapToRoads?path=";
    private static final String TAG = SnapToRoadGetData.class.getSimpleName();

    private final Context mContext;
    private List<LatLng> mLocationsCoordinates;
    private int mMinBoundIndex = 0;
    public final static  int PAGINATION_OVERLAP = 5;

    public SnapToRoadGetData(Context context, List<LatLng> locationsPoints, int minBoundIndex) {
        Log.d(TAG, "creating SnapToRoadGetData");
        mContext = context;
        mLocationsCoordinates = locationsPoints;
        mMinBoundIndex = minBoundIndex;
    }

    public SnapToRoadGetData(Context context) {
        mContext = context;
    }

    public void requestSnappedData(List<LatLng> locationsCoordinates) {
        mLocationsCoordinates = locationsCoordinates;
        new Thread(this).start();
        if (mLocationsCoordinates.size() - mMinBoundIndex >= PAGINATION_OVERLAP) {
            mMinBoundIndex = mLocationsCoordinates.size() - 1;
        }
    }

    @Override
    public void run() {
        Log.d(TAG, "SnapToRoadGetData run called");

        String urlString = SnapToRoadQueryUtils.getUrlString(BASE_URL_REQUEST,
                mLocationsCoordinates.subList(mMinBoundIndex, mLocationsCoordinates.size() -1),
                mContext.getString(R.string.snap_to_road_api_key));

        ArrayList<LatLng> locations = SnapToRoadQueryUtils.fetchDataSnapToRoad(urlString);
        sendNormalizedLocations(locations);

        //testing
        if (!ExerciseRecorderService.isRunning()) {
            sendNormalizedLocationsTest(locations);
        }
    }

    private void sendNormalizedLocations(ArrayList<LatLng> locationsPoints) {
        Intent intent = new Intent(ACTION_SEND_SNAPPED_LOCATION_POINTS);
        intent.putExtra(EXTRA_LOCATIONS, locationsPoints);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }


    //testing
    public static final String ACTION_SEND_SNAPPED_POINTS_TEST =
            "ACTION_SEND_SNAPPED_POINTS_TEST";

    private void sendNormalizedLocationsTest(ArrayList<LatLng> locationsPoints) {
        Intent intent = new Intent(ACTION_SEND_SNAPPED_POINTS_TEST);
        intent.putExtra(EXTRA_LOCATIONS, locationsPoints);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
    }

}
