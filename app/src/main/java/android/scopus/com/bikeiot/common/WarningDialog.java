package android.scopus.com.bikeiot.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.scopus.com.bikeiot.R;
import android.view.LayoutInflater;
import android.view.View;

/**
 *
 */

public class WarningDialog extends DialogFragment {

    public static final String WARNING_DIALOG_TAG = "WarningDialog";
    private static final String CONFIRMATION_CONTENT = "warning_info";
    private static final String TITLE_INFO = "title";

    public static WarningDialog newInstance(String content, String title,
                                            WarningDialog.Listener listener) {
        WarningDialog dialog = new WarningDialog();
        dialog.setListener(listener);
        Bundle args = new Bundle();
        args.putString(CONFIRMATION_CONTENT, content);
        args.putString(TITLE_INFO, title);
        dialog.setArguments(args);
        return dialog;
    }

    private void setListener(WarningDialog.Listener listener) {
        mListener = listener;
    }

    public interface Listener {
        void onOkClick();
    }

    private WarningDialog.Listener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Holo_Light_Dialog);
        String content = getArguments().getString(CONFIRMATION_CONTENT);
        String title = getArguments().getString(TITLE_INFO);

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());

        alertBuilder.setTitle(title)
                .setMessage(content)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onOkClick();
                    }
                });

        return alertBuilder.create();
    }

}
