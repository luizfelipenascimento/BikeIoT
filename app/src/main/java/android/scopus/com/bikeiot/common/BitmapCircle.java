package android.scopus.com.bikeiot.common;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by Luiz Felipe on 31/03/2018.
 */

public class BitmapCircle {

    private final int strokeColor;
    private final int fillColor;
    private Paint fillPaint;
    private Paint strokePaint;

    private int weight;
    private int height;
    private float radius;
    private Bitmap circleBitmap;

    public BitmapCircle(int weight, int height, int fillColor, int strokeColor) {
        this.weight = weight;
        this.height = height;
        this.radius = (weight/2 - ((weight/2) * 0.2f));
        this.fillColor = fillColor;
        this.strokeColor = strokeColor;
        strokePaint = new Paint();
        fillPaint = new Paint();
        createCircleBitmap();
    }

    public void createCircleBitmap() {
        circleBitmap = Bitmap.createBitmap(height, weight, Bitmap.Config.ARGB_8888);
        circleBitmap = circleBitmap.copy(circleBitmap.getConfig(), true);
        Canvas canvas = new Canvas(circleBitmap);

        fillPaint.setStyle(Paint.Style.FILL);
        fillPaint.setColor(fillColor);

        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setColor(strokeColor);
        strokePaint.setStrokeWidth(7);

        canvas.drawCircle(weight/2, height / 2, radius, fillPaint);
        canvas.drawCircle(weight/2, height /2 , radius, strokePaint);
    }

    public Bitmap getCircleBitmap() {
        return circleBitmap;
    }
}
