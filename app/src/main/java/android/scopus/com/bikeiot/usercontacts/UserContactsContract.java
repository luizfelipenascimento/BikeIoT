package android.scopus.com.bikeiot.usercontacts;

import android.scopus.com.bikeiot.model.beans.Contact;

import java.util.List;

/**
 *
 */

public interface UserContactsContract {

    interface View {
        void showContacts(List<Contact> contacts);
        void showNoContactRegistered();
        void showContactUi(int id);
        void showErrorToSelectContact();
        void showRegisterContactUi();
    }

    interface Presenter {
        void loadUserContacts();
        void selectContact(int id);
        void initialize();
    }
}
