package android.scopus.com.bikeiot.routes;

import android.scopus.com.bikeiot.model.beans.Route;

import java.util.List;

/**
 *
 */

public interface RoutesContract {

    interface View {
        void showRoutes(List<Route> routes);
        void showNoRoutes();
        void showRouteDetailsUi(int routeId);
        void showErrorInSelectProcess(int errorId);
    }

    interface Presenter {
        void initialize();
        void loadRoutes();
        void loadRoutesDoneByUser(int userId);
        void selectRoute(int routeId);
        void loadSharedRoutes();
    }

}
