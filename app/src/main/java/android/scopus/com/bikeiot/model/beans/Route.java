package android.scopus.com.bikeiot.model.beans;

import android.graphics.Bitmap;
import android.scopus.com.bikeiot.model.filerelated.CSVFile;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Luiz Felipe on 30/11/2017.
 */

public class Route implements Serializable {

    private static final String TAG = "Route";

    private int mId;
    private double mDistance;
    private LatLng mStartLatLng;
    private LatLng mEndLatLng;
    private String mFileRoutePath;
    private CSVFile mRouteCSVFile;
    private String mDescription;
    private Date mDate;
    private int lastIndexOffset = 0;
    private String imagePath;
    private Bitmap imageBitmap;
    private User mUser;
    private PolylineOptions mPolylineOptions;


    /**
     * shared values :
     * 0 = not shared - nao compartilhado/publicado
     * 1 = to share - compartilhado/publicado
     */
    private int shared;

    private List<LatLng> mLocationPoints;
    private ArrayList<LatLng> mLocationsNormalized;

    public Route() {
        mLocationPoints = new ArrayList<>();
        mLocationsNormalized = new ArrayList<>();
    }

    public void addLocationToNormalizedLocation(LatLng latLng) {
        mLocationsNormalized.add(latLng);
    }

    public void addNewLocationPoint(LatLng point) {
        mLocationPoints.add(point);
    }

    //TODO: (route) implementar o metodo para calcular a distancia
    public double calculateDistance() {
        return 0;
    }

    public List<LatLng> getLocationPoints() {
        return  mLocationPoints;
    }

    public double getDistance() {
        return mDistance;
    }

    public User getUser() {
        return mUser;
    }

    public LatLng getStartLatLng() {
        return mStartLatLng;
    }

    public LatLng getEndLatLng() {
        return mEndLatLng;
    }

    public int getId() {
        return mId;
    }

    public String getRouteCsvFilePath() {
        return mFileRoutePath;
    }

    public CSVFile getRouteCSVFile() {
        return mRouteCSVFile;
    }

    public String getDescription() {
        return mDescription;
    }

    public ArrayList<LatLng> getLocationsNormalized() {
        return mLocationsNormalized;
    }

    public int getLastIndexOffset() {
        return lastIndexOffset;
    }

    public Date getDate() {
        return mDate;
    }

    public String getImagePath() {
        return imagePath;
    }

    public Bitmap getImageBitmap() {
        return imageBitmap;
    }

    public int getShared() {
        return shared;
    }

    public PolylineOptions getPolylineOptions() {
        return mPolylineOptions;
    }

    public void setPolylineOptions(PolylineOptions polylineOptions) {
        mPolylineOptions = polylineOptions;
    }

    public void setShared(int shared) {
        this.shared = shared;
    }

    public void setUser(User user) {
        mUser = user;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public void setLastIndexOffset(int lastIndexOffset) {
        this.lastIndexOffset = lastIndexOffset;
    }

    public void setLocationsNormalized(ArrayList<LatLng> locationsNormalized) {
        mLocationsNormalized = locationsNormalized;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setImageBitmap(Bitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public void setRouteCSVFile(CSVFile routeCSVFile) {
        mRouteCSVFile = routeCSVFile;
    }

    public void setFileRoutePath(String fileRoutePath) {
        mFileRoutePath = fileRoutePath;
    }

    public void setDistance(double distance) {
        mDistance = distance;
    }

    public void setLocationPoints(List<LatLng> locationPoints) {
        mLocationPoints = locationPoints;
    }

    public void setStartLatLng(LatLng startLatLng) {
        mStartLatLng = startLatLng;
    }

    public void setEndLatLng(LatLng endLatLng) {
        mEndLatLng = endLatLng;
    }

    public void setDescription(String description) {
        mDescription = description;
    }
}
