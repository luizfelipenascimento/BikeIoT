package android.scopus.com.bikeiot.panicbutton;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.scopus.com.bikeiot.R;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class CallHelpDialogActivity extends Activity {

    private static final String TAG = "CallHelpDialogActivity";
    public static final String ACTION_CLOSE = "android.scopus.com.bikeiot.panicbutton.ACTION_CLOSE";

    private Button mCancelButton;

    private BroadcastReceiver mBroadcastDialogReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION_CLOSE)) {
                CallHelpDialogActivity.this.finish();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_help_dialog);
        setFinishOnTouchOutside(false);

        Log.d(TAG, "registering mBroadcastDialogReceiver");
        IntentFilter intentFilter = new IntentFilter(ACTION_CLOSE);
        registerReceiver(mBroadcastDialogReceiver, intentFilter);

        mCancelButton = (Button) findViewById(R.id.cancel_button);

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "mCancelButton clicked");
                cancelWarningEventOnService();
                CallHelpDialogActivity.this.finish();
            }
        });
    }

    private void cancelWarningEventOnService() {
        Intent intentCallHelpService = new Intent(this, CallHelpService.class);
        intentCallHelpService.setAction(CallHelpService.ACTION_CANCEL_WARNING_EVENT);
        startService(intentCallHelpService);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy called");
        unregisterReceiver(mBroadcastDialogReceiver);
    }
}
