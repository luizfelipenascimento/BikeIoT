package android.scopus.com.bikeiot.usercontact;

import android.content.SharedPreferences;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.utils.SharedPreferencesUtils;
import android.scopus.com.bikeiot.model.InputHandler;
import android.scopus.com.bikeiot.model.beans.Contact;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.model.data.ContactRepository;
import android.scopus.com.bikeiot.model.data.UserRepository;
import android.util.Log;

import static android.scopus.com.bikeiot.utils.SharedPreferencesUtils.LOGGED_USER_ID_SHARED_PREFERENCES_KEY;

/**
 *
 */

public class UserContactEditCreatePresenter implements UserContactEditCreateContract.Presenter {

    private static final String TAG = UserContactEditCreatePresenter.class.getSimpleName();
    private final UserContactEditCreateContract.View mView;
    private final int mContactId;
    private ContactRepository mContactRepository;
    private int userId;
    private UserRepository mUserRepository;

    public UserContactEditCreatePresenter(ContactRepository contactRepository, UserRepository userRepository,
                                          int contactId,
                                          SharedPreferences sharedPreferences,
                                          UserContactEditCreateContract.View view) {
        Log.d(TAG, "UserContactEditCreatePresenter called");

        mContactRepository = contactRepository;
        mContactId = contactId;
        mUserRepository = userRepository;
        this.userId = SharedPreferencesUtils.getIntSharedPreferences(sharedPreferences,
                LOGGED_USER_ID_SHARED_PREFERENCES_KEY, 0);
        mView = view;

        Log.d(TAG, "contact id : " + contactId);
        Log.d(TAG, "userId : " + userId);
    }

    @Override
    public void persist(String name, String phone) {
        Log.d(TAG, "persist called");

        //limpando a mascara do telefone
        phone = phone.replaceAll("[-\\s()]", "");

        if (!isInputCorrect(name, phone)) {
            return;
        }

        if (isContactNew()) {
            saveContact(name, phone);
        } else {
            updateContact(name, phone);
        }
    }

    private User getUser() {
        User user = mUserRepository.getUser(userId);
        if (user == null) {
            mView.showError(R.string.user_not_logged);
        }
        return user;
    }

    private void updateContact(String name, String phone) {
        Contact contact = mContactRepository.getContact(mContactId);
        contact.setName(name);
        contact.setPhoneNumber(phone);

        if (mContactRepository.edit(contact)) {
            mView.showSuccessMsg(R.string.edit_contact_success);
            mView.showContactsFromResponseUi();
        } else {
            Log.d(TAG, "");
            mView.showSuccessMsg(R.string.error_update_contact);
        }
    }

    private void saveContact(String name, String phone) {
        User user = getUser();
        if (user == null) {
            mView.showError(R.string.user_not_logged);
            return;
        }

        Contact contact = new Contact();
        contact.setName(name);
        contact.setPhoneNumber(phone);
        contact.setUser(user);

        if (mContactRepository.save(contact)) {
            mView.showSuccessMsg(R.string.add_contact_success);
            mView.showContactsFromResponseUi();
        } else {
            mView.showSuccessMsg(R.string.error_save_new_contact);
        }
    }

    @Override
    public void removeContact() {
        if (mContactRepository.remove(mContactId)) {
            mView.showSuccessMsg(R.string.remove_contact_success);
            mView.showContactsFromResponseUi();
        } else {
            mView.showError(R.string.general_process_error_msg);
        }
    }

    @Override
    public void initialize() {
        if (!isContactNew()) {
            Log.d(TAG, "contact is not new");
            Contact contact = mContactRepository.getContact(mContactId);
            mView.showContactInfo(contact);
            mView.forceMenuShowDeleteOption();
        }
    }

    private boolean isContactNew() {
        return mContactId == 0;
    }

    private boolean isInputCorrect(String name, String phone) {
        boolean result = true;
        if (InputHandler.isEmptyOrNull(name)) {
            mView.showNameError(R.string.empty_name_error);
            result = false;
        }

        if (InputHandler.isEmptyOrNull(phone)) {
            mView.showPhoneError(R.string.empty_phone_error);
            result = false;
        }

        if (!InputHandler.isPhoneNumberValid(phone)) {
            mView.showPhoneError(R.string.phone_format_not_valid);
            result = false;
        }

        return result;
    }
}
