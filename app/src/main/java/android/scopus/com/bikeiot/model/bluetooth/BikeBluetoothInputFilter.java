package android.scopus.com.bikeiot.model.bluetooth;

import android.util.Log;

/**
 *
 */

public class BikeBluetoothInputFilter {

    private static final String TAG = BikeBluetoothInputFilter.class.getSimpleName();
    private String mResultString;
    private double[] mAccelerationInfo;
    private double[] mGyroscopeInfo;
    private int mRotations;
    private double mPitch = 0;
    private double mRoll = 0;
    private double mLinearAccelerationAcc = 0;
    private double mAverageAccZ = 0;
    private double mAverageAccY = 0;
    private String mAccidentStatus = "N";

    public void setResultString(String resultString) {
        Log.d(TAG, "setResultString() called");
        resetAccelerometerInfoArray();
        resetGyroscopeInfoArray();
        resetRotations();
        mResultString = resultString;
        organizeInformation();
        cleanIdentifiers(resultString);
    }

    public void resetRotations() {
        mRotations = 0;
    }

    private void resetAccelerometerInfoArray() {
        Log.d(TAG, "resetAccelerometerInfoArray called");
        mAccelerationInfo = new double[3];
    }

    private void resetGyroscopeInfoArray() {
        Log.d(TAG, "resetGyroscopeInfoArray called");
        mGyroscopeInfo = new double[3];
    }

    private void organizeInformation() {
        String[] infos = mResultString.split(";");
        Log.d(TAG, "organizeInformation mResultString: " + mResultString);
        for(int i = 0; i < infos.length; i++) {
            String infoType = infos[i].substring(0, infos[i].indexOf(":"));
            Log.d(TAG, "organizeInformation() infoType: " + infoType);
            switch (infoType) {
                case "A": setAccelerometerInfo(infos[i]);
                    break;
                case "G": setGyroscopeInfo(infos[i]);
                    break;
                case "H": configRotationInfo(infos[i]);
                    break;
                case "O": configRollInfo(infos[i]);
                    break;
                case "P": configPitchInfo(infos[i]);
                    break;
                case "LAC": configLinearAccelerationAccInfo(infos[i]);
                    break;
                case "MAZ": configAverageAccelerometerZ(infos[i]);
                    break;
                case "MAY": configAverageAccelerometerY(infos[i]);
                    break;
                case "ACDNT": configAccidentInfo(infos[i]);
                    break;
            }
        }
    }

    public void configRollInfo(String roll) {
        roll = cleanIdentifiers(roll);
        Log.d(TAG, "configRollInfo : " + roll);
        mRoll = Double.parseDouble(roll);
    }

    private void configAccidentInfo(String info) {
        info = cleanIdentifiers(info);
        Log.d(TAG, "configAverageAccelerometerZ: " + info);
        mAccidentStatus = info;
    }

    private void configAverageAccelerometerY(String info) {
        info = cleanIdentifiers(info);
        Log.d(TAG, "configAverageAccelerometerZ: " + info);
        mAverageAccY = Double.parseDouble(info);
    }

    private void configAverageAccelerometerZ(String info) {
        info = cleanIdentifiers(info);
        Log.d(TAG, "configAverageAccelerometerZ: " + info);
        mAverageAccZ = Double.parseDouble(info);
    }

    private void configLinearAccelerationAccInfo(String info) {
        info = cleanIdentifiers(info);
        Log.d(TAG, "configLinearAccelerationAccInfo: " + info);
        mLinearAccelerationAcc = Double.parseDouble(info);
    }

    private void configPitchInfo(String info) {
        info = cleanIdentifiers(info);
        Log.d(TAG , "configPitchInfo : pitch: " + info);
        mPitch = Double.parseDouble(info);
    }

    public void setAccelerometerInfo(String accelerometerInfo) {

        String onlyNumericValues = cleanIdentifiers(accelerometerInfo);

        Log.d(TAG, "setAccelerometerInfo() - informacao sem identificadores " + onlyNumericValues);
        String[] accelerometerXYZ = onlyNumericValues.split(",");

        Log.d(TAG, "setAccelerometerInfo: colocando info no array de inteiros");
        for(int i = 0; i < accelerometerXYZ.length; i++) {

            String indexType = accelerometerXYZ [i].substring(0, accelerometerXYZ [i].indexOf(":"));
            Log.d(TAG, "setGyroscopeInfo indexType: " + indexType);

            switch (indexType) {
                case "x":
                    double x = Double.parseDouble(cleanIdentifiers(accelerometerXYZ[i]));
                    Log.d(TAG, "setGyroscopeInfo value x : " + x);
                    mAccelerationInfo[0] = x;
                    break;
                case "y":
                    double y = Double.parseDouble(cleanIdentifiers(accelerometerXYZ[i]));
                    Log.d(TAG, "setGyroscopeInfo value y : " + y);
                    mAccelerationInfo[1] = y;
                    break;
                case "z":
                    double z = Double.parseDouble(cleanIdentifiers(accelerometerXYZ[i]));
                    Log.d(TAG, "setGyroscopeInfo value z : " + z);
                    mAccelerationInfo[2] = z;
                    break;
                default: Log.e(TAG, "setGyroscopeInfo - nenhum index foi passado");
            }
        }
    }

    private void setGyroscopeInfo(String gyroscopeInfo) {

        String onlyNumericValues = cleanIdentifiers(gyroscopeInfo);
        Log.d(TAG, "setGyroscopeInfo() - informacao sem identificadores " + onlyNumericValues);

        String[] gyroscopeXYZ = onlyNumericValues.split(",");

        Log.d(TAG, "setGyroscopeInfo: colocando info no array de inteiros");
        for(int i = 0; i < gyroscopeXYZ.length; i++) {

            String indexType = gyroscopeXYZ[i].substring(0, gyroscopeXYZ[i].indexOf(":"));
            Log.d(TAG, "setGyroscopeInfo indexType: " + indexType);

            switch (indexType) {
                case "x":
                    double x = Double.parseDouble(cleanIdentifiers(gyroscopeXYZ[i]));
                    Log.d(TAG, "setGyroscopeInfo value x : " + x);
                    mGyroscopeInfo[0] = x;
                    break;
                case "y":
                    double y = Double.parseDouble(cleanIdentifiers(gyroscopeXYZ[i]));
                    Log.d(TAG, "setGyroscopeInfo value y : " + y);
                    mGyroscopeInfo[1] = y;
                    break;
                case "z":
                    double z = Double.parseDouble(cleanIdentifiers(gyroscopeXYZ[i]));
                    Log.d(TAG, "setGyroscopeInfo value z : " + cleanIdentifiers(gyroscopeXYZ[i]));
                    mGyroscopeInfo[2] = z;
                    break;
                default: Log.e(TAG, "setGyroscopeInfo - nenhum index foi passado");
            }
        }
    }

    public void configRotationInfo(String rotationString) {
        rotationString = cleanIdentifiers(rotationString);
        Log.d(TAG, "setStringRotationValuesToVelocity: rotationString result: " + rotationString);
        mRotations = Integer.parseInt(rotationString);
    }

    private String cleanIdentifiers(String info) {
        return info.substring(info.indexOf(":") + 1, info.length());
    }

}
