package android.scopus.com.bikeiot.bikebluetoothdevice;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.scopus.com.bikeiot.model.beans.BikeDevice;
import android.util.Log;

import java.util.ArrayList;
import java.util.Set;

/**
 *
 */

public class BikeDevicesPresenter implements BikeDevicesContract.Presenter {

    //TODO: FIX O PAREAMENTO AUTOMATICO - ESTA PAREANTO TODOS OS DEVICES ATE PELA SELECAO DO ANDROID

    private static final String TAG = "BikeDevicesPresenter";
    private BluetoothAdapter mBluetoothAdapter;
    private ArrayList<BluetoothDevice> mBluetoothBikeDevices;
    private BikeDevicesContract.View mView;
    private BluetoothDevice mSelectedBluetoothDevice;
    private Context mContext;
    private boolean isForResponse = false;
    private static final String BIKE_TAG = "bike";
    private Handler mSearchBluetoothTimerHandler;

    private Runnable mRunnableTimer = new Runnable() {
        @Override
        public void run() {

            if (mBluetoothAdapter.isDiscovering()) {
                Log.d(TAG, "cancel bluetooth Discovering");
                mBluetoothAdapter.cancelDiscovery();
            }

            if (mBluetoothBikeDevices.isEmpty()) {
                Log.d(TAG, "no bluetooth device was found");
                mView.hideSearchingBluetoothProgress();
                mView.displayNoBluetoothDevicesFound();
            } else {
                Log.d(TAG, "there is bluetooth devices");
            }
        }
    };

    BikeDevicesPresenter(BikeDevicesContract.View view, Context context,
                         ComponentName componentName){


        mContext = context;
        mView = view;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mBluetoothBikeDevices = new ArrayList<>();

        if (componentName != null) {
            isForResponse = true;
        }

        if(mBluetoothAdapter == null) {
            mView.showBluetoothNotAvailable();
        } else {
            if(!mBluetoothAdapter.isEnabled()) {
                mView.showBluetoothRequestEnableIntent();
            }
        }

        IntentFilter discoverBTIntentFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        mContext.registerReceiver(discoverBtBroadcastReceiver, discoverBTIntentFilter);

        IntentFilter nameChangedBtIntentFilter = new IntentFilter(BluetoothDevice.ACTION_NAME_CHANGED);
        mContext.registerReceiver(discoverBtBroadcastReceiver, nameChangedBtIntentFilter);

        IntentFilter bondDeviceFilter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        mContext.registerReceiver(mBondReceiver, bondDeviceFilter);

    }

    @Override
    public void discoverDevices() {

        mBluetoothBikeDevices.clear();
        mView.clearBluetoothDeviceList();

        if(!mBluetoothAdapter.isEnabled()) {
            mView.showBluetoothRequestEnableIntent();
            return;
        }

        mView.hideWarningGeneralInfo();

        cancelSearchBtTimer();
        startupSearchBtTimer();

        mView.showSearching();

        if(mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
            mView.checkPermissions();
            mBluetoothAdapter.startDiscovery();
        }

        if(!mBluetoothAdapter.isDiscovering()) {
            mView.checkPermissions();
            mBluetoothAdapter.startDiscovery();
        }
    }

    private void startupSearchBtTimer() {
        startTimer();
    }

    private void cancelSearchBtTimer() {
        if (mSearchBluetoothTimerHandler != null) {
            mSearchBluetoothTimerHandler.removeCallbacks(mRunnableTimer);
        }
    }

    private void pairToDevice(BluetoothDevice device) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Log.d(TAG, "pairToDevice - criando bond");
            IntentFilter pairDeviceFilter = new IntentFilter(BluetoothDevice.ACTION_PAIRING_REQUEST);
            pairDeviceFilter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY - 1);
            mContext.registerReceiver(mPairDeviceReceiver, pairDeviceFilter);
            device.createBond();
        }
    }

    private boolean isDeviceAlreadyBonded(BluetoothDevice device) {
        Set<BluetoothDevice> bondedDevices = mBluetoothAdapter.getBondedDevices();
        for (BluetoothDevice bt : bondedDevices) {
            Log.d(TAG, "bonded device name: " + bt.getName() + "address: " + bt.getAddress());
            Log.d(TAG, "received device name: " + device.getName() + "address: " + device.getAddress());
            if (bt.getAddress().equals(device.getAddress())) {
                Log.d(TAG, "isDeviceAlreadyBonded true");
                return true;
            }
        }
        Log.d(TAG, "isDeviceAlreadyBonded false");
        return false;
    }

    @Override
    public BluetoothDevice selectDevice(int index) {
        Log.d(TAG, "selectDevice called");

        BluetoothDevice device = mBluetoothBikeDevices.get(index);
        Log.d(TAG, "selected device: " + device.getAddress() + " name: " + device.getName());

        if (isDeviceAlreadyBonded(device)) {
            mSelectedBluetoothDevice = device;
            mView.displayDialogBikeRimNumberPicker();
        } else {
            pairToDevice(device);
        }

        return device;
    }

    private BroadcastReceiver discoverBtBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "discoverBtBroadcastReceiver called");

            String action = intent.getAction();

            if(action.equals(BluetoothDevice.ACTION_FOUND)) {

                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                Log.d(TAG, "ACTION_FOUND device found name: " + device.getName() +
                        " // address: " + device.getAddress());

                if (device.getName() != null && device.getName().indexOf(BIKE_TAG) != -1) {
                    addBluetoothDeviceToList(device);
                }

            } else if (action.equals(BluetoothDevice.ACTION_NAME_CHANGED)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                Log.d(TAG, "ACTION_NAME_CHANGED device new name : " + device.getName() +
                        " / address : " + device.getAddress());

                if (device.getName() != null && device.getName().indexOf(BIKE_TAG) != -1) {
                    addBluetoothDeviceToList(device);
                }
            }
        }
    };

    private void addBluetoothDeviceToList(BluetoothDevice device) {
        if (!checkIsInBluetoothList(device, mBluetoothBikeDevices)) {
            mBluetoothBikeDevices.add(device);
            mView.hideSearchingBluetoothProgress();
            mView.hideWarningGeneralInfo();
            mView.displayBluetoothDevices(mBluetoothBikeDevices);
        }
    }

    private boolean checkIsInBluetoothList(BluetoothDevice device, ArrayList<BluetoothDevice> bluetoothDevices) {
        for (BluetoothDevice bt : bluetoothDevices) {
            Log.d(TAG, "Bluetooth device.getAddress() : " + device.getAddress() +
                    " / bt address : " + bt.getAddress());
            if (device.getAddress().equals(bt.getAddress())) {
                Log.d(TAG, "bluetooth is in list address : " + device.getAddress());
                return true;
            } else {
                Log.d(TAG, " is not in list address : " + device.getAddress());
            }
        }
        return false;
    }

    private BroadcastReceiver mPairDeviceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals(BluetoothDevice.ACTION_PAIRING_REQUEST)) {
                Log.d(TAG, "mPairDeviceReceiver called");
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                int pin = intent.getIntExtra("android.bluetooth.device.extra.PAIRING_KEY", 1234);
                byte[] bytes = ("" + pin).getBytes();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    Log.d(TAG, "mPairDeviceReceiver realizando pareamento automatico");
                    device.setPin(bytes);

                } //else - aparelho não pode parear automaticamente
            }
            mContext.unregisterReceiver(mPairDeviceReceiver);
        }
    };


    private final BroadcastReceiver mBondReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if(action.equals(BluetoothDevice.ACTION_BOND_STATE_CHANGED)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                if(device.getBondState() == BluetoothDevice.BOND_BONDED) {
                    Log.d(TAG, "mBondReceiver : BOND_BONDED - device: "
                            + device.getName() + " " + device.getAddress());
                    mSelectedBluetoothDevice = device;
                    mView.displayDialogBikeRimNumberPicker();
                }

                if(device.getBondState() == BluetoothDevice.BOND_BONDING) {
                    Log.d(TAG, "mBondReceiver: BOND_BONDING " + device.getName() + " : " + device.getAddress());
                }

                if(device.getBondState() == BluetoothDevice.BOND_NONE) {
                    Log.d(TAG, "mBondReceiver: BOND_NONE");
                }
            }
        }
    };

    @Override
    public void onDestroy() {
        mContext.unregisterReceiver(discoverBtBroadcastReceiver);
        mContext.unregisterReceiver(mBondReceiver);
        try {
            mContext.unregisterReceiver(mPairDeviceReceiver);
        } catch (Exception e) {
            Log.e(TAG, "error to unregister mPairDeviceReceiver : " + e);
        }
    }

    @Override
    public void provideBikeRimValue(int rim) {
        BikeDevice bikeDevice = new BikeDevice();
        bikeDevice.setRimValue(rim);
        bikeDevice.setBluetoothDevice(mSelectedBluetoothDevice);
        concludeBikeDeviceSelect(mSelectedBluetoothDevice, bikeDevice);
    }

    @Override
    public void onEnableBtResponse() {
        if (mBluetoothAdapter.isEnabled()) {
            discoverDevices();
        }
    }

    private void startTimer() {
        Log.d(TAG, "startTimer called");
        mSearchBluetoothTimerHandler = new Handler();
        mSearchBluetoothTimerHandler.postDelayed(mRunnableTimer,30000);
    }

    private void concludeBikeDeviceSelect(BluetoothDevice bluetoothDevice, BikeDevice bikeDevice) {
        if (isForResponse) {
            mView.sendResponseForExerciseRecorderUi(bikeDevice);
        } else {
            mView.sendDeviceToExerciseRecorderUi(bluetoothDevice, bikeDevice);
        }
    }
}