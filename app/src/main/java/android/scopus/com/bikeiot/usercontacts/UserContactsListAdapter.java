package android.scopus.com.bikeiot.usercontacts;

import android.content.Context;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.common.BaseListAdapterOnClickListener;
import android.scopus.com.bikeiot.model.beans.Contact;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 *
 */

public class UserContactsListAdapter extends RecyclerView.Adapter<UserContactsListAdapter.ContactViewHolder>{

    private static final String TAG = UserContactsListAdapter.class.getSimpleName();

    private List<Contact> mContacts;
    private Context mContext;
    private BaseListAdapterOnClickListener<Contact> mCallback;

    public UserContactsListAdapter(List<Contact> contacts, Context context,
                                   BaseListAdapterOnClickListener<Contact> callback) {
        mContacts = contacts;
        mContext = context;
        mCallback = callback;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int idContactsRowItem = R.layout.contacts_item_row;
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(idContactsRowItem, parent, false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        Contact contact = mContacts.get(position);

        holder.mNameTextView.setText(contact.getName());
        holder.mPhoneTextView.setText(contact.getPhoneNumber());
        holder.itemView.setTag(contact.getId());

    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }


    class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mNameTextView;
        private TextView mPhoneTextView;

        public ContactViewHolder(View itemView) {
            super(itemView);
            mNameTextView = (TextView) itemView.findViewById(R.id.contact_name_text_view);
            mPhoneTextView = (TextView) itemView.findViewById(R.id.contact_phone_text_view);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int index = getAdapterPosition();
            Contact contact = mContacts.get(index);
            Log.d(TAG, "on click - index: " + index + " , contact name : " + contact.getName() +
                    " phone number : " + contact.getPhoneNumber() + " contact id : " + contact.getId());
            mCallback.onClickItem(contact);
        }
    }
}
