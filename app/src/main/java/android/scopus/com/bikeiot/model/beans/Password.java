package android.scopus.com.bikeiot.model.beans;

import java.io.Serializable;

/**
 * Created by Luiz Felipe on 30/11/2017.
 */

public class Password implements Serializable {

    private String content;

    public Password() {}

    public Password(String content) {
        this.content = content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }
}
