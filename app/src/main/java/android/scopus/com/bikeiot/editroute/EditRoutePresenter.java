package android.scopus.com.bikeiot.editroute;

import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.model.InputHandler;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.data.RouteRepository;
import android.scopus.com.bikeiot.model.data.local.BikeIoTContract.RouteEntry;

/**
 *
 */

public class EditRoutePresenter implements EditRouteContract.Presenter  {


    private RouteRepository mRouteRepository;
    private int routeId;
    private EditRouteContract.View mView;


    public EditRoutePresenter(RouteRepository routeRepository, int routeId, EditRouteContract.View view) {
        mRouteRepository = routeRepository;
        this.routeId = routeId;
        mView = view;
    }

    @Override
    public void initialize() {
        mRouteRepository.forceBreakCache();
        loadRoute();
    }

    @Override
    public void loadRoute() {
        Route route = mRouteRepository.getRoute(routeId);
        if (route != null) {
            mView.showRouteDetails(route);
        } else {
            mView.showNoRouteError();
        }
    }

    @Override
    public void update(String description, boolean toShare) {
        if (InputHandler.isEmptyOrNull(description)) {
            mView.showEmptyDescriptionError(R.string.empty_description_error_msg);
        } else {

            Route route = mRouteRepository.getRoute(routeId);
            int share = toShare ? RouteEntry.TO_SHARE : RouteEntry.NOT_TO_SHARE;

            if (isNotEqual(route, description, share)) {
                route.setDescription(description);
                route.setShared(share);

                if (mRouteRepository.update(route)) {
                    mView.showUpdateSuccess();
                    mView.sendResponseForRouteDetailsUi(routeId);

                } else {

                    mView.showUpdateError();
                }

            } else {

                mView.sendResponseForRouteDetailsUi(routeId);
            }
        }
    }

    private boolean isNotEqual(Route route, String description, int shareChecked) {
        return !route.getDescription().equals(description) || route.getShared() != shareChecked;
    }
}
