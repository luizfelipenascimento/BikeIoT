package android.scopus.com.bikeiot.routes;

import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.data.RouteDataSource;
import android.scopus.com.bikeiot.model.data.RouteRepository;

import java.util.List;

/**
 *
 */

public class RoutesPresenter implements RoutesContract.Presenter {

    private RoutesContract.View mView;
    private RouteRepository mRouteRepository;

    public RoutesPresenter(RoutesContract.View view, RouteRepository roteRepository) {
        mView = view;
        mRouteRepository = roteRepository;
    }

    @Override
    public void initialize() {
        mRouteRepository.forceBreakCache();
        loadRoutes();
    }

    @Override
    public void loadRoutes() {
        List<Route> routes = mRouteRepository.getRoutes();
        if (!routes.isEmpty()) {
            mView.showRoutes(routes);
        } else {
            mView.showNoRoutes();
        }
    }

    @Override
    public void loadRoutesDoneByUser(int userId) {

    }

    @Override
    public void selectRoute(int routeId) {
        mRouteRepository.forceBreakCache();
        Route route = mRouteRepository.getRoute(routeId);
        if (route != null) {
            mView.showRouteDetailsUi(route.getId());
        } else {
            mView.showErrorInSelectProcess(R.string.error_route_selection);
        }
    }

    @Override
    public void loadSharedRoutes() {
        //TODO: implementar a busca de rotas publicas
    }
}
