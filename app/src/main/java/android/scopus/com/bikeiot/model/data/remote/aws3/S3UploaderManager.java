package android.scopus.com.bikeiot.model.data.remote.aws3;

import android.content.Context;
import android.os.Environment;
import android.scopus.com.bikeiot.model.filerelated.FileRepository;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.ROOT_FOLDER;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.SEND_FOLDER;

/**
 *
 */

public class S3UploaderManager {

    private static final String TAG = S3UploaderManager.class.getSimpleName();

    private final int MAX_QUEUE = 30;
    private final Context mContext;

    private ArrayList<File> mFilesQueue;
    private S3UploaderWebService mS3UploaderWebService;
    private S3UploaderManagerThread mS3UploaderThread;
    private Iterator<File> mFileIterator;

    public S3UploaderManager(Context context) {
        mContext = context;
        mFilesQueue = new ArrayList<>();
        mFileIterator = mFilesQueue.iterator();
        mS3UploaderWebService = new S3UploaderWebService();
    }

    public void uploadFile(File file) {
        Log.d(TAG, "uploadFile called");
        Log.d(TAG, "files queue sile : " + mFilesQueue.size());
        addFileToQueue(file);

        if (mS3UploaderThread == null || !mS3UploaderThread.isAlive()) {
            startUploaderThread();
        }

    }

    private void addFileToQueue(File file) {
        if (mFilesQueue.size() <= MAX_QUEUE) {
            mFilesQueue.add(file);
        }
    }

    private void startUploaderThread() {
        mS3UploaderThread = new S3UploaderManagerThread();
        mS3UploaderThread.start();
    }

    private class S3UploaderManagerThread extends Thread {

        @Override
        public void run() {
            super.run();

            while (!mFilesQueue.isEmpty()) {

                Log.d(TAG, "run has next");

                File file = mFilesQueue.get(0);

                if (doUpload(file)) {
                    Log.d(TAG, "file doUpload success : " + file.getPath());
                } else {
                    Log.d(TAG, "error to doUpload file: " + file.getPath());
                    tryToUploadAgain(file);
                }

                mFilesQueue.remove(0);

                if (mFilesQueue.size() <= 1) {
                    loadFilesFromFolder();
                }

            }
        }

        private boolean tryToUploadAgain(File file) {
            int attempts = 0;
            while (attempts < 3) {
                attempts++;
                if (doUpload(file)) {
                    return true;
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return false;
        }
    }

    private void loadFilesFromFolder() {
        File folder = new File(Environment.getExternalStorageDirectory(),
                ROOT_FOLDER + File.separator+ SEND_FOLDER);

        for (final File file : folder.listFiles()) {
            if (!file.isDirectory()) {
                Log.d(TAG, "loadFilesFromFolder list file path: " + file.getPath());
            }
        }
    }

    private boolean doUpload(File file) {
        try {
            if (mS3UploaderWebService.uploadFileBlocking(file)) {
                file.getCanonicalFile().delete();
                FileRepository.scanFile(file, mContext);
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
