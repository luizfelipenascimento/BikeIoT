package android.scopus.com.bikeiot.exercisesummary;

import android.net.Uri;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.utils.UriHelper;
import android.scopus.com.bikeiot.exerciserecoder.ExerciseRecorderActivity;
import android.scopus.com.bikeiot.model.filerelated.FileContractConstants;
import android.scopus.com.bikeiot.model.filerelated.FileRepository;
import android.scopus.com.bikeiot.model.InputHandler;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.filerelated.CSVFile;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.data.BikeExerciseDataSource;
import android.scopus.com.bikeiot.model.data.RouteDataSource;
import android.util.Log;

import java.io.File;
import java.util.UUID;

/**
 * Created by Luiz Felipe on 21/03/2018.
 */

public class ExerciseSummaryPresenter implements ExerciseSummaryContract.Presenter {

    private static final String TAG = ExerciseRecorderActivity.class.getSimpleName();
    private final FileRepository mFileRepository;

    private ExerciseSummaryContract.View mView;
    private BikeExercise mBikeExercise;
    private BikeExerciseDataSource mBikeExerciseDataSource;
    private RouteDataSource mRouteDataSource;
    private int mBikeExerciseId;


    public ExerciseSummaryPresenter(int bikeExerciseId, BikeExerciseDataSource bikeExerciseDataSource,
                                    RouteDataSource routeDataSource, ExerciseSummaryContract.View view,
                                    FileRepository fileRepository) {

        mView = view;
        mBikeExerciseId = bikeExerciseId;
        mBikeExerciseDataSource = bikeExerciseDataSource;
        mRouteDataSource = routeDataSource;
        mFileRepository = fileRepository;
    }

    @Override
    public void initialize() {
        loadBikeExercise(mBikeExerciseId);
    }

    @Override
    public void loadBikeExercise(int bikeExerciseId) {
        mBikeExercise = getBikeExerciseData(bikeExerciseId);

        if (mBikeExercise == null) {
            mView.showNoBikeExerciseError();
            return;
        }

        Route route = mRouteDataSource.getRoute(mBikeExercise.getRoute().getId());

        Uri imageUri = UriHelper.getUriFromFile(mBikeExercise.getRoute().getImagePath());
        if (imageUri != null) {
            Log.d(TAG, "load image : uri " + imageUri.toString());
            mView.showRouteImage(imageUri);
        } else {
            mView.showNoRouteImage();
        }

        mBikeExercise.setRoute(route);
        mView.displayExerciseResume(mBikeExercise);
    }

    private void loadRouteImage(Route route) {

    }

    private BikeExercise getBikeExerciseData(int bikeExerciseId) {
        BikeExercise bikeExercise = mBikeExerciseDataSource.getBikeExercise(bikeExerciseId);
        if (bikeExercise != null) {
            Route route = mRouteDataSource.getRoute(bikeExercise.getRoute().getId());
            bikeExercise.setRoute(route);
        }
        return bikeExercise;
    }

    @Override
    public void updateBikeExerciseAndRouteDescription(String description) {
        if (InputHandler.isEmptyOrNull(description)) {
            mView.showGeneralErrorMsg(R.string.empty_description_error_msg);
            return;
        }

        if (mBikeExercise == null) {
            mBikeExercise = getBikeExerciseData(mBikeExerciseId);
            if (mBikeExercise == null) {
                //TODO: general error message
                return;
            } else {
                mBikeExercise.setRoute(mRouteDataSource.getRoute(mBikeExercise.getRoute().getId()));
            }
        }

        //TODO: adicionar verificação para descrição

        String newResultFileName = FileRepository.generateNameWithSeparator(
                description,
                String.valueOf(mBikeExercise.getId()),
                String.valueOf(mBikeExercise.getUser().getId()),
                FileContractConstants.RESULT_ROOT_FOLDER_NAME,
                String.valueOf(UUID.randomUUID()));
                /*mBikeExercise.getId() + CSVFile.UNDERLINE_SEPARATOR  + mBikeExercise.getUser().getId() +
                FileContractConstants.RESULT_ROOT_FOLDER_NAME + CSVFile.UNDERLINE_SEPARATOR +
                description + String.valueOf(UUID.randomUUID());*/

        File renamedResultF = mFileRepository.renameFile(mBikeExercise.getResult().getFileResultPath(),
                newResultFileName, CSVFile.EXTENSION);

        mBikeExercise.setDescription(description);
        mBikeExercise.getResult().setFileResultPath(renamedResultF.getPath());

        Route route = mBikeExercise.getRoute();

        String newRouteFileName = FileRepository.generateNameWithSeparator(
                description,
                String.valueOf(route.getId()),
                FileContractConstants.RESULT_ROOT_FOLDER_NAME,
                String.valueOf(UUID.randomUUID()));

                /*route.getId() + CSVFile.UNDERLINE_SEPARATOR +
                FileContractConstants.ROUTE_ROOT_FOLDER_NAME + CSVFile.UNDERLINE_SEPARATOR  +
                description + String.valueOf(UUID.randomUUID());*/

        File renamedRouteF = mFileRepository
                .renameFile(route.getRouteCsvFilePath(), newRouteFileName, CSVFile.EXTENSION);

        route.setDescription(description);
        route.setFileRoutePath(renamedRouteF.getPath());


        if (mRouteDataSource.update(route) &&
                mBikeExerciseDataSource.updateBikeExercise(mBikeExercise)) {

            mView.showBikeExerciseDetailsUi(mBikeExercise.getId());

        }
    }

    @Override
    public void removeBikeExercise() {
        if (mBikeExerciseDataSource.removeBikeExercise(mBikeExerciseId)) {
            mView.showRemoveSuccess();
            mView.endActivity();
        }
    }
}
