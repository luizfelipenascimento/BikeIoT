package android.scopus.com.bikeiot;

/**
 *
 */

public final class Constants {

    private Constants(){}

    public static final int CODE_CALL_PERMISSION = 1050;
    public static final int CODE_SEND_SMS_PERMISSION = 1051;
    public static final int CODE_FINE_LOCATION_PERMISSION = 1052;
    public static final int BASE_PERMISSION_CODE = 1053;
    public static final int REQUEST_ENABLE_BT = 1054;

    public static final int REQUEST_RESULT_EDIT_REGISTER_CONTACT_ID = 1102;

    public static final String KEY_S3 = "put_your_key_here";
    public static final String SECRET_S3 = "secret";
    public static final String S3_BUCKET_NAME = "ciab2018-bike-iot";

    public static final String SHARED_PREFERENCES_KEY = "my_shared_preferences_key";
    public static final String SHARED_USER_ID = "shared_user_id";
    public static final String SHARED_USER_UUID = "shared_user_uuid";


    public static final String EXTRA_CONTENT = "EXTRA_CONTENT";
    public static final String EXTRA_CONTACT_ID = "EXTRA_CONTACT_ID";

    public static final String BRAZIL_PHONE_CODE = "+55";

    public static final String GOOGLE_MAPS_HTTP_BASE_LINK = "http://www.google.com/maps/place/";
}
