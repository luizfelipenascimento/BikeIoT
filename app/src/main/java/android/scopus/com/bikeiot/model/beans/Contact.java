package android.scopus.com.bikeiot.model.beans;

/**
 *
 */

public class Contact {

    private String mName;
    private String mPhoneNumber;
    private User mUser;
    private int mId;

    public User getUser() {
        return mUser;
    }

    public int getId() {
        return mId;
    }

    public void setUser(User user) {
        mUser = user;
    }

    public void setId(int id) {
        mId = id;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    public String getName() {
        return mName;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }
}
