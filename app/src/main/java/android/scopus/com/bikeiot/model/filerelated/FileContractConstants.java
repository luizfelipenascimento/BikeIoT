package android.scopus.com.bikeiot.model.filerelated;

import java.io.File;

/**
 *
 */

final public class FileContractConstants {

    public static final String RESULT_ROOT_FOLDER_NAME = "Results";
    public static final String ROUTE_ROOT_FOLDER_NAME = "Routes";
    public static final String UNDERLINE_SEPARATOR = "_";

    public static final String ROOT_FOLDER = "BikeIot";
    public static final String ROOT_IMAGE_FOLDER = "imgs";
    public static final String SEND_FOLDER = "send";
    public static final String CSV_PART_FOLDER = "csvPart";

    public static final String GPS_INFO = "Gps";
    public static final String SENSORS_INFO = "Sensores";
    public static final String ROUTE_IMAGE_FOLDER =
            ROOT_FOLDER + File.separator + ROUTE_ROOT_FOLDER_NAME + File.separator + ROOT_IMAGE_FOLDER;

    public static final String TEST_FOLDER = "test";

    public static final String PNG_FILE_EXTENSION = ".png";

    private FileContractConstants() {}

    public static class CSVFileRouteContract {
        /*Não modificar a base das colunas*/
        public static final String CSV_ROUTE_COLUMN_LATITUDE = "latitude";
        public static final String CSV_ROUTE_COLUMN_LONGITUDE = "longitude";
        public static final String CSV_ROUTE_COLUMN_DATETIME = "dateTime";
        public static final String CSV_ROUTE_COLUMN_ID_USER = "id_user";
    }

    public static class CSVFileResultsContract {
        /*Não modificar o valor base das colunas*/
        public static final String CSV_RESULT_COLUMN_ACCELEROMETER_X = "AccelerometerX";
        public static final String CSV_RESULT_COLUMN_ACCELEROMETER_Y = "AccelerometerY";
        public static final String CSV_RESULT_COLUMN_ACCELEROMETER_Z = "AccelerometerZ";
        public static final String CSV_RESULT_COLUMN_GYROSCOPE_X = "GyroscopeX";
        public static final String CSV_RESULT_COLUMN_GYROSCOPE_Y = "GyroscopeY";
        public static final String CSV_RESULT_COLUMN_GYROSCOPE_Z = "GyroscopeZ";
        public static final String CSV_RESULT_COLUMN_VELOCITY = "Velocity";
        public static final String CSV_RESULT_COLUMN_ROTATIONS = "Rotations";
        public static final String CSV_RESULT_COLUMN_DATE_TIME = "DateTime";
        public static final String CSV_RESULT_COLUMN_PITCH = "Pitch";
        public static final String CSV_RESULT_COLUMN_ROLL = "Roll";
        public static final String CSV_RESULT_COLUMN_ACC_LINEAR_VELOCITY = "Acc_Linear_Velocity";
        public static final String CSV_RESULT_COLUMN_AVERAGE_ACCELEROMETER_Z = "Average_Acc_Z";
        public static final String CSV_RESULT_COLUMN_AVERAGE_LAST_FIVE_ACCELEROMETER_Y = "Average_Acc_Y";
        public static final String CSV_RESULT_COLUMN_ID_USER = "id_user";
        public static final String CSV_RESULT_COLUMN_APP_ID = "app_id";
    }
}
