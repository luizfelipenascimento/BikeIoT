package android.scopus.com.bikeiot.bikebluetoothdevice;

import android.app.Activity;
import android.scopus.com.bikeiot.R;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class BikeBluetoothLostConnectionDialogActivity extends Activity {

    private static final String TAG = BikeBluetoothLostConnectionDialogActivity.class.getSimpleName();

    private Button mConfirmButton;
    private TextView mContentTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFinishOnTouchOutside(false);
        setContentView(R.layout.activity_bluetooth_lost_connection_dialog);


        mContentTextView = (TextView) findViewById(R.id.content_bluetooth_lost_connection_text_view);

        mConfirmButton = (Button) findViewById(R.id.confirm_button);

        mConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BikeBluetoothLostConnectionDialogActivity.this.finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy called");
    }

}
