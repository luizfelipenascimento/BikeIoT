package android.scopus.com.bikeiot.model.beans;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Luiz Felipe on 08/03/2018.
 */

public class BikeDevice implements Parcelable {

    private int mRimValue;
    private BluetoothDevice mBluetoothDevice;

    public BikeDevice() {

    }

    protected BikeDevice(Parcel in) {
        mRimValue = in.readInt();
        mBluetoothDevice = in.readParcelable(BluetoothDevice.class.getClassLoader());
    }

    public static final Creator<BikeDevice> CREATOR = new Creator<BikeDevice>() {
        @Override
        public BikeDevice createFromParcel(Parcel in) {
            return new BikeDevice(in);
        }

        @Override
        public BikeDevice[] newArray(int size) {
            return new BikeDevice[size];
        }
    };

    public void setRimValue(int rim) {
        mRimValue = rim;
    }

    public void setBluetoothDevice(BluetoothDevice bluetoothDevice) {
        mBluetoothDevice = bluetoothDevice;
    }

    public int getRimValue() {
        return mRimValue;
    }

    public BluetoothDevice getBluetoothDevice() {
        return mBluetoothDevice;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mRimValue);
        dest.writeParcelable(mBluetoothDevice, flags);
    }
}
