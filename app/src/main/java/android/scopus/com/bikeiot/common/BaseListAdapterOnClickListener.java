package android.scopus.com.bikeiot.common;

/**
 * Created by Luiz Felipe on 19/05/2018.
 */

public interface BaseListAdapterOnClickListener<T> {

    void onClickItem(T object);

}
