package android.scopus.com.bikeiot.panicbutton;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.scopus.com.bikeiot.utils.PermissionHelper;
import android.scopus.com.bikeiot.R;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

import static android.scopus.com.bikeiot.Constants.CODE_CALL_PERMISSION;
import static android.scopus.com.bikeiot.Constants.CODE_FINE_LOCATION_PERMISSION;
import static android.scopus.com.bikeiot.Constants.CODE_SEND_SMS_PERMISSION;

public class CallHelpTestActivity extends AppCompatActivity {

    private TelephonyManager mTelephonyManager;
    private EditText mTelephoneNumberEditText;
    private Button mMakeCallButton;
    private static final String TAG = "CallHelpTestActivity";

    private Button mSendSmsTestButton;

    private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING:
                    Log.d(TAG, "CALL_STATE_RINGING");
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Log.d(TAG, "CALL_STATE_OFFHOOK");
                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    Log.d(TAG, "CALL_STATE_IDLE");
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_help_test);

        mTelephoneNumberEditText = (EditText) findViewById(R.id.telephone_number_edit_text);
        mMakeCallButton = (Button) findViewById(R.id.make_call_button);

        mSendSmsTestButton = (Button) findViewById(R.id.test_sms_send_button);

        if (!PermissionHelper.isPermissionGranted(this, Manifest.permission.SEND_SMS)) {
            PermissionHelper.requestPermissions(this, CODE_SEND_SMS_PERMISSION,
                    new String[]{Manifest.permission.SEND_SMS});
        } else {
            mSendSmsTestButton.setEnabled(true);
        }

        if (!PermissionHelper.isPermissionGranted(this, Manifest.permission.CALL_PHONE)) {
            PermissionHelper.requestPermissions(this, CODE_CALL_PERMISSION,
                    new String[]{Manifest.permission.CALL_PHONE});
        } else {
            mMakeCallButton.setEnabled(true);
        }

        if (!PermissionHelper.isPermissionGranted(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            PermissionHelper.requestPermissions(this, CODE_FINE_LOCATION_PERMISSION,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION});
        }

        mMakeCallButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tel = mTelephoneNumberEditText.getText().toString();
                makeCallIntent(tel);
            }
        });

        mSendSmsTestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //testing
                ArrayList<String> phoneNumbers = new ArrayList<>();
                //phoneNumbers.add("+5511972843509");

                String phoneNumber = mTelephoneNumberEditText.getText().toString();
                if (!phoneNumber.isEmpty()) {
                    phoneNumbers.add(phoneNumber);
                }

                sendRequestCallHelpService(phoneNumbers);
            }
        });

        mTelephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
    }

    private void sendRequestCallHelpService(ArrayList<String> phoneNumbers) {
        Intent intentCallHelpService = new Intent(this, CallHelpService.class);
        intentCallHelpService.setAction(CallHelpService.ACTION_START_WARNING_EVENT);
        intentCallHelpService.putExtra(CallHelpService.EXTRA_PHONE_NUMBERS, phoneNumbers);
        startService(intentCallHelpService);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CODE_CALL_PERMISSION:
                if (!PermissionHelper.isPermissionGranted(this, Manifest.permission.CALL_PHONE)) {
                    PermissionHelper.requestPermissions(this, CODE_CALL_PERMISSION,
                            new String[]{Manifest.permission.CALL_PHONE});
                } else {
                    mMakeCallButton.setEnabled(true);
                }
                break;
            case CODE_SEND_SMS_PERMISSION:
                if (!PermissionHelper.isPermissionGranted(this, Manifest.permission.SEND_SMS)) {
                    PermissionHelper.requestPermissions(this, CODE_SEND_SMS_PERMISSION,
                            new String[]{Manifest.permission.SEND_SMS});
                } else {
                    mSendSmsTestButton.setEnabled(true);
                }
                break;
            case CODE_FINE_LOCATION_PERMISSION:
                if (!PermissionHelper.isPermissionGranted(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    PermissionHelper.requestPermissions(this, CODE_FINE_LOCATION_PERMISSION,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION});
                }
                break;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);

        if (CallHelpService.isAlive()) {
            stopService(new Intent(this, CallHelpService.class));
        }
    }

    @SuppressLint("MissingPermission")
    private void makeCallIntent(String telNumber) {
        if (telNumber.isEmpty()) {
            telNumber = "24576878";
        }

        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + telNumber));
        startActivity(callIntent);
    }
}
