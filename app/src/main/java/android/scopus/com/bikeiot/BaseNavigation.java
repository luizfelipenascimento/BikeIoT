package android.scopus.com.bikeiot;

import android.support.design.widget.NavigationView;

/**
 * Created by Luiz Felipe on 27/03/2018.
 */

public interface BaseNavigation {
    void setupDrawerNavigationContent(NavigationView navigationView);
}
