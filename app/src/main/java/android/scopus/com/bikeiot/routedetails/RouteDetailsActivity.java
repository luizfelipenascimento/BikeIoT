package android.scopus.com.bikeiot.routedetails;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.scopus.com.bikeiot.BaseNavigation;
import android.scopus.com.bikeiot.utils.Helper;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.common.ConfirmationDialog;
import android.scopus.com.bikeiot.editroute.EditRouteActivity;
import android.scopus.com.bikeiot.exerciserecoder.ExerciseRecorderActivity;
import android.scopus.com.bikeiot.common.BitmapCircle;
import android.scopus.com.bikeiot.model.location.LocationUpdater;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.model.data.RouteRepository;
import android.scopus.com.bikeiot.model.data.UserRepository;
import android.scopus.com.bikeiot.model.data.local.RouteDAO;
import android.scopus.com.bikeiot.model.data.local.UserDAO;
import android.scopus.com.bikeiot.routes.RoutesActivity;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

import static android.scopus.com.bikeiot.utils.CalculateUtils.metersToKm;
import static android.scopus.com.bikeiot.utils.Helper.formatDistanceToKmOrMetersWithIndicator;
import static android.scopus.com.bikeiot.utils.Helper.formatKmDecimal;
import static android.scopus.com.bikeiot.utils.SharedPreferencesUtils.SHARED_PREFERENCES_DEFAULT_FILE_KEY;

public class RouteDetailsActivity extends AppCompatActivity implements RouteDetailsContract.View,
        BaseNavigation, OnMapReadyCallback, SelectRouteDialog.SelectRouteDialogListener,
        ConfirmationDialog.ConfirmationDialogListener {

    private static final String TAG = RouteDetailsActivity.class.getSimpleName();

    public static final String EXTRA_ROUTE_ID = "extra_route_id";
    public static final String ACTION_SEND_SELECTED_ROUTE_LOCATION_POINTS = "ACTION_SEND_SELECTED_ROUTE_LOCATION_POINTS";

    public static final String EXTRA_SELECTED_ROUTE_LOCATION_POINTS = "EXTRA_SELECTED_ROUTE_LOCATION_POINTS";

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private GoogleMap mMap;

    private TextView mUserNameTextView;
    private TextView mRouteDescriptionTextView;
    private TextView mRouteDistanceTextView;
    private TextView mDistanceToRouteTextView;

    private ImageButton mCenterMapOnRouteButton;
    private ImageButton mUpdateDistanceToRouteImageButton;
    private RouteDetailsPresenter mPresenter;
    private ProgressBar mDistanceToRouteProgressBar;
    private Button mSelectRouteButton;
    private LinearLayout mUserInfoContainer;
    private boolean isMyRoute = false;
    private int routeId;
    private TextView mSharedStatusTextView;
    private FrameLayout mNoRouteErrorContainerFrameLayout;
    private LinearLayout mRouteInfoContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_details);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(R.string.route_details_activity_title);

        //setting up navigation
        mNavigationView = (NavigationView) findViewById(R.id.main_navigation_view);
        setupDrawerNavigationContent(mNavigationView);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mUserNameTextView = (TextView) findViewById(R.id.user_name_route_details_text_view);
        mRouteDescriptionTextView = (TextView) findViewById(R.id.route_details_description_text_view);
        mRouteDistanceTextView = (TextView) findViewById(R.id.route_details_distance_text_view);
        mDistanceToRouteTextView = (TextView) findViewById(R.id.distance_to_route_text_view);
        mSharedStatusTextView = (TextView) findViewById(R.id.route_shared_status_text_view);
        mNoRouteErrorContainerFrameLayout = (FrameLayout) findViewById(R.id.no_route_error_container_frame_layout);
        mRouteInfoContainer = (LinearLayout) findViewById(R.id.route_info_container_linear_layout);

        mCenterMapOnRouteButton = (ImageButton) findViewById(R.id.center_map_on_route_image_button);
        mUpdateDistanceToRouteImageButton = (ImageButton) findViewById(R.id.update_distance_to_route_image_button);

        mDistanceToRouteProgressBar = (ProgressBar) findViewById(R.id.distance_to_route_progress_bar);

        mUserInfoContainer = (LinearLayout) findViewById(R.id.user_information_container_liner_layout);

        mUpdateDistanceToRouteImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideDistanceToRouteButton();
                showDistanceToRouteProgressBar();

                mPresenter.loadDistanceToRoad();
            }
        });

        mCenterMapOnRouteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.loadRoutePointLocationBounds();
            }
        });

        mSelectRouteButton = (Button) findViewById(R.id.select_route_button);

        FusedLocationProviderClient fusedClient = LocationServices.getFusedLocationProviderClient(this);
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);

        routeId = getIntent().getIntExtra(EXTRA_ROUTE_ID, 0);

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_DEFAULT_FILE_KEY,
                Context.MODE_PRIVATE);

        mPresenter = new RouteDetailsPresenter(new RouteRepository(new RouteDAO(this)),
                new LocationUpdater(fusedClient, settingsClient, LocationUpdater
                        .locationRequestHighAccuracy(500,1000,1)),
                new UserRepository(new UserDAO(this)),
                this, routeId, sharedPreferences);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.initialize();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(EXTRA_ROUTE_ID, routeId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void setupDrawerNavigationContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Log.d(TAG, "setNavigationItemSelectedListener");
                Helper.Menu.selectDrawerItem(RouteDetailsActivity.this, item, -1);
                mDrawerLayout.closeDrawers();
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mActionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.delete_menu_item:
                showDeleteDialogConfirmation();
                return true;
            case R.id.edit_menu_item:
                mPresenter.prepareToEditRouteUi();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Log.d(TAG,"onPrepareOptionsMenu called");
        if (isMyRoute) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.delete_edit_menu, menu);
        }
        return super.onPrepareOptionsMenu(menu);
    }



    @Override
    public void showRoutePolylineOnMap(PolylineOptions polylineOptions) {
        mMap.addPolyline(polylineOptions);
    }

    @Override
    public void showDistanceToRoute(double distanceToRoute) {
        hideDistanceToRouteProgressBar();
        showDistanceToRouteButton();

        String dtFormatted = formatDistanceToKmOrMetersWithIndicator(distanceToRoute, this);
        mDistanceToRouteTextView.setText(dtFormatted);
    }

    @Override
    public void showRouteDetails(Route route) {

        mNoRouteErrorContainerFrameLayout.setVisibility(View.GONE);
        mRouteInfoContainer.setVisibility(View.VISIBLE);

        String kmIndicator = getResources().getString(R.string.km_indicator);
        mRouteDescriptionTextView.setText(route.getDescription());
        mSharedStatusTextView.setText(route.getShared() == 0 ? R.string.route_not_shared_status : R.string.route_shared_status);
        mRouteDistanceTextView.setText(formatKmDecimal(metersToKm(route.getDistance())) + " " + kmIndicator);
    }

    @Override
    public void showDeleteUpdateMenu() {
        isMyRoute = true;
        invalidateOptionsMenu();
    }

    @Override
    public void showNoRouteError() {
        //TODO: criar um container so para exibir o erro
        mRouteInfoContainer.setVisibility(View.GONE);
        mNoRouteErrorContainerFrameLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showCenterMapButton() {
        mCenterMapOnRouteButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void centerRouteOnMap(LatLngBounds locationBounds) {
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDimensionPixelSize(R.dimen.map_container);
        int padding = (int) (width * 0.12);
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(locationBounds, width, height, padding));
        showCenterMapButton();
    }

    @Override
    public void showNoRoutePolylineLoaded() {
        //TODO: criar o erro para quando nao for possivel carregar o polyline
    }

    @Override
    public void sendSelectedRouteToExerciseRecorderUi(ArrayList<LatLng> routePoints) {
        Intent intent = new Intent(this, ExerciseRecorderActivity.class);
        intent.setAction(ACTION_SEND_SELECTED_ROUTE_LOCATION_POINTS);
        intent.putExtra(EXTRA_SELECTED_ROUTE_LOCATION_POINTS, routePoints);
        startActivity(intent);
    }

    @Override
    public void showSelectRouteDialogConfirmation(double distance) {
        FragmentManager fm = getFragmentManager();
        String distanceFormatted = formatDistanceToKmOrMetersWithIndicator(distance, this);
        SelectRouteDialog selectRouteDialog = SelectRouteDialog.newInstance(distanceFormatted);
        selectRouteDialog.show(fm, "SelectRouteDialog");
    }

    @Override
    public void showUserDetails(User user) {
        mUserInfoContainer.setVisibility(View.VISIBLE);
        mUserNameTextView.setText(user.getName());
    }

    @Override
    public void showNoUserDetails() {
        mUserInfoContainer.setVisibility(View.GONE);
    }

    @Override
    public void showEditRouteUi(int routeId) {
        Intent intent = new Intent(this, EditRouteActivity.class);
        intent.putExtra(EXTRA_ROUTE_ID, routeId);
        startActivityForResult(intent, EditRouteActivity.REQUEST_ROUTE_UPDATE_ID);
    }

    @Override
    public void showRoutesUi() {
        Intent intent = new Intent(this, RoutesActivity.class);
        //TODO: test flags
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public void showDeleteDialogConfirmation() {
        FragmentManager fm = getFragmentManager();
        String content = getResources().getString(R.string.remove_route_confirmation);
        String title = getResources().getString(R.string.dialog_remove_title);
        ConfirmationDialog confirmationDialog = ConfirmationDialog.newInstance(content, title, this);
        confirmationDialog.show(fm, ConfirmationDialog.DIALOG_TAG);
    }

    @Override
    public void showRemoveSuccess() {
        Toast.makeText(this, R.string.remove_success, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showRemoveRouteError() {
        Toast.makeText(this, R.string.remove_error_msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showStartEndMarkers(LatLng startLatLng, LatLng endLatLng) {
        mMap.addMarker(new MarkerOptions()
                .title(getString(R.string.start_route))
                .position(startLatLng)
                .icon(BitmapDescriptorFactory.fromBitmap(new BitmapCircle(25, 25,
                        Color.GREEN, Color.BLACK).getCircleBitmap())));

        mMap.addMarker(new MarkerOptions()
                .title(getString(R.string.end_route))
                .position(endLatLng)
                .icon(BitmapDescriptorFactory.fromBitmap(new BitmapCircle(25,25,
                        Color.RED, Color.BLACK).getCircleBitmap())));

    }

    @Override
    public void showImpossibleToSelect() {
        Toast.makeText(this, R.string.not_possible_to_select_route, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mPresenter.loadRoutePointLocationBounds();
        mPresenter.loadRoutePolyline();
        mMap.getUiSettings().setMapToolbarEnabled(false);
    }

    private void hideDistanceToRouteButton() {
        mUpdateDistanceToRouteImageButton.setVisibility(View.GONE);
    }

    private void showDistanceToRouteButton() {
        mUpdateDistanceToRouteImageButton.setVisibility(View.VISIBLE);
    }

    private void showDistanceToRouteProgressBar() {
        mDistanceToRouteProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideDistanceToRouteProgressBar() {
        mDistanceToRouteProgressBar.setVisibility(View.GONE);
    }

    public void selectRouteButton(View view) {
        mPresenter.selectRoute();
    }

    @Override
    public void onSelectRouteDialogPositiveChoose() {
        mPresenter.confirmRouteSelect();
    }

    @Override
    public void onSelectRouteDialogNegativeChoose() {
        // do nothing
    }

    @Override
    public void onConfirmationDialogPositiveChoose() {
        mPresenter.removeRoute();
    }

    @Override
    public void onConfirmationDialogNegativeChoose() {
        //do nothing
    }
}
