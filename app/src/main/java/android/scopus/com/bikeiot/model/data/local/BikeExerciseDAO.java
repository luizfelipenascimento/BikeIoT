package android.scopus.com.bikeiot.model.data.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.scopus.com.bikeiot.model.beans.BikeDevice;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.beans.Result;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.model.data.BikeExerciseDataSource;
import android.scopus.com.bikeiot.model.data.local.BikeIoTContract.BikeExerciseAndResultEntry;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static android.scopus.com.bikeiot.utils.Helper.*;


public class BikeExerciseDAO implements BikeExerciseDataSource {

    private static final String TAG = BikeExercise.class.getSimpleName();

    private BikeIotDbHelper mDbHelper;

    public BikeExerciseDAO(Context context) {
        mDbHelper = new BikeIotDbHelper(context);
    }

    public static final String[] DEFAULT_PROJECTION_SELECT = {
            BikeExerciseAndResultEntry._ID,
            BikeExerciseAndResultEntry.COLUMN_EXERCISE_DATE,
            BikeExerciseAndResultEntry.COLUMN_FK_ROUTE_ID,
            BikeExerciseAndResultEntry.COLUMN_FK_USER_ID,
            BikeExerciseAndResultEntry.COLUMN_RESULT_FILE_PATH,
            BikeExerciseAndResultEntry.COLUMN_RESULT_TOTAL_TIME_CHRONOMETER,
            BikeExerciseAndResultEntry.COLUMN_DESCRIPTION,
            BikeExerciseAndResultEntry.COLUMN_USED_BIKE_RIM,
            BikeExerciseAndResultEntry.COLUMN_RESULT_MEAN_VELOCITY,
            BikeExerciseAndResultEntry.COLUMN_RESULT_MAX_VELOCITY,
            BikeExerciseAndResultEntry.COLUMN_RESULT_MEAN_HEART_RATE,
            BikeExerciseAndResultEntry.COLUMN_RESULT_CALORIES_BURNED
    };


    @Override
    public List<BikeExercise> getBikeExercises() {
        return null;
    }

    @Override
    public List<BikeExercise> getBikeExercisesFromUser(int userId) {
        ArrayList<BikeExercise> bikeExercises = new ArrayList<>();
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String projection[] = DEFAULT_PROJECTION_SELECT;

        String whereClause = BikeExerciseAndResultEntry.COLUMN_FK_USER_ID + "=?";
        String whereArgs[] = {String.valueOf(userId)};

        Cursor cursor = db.query(BikeExerciseAndResultEntry.TABLE_NAME,
                projection,
                whereClause,
                whereArgs,
                null,
                null,
                null);

        try {

            while (cursor.moveToNext()) {
                BikeExercise bikeExercise = getBikeExerciseFromCursor(cursor);
                bikeExercises.add(bikeExercise);
            }

            return bikeExercises;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }

        return Collections.EMPTY_LIST;
    }

    @Override
    public boolean saveBikeExercise(BikeExercise bikeExercise) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(BikeExerciseAndResultEntry.COLUMN_FK_ROUTE_ID, bikeExercise.getRoute().getId());

        values.put(BikeExerciseAndResultEntry.COLUMN_FK_USER_ID, bikeExercise.getUser().getId());

        values.put(BikeExerciseAndResultEntry.COLUMN_RESULT_FILE_PATH, bikeExercise.getResult().getFileResultPath());
        //TODO: definir se a duracao ira ficar no exercicio ou no resultado

        values.put(BikeExerciseAndResultEntry.COLUMN_RESULT_TOTAL_TIME_CHRONOMETER,
                formatDateToTimeString(bikeExercise.getResult().getTotalTimeChronometer()));

        values.put(BikeExerciseAndResultEntry.COLUMN_EXERCISE_DATE,
                formatToDateString(bikeExercise.getExerciseDate()));

        values.put(BikeExerciseAndResultEntry.COLUMN_DESCRIPTION, bikeExercise.getDescription());

        values.put(BikeExerciseAndResultEntry.COLUMN_RESULT_MAX_VELOCITY,
                bikeExercise.getResult().getMaxVelocityMetersPerSec());

        values.put(BikeExerciseAndResultEntry.COLUMN_RESULT_MEAN_VELOCITY,
                bikeExercise.getResult().getMeanVelocityMetersPerSec());

        values.put(BikeExerciseAndResultEntry.COLUMN_USED_BIKE_RIM,
                bikeExercise.getBikeDevice().getRimValue());

        values.put(BikeExerciseAndResultEntry.COLUMN_RESULT_CALORIES_BURNED,
                bikeExercise.getResult().getCaloriesBurned());

        values.put(BikeExerciseAndResultEntry.COLUMN_RESULT_MEAN_HEART_RATE,
                bikeExercise.getResult().getMeanHeartRate());

        values.put(BikeExerciseAndResultEntry._ON_CREATE, formatToDateTimeString(new Date()));


        long idGenerated = db.insert(BikeExerciseAndResultEntry.TABLE_NAME, null, values);

        db.close();

        db.close();
        if (idGenerated != -1) {
            bikeExercise.setId((int)idGenerated);
            return true;
        }

        return false;
    }

    private BikeExercise getBikeExerciseFromCursor(Cursor c) {
        int id = c.getInt(c.getColumnIndex(BikeExerciseAndResultEntry._ID));
        String exerciseDate = c.getString(c.getColumnIndex(BikeExerciseAndResultEntry.COLUMN_EXERCISE_DATE));
        int fk_route_id = c.getInt(c.getColumnIndex(BikeExerciseAndResultEntry.COLUMN_FK_ROUTE_ID));
        int fk_user_id = c.getInt(c.getColumnIndex(BikeExerciseAndResultEntry.COLUMN_FK_USER_ID));
        String resultFilePath = c.getString(c.getColumnIndex(BikeExerciseAndResultEntry.COLUMN_RESULT_FILE_PATH));
        String resultTotalTimeChronometer = c.getString(c.getColumnIndex(BikeExerciseAndResultEntry.COLUMN_RESULT_TOTAL_TIME_CHRONOMETER));
        String description = c.getString(c.getColumnIndex(BikeExerciseAndResultEntry.COLUMN_DESCRIPTION));
        double resultMeanVelocity = c.getDouble(c.getColumnIndex(BikeExerciseAndResultEntry.COLUMN_RESULT_MEAN_VELOCITY));
        double resultMaxVelocity = c.getDouble(c.getColumnIndex(BikeExerciseAndResultEntry.COLUMN_RESULT_MAX_VELOCITY));
        int bikeRimValue = c.getInt(c.getColumnIndex(BikeExerciseAndResultEntry.COLUMN_USED_BIKE_RIM));
        double meanHeartRate = c.getDouble(c.getColumnIndex(BikeExerciseAndResultEntry.COLUMN_RESULT_MEAN_HEART_RATE));
        double caloriesBurned = c.getDouble(c.getColumnIndex(BikeExerciseAndResultEntry.COLUMN_RESULT_CALORIES_BURNED));

        BikeExercise bikeExercise = new BikeExercise();
        bikeExercise.setId(id);
        bikeExercise.setExerciseDate(parseToDate(exerciseDate));
        bikeExercise.setUser(new User());
        bikeExercise.getUser().setId(fk_user_id);
        bikeExercise.setRoute(new Route());
        bikeExercise.getRoute().setId(fk_route_id);
        bikeExercise.setResult(new Result());
        bikeExercise.setDescription(description);
        bikeExercise.getResult().setFileResultPath(resultFilePath);
        bikeExercise.getResult().setTotalTimeChronometer(resultTotalTimeChronometer);
        bikeExercise.getResult().setMaxVelocityMetersPerSec(resultMaxVelocity);
        bikeExercise.getResult().setMeanVelocityMetersPerSec(resultMeanVelocity);
        bikeExercise.getResult().setMeanHeartRate(meanHeartRate);
        bikeExercise.getResult().setCaloriesBurned(caloriesBurned);
        bikeExercise.setBikeDevice(new BikeDevice());
        bikeExercise.getBikeDevice().setRimValue(bikeRimValue);
        return bikeExercise;
    }

    @Override
    public BikeExercise getBikeExercise(int bikeExerciseId) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String projection[] = DEFAULT_PROJECTION_SELECT;

        String whereClause = BikeExerciseAndResultEntry._ID + "=?";
        String whereArgs[] = {Integer.toString(bikeExerciseId)};

        Cursor cursor = db.query(BikeExerciseAndResultEntry.TABLE_NAME,
                projection,
                whereClause,
                whereArgs,
                null,
                null,
                null);

        BikeExercise bikeExercise = null;

        try {
            cursor.moveToFirst();
            bikeExercise = getBikeExerciseFromCursor(cursor);
        } catch (Exception e) {
            System.err.print(e);
        } finally {
            cursor.close();
            db.close();
        }
        return bikeExercise;
    }

    @Override
    public boolean updateBikeExercise(BikeExercise bikeExercise) {

        BikeExercise registeredExercise = getBikeExercise(bikeExercise.getId());

        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        if (registeredExercise == null) {
            return false;
        }

        if (bikeExercise.getExerciseDate().compareTo(registeredExercise.getExerciseDate()) != 0) {
            values.put(BikeExerciseAndResultEntry.COLUMN_EXERCISE_DATE,
                    formatToDateString(bikeExercise.getExerciseDate()));
        }


        if (bikeExercise.getResult() != null) {
            if (!registeredExercise.getResult().getFileResultPath().equals(bikeExercise.getResult().getFileResultPath())){
                values.put(BikeExerciseAndResultEntry.COLUMN_RESULT_FILE_PATH,
                        bikeExercise.getResult().getFileResultPath());
            }

            if (registeredExercise.getResult().getTotalTimeChronometer().compareTo(bikeExercise.getResult().getTotalTimeChronometer()) != 0) {
                values.put(BikeExerciseAndResultEntry.COLUMN_RESULT_TOTAL_TIME_CHRONOMETER,
                        formatDateToTimeString(bikeExercise.getResult().getTotalTimeChronometer()));
            }

            if (registeredExercise.getResult().getMeanVelocityMetersPerSec()
                    != bikeExercise.getResult().getMeanVelocityMetersPerSec()
                    && bikeExercise.getResult().getMeanVelocityMetersPerSec() != 0) {
                values.put(BikeExerciseAndResultEntry.COLUMN_RESULT_MEAN_VELOCITY, bikeExercise.getResult().getMeanVelocityMetersPerSec());
            }

            if (registeredExercise.getResult().getMaxVelocityMetersPerSec()
                    != bikeExercise.getResult().getMaxVelocityMetersPerSec()
                    && bikeExercise.getResult().getMaxVelocityMetersPerSec() != 0) {

                values.put(BikeExerciseAndResultEntry.COLUMN_RESULT_MAX_VELOCITY, bikeExercise.getResult().getMaxVelocityMetersPerSec());

            }

            if (bikeExercise.getResult().getMeanHeartRate() != 0) {
                values.put(BikeExerciseAndResultEntry.COLUMN_RESULT_MEAN_HEART_RATE,
                        bikeExercise.getResult().getMeanHeartRate());
            }

            double caloriesBurned = bikeExercise.getResult().getCaloriesBurned();
            if (caloriesBurned != 0) {
                values.put(BikeExerciseAndResultEntry.COLUMN_RESULT_CALORIES_BURNED, caloriesBurned);
            }

        }

        if (bikeExercise.getUser() != null) {
            if (bikeExercise.getUser().getId() != 0) {
                values.put(BikeExerciseAndResultEntry.COLUMN_FK_USER_ID, bikeExercise.getUser().getId());
            }
        }

        if (bikeExercise.getRoute() != null) {
            if (bikeExercise.getRoute().getId() != 0) {
                values.put(BikeExerciseAndResultEntry.COLUMN_FK_ROUTE_ID, bikeExercise.getRoute().getId());
            }
        }

        if (bikeExercise.getBikeDevice() != null) {
            if (bikeExercise.getBikeDevice().getRimValue() != 0) {
                values.put(BikeExerciseAndResultEntry.COLUMN_USED_BIKE_RIM, bikeExercise.getBikeDevice().getRimValue());
            }
        }

        if (!TextUtils.isEmpty(bikeExercise.getDescription())) {
            values.put(BikeExerciseAndResultEntry.COLUMN_DESCRIPTION, bikeExercise.getDescription());
        }

        String[] whereArgs  = {Integer.toString(bikeExercise.getId())};

        int rowsAffected = db.update(BikeExerciseAndResultEntry.TABLE_NAME, values,
                BikeExerciseAndResultEntry._ID + "=?",
                whereArgs);

        db.close();

        if (rowsAffected == 1) {
            return true;
        }

        return false;
    }

    @Override
    public boolean renameResultFilePath(BikeExercise bikeExercise) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(BikeExerciseAndResultEntry.COLUMN_RESULT_FILE_PATH,
                bikeExercise.getResult().getFileResultPath());

        String whereArgs[] = {Integer.toString(bikeExercise.getId())};

        int rows = db.update(BikeExerciseAndResultEntry.TABLE_NAME, values,
                BikeExerciseAndResultEntry._ID + "=?", whereArgs);

        db.close();

        if (rows == 1) {
            return true;
        }

        return false;
    }

    @Override
    public boolean removeBikeExercise(int id) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String whereClause = BikeExerciseAndResultEntry._ID + "=?";
        String whereArgs[] = {String.valueOf(id)};

        int affected = db.delete(BikeExerciseAndResultEntry.TABLE_NAME, whereClause, whereArgs);
        db.close();

        if (affected == 1) {
            return true;
        }

        return false;
    }

    @Override
    public void deleteAll() {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        db.delete(BikeExerciseAndResultEntry.TABLE_NAME, null, null);
        db.close();
    }
}
