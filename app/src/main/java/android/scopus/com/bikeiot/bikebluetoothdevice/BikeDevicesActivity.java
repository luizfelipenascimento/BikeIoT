package android.scopus.com.bikeiot.bikebluetoothdevice;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.scopus.com.bikeiot.utils.Helper;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.exerciserecoder.ExerciseRecorderActivity;
import android.scopus.com.bikeiot.model.beans.BikeDevice;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static android.scopus.com.bikeiot.Constants.REQUEST_ENABLE_BT;

public class BikeDevicesActivity extends AppCompatActivity implements BikeDevicesContract.View {

    public static final String ACTION_SEND_DEVICE =
            "android.scopus.com.bikeiot.BikeDevicesActivity.SEND_DEVICE";
    public static final String BLUETOOTH_DEVICE =
            "android.scopus.com.bikeiot.BikeDevicesActivity.BLUETOOTH_DEVICE";
    public static final String EXTRA_BIKE_DEVICE =
            "android.scopus.com.bikeiot.BikeDevicesActivity.EXTRA_BIKE_DEVICE";

    private static final String TAG = "BikeDevicesActivity";
    private BikeDevicesContract.Presenter mPresenter;
    private ListView mBtListView;

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private NavigationView mNavigationView;

    private NumberPicker mBikeRimNumberPicker;
    private ListDeviceAdapter mListAdapter;
    private TextView mWarningStatusTextView;
    private ImageView mWarningStatusImageView;
    private LinearLayout mWarningStatusContainer;
    private LinearLayout mGeneralProgressBarContainer;
    private ProgressBar mGeneralStatusProgressBar;
    private TextView mGeneralStatusProgressTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bike_devices);


        mBtListView = (ListView) findViewById(R.id.bt_devices_list_view);

        //setting up toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.bike_devices_activity_title);

        //setting up navigation
        mNavigationView = (NavigationView) findViewById(R.id.main_navigation_view);
        setupDrawerNavigationContent(mNavigationView);

        mWarningStatusImageView = (ImageView) findViewById(R.id.warning_status_image_view);
        mWarningStatusTextView = (TextView) findViewById(R.id.warning_status_text_view);
        mWarningStatusContainer = (LinearLayout) findViewById(R.id.warning_status_container_linear_layout);

        mGeneralProgressBarContainer = (LinearLayout) findViewById(R.id.general_status_progress_container_linear_layout);
        mGeneralStatusProgressBar = (ProgressBar) findViewById(R.id.general_status_progress_bar);
        mGeneralStatusProgressTextView = (TextView) findViewById(R.id.general_status_progress_text_view);


        mBtListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemClick called");
                mPresenter.selectDevice(position);
            }
        });

        mListAdapter = new ListDeviceAdapter(this, R.layout.device_custom_row, new ArrayList<BluetoothDevice>());
        mBtListView.setAdapter(mListAdapter);

        ComponentName callingActivity = getCallingActivity();
        mPresenter = new BikeDevicesPresenter(this, BikeDevicesActivity.this,
                callingActivity);

        hideSearchingBluetoothProgress();
        showHelperInfo();
        clearBluetoothDeviceList();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "on resume called");
    }

    @Override
    public void sendDeviceToExerciseRecorderUi(BluetoothDevice device, BikeDevice bikeDevice) {
        Log.d(TAG, "sendDeviceToExerciseRecorderUi() called");

        Intent intent = new Intent(this, ExerciseRecorderActivity.class);
        intent.setAction(ACTION_SEND_DEVICE);
        intent.putExtra(BLUETOOTH_DEVICE, device);
        intent.putExtra(EXTRA_BIKE_DEVICE, bikeDevice);

        startActivity(intent);
    }

    @Override
    public void sendResponseForExerciseRecorderUi(BikeDevice bikeDevice) {
        Log.d(TAG, "sendResponseForExerciseRecorderUi called");
        Intent intent = new Intent();
        intent.putExtra(EXTRA_BIKE_DEVICE, bikeDevice);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mActionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerNavigationContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Helper.Menu.selectDrawerItem(BikeDevicesActivity.this, item, R.id.bike_selection_menu_item);
                mDrawerLayout.closeDrawers();
                return true;
            }
        });
    }


    @Override
    public void displayBluetoothDevices(ArrayList<BluetoothDevice> devices) {
        mListAdapter = new ListDeviceAdapter(this, R.layout.device_custom_row, devices);
        mBtListView.setAdapter(mListAdapter);
    }

    @Override
    public void displayNoBluetoothDevicesFound() {
        mWarningStatusTextView.setText(R.string.no_bike_bluetooth_found);
        mWarningStatusImageView.setImageDrawable(getDrawable(R.drawable.ic_bluetooth_black_24dp));
        mWarningStatusContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSearching() {
        mGeneralProgressBarContainer.setVisibility(View.VISIBLE);
        mGeneralStatusProgressTextView.setText(R.string.searching_for_bike_bluetooth);
    }

    @Override
    public void hideSearchingBluetoothProgress() {
        mGeneralProgressBarContainer.setVisibility(View.GONE);
    }

    @Override
    public void hideWarningGeneralInfo() {
        Log.d(TAG, "hideWarningGeneralInfo called");
        mWarningStatusContainer.setVisibility(View.GONE);
    }

    @Override
    public void showHelperInfo() {
        mWarningStatusContainer.setVisibility(View.VISIBLE);
        mWarningStatusImageView.setImageDrawable(getDrawable(R.drawable.ic_bluetooth_black_24dp));
        mWarningStatusTextView.setText(R.string.helper_bluetooth_usability);
    }

    @Override
    public void showBluetoothNotAvailable() {
        Toast toast = Toast.makeText(this, R.string.bluetooth_not_available, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }

    @Override
    public void showBluetoothRequestEnableIntent() {
        Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(intent, REQUEST_ENABLE_BT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                mPresenter.onEnableBtResponse();
            }
        }
    }

    @Override
    public void checkPermissions() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            int permissionCheck = this.checkSelfPermission("Manifest.permission.ACCESS_FINE_LOCATION");
            permissionCheck += this.checkSelfPermission("Manifest.permission.ACCESS_COARSE_LOCATION");
            if (permissionCheck != 0) {
                this.requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001); //Any number
            }
        } else {
            Log.d(TAG, "checkBTPermissions: nao precisa checar as permissoes. SDK version < LOLLIPOP.");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestro called");
        mPresenter.onDestroy();
    }

    public void findBluetoothDevices(View view) {
        mPresenter.discoverDevices();
    }

    public void displayDialogBikeRimNumberPicker() {

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.bike_rim_bike_devices_dialog, null);

        //TODO: set array to number picker
        final String[] rimsArray = getResources().getStringArray(R.array.rims);

        mBikeRimNumberPicker = (NumberPicker) dialogView.findViewById(R.id.bike_rim_number_picker);
        mBikeRimNumberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mBikeRimNumberPicker.setMinValue(0);
        mBikeRimNumberPicker.setMaxValue(rimsArray.length - 1);
        mBikeRimNumberPicker.setDisplayedValues(getResources().getStringArray(R.array.rims));
        mBikeRimNumberPicker.setWrapSelectorWheel(true);
        mBikeRimNumberPicker.setValue(5 < rimsArray.length ? 5 : 0);

        builder.setMessage(R.string.bike_request_rim_value_msg);
        builder.setView(dialogView)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int valueIndex = mBikeRimNumberPicker.getValue();
                        int rimValue = Integer.parseInt(rimsArray[valueIndex]);
                        Log.d(TAG, "index selecionado: " + valueIndex + " rim value: " + rimValue) ;
                        mPresenter.provideBikeRimValue(rimValue);
                    }
                });

        builder.create().show();
    }

    @Override
    public void clearBluetoothDeviceList() {
        mListAdapter.clear();
    }

}
