package android.scopus.com.bikeiot.hospitals;

import android.net.Uri;
import android.scopus.com.bikeiot.model.beans.Place;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

/**
 *
 */

public interface HospitalsContract {

    interface View {
        void showNetworkConnectionError();
        void showNoCloseHospitalWarning();
        void showMarkerInMap(LatLng locationLatLng, int titleId) ;
        void showRouteToHospital(PolylineOptions polylineOptions);
        void showRouteTraceOnCenter(LatLngBounds latLngBounds);
        void showClosestHospitalsPlaces(List<Place> hospitalsLocations);
        void showGoogleMapsNavigation(Uri uriGoogleMaps);
        void showHospitalSearchProgress();
        void hideHospitalSearchProgress();
    }

    interface Presenter {
        void initialize();
        //void requestClosestHospitals();
        void finalize();
        void getMyCurrentLocationAndRequestClosestHospitals();
        void selectPlace(Place hospitalPlace);
    }

}
