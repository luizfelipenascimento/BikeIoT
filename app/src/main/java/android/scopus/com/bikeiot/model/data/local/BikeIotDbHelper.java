package android.scopus.com.bikeiot.model.data.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.model.data.local.BikeIoTContract.*;
import android.util.Log;

/**
 * Created by Luiz Felipe on 08/02/2018.
 */

public class BikeIotDbHelper extends SQLiteOpenHelper {

    private static final String TAG = "BikeIotDbHelper";

    public static final String DATABASE_NAME = "bikeiotscopus.db";

    private static final int DATABASE_VERSION = 1;

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String DATETIME_TYPE = " DATETIME";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA = ",";

    private static final String SQL_ENTRY_CREATE_USER_TABLE =
            "CREATE TABLE " + UserEntry.TABLE_NAME + " (" +
                    UserEntry._ID + INTEGER_TYPE + " PRIMARY KEY AUTOINCREMENT" + COMMA +
                    UserEntry.COLUMN_USER_NAME + TEXT_TYPE + COMMA +
                    UserEntry.COLUMN_USER_BIRTHDAY + DATETIME_TYPE + COMMA +
                    UserEntry.COLUMN_USER_EMAIL + TEXT_TYPE + " NOT NULL"+ COMMA +
                    UserEntry.COLUMN_USER_WEIGHT + INTEGER_TYPE + COMMA +//decidir se o weight sera double ou inteiro
                    UserEntry.COLUMN_USER_HEIGHT + REAL_TYPE + COMMA +
                    UserEntry.COLUMN_USER_PASSWORD + TEXT_TYPE + " NOT NULL"+ COMMA +
                    UserEntry.COLUMN_USER_UUID + TEXT_TYPE + COMMA +
                    UserEntry._ON_CREATE + DATETIME_TYPE + " DEFAULT CURRENT_TIMESTAMP" + COMMA +
                    UserEntry._ON_UPDATE + DATETIME_TYPE +
            ")";

    private static final String SQL_ENTRY_CREATE_BIKE_EXERCISE_AND_RESULT_TABLE =
            "CREATE TABLE " + BikeExerciseAndResultEntry.TABLE_NAME + " (" +
                    BikeExerciseAndResultEntry._ID + INTEGER_TYPE + " PRIMARY KEY AUTOINCREMENT" + COMMA +
                    BikeExerciseAndResultEntry.COLUMN_DESCRIPTION + TEXT_TYPE + COMMA +
                    BikeExerciseAndResultEntry.COLUMN_FK_ROUTE_ID + INTEGER_TYPE + COMMA +
                    BikeExerciseAndResultEntry.COLUMN_FK_USER_ID + INTEGER_TYPE + COMMA +
                    BikeExerciseAndResultEntry.COLUMN_EXERCISE_DATE + DATETIME_TYPE + COMMA +
                    BikeExerciseAndResultEntry.COLUMN_RESULT_FILE_PATH + TEXT_TYPE + COMMA +
                    BikeExerciseAndResultEntry.COLUMN_RESULT_TOTAL_TIME_CHRONOMETER + DATETIME_TYPE + COMMA +
                    BikeExerciseAndResultEntry.COLUMN_RESULT_MAX_VELOCITY + REAL_TYPE + COMMA +
                    BikeExerciseAndResultEntry.COLUMN_RESULT_MEAN_VELOCITY + REAL_TYPE + COMMA +
                    BikeExerciseAndResultEntry.COLUMN_RESULT_MEAN_HEART_RATE + REAL_TYPE + COMMA +
                    BikeExerciseAndResultEntry.COLUMN_RESULT_CALORIES_BURNED + REAL_TYPE + COMMA +
                    BikeExerciseAndResultEntry.COLUMN_USED_BIKE_RIM + INTEGER_TYPE + COMMA +
                    BikeExerciseAndResultEntry._ON_CREATE + DATETIME_TYPE + " DEFAULT CURRENT_TIMESTAMP" + COMMA +
                    BikeExerciseAndResultEntry._ON_UPDATE + DATETIME_TYPE + COMMA +

                    "FOREIGN KEY (" + BikeExerciseAndResultEntry.COLUMN_FK_USER_ID + ") " +
                        " REFERENCES "+ UserEntry.TABLE_NAME +"("+ UserEntry._ID +")" + COMMA +

                    "FOREIGN KEY (" + BikeExerciseAndResultEntry.COLUMN_FK_ROUTE_ID + ") " +
                        " REFERENCES "+ RouteEntry.TABLE_NAME +"("+ RouteEntry._ID +")" +
            ")";


    private static final String SQL_ENTRY_CREATE_ROUTE_TABLE =
            "CREATE TABLE " + RouteEntry.TABLE_NAME + " (" +
                    RouteEntry._ID + INTEGER_TYPE + " PRIMARY KEY AUTOINCREMENT" + COMMA +
                    RouteEntry.COLUMN_ROUTE_DESCRIPTION + TEXT_TYPE + COMMA +
                    RouteEntry.COLUMN_ROUTE_DISTANCE + REAL_TYPE + COMMA +
                    RouteEntry.COLUMN_ROUTE_START_LATITUDE + REAL_TYPE + COMMA +
                    RouteEntry.COLUMN_ROUTE_START_LONGITUDE + REAL_TYPE + COMMA +
                    RouteEntry.COLUMN_ROUTE_END_LATITUDE + REAL_TYPE + COMMA +
                    RouteEntry.COLUMN_ROUTE_END_LONGITUDE + REAL_TYPE + COMMA +
                    RouteEntry.COLUMN_ROUTE_FILE_PATH + TEXT_TYPE + COMMA +
                    RouteEntry.COLUMN_ROUTE_DATE + DATETIME_TYPE + COMMA +
                    RouteEntry.COLUMN_IMAGE_PATH + TEXT_TYPE + COMMA +
                    RouteEntry.COLUMN_ROUTE_SHARED + INTEGER_TYPE + " DEFAULT " + RouteEntry.NOT_TO_SHARE + COMMA +
                    RouteEntry.COLUMN_ROUTE_FK_USER_ID + INTEGER_TYPE + COMMA +
                    RouteEntry._ON_CREATE + DATETIME_TYPE +  " DEFAULT CURRENT_TIMESTAMP" + COMMA +
                    RouteEntry._ON_UPDATE + DATETIME_TYPE + COMMA +

                    "FOREIGN KEY (" + RouteEntry.COLUMN_ROUTE_FK_USER_ID + ") " +
                        " REFERENCES "+ UserEntry.TABLE_NAME +"("+ UserEntry._ID +")" +
            ")";

    private static final String SQL_ENTRY_CREATE_CONTACT_TABLE =
            "CREATE TABLE " + ContactsEntry.TABLE_NAME + " (" +
                    ContactsEntry._ID + INTEGER_TYPE + " PRIMARY KEY AUTOINCREMENT" + COMMA +
                    ContactsEntry.COLUMN_NAME + TEXT_TYPE + COMMA +
                    ContactsEntry.COLUMN_PHONE + TEXT_TYPE + COMMA +
                    ContactsEntry.COLUMN_FK_USER + INTEGER_TYPE + COMMA +
                    ContactsEntry._ON_CREATE + DATETIME_TYPE +  " DEFAULT CURRENT_TIMESTAMP" + COMMA +
                    ContactsEntry._ON_UPDATE + DATETIME_TYPE + COMMA +

                    "FOREIGN KEY (" + ContactsEntry.COLUMN_FK_USER + ") " +
                        " REFERENCES " + UserEntry.TABLE_NAME + "(" + UserEntry._ID + ")" +
            ")";

    private static final String SQL_ENTRY_DROP_USER_TABLE =
            "DROP TABLE IF EXISTS " + UserEntry.TABLE_NAME;

    private static final String SQL_ENTRY_DROP_BIKE_EXERCISE_RESULT_TABLE =
            "DROP TABLE IF EXISTS " + BikeExerciseAndResultEntry.TABLE_NAME;

    private static final String SQL_ENTRY_DROP_ROUTE_TABLE =
            "DROP TABLE IF EXISTS " + RouteEntry.TABLE_NAME;

    private static final String SQL_ENTRY_DROP_CONTACTS_TABLE =
            "DROP TABLE IF EXISTS " + ContactsEntry.TABLE_NAME;

    public BikeIotDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate called");
        db.execSQL(SQL_ENTRY_CREATE_USER_TABLE);
        db.execSQL(SQL_ENTRY_CREATE_CONTACT_TABLE);
        db.execSQL(SQL_ENTRY_CREATE_ROUTE_TABLE);
        db.execSQL(SQL_ENTRY_CREATE_BIKE_EXERCISE_AND_RESULT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropDatabaseTables(db);
        onCreate(db);
    }

    public void dropDatabaseTables(SQLiteDatabase db) {
        db.execSQL(SQL_ENTRY_DROP_BIKE_EXERCISE_RESULT_TABLE);
        db.execSQL(SQL_ENTRY_DROP_ROUTE_TABLE);
        db.execSQL(SQL_ENTRY_DROP_CONTACTS_TABLE);
        db.execSQL(SQL_ENTRY_DROP_USER_TABLE);
    }
}
