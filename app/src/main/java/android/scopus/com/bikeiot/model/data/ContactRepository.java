package android.scopus.com.bikeiot.model.data;

import android.scopus.com.bikeiot.model.beans.Contact;

import java.util.List;

/**
 *
 */

public class ContactRepository implements ContactDataSource {

    private ContactDataSource mContactDataSource;
    private List<Contact> contactsCache;
    private Contact selectedContactCache;
    private boolean cacheBroken = true;

    public ContactRepository(ContactDataSource contactDataSource) {
        mContactDataSource = contactDataSource;
    }

    @Override
    public List<Contact> getContactsFromUser(int userId) {
        if (contactsCache == null || contactsCache.isEmpty() || cacheBroken) {
            List<Contact> contacts = mContactDataSource.getContactsFromUser(userId);
            refreshContactList(contacts);
        }
        return contactsCache;
    }

    @Override
    public Contact getContact(int id) {

        if (selectedContactCache == null || cacheBroken) {
            Contact contact = mContactDataSource.getContact(id);
            refreshSelectedContact(contact);
        }

        return selectedContactCache;
    }

    @Override
    public boolean save(Contact contact) {
        forceBreakCache();
        return mContactDataSource.save(contact);
    }

    @Override
    public boolean edit(Contact contact) {
        forceBreakCache();
        return mContactDataSource.edit(contact);
    }

    @Override
    public boolean remove(int id) {
        forceBreakCache();
        return mContactDataSource.remove(id);
    }

    public void forceBreakCache() {
        cacheBroken = true;
    }

    private void refreshContactList(List<Contact> contacts) {
        contactsCache = contacts;
        cacheBroken = false;
    }

    private void refreshSelectedContact(Contact contact) {
        selectedContactCache = contact;
        cacheBroken = false;
    }
}
