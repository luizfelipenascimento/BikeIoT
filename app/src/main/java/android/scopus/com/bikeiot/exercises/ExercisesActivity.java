package android.scopus.com.bikeiot.exercises;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.scopus.com.bikeiot.utils.Helper;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.exercisedetails.ExerciseDetailsActivity;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.data.BikeExerciseRepository;
import android.scopus.com.bikeiot.model.data.local.BikeExerciseDAO;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ExercisesActivity extends AppCompatActivity implements ExercisesContract.View,
        ExercisesListAdapter.ExercisesListClickListener {

    private static final String TAG = ExercisesActivity.class.getSimpleName();

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private NavigationView mNavigationView;
    private RecyclerView mExercisesRecyclerView;
    private ExercisesListAdapter mAdapter;

    private ExercisesPresenter mPresenter;
    private TextView mNoExerciseRegisteredTextView;
    private ImageView mNoExerciseRegisteredImageView;
    private LinearLayout mWarningContainerLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercises);

        //setting up toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.exercises_activity_title);

        //setting up navigation
        mNavigationView = (NavigationView) findViewById(R.id.main_navigation_view);
        setupDrawerNavigationContent(mNavigationView);

        //setting up adapter
        mExercisesRecyclerView = (RecyclerView) findViewById(R.id.exercises_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mExercisesRecyclerView.setLayoutManager(linearLayoutManager);

        mNoExerciseRegisteredTextView = (TextView) findViewById(R.id.warning_status_text_view);
        mNoExerciseRegisteredTextView.setText(R.string.no_exercise_done);
        mNoExerciseRegisteredImageView = (ImageView) findViewById(R.id.warning_status_image_view);
        mWarningContainerLinearLayout = (LinearLayout) findViewById(R.id.warning_status_container_linear_layout);
        mWarningContainerLinearLayout.setVisibility(View.GONE);

        //TODO: pegar o id do usuario na sessao para exibir os seus exercicios
        mPresenter = new ExercisesPresenter(this, new BikeExerciseRepository(new BikeExerciseDAO(this)),
                getSharedPreferencesUserId());

    }

    private int getSharedPreferencesUserId() {
        SharedPreferences preferences = this.getSharedPreferences(getString(R.string.shared_preferences_key), Context.MODE_PRIVATE);
        int id = preferences.getInt(getString(R.string.shared_user_id), 0);
        Log.d(TAG, "getSharedPreferencesUserId id: #" + id);
        return id;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mActionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerNavigationContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Helper.Menu.selectDrawerItem(ExercisesActivity.this, item, R.id.exercises_list_menu_item);
                mDrawerLayout.closeDrawers();
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume called");
        mPresenter.initialize();
    }

    @Override
    public void showExercises(List<BikeExercise> bikeExercises) {
        Log.d(TAG, "showExercises called");
        mWarningContainerLinearLayout.setVisibility(View.GONE);
        mAdapter = new ExercisesListAdapter(bikeExercises, this);
        mExercisesRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void showExerciseDetailsUi(int bikeExerciseId) {
        Log.d(TAG, "showExerciseDetailsUi() called");
        Intent intent = new Intent(this, ExerciseDetailsActivity.class);
        intent.putExtra(ExerciseDetailsActivity.EXTRA_BIKE_EXERCISE_ID, bikeExerciseId);
        startActivity(intent);
    }

    @Override
    public void showNoExercisesDone() {
        Log.d(TAG, "showNoExercisesDone() called");
        mWarningContainerLinearLayout.setVisibility(View.VISIBLE);
        mAdapter = new ExercisesListAdapter(new ArrayList<BikeExercise>(), this);
        mExercisesRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClickListItem(BikeExercise bikeExercise) {
        Log.d(TAG, "voce clicou no exercicio : #" + bikeExercise.getId() +
            "data: " + Helper.formatDateToMyDateFormat(bikeExercise.getExerciseDate()) +
            "descricao: " + bikeExercise.getDescription());
        mPresenter.openBikeExercise(bikeExercise);
    }
}
