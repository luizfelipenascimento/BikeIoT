package android.scopus.com.bikeiot.model.data.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.scopus.com.bikeiot.utils.Helper;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.model.data.RouteDataSource;
import android.scopus.com.bikeiot.model.data.local.BikeIoTContract.RouteEntry;


import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static android.scopus.com.bikeiot.utils.Helper.formatToDateString;
import static android.scopus.com.bikeiot.utils.Helper.parseToDate;


public class RouteDAO implements RouteDataSource {

    private BikeIotDbHelper mDbHelper;

    public RouteDAO(Context context) {
        mDbHelper = new BikeIotDbHelper(context);
    }


    private final String baseSelectProjection[] = {
            RouteEntry._ID,
            RouteEntry.COLUMN_ROUTE_DISTANCE,
            RouteEntry.COLUMN_ROUTE_DESCRIPTION,
            RouteEntry.COLUMN_ROUTE_END_LATITUDE,
            RouteEntry.COLUMN_ROUTE_END_LONGITUDE,
            RouteEntry.COLUMN_ROUTE_FILE_PATH,
            RouteEntry.COLUMN_ROUTE_START_LATITUDE,
            RouteEntry.COLUMN_ROUTE_START_LONGITUDE,
            RouteEntry.COLUMN_ROUTE_DATE,
            RouteEntry.COLUMN_IMAGE_PATH,
            RouteEntry.COLUMN_ROUTE_SHARED,
            RouteEntry.COLUMN_ROUTE_FK_USER_ID
    };

    private Route getRouteFromCursor(Cursor c) {
        int id = c.getInt(c.getColumnIndex(RouteEntry._ID));
        double distance = c.getDouble(c.getColumnIndex(RouteEntry.COLUMN_ROUTE_DISTANCE));
        String description = c.getString(c.getColumnIndex(RouteEntry.COLUMN_ROUTE_DESCRIPTION));
        double endLatitude = c.getDouble(c.getColumnIndex(RouteEntry.COLUMN_ROUTE_END_LATITUDE));
        double endLongitude = c.getDouble(c.getColumnIndex(RouteEntry.COLUMN_ROUTE_END_LONGITUDE));
        String filePath = c.getString(c.getColumnIndex(RouteEntry.COLUMN_ROUTE_FILE_PATH));
        double startLatitude = c.getDouble(c.getColumnIndex(RouteEntry.COLUMN_ROUTE_START_LATITUDE));
        double startLongitude = c.getDouble(c.getColumnIndex(RouteEntry.COLUMN_ROUTE_START_LONGITUDE));
        String date = c.getString(c.getColumnIndex(RouteEntry.COLUMN_ROUTE_DATE));
        String imagePath = c.getString(c.getColumnIndex(RouteEntry.COLUMN_IMAGE_PATH));
        int userId = c.getInt(c.getColumnIndex(RouteEntry.COLUMN_ROUTE_FK_USER_ID));
        int shared = c.getInt(c.getColumnIndex(RouteEntry.COLUMN_ROUTE_SHARED));

        Route route = new Route();
        route.setId(id);
        route.setUser(new User());
        route.setShared(shared);
        route.getUser().setId(userId);
        route.setDistance(distance);
        route.setDescription(description);
        route.setFileRoutePath(filePath);
        route.setEndLatLng(new LatLng(endLatitude, endLongitude));
        route.setStartLatLng(new LatLng(startLatitude, startLongitude));
        route.setDate(parseToDate(date));
        route.setImagePath(imagePath);
        return route;
    }

    private ContentValues createRouteBaseContentValues(Route route) {
        ContentValues values = new ContentValues();
        values.put(RouteEntry.COLUMN_ROUTE_DISTANCE, route.getDistance());
        values.put(RouteEntry.COLUMN_ROUTE_END_LATITUDE, route.getEndLatLng().latitude);
        values.put(RouteEntry.COLUMN_ROUTE_END_LONGITUDE, route.getEndLatLng().longitude);
        values.put(RouteEntry.COLUMN_ROUTE_START_LATITUDE, route.getStartLatLng().latitude);
        values.put(RouteEntry.COLUMN_ROUTE_START_LONGITUDE, route.getStartLatLng().longitude);
        values.put(RouteEntry.COLUMN_ROUTE_DISTANCE, route.getDistance());
        values.put(RouteEntry.COLUMN_ROUTE_DESCRIPTION, route.getDescription());
        values.put(RouteEntry.COLUMN_ROUTE_DATE, formatToDateString(route.getDate()));
        values.put(RouteEntry.COLUMN_ROUTE_SHARED, route.getShared());
        if (route.getUser() != null) {
            values.put(RouteEntry.COLUMN_ROUTE_FK_USER_ID, route.getUser().getId());
        }
        if (route.getImagePath() != null) {
            values.put(RouteEntry.COLUMN_IMAGE_PATH, route.getImagePath());
        }
        if (route.getRouteCsvFilePath() != null) {
            values.put(RouteEntry.COLUMN_ROUTE_FILE_PATH, route.getRouteCsvFilePath());
        }
        return values;
    }

    @Override
    public List<Route> getRoutes() {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = baseSelectProjection;

        Cursor cursor = db.query(RouteEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
                );

        try {
            ArrayList<Route> routes = new ArrayList<>();

            while (cursor.moveToNext()) {
                Route route = getRouteFromCursor(cursor);
                routes.add(route);
            }

            return routes;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }

        return Collections.EMPTY_LIST;
    }

    @Override
    public boolean save(Route route) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        ContentValues values = createRouteBaseContentValues(route);

        long idGenerated = db.insert(RouteEntry.TABLE_NAME, null, values);

        db.close();

        if (idGenerated != -1) {
            route.setId((int)idGenerated);
            return true;
        }

        return false;
    }

    @Override
    public boolean update(Route route) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = createRouteBaseContentValues(route);
        String[] whereArgs = {Integer.toString(route.getId())};

        int affectedRows = db.update(RouteEntry.TABLE_NAME,
                values, RouteEntry._ID + "=?", whereArgs);

        db.close();

        if (affectedRows == 1) {
            return true;
        }
        
        return false;
    }

    @Override
    public boolean renameRouteFilePath(Route route) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(RouteEntry.COLUMN_ROUTE_FILE_PATH, route.getRouteCsvFilePath());
        values.put(RouteEntry._ON_UPDATE, Helper.formatToDateTimeString(new Date()));

        String whereArgs[] = {Integer.toString(route.getId())};

        int rows = db.update(RouteEntry.TABLE_NAME, values, RouteEntry._ID + "=?", whereArgs);

        db.close();

        if (rows == 1) {
            return true;
        }

        return false;
    }

    @Override
    public boolean updateRenameImagePath(String newImagePath, int routeId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(RouteEntry.COLUMN_IMAGE_PATH, newImagePath);
        values.put(RouteEntry._ON_UPDATE, Helper.formatToDateTimeString(new Date()));

        String whereArgs[] = {Integer.toString(routeId)};

        int rows = db.update(RouteEntry.TABLE_NAME, values, RouteEntry._ID + "=?", whereArgs);

        db.close();

        if (rows == 1) {
            return true;
        }

        return false;
    }

    @Override
    public boolean remove(int routeId) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String whereClause = RouteEntry._ID + "=?";

        String[] whereArgs = {String.valueOf(routeId)};

        int affected = db.delete(RouteEntry.TABLE_NAME, whereClause, whereArgs);

        db.close();

        if (affected == 1) {
            return true;
        }

        return false;
    }

    @Override
    public boolean updateShare(int id ,int share) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String whereClause = RouteEntry._ID + "=?";

        String[] whereArgs = {String.valueOf(id)};

        ContentValues values = new ContentValues();
        values.put(RouteEntry.COLUMN_ROUTE_SHARED, share);
        values.put(RouteEntry._ON_UPDATE, Helper.formatDateToTimeString(new Date()));

        int affected = db.update(RouteEntry.TABLE_NAME, values, whereClause, whereArgs);

        db.close();

        if (affected == 1) {
            return true;
        }

        return false;
    }

    @Override
    public Route getRoute(int routeId) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = baseSelectProjection;

        String whereClause = RouteEntry._ID + "=?";

        String[] whereArgs = {String.valueOf(routeId)};

        Cursor cursor = db.query(RouteEntry.TABLE_NAME,
                projection,
                whereClause,
                whereArgs,
                null,
                null,
                null
        );

        Route route = null;

        try {
            cursor.moveToFirst();
            route = getRouteFromCursor(cursor);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
            db.close();
        }

        return route;
    }
}
