package android.scopus.com.bikeiot.bikebluetoothdevice;

import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.scopus.com.bikeiot.model.beans.BikeDevice;

import java.util.ArrayList;

/**
 * Created by Luiz Felipe on 13/12/2017.
 */

public interface BikeDevicesContract {

    interface View {
        void displayBluetoothDevices(ArrayList<BluetoothDevice> devices);
        void displayNoBluetoothDevicesFound();
        void showSearching();
        void hideSearchingBluetoothProgress();
        void hideWarningGeneralInfo();
        void showHelperInfo();
        void showBluetoothNotAvailable();
        void showBluetoothRequestEnableIntent();
        void checkPermissions();
        void sendDeviceToExerciseRecorderUi(BluetoothDevice device, BikeDevice bikeDevice);
        void sendResponseForExerciseRecorderUi(BikeDevice bikeDevice);
        void displayDialogBikeRimNumberPicker();
        void clearBluetoothDeviceList();

    }

    interface Presenter {
        void discoverDevices();
        BluetoothDevice selectDevice(int index);
        void onDestroy();
        void provideBikeRimValue(int rim);

        void onEnableBtResponse();
    }

}
