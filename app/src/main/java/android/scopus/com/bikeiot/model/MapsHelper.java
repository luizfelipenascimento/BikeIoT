package android.scopus.com.bikeiot.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import static android.scopus.com.bikeiot.utils.CalculateUtils.createLatLngFromReference;

/**
 * Created by Luiz Felipe on 03/04/2018.
 */

public class MapsHelper {

    /**
     * Calcula os limites para uma sequencia de pontos de localização adicionando dois pontos extras
     * próximo ao centro da rota.
     * Esse método serve para auxiliar na exibição do LatLngBounds em uma mapa. Ao adicionar pontos
     * próximo ao centro é possivel controlar o valor do zoom que será utilizado pelo mapa para
     * exibir os LatLngBounds.
     * @param locations pontos de localização
     * @param displaceUp deslocamento em metros para cima
     * @param displaceDown deslocamento em metros para baixo
     * @param displaceRight deslocamento em metros para direita
     * @param displaceLeft deslocamento em metros para esquerda
     * @return LatLngBounds com os deslocamentos adicionados em seu centro.
     */
    public static LatLngBounds calculateLatLngBoundsMinCenterDisplacement(
            List<LatLng> locations, double displaceUp, double displaceDown,
            double displaceRight, double displaceLeft) {
        LatLngBounds.Builder latLngBounds = LatLngBounds.builder();
        for (LatLng latLng : locations) {
            latLngBounds.include(latLng);
        }

        //adicionando novos pontos ao centro para regular o zoom
        LatLngBounds auxLatLngBounds = latLngBounds.build();
        LatLng center = auxLatLngBounds.getCenter();

        //criando novo ponto ao norte e ao sul com deslocamento de 200 metros para cada lado
        LatLng displacementNorthEast = createLatLngFromReference(center, displaceUp, displaceRight);
        LatLng displacementSouthWest = createLatLngFromReference(center, displaceDown, displaceLeft);

        //incluindo os pontos para deslocamento ao bound
        latLngBounds.include(displacementNorthEast);
        latLngBounds.include(displacementSouthWest);

        return latLngBounds.build();
    }

    public static PolylineOptions createPolylineOptions(List<LatLng> locations, int color, int width) {
        PolylineOptions polylineOptions = new PolylineOptions();
        for(LatLng location : locations) {
            polylineOptions.add(location);
        }
        polylineOptions.color(color);
        polylineOptions.width(width);
        return polylineOptions;
    }

}
