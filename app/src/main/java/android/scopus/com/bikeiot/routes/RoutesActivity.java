package android.scopus.com.bikeiot.routes;

import android.content.Intent;
import android.scopus.com.bikeiot.utils.Helper;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.data.RouteRepository;
import android.scopus.com.bikeiot.model.data.local.RouteDAO;
import android.scopus.com.bikeiot.routedetails.RouteDetailsActivity;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

public class RoutesActivity extends AppCompatActivity implements RoutesContract.View,
        RouteListAdapter.RouteListClickListener {

    private static final String TAG = RoutesActivity.class.getSimpleName();

    private Toast mToast;
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private NavigationView mNavigationView;
    private RecyclerView mRoutesRecyclerView;
    private RoutesPresenter mRoutesPresenter;
    private RouteListAdapter mRouteListAdapter;
    private ImageView mNoRoutesRegisteredImageView;
    private TextView mNoRoutesRegisteredTextView;
    private LinearLayout mWarningContainerLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routes);


        //setting up toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.routes_activity_title);

        //setting up navigation
        mNavigationView = (NavigationView) findViewById(R.id.main_navigation_view);
        setupDrawerNavigationContent(mNavigationView);

        //setting up adapter
        mRoutesRecyclerView = (RecyclerView) findViewById(R.id.routes_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRoutesRecyclerView.setLayoutManager(linearLayoutManager);

        mNoRoutesRegisteredTextView = (TextView) findViewById(R.id.warning_status_text_view);
        mNoRoutesRegisteredTextView.setText(R.string.no_route_registered);
        mNoRoutesRegisteredImageView = (ImageView) findViewById(R.id.warning_status_image_view);
        mWarningContainerLinearLayout = (LinearLayout) findViewById(R.id.warning_status_container_linear_layout);
        mWarningContainerLinearLayout.setVisibility(View.GONE);


        mRoutesPresenter = new RoutesPresenter(this, new RouteRepository(new RouteDAO(this)));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mRoutesPresenter.initialize();
    }

    private void setupDrawerNavigationContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Helper.Menu.selectDrawerItem(RoutesActivity.this, item, R.id.routes_list_menu_item);
                mDrawerLayout.closeDrawers();
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mActionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showRoutes(List<Route> routes) {
        mWarningContainerLinearLayout.setVisibility(View.GONE);
        mRouteListAdapter = new RouteListAdapter(routes, this, this, Glide.with(this));
        mRoutesRecyclerView.setAdapter(mRouteListAdapter);
    }

    @Override
    public void showNoRoutes() {
        mRoutesRecyclerView.setAdapter(null);
        mWarningContainerLinearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showRouteDetailsUi(int routeId) {
        Intent intent = new Intent(this, RouteDetailsActivity.class);
        intent.putExtra(RouteDetailsActivity.EXTRA_ROUTE_ID, routeId);
        startActivity(intent);
    }

    @Override
    public void showErrorInSelectProcess(int errorId) {
        if (mToast != null) {
            mToast.cancel();
        }

        mToast = Toast.makeText(this, errorId, Toast.LENGTH_SHORT);
        mToast.show();
    }

    @Override
    public void onClickListItem(Route route) {
        mRoutesPresenter.selectRoute(route.getId());
    }
}
