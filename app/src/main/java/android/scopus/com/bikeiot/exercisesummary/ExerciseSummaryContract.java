package android.scopus.com.bikeiot.exercisesummary;

import android.graphics.Bitmap;
import android.net.Uri;
import android.scopus.com.bikeiot.model.beans.BikeExercise;

/**
 * Created by Luiz Felipe on 21/03/2018.
 */

public interface ExerciseSummaryContract {

    interface View {
        void displayExerciseResume(BikeExercise bikeExercise);
        void showBikeExerciseDetailsUi(int bikeExerciseId);
        void showNoBikeExerciseError();
        void showGeneralErrorMsg(int msgId);
        void showRouteImage(Uri uri);
        void showNoRouteImage();
        void showRemoveSuccess();
        void endActivity();
    }

    interface Presenter {
        void initialize();
        void loadBikeExercise(int bikeExerciseId);
        void updateBikeExerciseAndRouteDescription(String description);
        void removeBikeExercise();
    }

}
