package android.scopus.com.bikeiot.model.network.snaptoroadAPI;

import android.content.Context;
import android.scopus.com.bikeiot.R;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luiz Felipe on 16/03/2018.
 */

public class SnapToRoadQueryUtils {

    public static final String TAG = SnapToRoadQueryUtils.class.getSimpleName();

    public static String getUrlString(String baseUrl, List<LatLng> locationPoints, String apiKey) {
        StringBuilder urlStringBuilder = new StringBuilder(baseUrl);
        for (int i = 0; i < locationPoints.size(); i++) {
            urlStringBuilder.append(locationPoints.get(i).latitude);
            urlStringBuilder.append(",");
            urlStringBuilder.append(locationPoints.get(i).longitude);
            if (i != locationPoints.size() - 1) {
                urlStringBuilder.append("|");
            }
        }
        urlStringBuilder.append("&interpolate=true");
        urlStringBuilder.append("&key=" + apiKey);

        Log.d(TAG, "getUrlString generated Url: " + urlStringBuilder.toString());

        return urlStringBuilder.toString();
    }

    public static URL createURL(String urlString) {
        URL url = null;
        try {
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            Log.e(TAG, "createURL error : " + e);
            e.printStackTrace();
        }
        return url;
    }

    public static ArrayList<LatLng> fetchDataSnapToRoad(String urlString) {
        URL url = createURL(urlString);
        String jsonResponse = null;

        try {
            jsonResponse = makeRequest(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayList<LatLng> locations = parseJsonToLatLngList(jsonResponse);
        return locations;
    }

    private static String makeRequest(URL url) throws IOException {
        String jsonResponse = "";

        if (url == null) {
            Log.d(TAG, "makeRequest url is null");
            return jsonResponse;
        }

        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;

        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setConnectTimeout(1000);
            httpURLConnection.setReadTimeout(1500);
            httpURLConnection.connect();

            if (httpURLConnection.getResponseCode() == 200) {
                inputStream = httpURLConnection.getInputStream();
                jsonResponse = readInputStream(inputStream);
                Log.d(TAG, "makeRequest jsonResponse: " + jsonResponse);
            }

        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "makeRequest error: " + e);
        }

        return jsonResponse;
    }

    private static String readInputStream(InputStream inputStream) throws IOException {
        StringBuilder jsonResponse = new StringBuilder();
        if (inputStream != null) {
            Log.d(TAG, "readInputStream inputStream is not null");
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                jsonResponse.append(line);
                line = reader.readLine();
            }
        }
        return jsonResponse.toString();
    }


    private static ArrayList<LatLng> parseJsonToLatLngList(String jasonResponseString) {
        Log.d(TAG, "parseJsonToLatLngList called");
        ArrayList<LatLng> locationPoints = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(jasonResponseString);
            JSONArray snappedPoints = jsonObject.getJSONArray("snappedPoints");
            for (int i = 0; i < snappedPoints.length(); i++) {
                JSONObject snappedJsonObject = snappedPoints.getJSONObject(i);
                JSONObject locationJsonObject = snappedJsonObject.getJSONObject("location");
                double lat = locationJsonObject.getDouble("latitude");
                double lng = locationJsonObject.getDouble("longitude");
                Log.d(TAG, "parseJsonToLatLngList lat: " + lat + " lng: " + lng);
                LatLng latLng = new LatLng(lat, lng);
                locationPoints.add(latLng);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return locationPoints;
    }

}
