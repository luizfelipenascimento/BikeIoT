package android.scopus.com.bikeiot.model.data.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.scopus.com.bikeiot.utils.Helper;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.model.data.UserDataSource;
import android.scopus.com.bikeiot.model.data.local.BikeIoTContract.UserEntry;
import android.util.Log;

import java.util.UUID;

public class UserDAO implements UserDataSource {

    private static final String TAG = UserDAO.class.getSimpleName();
    private BikeIotDbHelper mDbHelper;

    public UserDAO(Context context) {
        mDbHelper = new BikeIotDbHelper(context);
    }

    public boolean saveUser(User user) {

        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = generateContentValues(user);

        long generatedId = db.insert(UserEntry.TABLE_NAME, null,values);

        db.close();

        if(generatedId != -1) {
            user.setId((int) generatedId);
            return true;
        }

        return false;
    }

    @Override
    public boolean update(User user) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = generateContentValues(user);
        String whereClause = UserEntry._ID + "=?";
        String[] whereArgs = {String.valueOf(user.getId())};

        int affected = db.update(UserEntry.TABLE_NAME, values, whereClause, whereArgs);

        db.close();

        return affected == 1;
    }

    private ContentValues generateContentValues(User user) {
        ContentValues values = new ContentValues();
        values.put(UserEntry.COLUMN_USER_NAME, user.getName());
        values.put(UserEntry.COLUMN_USER_EMAIL, user.getEmail());
        values.put(UserEntry.COLUMN_USER_PASSWORD, user.getPassword());
        values.put(UserEntry.COLUMN_USER_BIRTHDAY,
                Helper.formatToDateString(user.getBirthday()));
        values.put(UserEntry.COLUMN_USER_WEIGHT, user.getWeight());
        values.put(UserEntry.COLUMN_USER_HEIGHT, user.getHeight());
        values.put(UserEntry.COLUMN_USER_UUID, user.getUUID().toString());
        return values;
    }

    @Override
    public User getUser(int userId) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] project = {
                UserEntry._ID,
                UserEntry.COLUMN_USER_NAME,
                UserEntry.COLUMN_USER_BIRTHDAY,
                UserEntry.COLUMN_USER_EMAIL,
                UserEntry.COLUMN_USER_PASSWORD,
                UserEntry.COLUMN_USER_WEIGHT,
                UserEntry.COLUMN_USER_HEIGHT,
                UserEntry.COLUMN_USER_UUID
        };

        String selection = UserEntry._ID + "=?";
        String[] args = {Integer.toString(userId)};
        Cursor cursor = db.query(UserEntry.TABLE_NAME,
                project,
                selection,
                args,
                null,
                null,
                null
                );

        try {
            cursor.moveToFirst();

            User user = getDataFromCursor(cursor);

            return user;
        } catch (Exception e ) {
            e.printStackTrace();
            Log.d(TAG, "getUser: ocorreu um erro durante a consulta");
        } finally {
            cursor.close();
            db.close();
        }
        return null;
    }

    private User getDataFromCursor(Cursor c) {
        int id = c.getInt(c.getColumnIndex(UserEntry._ID));
        String name = c.getString(c.getColumnIndex(UserEntry.COLUMN_USER_NAME));
        String email = c.getString(c.getColumnIndex(UserEntry.COLUMN_USER_EMAIL));
        String birthday = c.getString(c.getColumnIndex(UserEntry.COLUMN_USER_BIRTHDAY));
        String password = c.getString(c.getColumnIndex(UserEntry.COLUMN_USER_PASSWORD));
        int weight = c.getInt(c.getColumnIndex(UserEntry.COLUMN_USER_WEIGHT));
        double height = c.getDouble(c.getColumnIndex(UserEntry.COLUMN_USER_HEIGHT));
        String UUIDString = c.getString(c.getColumnIndex(UserEntry.COLUMN_USER_UUID));

        User user = new User();
        user.setBirthday(Helper.parseToDate(birthday));
        user.setName(name);
        user.setEmail(email);
        user.setPassword(password);
        user.setId(id);
        user.setWeight(weight);
        user.setHeight(height);
        user.setUUID(UUID.fromString(UUIDString));
        return user;
    }

    @Override
    public User getUserByEmail(String email) {
        return null;
    }
}
