package android.scopus.com.bikeiot.model.filerelated;

import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 */

public class FileRepository {

    private static final String TAG = FileRepository.class.getSimpleName();
    public static String UNDERLINE_SEPARATOR = "_";

    private final Context mContext;
    protected String mDirectory;
    protected FileOutputStream mFileOutputStream;
    protected File mFile;
    protected File mFolder;
    protected File mExtension;

    public FileRepository(Context context) {
        mContext = context;
    }

    public File createFile(String fileName, String directory, String fileExtension, File storage) {
        mDirectory = directory;
        mFolder = new File(storage, mDirectory);
        createFolderIfDoesNotExists(mFolder);

        String fileGeneratedName = setupFileName(fileName, fileExtension);
        mFile = new File(mFolder, fileGeneratedName);
        try {
            mFileOutputStream = new FileOutputStream(mFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally {
            try {
                mFileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        scanFile(mFile);
        if (mFile.exists()) {
            return mFile;
        } else {
            return null;
        }
    }

    public static String setupFileName(String fileName, String fileExtension) {
        return fileName + fileExtension;
    }

    public String setupFileName(String fileName) {
        return fileName + mExtension;
    }

    public File createFolderIfDoesNotExists(File folder) {
        if (!folder.exists()) {
            folder.mkdirs();
            folder.setReadable(true);
            folder.setExecutable(true);
            folder.setWritable(true);
        }
        return folder;
    }

    public File renameFile(String oldFilePathName, String newName, String newFileExtension) {
        Log.d(TAG, "renameFile called");
        mDirectory = extractDirectory(oldFilePathName);
        mFile = new File(oldFilePathName);
        File newFile = new File(mDirectory, setupFileName(newName, newFileExtension));
        makeFileAvailable(newFile);
        if (mFile.renameTo(newFile)) {
            scanFile(mFile);
            Log.d(TAG, "renameFile: succeed");
            mFile = newFile;
            scanFile(mFile);
        }
        return mFile;
    }

    protected static void makeFileAvailable(File file) {
        file.setReadable(true);
        file.setExecutable(true);
        file.setWritable(true);
    }

    public static String renameFile(Context context, String oldFilePathName, String newName, String fileExtension) {
        Log.d(TAG, "renameFile called");
        String directory = extractDirectory(oldFilePathName);
        File file = new File(oldFilePathName);
        File newFile = new File(directory, setupFileName(newName, fileExtension));
        makeFileAvailable(newFile);
        if (file.renameTo(newFile)) {
            scanFile(file, context);
            Log.d(TAG, "renameFile: succeed");
            file = newFile;
            scanFile(file, context);
        }
        return file.getAbsolutePath();
    }

    protected static String extractDirectory(String filePathName) {
        if (filePathName.lastIndexOf(File.separator) + 1 == filePathName.length()) {
            return filePathName;
        } else {
            return filePathName.substring(0, filePathName.lastIndexOf(File.separator) + 1);
        }
    }

    public void scanFile(File file) {
        MediaScannerConnection.scanFile(mContext, new String[] {file.toString()}, null, null);
    }

    public static void scanFile(File file, Context context) {
        MediaScannerConnection.scanFile(context, new String[] {file.toString()}, null, null);
        sendBroadcastScanner(file, context);
    }

    public static String generateNameWithSeparator(String... strings) {
        String completeName = "";
        for (String st : strings) {
            st = st.replaceAll("\\s+", UNDERLINE_SEPARATOR);
            Log.d(TAG, "novo nome: " + st);
        }

        for (int i = 0; i < strings.length; i++) {
            completeName += strings[i] + UNDERLINE_SEPARATOR;
        }
        return completeName;
    }

    public File moveFile(File newFolder, File file) {

        if (!newFolder.exists()) {
            newFolder = createFolderIfDoesNotExists(newFolder);
            scanFile(newFolder);
        }

        File newFilePlace = new File(newFolder, file.getName());

        if (file.renameTo((newFilePlace))) {
            scanFile(file);
            file = newFilePlace;
            scanFile(file);
            return file;
        }

        return file;
    }

    private static void sendBroadcastScanner(File file, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            final Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            final Uri contentUri = Uri.fromFile(file);
            scanIntent.setData(contentUri);
            context.sendBroadcast(scanIntent);
        } else {
            final Intent intent = new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse(file.getAbsolutePath()));
            context.sendBroadcast(intent);
        }
    }
}
