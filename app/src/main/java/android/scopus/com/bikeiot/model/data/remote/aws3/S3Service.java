package android.scopus.com.bikeiot.model.data.remote.aws3;

import android.content.Context;
import android.scopus.com.bikeiot.Constants;
import android.scopus.com.bikeiot.model.network.NetworkHelper;
import android.util.Log;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobileconnectors.s3.transfermanager.internal.TransferManagerUtils;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.File;

import static android.scopus.com.bikeiot.Constants.KEY_S3;
import static android.scopus.com.bikeiot.Constants.SECRET_S3;

/**
 *
 */

public class S3Service {

    private static final String TAG = S3Service.class.getSimpleName();

    private final Context mContext;
    private AmazonS3Client s3Client;
    private BasicAWSCredentials credentials;
    private NetworkHelper mNetworkHelper;

    private OnUploadedFileListener mOnUploadListener;

    interface OnUploadedFileListener {
        void OnUploadedFileCompleted(File file);
    }

    public S3Service(Context context, OnUploadedFileListener callbackListener) {
        Log.d(TAG, "S3Service");
        mContext = context;
        mNetworkHelper = new NetworkHelper(context);
        mOnUploadListener = callbackListener;
        AWSMobileClient.getInstance().initialize(context).execute();
        credentials = new BasicAWSCredentials(KEY_S3, SECRET_S3);
        s3Client = new AmazonS3Client(credentials);
    }

    public void uploadFile(final File file) {
        Log.d(TAG, "uploadFile called");
        s3Client.putObject(new PutObjectRequest(Constants.S3_BUCKET_NAME, file.getName(), file));
        if (file.exists() && !file.isDirectory()) {



            TransferUtility transferUtility =
                    TransferUtility.builder()
                            .context(mContext)
                            .awsConfiguration(AWSMobileClient.getInstance().getConfiguration())
                            .s3Client(s3Client)
                            .build();

            TransferObserver uploadObserver =
                    transferUtility.upload(file.getName(), file);

            uploadObserver.setTransferListener(new TransferListener() {
                @Override
                public void onStateChanged(int id, TransferState state) {
                    if (TransferState.COMPLETED == state) {
                        Log.d(TAG, "completed");
                        mOnUploadListener.OnUploadedFileCompleted(file);
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

                }

                @Override
                public void onError(int id, Exception ex) {
                    Log.d(TAG, "error happened on upload file");
                }
            });
        }
    }
}
