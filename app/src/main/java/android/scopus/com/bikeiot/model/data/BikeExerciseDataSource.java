package android.scopus.com.bikeiot.model.data;

import android.scopus.com.bikeiot.model.beans.BikeExercise;

import java.util.List;

/**
 * Created by Luiz Felipe on 22/02/2018.
 */

public interface BikeExerciseDataSource {

    List<BikeExercise> getBikeExercises();
    List<BikeExercise> getBikeExercisesFromUser(int userId);
    boolean saveBikeExercise(BikeExercise bikeExercise);
    BikeExercise getBikeExercise(int bikeExerciseId);
    boolean updateBikeExercise(BikeExercise bikeExercise);
    boolean renameResultFilePath(BikeExercise bikeExercise);
    boolean removeBikeExercise(int id);
    void deleteAll();
}
