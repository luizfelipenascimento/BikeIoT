package android.scopus.com.bikeiot.model.data;

import android.scopus.com.bikeiot.model.beans.Contact;

import java.util.List;

/**
 *
 */

public interface ContactDataSource {

    List<Contact> getContactsFromUser(int userId);
    Contact getContact(int id);
    boolean save(Contact contact);
    boolean edit(Contact contact);
    boolean remove(int id);

}
