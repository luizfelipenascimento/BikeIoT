package android.scopus.com.bikeiot.model.data;

import android.scopus.com.bikeiot.model.beans.BikeExercise;

import java.util.List;

/**
 *
 */

public class BikeExerciseRepository implements BikeExerciseDataSource {

    private List<BikeExercise> mBikeExercisesCache;
    private BikeExercise mBikeExerciseCache;
    private boolean cacheBroken = true;
    private BikeExerciseDataSource mBikeExerciseDataSource;

    public BikeExerciseRepository(BikeExerciseDataSource bikeExerciseDataSource) {
        mBikeExerciseDataSource = bikeExerciseDataSource;
    }

    @Override
    public List<BikeExercise> getBikeExercises() {
        if (mBikeExercisesCache == null || mBikeExercisesCache.isEmpty() || cacheBroken) {
            List<BikeExercise> bikeExercises = mBikeExerciseDataSource.getBikeExercises();
            refreshBikeExercises(bikeExercises);
        }
        return mBikeExercisesCache;
    }

    @Override
    public List<BikeExercise> getBikeExercisesFromUser(int userId) {
        if (mBikeExercisesCache == null || mBikeExercisesCache.isEmpty() || cacheBroken) {
            List<BikeExercise> bikeExercises = mBikeExerciseDataSource.getBikeExercisesFromUser(userId);
            refreshBikeExercises(bikeExercises);
        }
        return mBikeExercisesCache;
    }

    @Override
    public boolean saveBikeExercise(BikeExercise bikeExercise) {
        forceCacheBroke();
        return mBikeExerciseDataSource.saveBikeExercise(bikeExercise);
    }

    @Override
    public BikeExercise getBikeExercise(int bikeExerciseId) {
        if (mBikeExerciseCache == null || cacheBroken) {
            BikeExercise bikeExercise = mBikeExerciseDataSource.getBikeExercise(bikeExerciseId);
            refreshBikeExercise(bikeExercise);
        }
        return mBikeExerciseCache;
    }

    private void refreshBikeExercise(BikeExercise bikeExercise) {
        mBikeExerciseCache = bikeExercise;
        cacheBroken = false;
    }

    @Override
    public boolean updateBikeExercise(BikeExercise bikeExercise) {
        forceCacheBroke();
        return mBikeExerciseDataSource.updateBikeExercise(bikeExercise);
    }

    @Override
    public boolean renameResultFilePath(BikeExercise bikeExercise) {
        forceCacheBroke();
        return mBikeExerciseDataSource.renameResultFilePath(bikeExercise);
    }

    @Override
    public boolean removeBikeExercise(int id) {
        forceCacheBroke();
        return mBikeExerciseDataSource.removeBikeExercise(id);
    }

    @Override
    public void deleteAll() {
        forceCacheBroke();
        mBikeExerciseDataSource.deleteAll();
    }

    public void forceCacheBroke() {
        cacheBroken = true;
    }

    private void refreshBikeExercises(List<BikeExercise> bikeExercises) {
        if(mBikeExerciseCache != null) {
            mBikeExercisesCache.clear();
        }

        mBikeExercisesCache = bikeExercises;
        cacheBroken = false;
    }
}
