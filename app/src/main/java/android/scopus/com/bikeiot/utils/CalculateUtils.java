package android.scopus.com.bikeiot.utils;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Luiz Felipe on 20/03/2018.
 */

public class CalculateUtils {

    private static final double EARTH_RADIUS = 6366198;

    private CalculateUtils() {}

    public static double inchToMillimeters(double inches) {
        return inches * 25.4;
    }

    public static double centimeterToMeter(double centimeter) {
        return centimeter / 100;
    }

    public static double metersToKm(double meters){
        return meters / 1000;
    }

    public static double millimetersToCentimeters(double millimeters) {
        return millimeters / 10;
    }

    public static double metersPerSecToKmPerH(double metersPerSec) {
        return metersPerSec * 3.6;
    }

    public static LatLng createLatLngFromReference(LatLng referenceLatLng, double toNorth, double toEast) {
        double lonDiff = meterToLongitude(toEast, referenceLatLng.latitude);
        double latDiff = meterToLatitude(toNorth);
        return new LatLng(referenceLatLng.latitude + latDiff, referenceLatLng.longitude
                + lonDiff);
    }

    private static double meterToLongitude(double metersEast, double latitude) {
        double latArc = Math.toRadians(latitude);
        double radius = Math.cos(latArc) * EARTH_RADIUS;
        double rad = metersEast / radius;
        return Math.toDegrees(rad);
    }


    private static double meterToLatitude(double metersNorth) {
        double rad = metersNorth / EARTH_RADIUS ;
        return Math.toDegrees(rad);
    }

}
