package android.scopus.com.bikeiot.model.beans;

import android.bluetooth.BluetoothDevice;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Luiz Felipe on 30/11/2017.
 */

public class BikeExercise implements Serializable {

    private int mId;
    private User mUser;
    private Result mResult;
    private Route mRoute;
    private Date mExerciseDate;
    private String mDescription;
    private BikeDevice mBikeDevice;

    //TODO: colocar o total time no result! ?
    private Date mTotalTime;

    //private BluetoothDevice mBluetoothBikeDevice;

    public void setUser(User user) {
        mUser = user;
    }

//    public void setBluetoothBikeDevice(BluetoothDevice bluetoothBikeDevice) {
//        mBluetoothBikeDevice = bluetoothBikeDevice;
//    }

    public void setResult(Result result) {
        mResult = result;
    }

    public void setRoute(Route route) {
        mRoute = route;
    }

    public void setExerciseDate(Date exerciseDate) {
        mExerciseDate = exerciseDate;
    }

    public void setId(int id) {
        mId = id;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public void setBikeDevice(BikeDevice bikeDevice) {
        mBikeDevice = bikeDevice;
    }

    public int getId() {
        return mId;
    }

    public User getUser() {
        return mUser;
    }

//    public BluetoothDevice getBluetoothBikeDevice() {
//        return mBluetoothBikeDevice;
//    }


    public BikeDevice getBikeDevice() {
        return mBikeDevice;
    }

    public String getDescription() {
        return mDescription;
    }

    public Result getResult() {
        return mResult;
    }

    public Route getRoute() {
        return mRoute;
    }

    public Date getExerciseDate() {
        return mExerciseDate;
    }
}
