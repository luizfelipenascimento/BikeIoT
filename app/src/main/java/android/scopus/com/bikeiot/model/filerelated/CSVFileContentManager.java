package android.scopus.com.bikeiot.model.filerelated;

import java.util.List;

/**
 */

public class CSVFileContentManager {

    private List<String[]> mFileContent;
    private String[] mFileHeader;

    public CSVFileContentManager(List<String[]> fileContent) {
        mFileContent = fileContent;
        setupHeader();
    }

    public int getNumberOfLines() {
        return mFileContent.size();
    }

    public String getColumnValue(String column, int line) throws NoCSVColumnException {
        int index = getColumnIndex(column);
        if (index == -1) {
            throw new NoCSVColumnException("Column not found");
        }

        String[] lineContent = mFileContent.get(line);
        return lineContent[index];
    }

    private int getColumnIndex(String column) {
        for (int i = 0;  i < mFileHeader.length; i++) {
            if (mFileHeader[i].equals(column)) {
                return i;
            }
        }
        return -1;
    }

    public void setupHeader() {
        mFileHeader = mFileContent.get(0);
        mFileContent.remove(0);
    }

    public String[] getFileHeader() {
        return mFileHeader;
    }

    public class NoCSVColumnException extends Exception {
        NoCSVColumnException(String message) {
            super(message);
        }
    }
}
