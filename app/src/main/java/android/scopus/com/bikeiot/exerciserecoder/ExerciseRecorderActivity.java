package android.scopus.com.bikeiot.exerciserecoder;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaScannerConnection;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.scopus.com.bikeiot.utils.CalculateUtils;
import android.scopus.com.bikeiot.utils.Helper;
import android.scopus.com.bikeiot.utils.PermissionHelper;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.bikebluetoothdevice.BikeDevicesActivity;
import android.scopus.com.bikeiot.common.ConfirmationDialog;
import android.scopus.com.bikeiot.common.WarningDialog;
import android.scopus.com.bikeiot.exercisesummary.ExerciseSummaryActivity;
import android.scopus.com.bikeiot.hospitals.HospitalsActivity;
import android.scopus.com.bikeiot.common.BitmapCircle;
import android.scopus.com.bikeiot.model.filerelated.FileContractConstants;
import android.scopus.com.bikeiot.model.filerelated.FileRepository;
import android.scopus.com.bikeiot.model.location.LocationHandler;
import android.scopus.com.bikeiot.model.MapsHelper;
import android.scopus.com.bikeiot.model.beans.BikeDevice;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.filerelated.CSVFile;
import android.scopus.com.bikeiot.model.beans.Result;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.model.data.local.BikeExerciseDAO;
import android.scopus.com.bikeiot.model.data.local.RouteDAO;
import android.scopus.com.bikeiot.model.data.local.UserDAO;
import android.scopus.com.bikeiot.model.network.NetworkHelper;
import android.scopus.com.bikeiot.model.network.snaptoroadAPI.SnapToRoadGetData;
import android.scopus.com.bikeiot.routedetails.RouteDetailsActivity;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static android.scopus.com.bikeiot.utils.CalculateUtils.metersPerSecToKmPerH;
import static android.scopus.com.bikeiot.utils.CalculateUtils.metersToKm;
import static android.scopus.com.bikeiot.Constants.BASE_PERMISSION_CODE;
import static android.scopus.com.bikeiot.Constants.CODE_SEND_SMS_PERMISSION;
import static android.scopus.com.bikeiot.Constants.SHARED_USER_UUID;
import static android.scopus.com.bikeiot.model.filerelated.CSVFile.ROUTE_ROOT_FOLDER_NAME;

public class ExerciseRecorderActivity extends AppCompatActivity
        implements ExerciseRecorderContract.View, OnMapReadyCallback {

    //TODO: Remove coarse_location permission check

    //TODO: broadcast receiver para o location do service no onResume()
    //TODO: remover os broadcastReceivers
    //TODO: remover o location manager dessa activity antes de iniciar o service
    //TODO: criar o location manager dessa activity quando terminar o service

    //TODO: filtrar o bluetooth device - bike ou outros dispositivos
    //TODO: fix bug - salvar o bikeBluetoothDevie mesmo que o usuario troque de activity
    //TODO: fix - quando o usuario volta para a recorder page o savedInstance é perdido

    //TODO: Remover todos os LocalManager Broadcast

    private static final String TAG = "ExerciseRecorderActiv";
    public static final String ACTION_CONTINUE_EXERCISE =
            "android.scopus.com.bikeiot.exerciserecoder.ExerciseRecorderActivity.CONTINUE";
    public static final String CHRONOMETER_KEY =
            "android.scopus.com.bikeiot.exerciserecoder.ExerciseRecorderActivity.CHRONOMETER";
    public static final String ACTION_START_EXERCISE =
            "android.scopus.com.bikeiot.exerciserecoder.ExerciseRecorderActivity.START";
    public static final String ACTION_PAUSE_EXERCISE =
            "android.scopus.com.bikeiot.exerciserecoder.ExerciseRecorderActivity.PAUSE";
    public static final String ACTION_FINISH_EXERCISE =
            "android.scopus.com.bikeiot.exerciserecoder.ExerciseRecorderActivity.FINISH";

    public static final String BIKE_DEVICE_KEY =
            "android.scopus.com.bikeiot.exerciserecoder.ExerciseRecorderActivity.BIKE_DEVICE_KEY";
    public static final String BIKE_EXERCISE_EXTRA =
            "android.scopus.com.bikeiot.exerciserecoder.BIKE_EXERCISE_EXTRA";
    public static final String EXTRA_BIKE_DEVICE =
            "android.scopus.com.bikeiot.exerciserecoder.EXTRA_BIKE_DEVICE";

    public static final String EXTRA_LAST_KNOWN_LOCATION = "android.scopus.com.bikeiot.exerciserecoder.EXTRA_LAST_KNOWN_LOCATION";

    private static final int MY_LOCATION_REQUEST_CODE = 102;

    private static final float CAMERA_MAP_ZOOM = 18.0f;
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final int LOCATION_REQUEST_INTERVAL = 2000;
    private static final int LOCATION_REQUEST_FASTEST_INTERVAL = LOCATION_REQUEST_INTERVAL / 2;
    public static final String ACTION_RESET_INFO = "ACTION_RESET_INFO";

    private LinearLayout mStartExerciseLinearLayout;
    private LinearLayout mExerciseControlLinearLayout;
    private Button mStartButton;
    private Button mPauseButton;
    private Button mContinueButton;
    private Button mFinishButton;
    private TextView mExerciseChronometerTextView;
    private TextView mFirstInfoTextView;
    private TextView mSecondInfoTextView;
    private TextView mThirdInfoTextView;

    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private NavigationView mNavigationView;

    //private BluetoothDevice mBikeBluetoothDevice;
    private BikeDevice mBikeDevice;
    private ArrayList<BluetoothDevice> mOtherBluetoothDevices;

    private GoogleMap mGoogleMap;
    private MapView mMapView;

    private LocationManager mLocationManager;
    private MyLocationListener mMyLocationListener;

    private Location mLastKnownLocation;

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;

    private LocationRequest mLocationRequestBalancedAccuracy;
    private LocationRequest mLocationRequestHighAccuracy;
    private BroadcastReceiver mLocationServiceReceiver;
    private BroadcastReceiver mRouteReceiver;
    private LocationSettingsRequest mLocationSettingsRequest;
    private SettingsClient mSettingsClient;
    private Location mCurrentLocation;
    private LocationHandler mLocationHandler;
    private Route mRoute;
    private User mUser;
    private BroadcastReceiver mUsedDataReceiver;
    private TextView mFirstLabelTextView;
    private TextView mSecondLabelTextView;
    private TextView mThirdLabelTextView;
    private Marker mMapMarker;

    private Runnable mTimerHelpButton;
    private Handler mHandler;

    private NetworkHelper mNetworkHelper;

    public static final int REQUEST_BIKE_DEVICE_CODE = 106;

    //polylines
    private Polyline mSelectedRoutePolyline;
    private Polyline mPolyline;
    private List<LatLng> mRouteLocationSelected;
    private ConfirmationDialog.ConfirmationDialogListener mPanicButtonDialogCallback;


    private WarningDialog.Listener mBikeBtConnectionLostWarningDialog;

    private BroadcastReceiver mBikeBluetoothLostConnectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ExerciseRecorderService.ACTION_SEND_LOST_CONNECTION_BIKE_DEVICE)) {
                mBikeDevice = null;
                resetInfoValues();
                clearPolyline();
                showStartExercise();
                showNoBikeDeviceSelectedIcon();
            }
        }
    };

    private BroadcastReceiver mResetInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_RESET_INFO)) {
                clearPolyline();
                resetInfoValues();
                showStartExercise();
            }
        }
    };

    private BroadcastReceiver mServiceStatusChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ExerciseRecorderService.isPaused()) {
                showContinueButton();
                //clearPolyline();
            }

            if (!ExerciseRecorderService.isRunning()) {
                showStartExercise();
            }



        }
    };
    private ImageView mSelectedBikeDeviceIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_recorder);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //setting up toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //setting up navigation
        mNavigationView = (NavigationView) findViewById(R.id.main_navigation_view);
        setupDrawerNavigationContent(mNavigationView);

        /*SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);*/

        mMapView = (MapView) findViewById(R.id.map_view);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(this);

        mExerciseChronometerTextView = (TextView) findViewById(R.id.exercise_chronometer_text_view);
        mFirstInfoTextView = (TextView) findViewById(R.id.first_info_text_view);
        mSecondInfoTextView = (TextView) findViewById(R.id.second_info_text_view);
        mThirdInfoTextView = (TextView) findViewById(R.id.third_info_text_view);
        mFirstLabelTextView = (TextView) findViewById(R.id.first_info_label_text_view);
        mSecondLabelTextView = (TextView) findViewById(R.id.second_info_label_text_view);
        mThirdLabelTextView = (TextView) findViewById(R.id.third_info_label_text_view);

        mSelectedBikeDeviceIcon = (ImageView) findViewById(R.id.selected_bike_device_icon);

        mStartButton = (Button) findViewById(R.id.start_exercise_button);
        mPauseButton = (Button) findViewById(R.id.pause_exercise_button);
        mContinueButton = (Button) findViewById(R.id.continue_exercise_button);
        mFinishButton = (Button) findViewById(R.id.find_bt_button);

        mStartExerciseLinearLayout = (LinearLayout) findViewById(R.id.start_exercise_linear_layout);
        mExerciseControlLinearLayout = (LinearLayout) findViewById(R.id.exercise_control_linear_layout);

        //configurando confirmation callback para o botao do panico helpToHospital.
        createPanicButtonDialogCallback();
        createBtLostWarningCallback();
        mNetworkHelper = new NetworkHelper(this);


        //inicializando o timer do help button
        //TODO: refatorar para mvp ou mvc
        LocalBroadcastManager.getInstance(this).registerReceiver(mExerciseChronometerReceiver,
                new IntentFilter(CHRONOMETER_KEY));

        LocalBroadcastManager.getInstance(this).registerReceiver(mExerciseResultReceiver,
                new IntentFilter(ExerciseRecorderService.ACTION_SEND_RESULT));

        LocalBroadcastManager.getInstance(this).registerReceiver(mNormalizedLocationRouteReceiver,
                new IntentFilter(ExerciseRecorderService.SEND_NORMALIZED_LOCATION_POINTS));

        LocalBroadcastManager.getInstance(this).registerReceiver(mBikeBluetoothLostConnectionReceiver,
                new IntentFilter(ExerciseRecorderService.ACTION_SEND_LOST_CONNECTION_BIKE_DEVICE));

        LocalBroadcastManager.getInstance(this).registerReceiver(mResetInfoReceiver,
                new IntentFilter(ACTION_RESET_INFO));

        mOtherBluetoothDevices = new ArrayList<>();

        handleIntent(getIntent());

        if (ExerciseRecorderService.isRunning()) {
            showExerciseControl();
        } else {
            showStartExercise();
        }

        mRoute = new Route();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        mLocationHandler = new LocationHandler();

        createLocationCallback();
        createLocationRequestHighAccuracy();
        buildLocationRequest();

        /*if(checkLocationPermission()) {
            getLastKnownLocation();
        } else {
            requestPermissions();o
        }*/

        //testando usuario mocking (remover)
        mockingUser();
        if (mUser != null) {
            createSharedPreferencesUser(mUser.getId(), mUser.getUUID());
        }

        //testing snapToRoad
        LocalBroadcastManager.getInstance(this).registerReceiver(mTestSnappedPointsReceiver,
                new IntentFilter(SnapToRoadGetData.ACTION_SEND_SNAPPED_POINTS_TEST));

    }

    private void createBtLostWarningCallback() {
        mBikeBtConnectionLostWarningDialog = new WarningDialog.Listener() {

            @Override
            public void onOkClick() {

            }
        };
    }

    public void createPanicButtonDialogCallback() {
        mPanicButtonDialogCallback = new ConfirmationDialog.ConfirmationDialogListener() {

            @Override
            public void onConfirmationDialogPositiveChoose() {
                stopExerciseRecorderService();
                showHelpToHospitalUi();
            }

            @Override
            public void onConfirmationDialogNegativeChoose() {
                //do nothing
            }
        };
    }

    private void configureUsedDataReceiver() {
        Log.d(TAG, "configureUsedDataReceiver called");
        if (mUsedDataReceiver == null) {
            mUsedDataReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "mUsedDataReceiver called");
                String action = intent.getAction();
                    if (action.equals(ExerciseRecorderService.ACTION_SEND_USED_DATA)) {
                        Log.d(TAG, "ACTION_SEND_USED_DATA called");
                        CSVFile csvFile = new CSVFile(ExerciseRecorderActivity.this);

                        mBikeDevice = intent.getParcelableExtra(ExerciseRecorderService.EXTRA_USED_BIKE_DEVICE);
                        Log.d(TAG, "mUsedDataReceiver mBikeDevice " + mBikeDevice.getBluetoothDevice().getName());

                        final BikeExercise bikeExercise = (BikeExercise) intent.getSerializableExtra(
                                ExerciseRecorderService.EXTRA_USED_BIKE_EXERCISE);

                        Log.d(TAG, "bikeDevice from bikeExercise: rim value: " + bikeExercise.getBikeDevice().getRimValue());

                        //TODO: salvar o route distance, end e o final latitude e longitude
                        //dados de latitude e longitude simulados
                        //salvando route no bando
                        Route route = bikeExercise.getRoute();
                        //se o usuario iincia a tividade e rapidamente a finaliza um error é mostrado por que
                        //ainda não foi adicionado nennhum ponto a locationPoints
                        //TODO: arrumar esse bug relacionado ao locationPoints, enviar a ultima localizacao para o inicio do service
                        if (!route.getLocationPoints().isEmpty()) {
                            route.setStartLatLng(route.getLocationPoints().get(0));
                            route.setEndLatLng(route.getLocationPoints().get(route.getLocationPoints().size()-1));
                        }
                        //TODO: definir se ira ser utilizado a distancia calculada com base nas ratacoes da bike
                        route.setDistance(bikeExercise.getResult().getDistanceBasedOnRotationsMeters());
                        route.setUser(mUser);
                        saveRoute(route);

                        RouteDAO routeDAO = new RouteDAO(ExerciseRecorderActivity.this);
                        List<Route> routes = routeDAO.getRoutes();
                        Log.d(TAG, "routes size: " + routes.size());
                        //renomeando routePathFile
                        String newRouteNameFile = route.getId() + CSVFile.UNDERLINE_SEPARATOR +
                                ROUTE_ROOT_FOLDER_NAME + CSVFile.UNDERLINE_SEPARATOR +
                                String.valueOf(UUID.randomUUID());

                        csvFile.renameFile(route.getRouteCsvFilePath(), newRouteNameFile);
                        route.setFileRoutePath(csvFile.getPathFile());

                        renameRouteFilePathName(route);

                        //salvando o bikeExercise
                        bikeExercise.setDescription("Exercicio");
                        bikeExercise.setUser(mUser);
                        saveBikeExercise(bikeExercise);

                        //renomeando result Path File
                        String newResultNameFile = bikeExercise.getId()+ CSVFile.UNDERLINE_SEPARATOR +
                                mUser.getId()+ CSVFile.UNDERLINE_SEPARATOR + CSVFile.RESULT_ROOT_FOLDER_NAME +
                                CSVFile.UNDERLINE_SEPARATOR + String.valueOf(UUID.randomUUID());

                        csvFile.renameFile(bikeExercise.getResult().getFileResultPath(), newResultNameFile);
                        bikeExercise.getResult().setFileResultPath(csvFile.getPathFile());
                        renameBikeExerciseResultFilePath(bikeExercise);

                        //taking map picture
                        LatLngBounds latLngBounds = calcRouteImageBounds(bikeExercise.getRoute().getLocationPoints());
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(latLngBounds, 0);
                        mGoogleMap.moveCamera(cameraUpdate);
                        mGoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                            @Override
                            public void onMapLoaded() {
                                GoogleMap.SnapshotReadyCallback snapshotCallback = new GoogleMap.SnapshotReadyCallback() {
                                    Bitmap imageBitmap;

                                    @Override
                                    public void onSnapshotReady(Bitmap snapshot) {
                                        Log.d(TAG, "onSnapshotReady called");
                                        imageBitmap = snapshot;
                                        try {
                                            String fileName = bikeExercise.getRoute().getId() + "_" +
                                                    UUID.randomUUID();

                                            File file = new FileRepository(ExerciseRecorderActivity.this)
                                                    .createFile(fileName,
                                                            FileContractConstants.ROUTE_IMAGE_FOLDER,
                                                            FileContractConstants.PNG_FILE_EXTENSION,
                                                            Environment.getExternalStorageDirectory());

                                            FileOutputStream out = new FileOutputStream(file);
                                            imageBitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                                            MediaScannerConnection.scanFile(ExerciseRecorderActivity.this, new String[] {file.getAbsolutePath()}, null, null);
                                            bikeExercise.getRoute().setImageBitmap(imageBitmap);
                                            bikeExercise.getRoute().setImagePath(file.getPath());
                                            updateRenameImagePath(bikeExercise.getRoute());
                                            showExerciseResumeUi(bikeExercise.getId());
                                        } catch (FileNotFoundException e) {
                                            Log.e(TAG, "onSnapshotReady FileNotFoundException error" + e);
                                            e.printStackTrace();
                                        }
                                    }
                                };
                                mGoogleMap.snapshot(snapshotCallback);
                                mGoogleMap.setOnMapLoadedCallback(null);
                            }
                        });
                    }
                }
            };
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mUsedDataReceiver,
                new IntentFilter(ExerciseRecorderService.ACTION_SEND_USED_DATA));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.help_button_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private LatLngBounds calcRouteImageBounds(List<LatLng> locations) {
        LatLngBounds.Builder latLngBounds = LatLngBounds.builder();
        for (LatLng latLng : locations) {
            latLngBounds.include(latLng);
        }

        //adicionando novos pontos ao centro para regular o zoom
        LatLngBounds auxLatLngBounds = latLngBounds.build();
        LatLng center = auxLatLngBounds.getCenter();

        //criando novo ponto ao norte e ao sul com deslocamento de 200 metros para cada lado
        LatLng displacementNorthEast = CalculateUtils.createLatLngFromReference(center, 200, 200);
        LatLng displacementSouthWest = CalculateUtils.createLatLngFromReference(center, -200, -200);

        //incluindo os pontos para deslocamento ao bound
        latLngBounds.include(displacementNorthEast);
        latLngBounds.include(displacementSouthWest);

        return latLngBounds.build();
    }

    /**
     * Metodo para configurar o RouteReceiver que irá receber a rota gerada pelo service até dado
     * momento e realizar a requisição ao service para que a rota seja enviada.
     * Esse metodo serve como auxiliar para desenhar no mapa a rota que esta sendo gerada
     * logo após o inicio da activity;
     */
    private void prepareToRequestRouteGeneratedInService() {
        Log.d(TAG, "prepareToRequestRouteGeneratedInService called");
        if (mRouteReceiver == null) {
            Log.d(TAG, "prepareToRequestRouteGeneratedInService criando mRouteReceiver");
            createRouteReceiver();
        }

        Log.d(TAG, "prepareToRequestRouteGeneratedInService registrando mRouteReceiver");
        LocalBroadcastManager.getInstance(this).registerReceiver(mRouteReceiver,
                new IntentFilter(ExerciseRecorderService.ACTION_SEND_ROUTE));

        Log.d(TAG, "prepareToRequestRouteGeneratedInService fazendo requisicao ao ExerciseRecorderService");
        Intent recorderServiceIntent = new Intent(this, ExerciseRecorderService.class);
        recorderServiceIntent.setAction(ExerciseRecorderService.ACTION_REQUEST_ROUTE_LOCATION);
        startService(recorderServiceIntent);
    }

    //TODO: remover quando a sessão de usuario estiver fucionando
    private void mockingUser() {
        Log.d(TAG, "mockingUser called");
        UserDAO userDAO = new UserDAO(this);
        User user = userDAO.getUser(1);
        if (user == null) {

            user = new User("luiztest",
                    "luiz.test@gmail.com",
                    "123456",
                    90,
                    1.75,
                    "1995-02-06",
                    UUID.randomUUID());

            if (userDAO.saveUser(user)) {
                Log.d(TAG, "mockingUser new user id #: " + user.getId());
                mUser = user;
            } else {
                Log.d(TAG, "mockingUser erro ao criar o usuario");
                return;
            }
        } else {
            Log.d(TAG, "mockingUser user id #" + user.getId());
            mUser = user;
        }
    }

    private void createSharedPreferencesUser(int userId, UUID userUuid) {
        Log.d(TAG, "createSharedPreferencesUser called");
        SharedPreferences mSharedPreferences = getSharedPreferences(
                getString(R.string.shared_preferences_key), Context.MODE_PRIVATE);;
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(getString(R.string.shared_user_id), userId);
        editor.putString(SHARED_USER_UUID, userUuid.toString());
        editor.commit();
    }

    private void buildLocationRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequestHighAccuracy)
                .addLocationRequest(mLocationRequestBalancedAccuracy);
        mLocationSettingsRequest = builder.build();
    }

    private void stopLocationUpdates() {
        Log.d(TAG, "stopLocationUpdates");
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Log.d(TAG, "LocationCallback onLocationResult called");
                Location location = locationResult.getLastLocation();
                if (mLocationHandler.isLocationBetter(location, mCurrentLocation)) {
                    Log.d(TAG, "LocationCallback new location is better than current");

                    mCurrentLocation = location;

                    animateCameraToLocation(new LatLng(mCurrentLocation.getLatitude(),
                            mCurrentLocation.getLongitude()));

                    updateMarkerPosition(new LatLng(mCurrentLocation.getLatitude(),
                            mCurrentLocation.getLongitude()));


                    mRoute.addNewLocationPoint(new LatLng(mCurrentLocation.getLatitude(),
                            mCurrentLocation.getLongitude()));

                    //testing snap to road
                    // TESTING DRAW ON MAP
                    /*if (mNetworkHelper.isNetworkConnected()) {
                        Log.d(TAG, "isInternetConnected() called ");
                        if (mRoute.getLocationPoints().size() - mRoute.getLastIndexOffset() >=
                                SnapToRoadGetData.PAGINATION_OVERLAP) {

                            Log.d(TAG, "delta (size - lastIndex) is 5 or greater");

                            new Thread(new SnapToRoadGetData(getBaseContext(), mRoute.getLocationPoints(),
                                    mRoute.getLastIndexOffset())).start();

                            mRoute.setLastIndexOffset(mRoute.getLocationPoints().size());

                        }
                    } else {
                        Log.d(TAG, "Testing Drawing on location callback");
                        drawOnMap(mRoute.getLocationPoints());
                    }*/
                    //testing drawing
                    Log.d(TAG, "Testing Drawing on location callback");

                    //mRoute.drawOnMap(mRoute.getLocationPoints(), mGoogleMap);
                    //drawOnMap(mRoute.getLocationPoints());
                }
            }
        };
    }

    BroadcastReceiver mTestSnappedPointsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "mTestSnappedPointsReceiver called");
            String action = intent.getAction();
            if (action.equals(SnapToRoadGetData.ACTION_SEND_SNAPPED_POINTS_TEST)) {
                Log.d(TAG, "mTestSnappedPointsReceiver ACTION_SEND_SNAPPED_POINTS_TEST");
                ArrayList<LatLng> locations = intent.getParcelableArrayListExtra(SnapToRoadGetData.EXTRA_LOCATIONS);
                if (mRoute != null) {
                    Log.d(TAG, "mTestSnappedPointsReceiver mRoute is not null");

                    for (LatLng latLng : locations) {
                        mRoute.addLocationToNormalizedLocation(latLng);
                    }

                    Log.d(TAG, "mTestSnappedPointsReceiver drawing on map normalized location");
                    drawOnMap(mRoute.getLocationsNormalized());
                }
            }
        }
    };

    private void clearPolyline() {
        Log.d(TAG, "clearPolyline called");
        if (mPolyline != null) {
            mPolyline.remove();
            mPolyline = null;
        }
    }

    private void drawOnMap(List<LatLng> locationPoints) {
        Log.d(TAG, "drawOnMap called");
        PolylineOptions polylineOptions = new PolylineOptions();

        if(mPolyline == null) {
            Log.d(TAG, "drawOnMap mPolyline is null");
            for(LatLng point : locationPoints) {
                polylineOptions.add(point);
                Log.d(TAG, "drawOnMap points lat:" + point.latitude + " long:" + point.longitude);
            }
            polylineOptions.color(Color.BLACK);
            mPolyline = mGoogleMap.addPolyline(polylineOptions);
        } else {
            Log.d(TAG, "drawOnMap mPolyline is not null");
            mPolyline.setPoints(locationPoints);
        }
    }
    
    private void createLocationRequestBalancedAccuracy() {
        Log.d(TAG, "createLocationRequestHighAccuracy called");
        mLocationRequestBalancedAccuracy = new LocationRequest();
        mLocationRequestBalancedAccuracy.setInterval(LOCATION_REQUEST_INTERVAL);
        mLocationRequestBalancedAccuracy.setFastestInterval(LOCATION_REQUEST_FASTEST_INTERVAL);
        mLocationRequestBalancedAccuracy.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }
    
    private void createLocationRequestHighAccuracy() {
        Log.d(TAG, "createLocationRequestHighAccuracy called");
        mLocationRequestHighAccuracy = new LocationRequest();
        mLocationRequestHighAccuracy.setFastestInterval(LOCATION_REQUEST_FASTEST_INTERVAL);
        mLocationRequestHighAccuracy.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //mLocationRequestHighAccuracy.setInterval(LOCATION_REQUEST_INTERVAL);
        //mLocationRequestHighAccuracy.setSmallestDisplacement(1);

        //testing snap to road
        mLocationRequestHighAccuracy.setInterval(0);
        mLocationRequestHighAccuracy.setSmallestDisplacement(0);
    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        Log.d(TAG, "startLocationUpdates called");
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.d(TAG, "startLocationUpdates onSuccess called");
                        mFusedLocationClient.requestLocationUpdates(mLocationRequestHighAccuracy, mLocationCallback, Looper.myLooper());
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof ResolvableApiException) {
                            try {
                                ResolvableApiException resolvable = (ResolvableApiException) e;
                                resolvable.startResolutionForResult(ExerciseRecorderActivity.this,
                                        REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException sendEx) {
                                // Ignore the error.
                            }
                        }
                    }
                });
    }

    @SuppressLint("MissingPermission")
    private void configureLocationManagerUpdates() {
        Log.d(TAG, "configureLocationManagerUpdates configurando updater location manager");
        if(mMyLocationListener == null) mMyLocationListener = new MyLocationListener();
        mLocationManager.requestLocationUpdates("gps", 3000, 0, mMyLocationListener);
    }

    @SuppressLint("MissingPermission")
    private void getLastKnownLocation() {
        mFusedLocationClient.getLastLocation().addOnCompleteListener(this, new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                Log.d(TAG, "getLastKnownLocation called");
                Log.d(TAG, "onComplete called");
                if(task.isComplete() && task.getResult() != null) {
                    mLastKnownLocation = task.getResult();
                    if (mGoogleMap != null) {
                        setMapToLocation(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()));
                    }
                }
            }
        });
    }

    private boolean checkLocationPermission() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.d(TAG, "checkLocationPermission called");
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    private void requestAllPermissions() {
        PermissionHelper.requestPermissions(this, BASE_PERMISSION_CODE, new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.SEND_SMS,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "onRequestPermissionsResult called");
        switch (requestCode) {
            case MY_LOCATION_REQUEST_CODE:
                if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Log.d(TAG, "onRequestPermissionsResult permissoes nao garantidas");
                    requestAllPermissions();
                } else Log.d(TAG, "onRequestPermissionsResult permissoes garantidas");
                break;
            case BASE_PERMISSION_CODE:
                if (!PermissionHelper.isPermissionsGranted(this, new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.SEND_SMS,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE})) {
                    Log.d(TAG, "permissions not granted");
                    requestAllPermissions();
                }
                break;
            case CODE_SEND_SMS_PERMISSION:
                if (!PermissionHelper.isPermissionGranted(this, Manifest.permission.SEND_SMS)) {
                    PermissionHelper.requestPermissions(this, CODE_SEND_SMS_PERMISSION,
                            new String[]{Manifest.permission.SEND_SMS});
                }
                break;
        }
    }

    private void createRouteReceiver() {
        mRouteReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(ExerciseRecorderService.ACTION_SEND_ROUTE)) {
                    Route route = (Route) intent.getSerializableExtra(ExerciseRecorderService.EXTRA_ROUTE);
                    Log.d(TAG, "mRouteReceiver recebendo rota");
                    drawOnMap(route.getLocationPoints());
                }
            }
        };
    }

    //metodos para operacao com bando de dados
    //TODO: refatorar para mvp
    private void renameRouteFilePathName(Route route) {
        RouteDAO routeDAO = new RouteDAO(this);
        routeDAO.renameRouteFilePath(route);
    }

    private void updateRenameImagePath(Route route) {
        RouteDAO routeDAO = new RouteDAO(this);
        routeDAO.updateRenameImagePath(route.getImagePath(), route.getId());
    }

    private void saveRoute(Route route) {
        RouteDAO routeDAO = new RouteDAO(this);
        routeDAO.save(route);
    }

    private void saveBikeExercise(BikeExercise bikeExercise) {
        BikeExerciseDAO bikeExerciseDAO = new BikeExerciseDAO(this);
        bikeExerciseDAO.saveBikeExercise(bikeExercise);
    }

    private void renameBikeExerciseResultFilePath(BikeExercise bikeExercise) {
        BikeExerciseDAO bikeExerciseDAO = new BikeExerciseDAO(this);
        bikeExerciseDAO.renameResultFilePath(bikeExercise);
    }

    BroadcastReceiver mExerciseChronometerReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String chronometerTime = intent.getStringExtra("data");
            mExerciseChronometerTextView.setText(chronometerTime);
        }
    };

    BroadcastReceiver mNormalizedLocationRouteReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "mNormalizedLocationRouteReceiver called");
            String action = intent.getAction();
            if (action.equals(ExerciseRecorderService.SEND_NORMALIZED_LOCATION_POINTS)) {
                Log.d(TAG, "mNormalizedLocationRouteReceiver SEND_NORMALIZED_LOCATION_POINTS");
                Route route = (Route) intent.getSerializableExtra(ExerciseRecorderService.EXTRA_ROUTE);
                for (LatLng latLng : route.getLocationsNormalized()) {
                    Log.d(TAG, "mNormalizedLocationRouteReceiver lat: "
                            + latLng.latitude + " lng: " + latLng.longitude);
                }

                drawOnMap(route.getLocationsNormalized());
            }
        }
    };

    BroadcastReceiver mExerciseResultReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "mExerciseResultReceiver: called");
            String action = intent.getAction();
            if (action.equals(ExerciseRecorderService.ACTION_SEND_RESULT)) {
                Result result = (Result) intent.getSerializableExtra(ExerciseRecorderService.EXTRA_RESULT);

                double KmHVelocity = metersPerSecToKmPerH(result.getVelocityMetersPerSec());
                double meanVelocity = metersPerSecToKmPerH(result.getMeanVelocityMetersPerSec());
                double distanceKm = metersToKm(result.getDistanceBasedOnRotationsMeters());
                //TODO: mudar os dados que sao apresentados
                if (ExerciseRecorderService.isRunning()) {
                    String kmIndication = getResources().getString(R.string.km_indicator);

                    mFirstInfoTextView.setText(formatKm(distanceKm) + " " + kmIndication);
                    mSecondInfoTextView.setText(formatKm(meanVelocity) + " " + kmIndication + "/h");
                    mThirdInfoTextView.setText(formatKm(KmHVelocity));
                } else {
                    resetInfoValues();
                }
            }
        }
    };

    private String formatKm(double kmH) {
        return new DecimalFormat("0.0").format(kmH);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.d(TAG, "onSaveInstanceState called");
        savedInstanceState.putParcelable(EXTRA_BIKE_DEVICE, mBikeDevice);

    }

    private void resetInfoValues() {
        mFirstInfoTextView.setText("0 " + getResources().getString(R.string.km_indicator));
        mSecondInfoTextView.setText("0 "+ getResources().getString(R.string.km_indicator) + "/h");
        mThirdInfoTextView.setText("0 ");
        mExerciseChronometerTextView.setText("00:00:00");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mActionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        if (item.getItemId() == R.id.help_menu_item) {
            showPanicButtonConfirmationDialog();
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupDrawerNavigationContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Helper.Menu.selectDrawerItem(ExerciseRecorderActivity.this, item, R.id.exercise_recorder_menu_item);
                mDrawerLayout.closeDrawers();
                return true;
            }
        });
    }

    @Override
    public void showStartExercise() {
        mStartExerciseLinearLayout.setVisibility(View.VISIBLE);
        mExerciseControlLinearLayout.setVisibility(View.GONE);
    }

    @Override
    public void showExerciseControl() {
        mExerciseControlLinearLayout.setVisibility(View.VISIBLE);
        mStartExerciseLinearLayout.setVisibility(View.GONE);
        if (ExerciseRecorderService.isPaused()) {
            showContinueButton();
        } else {
            showPauseButton();
        }
    }

    @Override
    public void requestBikeDeviceUiForResult() {
        Log.d(TAG, "requestBikeDeviceUiForResult called");
        Intent intent = new Intent(this, BikeDevicesActivity.class);
        startActivityForResult(intent, REQUEST_BIKE_DEVICE_CODE);
    }

    @Override
    public void showHelpToHospitalUi() {
        Intent intent = new Intent(this, HospitalsActivity.class);
        Location location = null;

        if (mCurrentLocation != null) {
            location = mCurrentLocation;
        } else if (mLastKnownLocation != null){
            location = mLastKnownLocation;
        }

        intent.putExtra(EXTRA_LAST_KNOWN_LOCATION, location);
        //TODO: change to start for result
        startActivity(intent);
    }

    @Override
    public void showPanicButtonConfirmationDialog() {
        FragmentManager fm = getFragmentManager();
        String content = getString(R.string.panic_button_warning_description) + "\n\n";
        content += getResources().getString(R.string.panic_button_confirmation);
        String title = getString(R.string.panic_button_dialog_title);
        ConfirmationDialog confirmationDialog = ConfirmationDialog.newInstance(content, title, mPanicButtonDialogCallback);
        confirmationDialog.show(fm, ConfirmationDialog.DIALOG_TAG);
    }

    @Override
    public void stopExerciseRecorderService() {
        Log.d(TAG, "stopExerciseRecorderService called");
        stopService(new Intent(this, ExerciseRecorderService.class));
        showStartExercise();
        resetInfoValues();
    }

    @Override
    public void showSelectedBikeDeviceIcon() {
        mSelectedBikeDeviceIcon.setImageDrawable(getDrawable(R.drawable.ic_bike_white_24dp));
    }

    @Override
    public void showNoBikeDeviceSelectedIcon() {
        mSelectedBikeDeviceIcon.setImageDrawable(getDrawable(R.drawable.ic_no_bike_selected_gray_24dp));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult called");

        if (requestCode == REQUEST_BIKE_DEVICE_CODE) {

            if (resultCode == RESULT_OK) {
                Log.d(TAG, "bikeDevice from result");

                BikeDevice bikeDevice = data.getParcelableExtra(BikeDevicesActivity.EXTRA_BIKE_DEVICE);
                setBikeDevice(bikeDevice);
            }
        }
    }

    public void showContinueButton() {
        mContinueButton.setVisibility(View.VISIBLE);
        mPauseButton.setVisibility(View.GONE);
    }

    public void showPauseButton() {
        mPauseButton.setVisibility(View.VISIBLE);
        mContinueButton.setVisibility(View.GONE);
    }

    public void startExerciseButton(View view) {
        //TODO: adaptar o filtro somente para bluetooth device de bicicleta
        if (mBikeDevice == null || !isABikeBluetooth(mBikeDevice.getBluetoothDevice())) {
            Toast.makeText(this, R.string.warning_no_bike_device_selected, Toast.LENGTH_SHORT).show();

            //TODO: refatorar para mvp
            requestBikeDeviceUiForResult();
        } else {
            showExerciseControl();
            showPauseButton();

            stopLocationUpdates();
            //removeLocationListener();

            //TODO:adicionaor outros devices
            Location lastKnownLocation = null;
            if (mCurrentLocation != null) {
                lastKnownLocation = mCurrentLocation;
            } else if (mLastKnownLocation != null) {
                lastKnownLocation = mLastKnownLocation;
            }

            Log.d(TAG, "startExerciseButton: last known location: " + lastKnownLocation);

            Intent recorderServiceIntent = new Intent(this, ExerciseRecorderService.class);
            recorderServiceIntent.putExtra(EXTRA_BIKE_DEVICE, mBikeDevice);
            recorderServiceIntent.putExtra(EXTRA_LAST_KNOWN_LOCATION, lastKnownLocation);
            recorderServiceIntent.setAction(ACTION_START_EXERCISE);
            startService(recorderServiceIntent);
            //TODO:chamar makeconnection para os outros dispositivos bluetooth
        }
    }

    private void removeLocationListener() {
        Log.d(TAG, "removeLocationListener removendo location listener");
        if(mLocationManager != null && mMyLocationListener != null) {
            mLocationManager.removeUpdates(mMyLocationListener);
        }
    }

    public void continueExerciseButton(View view) {
        showPauseButton();
        Intent recorderServiceIntent = new Intent(this, ExerciseRecorderService.class);
        recorderServiceIntent.setAction(ACTION_CONTINUE_EXERCISE);
        startService(recorderServiceIntent);
    }

    public void pauseExerciseButton(View view) {
        showContinueButton();
        Intent recorderServiceIntent = new Intent(this, ExerciseRecorderService.class);
        recorderServiceIntent.setAction(ACTION_PAUSE_EXERCISE);
        startService(recorderServiceIntent);
    }

    public void finishExerciseButton(View view) {
        Intent recorderServiceIntent = new Intent(this, ExerciseRecorderService.class);
        recorderServiceIntent.setAction(ACTION_FINISH_EXERCISE);
        startService(recorderServiceIntent);
        stopExerciseRecorderService();

        //TODO: start location updates só se a finalizacao for para o botao do panico
        //startLocationUpdates();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume called");
        mMapView.onResume();
        super.onResume();
        //TODO: refatorar MVP
        if (mBikeDevice == null) {
            showNoBikeDeviceSelectedIcon();
        } else {
            showSelectedBikeDeviceIcon();
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mServiceStatusChangedReceiver,
                new IntentFilter(ExerciseRecorderService.ACTION_SEND_SERVICE_STATUS));

        if (!ExerciseRecorderService.isRunning()) {
            resetInfoValues();
            clearPolyline();
        }

        if (checkLocationPermission()) {
            getLastKnownLocation();

            if(!ExerciseRecorderService.isRunning()) {
                startLocationUpdates();
            }

            if (ExerciseRecorderService.isRunning()) {
                prepareToRequestRouteGeneratedInService();
            }

        } else {
            requestAllPermissions();
        }

        configureUsedDataReceiver();

        if (mLocationServiceReceiver == null) {
            mLocationServiceReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    Log.d(TAG, "mLocationServiceReceiver  called");
                    Location location = intent.getParcelableExtra(ExerciseRecorderService.EXTRA_LOCATION);
                    animateCameraToLocation(new LatLng(location.getLatitude(), location.getLongitude()));
                    updateMarkerPosition(new LatLng(location.getLatitude(),location.getLongitude()));

                    Route route = (Route) intent.getSerializableExtra(ExerciseRecorderService.EXTRA_ROUTE);
                    if( route != null) {
                        mRoute = route;
                        Log.d(TAG, "Route received");
                        Log.d(TAG, "Drawing route points on map");
                        //mRoute.drawOnMap(mRoute.getLocationPoints(), mGoogleMap);
                        drawOnMap(mRoute.getLocationPoints());
                    }
                }
            };

            Log.d(TAG, "mLocationServiceReceiver Registering receiver");
            LocalBroadcastManager.getInstance(this).registerReceiver(mLocationServiceReceiver,
                    new IntentFilter(ExerciseRecorderService.ACTION_SEND_LOCATION));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopLocationUpdates();
        unregisterLocalBroadcastUsedDataReceiver();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRouteReceiver);
    }

    @Override
    protected void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();
        Log.d(TAG, "onDestroy called");
        stopLocationUpdates();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mServiceStatusChangedReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mNormalizedLocationRouteReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBikeBluetoothLostConnectionReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mResetInfoReceiver);
        unregisterLocalBroadcastLocationServiceReceiver();
        //removeLocationListener();


        //TODO: remove test receiver after conclude test
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mTestSnappedPointsReceiver);
    }

    private void unregisterLocalBroadcastLocationServiceReceiver() {
        if (mLocationServiceReceiver != null) {
            try {
                LocalBroadcastManager.getInstance(this).unregisterReceiver(mLocationServiceReceiver);
            } catch (Exception e) {
                Log.e(TAG, "error ao utilizar unregister no mLocationServiceReceiver" + e);
            }
        }
    }

    private void unregisterLocalBroadcastUsedDataReceiver() {
        Log.d(TAG, "unregisterLocalBroadcastUsedDataReceiver called");
        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mUsedDataReceiver);
        } catch (Exception e) {
            Log.e(TAG, "unregisterLocalBroadcastUsedDataReceiver erro ao utilizar unregister no mUsedDataReceiver" + e);
        }
    }

    private boolean isABikeBluetooth(BluetoothDevice device) {
        if (device == null || device.getName() == null || device.getName().isEmpty()) return false;
        return device.getName().indexOf("bike") != -1;
    }

    //TODO: organizar em mvp ou mvc
    private void filterOtherDevice(BluetoothDevice device) {
        Log.d(TAG, "filterOtherDevice() called");
        if (device != null) {
            mOtherBluetoothDevices.add(device);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(TAG, "onRestoreInstanceState called");
        mBikeDevice = savedInstanceState.getParcelable(EXTRA_BIKE_DEVICE);
        //TODO: salvar ArrayList de outros dispositivos bluetooth
    }

    private void handleIntent(Intent intent) {
        Log.d(TAG, "handleIntent() called");
        if (intent != null) {
            String action = intent.getAction();

            //TODO: atulizar o filtro do acotion para cada tipo de requisicao
            if (action != null && intent.getAction().equals(BikeDevicesActivity.ACTION_SEND_DEVICE)) {
                Log.d(TAG, "handleIntent : ACTION_SEND_DEVICE");
                BluetoothDevice device = intent.getParcelableExtra(BikeDevicesActivity.BLUETOOTH_DEVICE);

                BikeDevice bikeDevice = intent.getParcelableExtra(BikeDevicesActivity.EXTRA_BIKE_DEVICE);
                if (bikeDevice != null && bikeDevice.getBluetoothDevice() != null) {
                    mBikeDevice = bikeDevice;
                }

                filterOtherDevice(device);
            } else if (action != null && intent.getAction().equals(RouteDetailsActivity.ACTION_SEND_SELECTED_ROUTE_LOCATION_POINTS)) {
                mRouteLocationSelected = intent.getParcelableArrayListExtra(
                        RouteDetailsActivity.EXTRA_SELECTED_ROUTE_LOCATION_POINTS);
            }
        }
    }

    //TODO: refatorar para MVP
    public void setBikeDevice(BikeDevice bikeDevice) {
        if (bikeDevice != null && bikeDevice.getBluetoothDevice() != null) {
            mBikeDevice = bikeDevice;
        }
    }

    private void setupSelectedRoutePolyline(List<LatLng> locations) {
        PolylineOptions polylineOptions = MapsHelper.createPolylineOptions(locations, Color.GREEN, 20);
        mSelectedRoutePolyline = mGoogleMap.addPolyline(polylineOptions);
    }

    private void animateCameraToLocation(LatLng latLng) {
        Log.d(TAG, "animateCameraToLocation called");
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, CAMERA_MAP_ZOOM));
    }

    private void updateMarkerPosition(LatLng currentPosition) {

        if (mMapMarker == null) {
            mMapMarker = mGoogleMap.addMarker(new MarkerOptions()
                    .position(currentPosition)
                    .icon(BitmapDescriptorFactory.fromBitmap(
                            new BitmapCircle(50, 50, Color.BLUE, Color.GREEN)
                                    .getCircleBitmap())));
        }
        mMapMarker.setPosition(currentPosition);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady called");
        mGoogleMap = googleMap;

        if (mRouteLocationSelected != null) {
            setupSelectedRoutePolyline(mRouteLocationSelected);
        }

        if(mLastKnownLocation != null) {
            Log.d(TAG, "onMapReady set last location do maps");
            setMapToLocation(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()));
        }
    }


    public void setMapToLocation(LatLng latLng) {
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, CAMERA_MAP_ZOOM));
        updateMarkerPosition(latLng);
    }

    //TODO: organizar em mvp
    private void setMapToLastLocation() {
        Log.d(TAG, "setMapToLastLocation called");
        if(checkLocationPermission()) {
            Log.d(TAG, "setMapToLastLocation setting last known location");
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            Location lastLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if(lastLocation == null) {
                lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }

            if (lastLocation != null) {
                Log.d(TAG, "setMapToLastLocation Movendo a camera para a ultima localizacao conhecida");
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation.getLatitude(),
                        lastLocation.getLongitude()), CAMERA_MAP_ZOOM));
                updateMarkerPosition(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()));
            }
        }
    }

    @Override
    public void showExerciseResumeUi(int bikeExerciseId) {
        Log.d(TAG, "showExerciseResumeUi called");
        Intent intent = new Intent(this, ExerciseSummaryActivity.class);
        intent.putExtra(ExerciseSummaryActivity.EXTRA_BIKE_EXERCISE_ID, bikeExerciseId);
        startActivity(intent);
    }

    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            Log.d(TAG, "MyLocationListener onLocationChanged called");
            Log.d(TAG, "latitude: " + location.getLatitude() + " longitude: " + location.getLongitude());
            Log.d(TAG, "onLocationChanged provider: " + location.getProvider());
            animateCameraToLocation(new LatLng(location.getLatitude(), location.getLongitude()));
            updateMarkerPosition(new LatLng(location.getLatitude(),location.getLongitude()));
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}
