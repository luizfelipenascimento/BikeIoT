package android.scopus.com.bikeiot.hospitals;

import android.content.Context;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.model.beans.Place;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import static android.scopus.com.bikeiot.utils.Helper.formatDistanceToKmOrMetersWithIndicator;

/**
 *
 */

public class HospitalsLocationsListAdapter extends RecyclerView.Adapter<HospitalsLocationsListAdapter.HospitalsLocationsViewHolder> {

    private final Context mContext;
    private List<Place> mHospitalsPlaces;

    interface HospitalClickListener {
        void onClickListItem(Place hospitalPlace);
    }

    public HospitalsLocationsListAdapter(List<Place> hospitalsPlaces, Context context,
                                         HospitalClickListener hospitalClickListener) {
        mContext = context;
        mHospitalsPlaces = hospitalsPlaces;
        mHospitalClickListener = hospitalClickListener;
    }

    private HospitalClickListener mHospitalClickListener;

    @Override
    public HospitalsLocationsViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        int layoutForHospitalLocationRow = R.layout.hospital_item_row;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutForHospitalLocationRow, viewGroup, shouldAttachToParentImmediately);

        return new HospitalsLocationsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HospitalsLocationsViewHolder holder, int position) {
        Place place = mHospitalsPlaces.get(position);

        holder.mDistanceToHospital.setText(formatDistanceToKmOrMetersWithIndicator
                (place.getDistanceToPlace(), mContext));
        holder.mHospitalName.setText(place.getName());

    }

    @Override
    public int getItemCount() {
        return mHospitalsPlaces.size();
    }

    class HospitalsLocationsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mDistanceToHospital;
        private TextView mHospitalName;

        public HospitalsLocationsViewHolder(View itemView) {
            super(itemView);
            mHospitalName = (TextView) itemView.findViewById(R.id.place_name_text_view);
            mDistanceToHospital = (TextView) itemView.findViewById(R.id.distance_to_hospital_label);

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            mHospitalClickListener.onClickListItem(mHospitalsPlaces.get(position));
        }
    }
}
