package android.scopus.com.bikeiot.signup;


import android.content.Context;

public interface SignUpContract {

    interface View {
        void showEmailMessageError(int msgId);
        void showPasswordMessageError(int msgId);
        void showLogin();
        void showGeneralError(int mgsId);
        void showNameMessageError(int msgId);
        void showBirthdayMessageError(int msgId);
    }

    interface Presenter {
        void signUp(String email, String password, String name, String birthday);
        void setContext(Context context);
    }
}
