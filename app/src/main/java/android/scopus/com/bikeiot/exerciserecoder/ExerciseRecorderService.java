package android.scopus.com.bikeiot.exerciserecoder;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.Looper;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import android.scopus.com.bikeiot.utils.Helper;
import android.scopus.com.bikeiot.R;
import android.scopus.com.bikeiot.utils.SharedPreferencesUtils;
import android.scopus.com.bikeiot.bikebluetoothdevice.BikeBluetoothLostConnectionDialogActivity;
import android.scopus.com.bikeiot.common.WarningDialogActivity;
import android.scopus.com.bikeiot.model.bluetooth.BluetoothConnectionService;
import android.scopus.com.bikeiot.model.filerelated.FileRepository;
import android.scopus.com.bikeiot.model.location.LocationHandler;
import android.scopus.com.bikeiot.model.beans.BikeDevice;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.bluetooth.BluetoothProtocol;
import android.scopus.com.bikeiot.model.filerelated.CSVFile;
import android.scopus.com.bikeiot.model.beans.ExerciseChronometer;
import android.scopus.com.bikeiot.model.beans.Result;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.data.remote.aws3.S3UploaderManager;
import android.scopus.com.bikeiot.model.filerelated.CSVPartTimerFactory;
import android.scopus.com.bikeiot.model.network.snaptoroadAPI.SnapToRoadGetData;
import android.scopus.com.bikeiot.panicbutton.CallHelpService;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import static android.scopus.com.bikeiot.Constants.EXTRA_CONTENT;
import static android.scopus.com.bikeiot.Constants.SHARED_PREFERENCES_KEY;
import static android.scopus.com.bikeiot.Constants.SHARED_USER_UUID;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.CSVFileResultsContract.*;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.CSVFileRouteContract.CSV_ROUTE_COLUMN_DATETIME;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.CSVFileRouteContract.CSV_ROUTE_COLUMN_LATITUDE;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.CSVFileRouteContract.CSV_ROUTE_COLUMN_LONGITUDE;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.CSVFileRouteContract.CSV_ROUTE_COLUMN_ID_USER;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.GPS_INFO;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.ROOT_FOLDER;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.SEND_FOLDER;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.SENSORS_INFO;
import static android.scopus.com.bikeiot.model.filerelated.FileContractConstants.UNDERLINE_SEPARATOR;

public class ExerciseRecorderService extends Service implements BluetoothConnectionService.ConnectionListener {

    //TODO: definir o minDistance para o update do location manger

    //TODO: Sempre que a service for encerrada enviar para a activity o bike device e/ou lista de devices
    //TODO: caso a conexao tenha sido perdida no onDestroy (remover da lista de devices o device conexao perdida)
    //TODO: caso a conexao com bike device tenha sido perdida retornar um bike device null no ondestroy
    //TODO: caso a conexao com bike device tenha sido perdida enviar uma mensagem para a UI
    //TODO: Enviar para o snap to road as ultimas 5 localizacoes sempre (sem interrupcao)

    public static final String ACTION_MAKE_CONNECTION =
            "android.scopus.com.bikeiot.ACTION_MAKE_CONNECTION";

    public static final String ACTION_SEND_USED_DATA
            = "android.scopus.com.bikeiot.ExerciseRecorderService.ACTION_SEND_USED_DATA";

    public static final  String EXTRA_USED_BIKE_DEVICE
            = "android.scopus.com.bikeiot.EXTRA_USED_BIKE_DEVICE";

    public static final String EXTRA_BIKE_DEVICE =
            "android.scopus.com.bikeiot.EXTRA_BIKE_DEVICE";

    public static final String ACTION_SEND_RESULT =
            "android.scopus.com.bikeiot.ACTION_SEND_RESULT";

    public static final String EXTRA_RESULT =
            "android.scopus.com.bikeiot.EXTRA_RESULT";

    public static final String ACTION_SEND_LOCATION
            = "android.scopus.com.bikeiot.ACTION_SEND_LOCATION";

    public static final String ACTION_SEND_SERVICE_STATUS =
            "android.scopus.com.bikeiot.ACTION_SEND_SERVICE_STATUS";

    public static final String ACTION_SEND_ROUTE
            = "android.scopus.com.bikeiot.ACTION_SEND_ROUTE";

    public static final String SEND_NORMALIZED_LOCATION_POINTS =
            "android.scopus.com.bikeiot.SEND_NORMALIZED_LOCATION_POINTS";

    public static final String ACTION_REQUEST_ROUTE_LOCATION
            = "android.scopus.com.bikeiot.ACTION_REQUEST_ROUTE_LOCATION";

    public static final String EXTRA_LOCATION
            = "android.scopus.com.bikeiot.EXTRA_LAST_KNOWN_LOCATION";

    public static final String EXTRA_USED_BIKE_EXERCISE =
            "android.scopus.com.bikeiot.EXTRA_USED_BIKE_EXERCISE";

    public static final String EXTRA_ROUTE =
            "android.scopus.com.bikeiot.EXTRA_ROUTE";
    private static final int FOREGROUND_NOTIFICATION_ID = 101;
    private static final String TAG = "ExerciseRecorderService";
    private static final int LOCATION_REQUEST_INTERVAL = 1000 * 5;
    private static final int LOCATION_REQUEST_FASTEST_INTERVAL = LOCATION_REQUEST_INTERVAL / 2;

    public static final String ACTION_SEND_LOST_CONNECTION_BIKE_DEVICE =
            "ACTION_LOST_CONNECTION_BIKE_DEVICE";

    private static final int PENDING_INTENT_CODE_EXERCISE_SERVICE = 0;
    private static final int PENDING_INTENT_CODE_BT_CONNECTION = 1;

    private static final int BT_CONNECTION_LOST_NOTIFICATION_CODE = 10;
    public static final String EXTRA_PAUSE_SITUATION = "EXTRA_PAUSE_SITUATION";
    public static final String EXTRA_RUNNING_SITUATION = "EXTRA_RUNNING_SITUATION";
    private static final long PART_TIME_LIMIT = 30000;


    private BluetoothDevice mBikeBluetoothDevice;
    private ArrayList<BluetoothDevice> mBluetoothDevices;
    private ArrayList<BluetoothConnectionService> mBluetoothConnections;
    private Result mResult;
    private CSVFile mResultCompleteFile;
    private CSVFile mRouteCompleteCSVFile;
    private boolean onAccidentMode = false;

    public static boolean isRunning = false;
    public static boolean isPaused = false;

    private Thread mThreadChronometer;
    private ExerciseChronometer mExerciseChronometer;
    private LocationManager mLocationManager;
    private LocationListener mLocationListener;

    private LocationHandler mLocationHandler;
    private Location mCurrentLocation;
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequestBalancedAccuracy;
    private LocationRequest mLocationRequestHighAccuracy;
    private LocationSettingsRequest mLocationSettingsRequest;
    private BikeExercise mBikeExercise;
    private Route mRoute;
    private BikeDevice mBikeDevice;
    private Location mLastKnownLocation;

    private int userId;
    private String mUserUUIDString;

    private FileRepository mFileRepository;

    private String[] mResultCsvFileHeader = {"AccelerometerX","AccelerometerY","AccelerometerZ", "GyroscopeX",
            "GyroscopeY","GyroscopeZ", "Velocity","Rotations","DateTime", "Paused",
            CSV_RESULT_COLUMN_PITCH, CSV_RESULT_COLUMN_ROLL, CSV_RESULT_COLUMN_ACC_LINEAR_VELOCITY,
            CSV_RESULT_COLUMN_AVERAGE_LAST_FIVE_ACCELEROMETER_Y, CSV_RESULT_COLUMN_AVERAGE_ACCELEROMETER_Z,
            CSV_RESULT_COLUMN_ID_USER
    };

    private String[] mRouteCsvFileHeader = {CSV_ROUTE_COLUMN_ID_USER, CSV_ROUTE_COLUMN_LATITUDE,
            CSV_ROUTE_COLUMN_LONGITUDE, CSV_ROUTE_COLUMN_DATETIME
    };

    private S3UploaderManager mS3UploaderManager;

    private CSVPartTimerFactory mCSVPartTimerFactoryRoute;

    private CSVPartTimerFactory mCSVPartTimerFactoryResult;

    private CSVPartTimerFactory.OnCreateFilePartListener mListenerResultCreationPart =
            new CSVPartTimerFactory.OnCreateFilePartListener() {
        @Override
        public void onCreatedFile(File file) {
            Log.d(TAG, "result file created " + file.getName());

            file = mFileRepository.moveFile(new File(Environment.getExternalStorageDirectory(),
                    ROOT_FOLDER + File.separator + SEND_FOLDER), file);

            mS3UploaderManager.uploadFile(file);
        }
    };

    private CSVPartTimerFactory.OnCreateFilePartListener mListenerRouteCreationPart =
            new CSVPartTimerFactory.OnCreateFilePartListener() {
        @Override
        public void onCreatedFile(File file) {
            Log.d(TAG, "route file created " + file.getName());

            file = mFileRepository.moveFile(new File(Environment.getExternalStorageDirectory(),
                    ROOT_FOLDER + File.separator + SEND_FOLDER), file);

            Log.d(TAG, "route file new path : " + file.getPath() + " file exist : " + file.exists() +
                    " is directory : " + file.isDirectory());

            mS3UploaderManager.uploadFile(file);
        }
    };

    //testing factory
    private CSVPartTimerFactory mTestCSVPartTimerFactory;
    private CSVPartTimerFactory.OnCreateFilePartListener mListenerTestFileCreation = new CSVPartTimerFactory.OnCreateFilePartListener() {
        @Override
        public void onCreatedFile(File file) {
            Log.d(TAG, "test file created" + file.getName());
            mS3UploaderManager.uploadFile(file);
        }
    };


    @SuppressLint("MissingPermission")
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate()");

        isRunning = true;

        mBluetoothConnections = new ArrayList<>();

        mLocationHandler = new LocationHandler();

        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mReceiverInput, new IntentFilter("inputMessage"));

        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mSnapToRoadReceiver, new IntentFilter(SnapToRoadGetData.ACTION_SEND_SNAPPED_LOCATION_POINTS));

        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mConnectionLostFeedBackReceiver, new IntentFilter(
                        BluetoothConnectionService.BT_SERVICE_CONNECTION_LOST_FEEDBACK));

        //setting up notification
        Intent notificationIntent = new Intent(this, ExerciseRecorderActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                PENDING_INTENT_CODE_EXERCISE_SERVICE ,notificationIntent,0);

        Log.d(TAG, "onCreate(): setting up notification");
        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setTicker(getString(R.string.app_name))
                .setContentText(getString(R.string.doing_exercise_foreground))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .build();
        startForeground(FOREGROUND_NOTIFICATION_ID, notification);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(getBaseContext());

        createLocationCallback();
        createLocationRequestHighAccuracy();
        buildLocationRequest();

        mS3UploaderManager = new S3UploaderManager(this);
        mFileRepository = new FileRepository(this);
    }


    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        Log.d(TAG, "startLocationUpdates called");
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.d(TAG, "startLocationUpdates onSuccess called");
                        mFusedLocationClient.requestLocationUpdates(mLocationRequestHighAccuracy, mLocationCallback, Looper.myLooper());
                    }
                });
    }

    private void buildLocationRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequestHighAccuracy)
                .addLocationRequest(mLocationRequestBalancedAccuracy);
        mLocationSettingsRequest = builder.build();
    }

    private void createLocationRequestBalancedAccuracy() {
        Log.d(TAG, "createLocationRequestHighAccuracy called");
        mLocationRequestBalancedAccuracy = new LocationRequest();
        mLocationRequestBalancedAccuracy.setInterval(LOCATION_REQUEST_INTERVAL);
        mLocationRequestBalancedAccuracy.setFastestInterval(LOCATION_REQUEST_FASTEST_INTERVAL);
        mLocationRequestBalancedAccuracy.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    private void createLocationRequestHighAccuracy() {
        Log.d(TAG, "createLocationRequestHighAccuracy called");
        mLocationRequestHighAccuracy = new LocationRequest();
        //mLocationRequestHighAccuracy.setInterval(LOCATION_REQUEST_INTERVAL);
        //mLocationRequestHighAccuracy.setFastestInterval(LOCATION_REQUEST_FASTEST_INTERVAL);
        mLocationRequestHighAccuracy.setInterval(0);
        mLocationRequestHighAccuracy.setFastestInterval(0);
        mLocationRequestHighAccuracy.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //mLocationRequestHighAccuracy.setSmallestDisplacement(2);
    }

    @SuppressLint("MissingPermission")
    private void getLastKnownLocation() {
        mFusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                Log.d(TAG, "getLastKnownLocation called");
                Log.d(TAG, "onComplete called");
                if(task.isComplete() && task.getResult() != null) {
                    mLastKnownLocation = task.getResult();
                }
            }
        });
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback(){


            @Override
            public void onLocationResult(LocationResult locationResult) {

                super.onLocationResult(locationResult);

                //Log.d(TAG, "LocationCallback onLocationResult called");

                Location location = locationResult.getLastLocation();

                if (mLocationHandler.isLocationBetter(location, mCurrentLocation)) {
                    //Log.d(TAG, "location is better than current location");
                    mCurrentLocation = location;

                    mRouteCompleteCSVFile.createInfoLineArray(mRouteCompleteCSVFile.getFileHeader().length);
                    mRouteCompleteCSVFile.appendInfoInLineArray(String.valueOf(mUserUUIDString));//id_user
                    mRouteCompleteCSVFile.appendInfoInLineArray(Double.toString(mCurrentLocation.getLatitude()));
                    mRouteCompleteCSVFile.appendInfoInLineArray(Double.toString(mCurrentLocation.getLongitude()));
                    mRouteCompleteCSVFile.appendInfoInLineArray(new SimpleDateFormat("dd-MM-yyy HH:mm:ss:SSS").format(new Date()));
                    mRouteCompleteCSVFile.writeLine(mRouteCompleteCSVFile.getInfoLineArray());

                    mCSVPartTimerFactoryRoute.appendInfo(mRouteCompleteCSVFile.getInfoLineArray());

                    /*mRoutePartCsvFile.writeLine(mRouteCompleteCSVFile.getInfoLineArray());
                    if ((System.currentTimeMillis() - routeCsvPartStartTimerCreation) >= PART_TIME_LIMIT) {
                        Log.d(TAG, "Send route part");
                        routeCsvPartStartTimerCreation = System.currentTimeMillis();
                        createRouteCsvPart();
                    }*/

                    mRouteCompleteCSVFile.clearInfoLineArray();

                    mRoute.addNewLocationPoint(new LatLng(location.getLatitude(), location.getLongitude()));
                    sendLocationToActivity(location);
                    sendLocationAndRouteToActivity(mCurrentLocation, mRoute);


                    /*if (isInternetConnected()) {
                        Log.d(TAG, "isInternetConnected() called ");
                        if (mRoute.getLocationPoints().size() - mRoute.getLastIndexOffset() >=
                                SnapToRoadGetData.PAGINATION_OVERLAP) {

                            Log.d(TAG, "delta (size - lastIndex) is 5 or greater");

                            new Thread(new SnapToRoadGetData(getBaseContext(), mRoute.getLocationPoints(),
                                    mRoute.getLastIndexOffset())).start();

                            mRoute.setLastIndexOffset(mRoute.getLocationPoints().size());

                        }
                    } else {
                        sendLocationAndRouteToActivity(mCurrentLocation, mRoute);
                    }*/
                }
            }
        };
    }

    private boolean isInternetConnected() {
        ConnectivityManager cm = (ConnectivityManager)
                getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo info = cm.getActiveNetworkInfo();
        return info != null && info.isConnected();
    }

    private void stopLocationUpdates() {
        Log.d(TAG, "stopLocationUpdates");
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    @SuppressLint("MissingPermission")
    private void configureRequestLocationUpdates() {
        mLocationManager.requestLocationUpdates
                (LocationManager.NETWORK_PROVIDER, 5000, 0, mLocationListener);
        mLocationManager.requestLocationUpdates
                (LocationManager.GPS_PROVIDER, 5000, 0, mLocationListener);
    }

    private void createLocationListener() {
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                Log.d(TAG, "onLocationChanged lat : " + location.getLatitude()
                        + " long : " + location.getLongitude());

                if(mLocationHandler.isLocationBetter(location, mCurrentLocation)) {
                    Log.d(TAG, "location is better than current location");
                    mCurrentLocation = location;
                    mRouteCompleteCSVFile.createInfoLineArray(mRouteCompleteCSVFile.getFileHeader().length);
                    mRouteCompleteCSVFile.appendInfoInLineArray("1");//id_exercise
                    mRouteCompleteCSVFile.appendInfoInLineArray("1");//id_route
                    mRouteCompleteCSVFile.appendInfoInLineArray("1");//id_user
                    mRouteCompleteCSVFile.appendInfoInLineArray(Double.toString(mCurrentLocation.getLatitude()));
                    mRouteCompleteCSVFile.appendInfoInLineArray(Double.toString(mCurrentLocation.getLongitude()));
                    mRouteCompleteCSVFile.writeLine(mRouteCompleteCSVFile.getInfoLineArray());
                    mRouteCompleteCSVFile.clearInfoLineArray();
                    mRoute.addNewLocationPoint(new LatLng(location.getLatitude(), location.getLongitude()));
                    sendLocationAndRouteToActivity(mCurrentLocation, mRoute);
                }


            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        };
    }

    //startId = o id de cada processo inicializado no service
    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand()");

        if (intent != null) {
            if (intent.getAction().equals(ExerciseRecorderActivity.ACTION_START_EXERCISE)) {
                Log.d(TAG, "ACTION_START_EXERCISE called");

                isRunning = true;
                isPaused = false;

                Bundle extras = intent.getExtras();
                mBikeDevice = extras.getParcelable(ExerciseRecorderActivity.EXTRA_BIKE_DEVICE);

                mBikeExercise = new BikeExercise();
                mBikeExercise.setExerciseDate(new Date());
                mBikeExercise.setBikeDevice(mBikeDevice);
                mResult = new Result();
                mRoute = new Route();
                mRoute.setDate(new Date());
                mRoute.setDescription("Route");

                //adicionando o primeiro ponto a lista de localizacoes da rota
                Location location = extras.getParcelable(ExerciseRecorderActivity.EXTRA_LAST_KNOWN_LOCATION);
                if (location!= null) {
                    Log.d(TAG, "location: lat" + location.getLatitude() + " lng: " + location.getLongitude());
                    mRoute.addNewLocationPoint(new LatLng(location.getLatitude(), location.getLongitude()));
                }

                mBikeExercise.setResult(mResult);
                mBikeExercise.setRoute(mRoute);

                mRouteCompleteCSVFile = createRouteFile();
                mRoute.setRouteCSVFile(mRouteCompleteCSVFile);
                mRoute.setFileRoutePath(mRouteCompleteCSVFile.getPathFile());

                mResultCompleteFile = createResultFile();
                mResult.setBikeExercise(mBikeExercise);
                mResult.setResultCSVFile(mResultCompleteFile);
                mResult.setFileResultPath(mResultCompleteFile.getPathFile());

                //criacao da parte do csv
                //createResultCsvPart();

                userId = SharedPreferencesUtils.getIntSharedPreferences(this,
                        getString(R.string.shared_preferences_key),
                        getString(R.string.shared_user_id),0);

                mUserUUIDString = SharedPreferencesUtils.getStringSharedPreferences(this,
                        SHARED_PREFERENCES_KEY, SHARED_USER_UUID,null);

                getLastKnownLocation();
                if (mLastKnownLocation != null) {
                    mRoute.addNewLocationPoint(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()));
                }

                String resultPartFileName = SENSORS_INFO + UNDERLINE_SEPARATOR + mUserUUIDString + UNDERLINE_SEPARATOR;
                mCSVPartTimerFactoryResult = new CSVPartTimerFactory(this, resultPartFileName,
                        mResultCsvFileHeader, mListenerResultCreationPart, 30000);

                String routePartFileName = GPS_INFO + UNDERLINE_SEPARATOR + mUserUUIDString + UNDERLINE_SEPARATOR;
                mCSVPartTimerFactoryRoute = new CSVPartTimerFactory(this, routePartFileName,
                        mRouteCsvFileHeader, mListenerRouteCreationPart, 30000);

                /*mTestCSVPartTimerFactory = new CSVPartTimerFactory(this,
                        "test-file-creation", new String[]{"test"}, mListenerTestFileCreation, 10000);*/

                startLocationUpdates();

                makeConnection(mBikeDevice.getBluetoothDevice(), startId);

                initializeChronometer();
            }

            if (intent.getAction().equals(ExerciseRecorderActivity.ACTION_PAUSE_EXERCISE)) {
                Log.d(TAG, ExerciseRecorderActivity.ACTION_PAUSE_EXERCISE + "called");
                mExerciseChronometer.pause();
                isPaused = true;
            }

            if(intent.getAction().equals(ExerciseRecorderActivity.ACTION_CONTINUE_EXERCISE)) {
                Log.d(TAG, "ACTION_CONTINUE_EXERCISE called");
                mExerciseChronometer.play();
                continueAllBluetoothCommunications();
                onAccidentMode = false;
                isPaused = false;
            }

            if(intent.getAction().equals(ACTION_MAKE_CONNECTION)) {
                Log.d(TAG, "ACTION_MAKE_CONNECTION called");
                BluetoothDevice device = intent.getExtras().getParcelable("device");
                Log.d(TAG, "make connection with bluetooth name: " + device.getName());
                mBluetoothConnections.add(new BluetoothConnectionService(
                        ExerciseRecorderService.this, startId, this));
                mBluetoothConnections.get(mBluetoothConnections.size() - 1).startClient(device);
            }

            if (intent.getAction().equals(ExerciseRecorderActivity.ACTION_FINISH_EXERCISE)) {
                Log.d(TAG, "ACTION_FINISH_EXERCISE called");
                try {
                    mResult.setTotalTimeChronometer(mExerciseChronometer.getTime());
                } catch (Exception e) {
                    Log.e(TAG, "Exception " + e);
                }
                closeFiles();
                sendUsedDataToActivity();
            }

            if (intent.getAction().equals(ACTION_REQUEST_ROUTE_LOCATION)) {
                Log.d(TAG, "ACTION_REQUEST_ROUTE_LOCATION called");
                if (mRoute != null) {
                    Log.d(TAG, "ACTION_REQUEST_ROUTE_LOCATION mRoute is not null");
                    sendRouteToActivity(mRoute);
                }
            }
        }
        return START_STICKY;
    }

    private void continueAllBluetoothCommunications() {
        for (BluetoothConnectionService bts : mBluetoothConnections) {
            if (bts.getConnectedDevice().getAddress().equals(mBikeDevice.getBluetoothDevice().getAddress())) {
                bts.write(BluetoothProtocol.FIRST_RESET_KEY_WORD);
            }

            bts.continueCommunication();

        }
    }

    //TODO: update action name also in activity
    private void sendLocationAndRouteToActivity(Location location, Route route) {
        Intent intent = new Intent(ACTION_SEND_LOCATION);
        intent.putExtra(EXTRA_LOCATION, location);
        intent.putExtra(EXTRA_ROUTE, route);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendRouteToActivity(Route route) {
        Log.d(TAG, "sendRouteToActivity called");
        Intent intent = new Intent(ACTION_SEND_ROUTE);
        intent.putExtra(EXTRA_ROUTE, route);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void sendLocationToActivity(Location location) {
        Intent intent = new Intent(ACTION_SEND_LOCATION);
        intent.putExtra(EXTRA_LOCATION, location);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void makeConnection(BluetoothDevice device, int startId) {
        Log.d(TAG, "makeConnection() bluetooth name: " + device.getName());
        mBluetoothConnections.add(new BluetoothConnectionService(
                ExerciseRecorderService.this, startId, this));
        mBluetoothConnections.get(mBluetoothConnections.size() - 1).startClient(device);
    }

    private void makeConnection(BluetoothDevice device, BluetoothProtocol bluetoothProtocol, int startId) {
        Log.d(TAG, "makeConnection() bluetooth name: " + device.getName());
        mBluetoothConnections.add(new BluetoothConnectionService(
                ExerciseRecorderService.this, startId, this));
        mBluetoothConnections.get(mBluetoothConnections.size() - 1).startClient(device);
    }

    private void finishChronometer() {
        mExerciseChronometer.stop();
        mExerciseChronometer.resetTime();
        mThreadChronometer.interrupt();
        mThreadChronometer = null;
        mExerciseChronometer = null;
    }

    @NonNull
    private CSVFile createResultFile() {
        String fileName = String.valueOf(UUID.randomUUID()) + "BikeResult";
        return new CSVFile(fileName,this, CSVFile.RESULT_ROOT_FOLDER_NAME,mResultCsvFileHeader);
    }

    @NonNull
    private CSVFile createRouteFile() {
        String fileName = String.valueOf(UUID.randomUUID()) + "Location";
        return new CSVFile(fileName, this, CSVFile.ROUTE_ROOT_FOLDER_NAME, mRouteCsvFileHeader);
    }

    private void initializeChronometer() {
        mExerciseChronometer = new ExerciseChronometer(this);
        mThreadChronometer = new Thread(mExerciseChronometer);
        mThreadChronometer.start();
        mExerciseChronometer.start();
    }

    private void closeFiles() {
        Log.d(TAG, "closeFiles called");
        mResultCompleteFile.close();
        mRouteCompleteCSVFile.close();
    }

    private void stopBluetoothConnections() {
        Log.d(TAG, "stopBluetoothConnections called");
        for(BluetoothConnectionService bts : mBluetoothConnections) {
            bts.stopConnection();
            stopSelf(bts.getStartId());
        }
    }

    private void sendUsedDataToActivity() {
        Log.d(TAG, "sendUsedDataToActivity called");
        Intent sendUsedDataIntent = new Intent();
        sendUsedDataIntent.setAction(ACTION_SEND_USED_DATA);
        sendUsedDataIntent.putExtra(EXTRA_USED_BIKE_DEVICE, mBikeDevice);
        sendUsedDataIntent.putExtra(EXTRA_USED_BIKE_EXERCISE, mBikeExercise);
        LocalBroadcastManager.getInstance(this).sendBroadcast(sendUsedDataIntent);
    }

    private void sendSendRouteNormalizedLocationPoints(Route route) {
        Log.d(TAG, "sendSendRouteNormalizedLocationPoints called");
        Intent intent = new Intent(SEND_NORMALIZED_LOCATION_POINTS);
        intent.putExtra(EXTRA_ROUTE, route);
        intent.putExtra("points", route.getLocationsNormalized());
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void destroyBroadcastReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiverInput);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mSnapToRoadReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mConnectionLostFeedBackReceiver);
    }

    public static boolean isRunning() {
        return isRunning;
    }

    public static boolean isPaused() {
        return isPaused;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() terminando os processos");

        stopForeground(true);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(FOREGROUND_NOTIFICATION_ID);

        //resentando flags
        isRunning = false;
        isPaused = false;
        onAccidentMode = false;

        //colocando o ultimo valor do cronometro no resultado

        finishChronometer();
        stopBluetoothConnections();
        closeFiles();
        stopLocationUpdates();

        //sendUsedDataToActivity();
        destroyBroadcastReceivers();

        //        if (mLocationManager != null && mLocationListener != null) {
//            Log.d(TAG, "Removing update mLocationListener");
//            mLocationManager.removeUpdates(mLocationListener);
//        }
        stopService(new Intent(this, CallHelpService.class));

        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(ACTION_SEND_SERVICE_STATUS));
    }

    BroadcastReceiver mSnapToRoadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "mSnapToRoadReceiver onReceive called");
            String action = intent.getAction();
            if (action.equals(SnapToRoadGetData.ACTION_SEND_SNAPPED_LOCATION_POINTS)) {
                Log.d(TAG, "mSnapToRoadReceiver ACTION_SEND_SNAPPED_LOCATION_POINTS");
                ArrayList<LatLng> locations = intent.getParcelableArrayListExtra(SnapToRoadGetData.EXTRA_LOCATIONS);
                Log.d(TAG, "mSnapToRoadReceiver location size:" +locations.size());
                for (LatLng latLng : locations) {
                    mRoute.addLocationToNormalizedLocation(latLng);
                    sendSendRouteNormalizedLocationPoints(mRoute);
                }
            }
        }
    };

    BroadcastReceiver mConnectionLostFeedBackReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(BluetoothConnectionService.BT_SERVICE_CONNECTION_LOST_FEEDBACK)) {
                String bluetoothAddress = intent.getStringExtra(BluetoothConnectionService.EXTRA_USED_DEVICE_ADDRESS);

                if (bluetoothAddress.equals(mBikeDevice.getBluetoothDevice().getAddress())) {
                    Log.d(TAG, "mConnectionLostFeedBackReceiver : bikeBluetooth connection was lost");
                    sendLostBikeBluetoothConnectionWarning();
                    showBikeBluetoothLostConnectionDialog();
                    createLostConnectionNotification(bluetoothAddress);
                    vibrate(500);

                    Log.d(TAG, "mConnectionLostFeedBackReceiver stopSelf terminando os processos");
                    stopSelf();
                } else {
                    createLostConnectionNotification(bluetoothAddress);
                    vibrate(200);
                }
            }
        }
    };

    private void showBikeBluetoothLostConnectionDialog() {
        Intent dialogIntent = new Intent(this,
                BikeBluetoothLostConnectionDialogActivity.class);

        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(dialogIntent);
    }

    private void vibrate(long seconds) {
        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrator.vibrate(seconds);
        }
    }

    private void sendLostBikeBluetoothConnectionWarning() {
        LocalBroadcastManager.getInstance(this)
                .sendBroadcast(new Intent(ACTION_SEND_LOST_CONNECTION_BIKE_DEVICE));
    }

    private void createLostConnectionNotification(String bluetoothAddress) {

        String detailsLabel = getString(R.string.details_label);
        String addressLabel = getString(R.string.address_label);
        String bluetoothDetails = "";

        if (bluetoothAddress.equals(mBikeDevice.getBluetoothDevice().getAddress())) {
            Log.d(TAG, "mConnectionLostFeedBackReceiver : bikeBluetooth connection was lost");
            Log.d(TAG, "bluetooth name: " + mBikeDevice.getBluetoothDevice().getName());
            bluetoothDetails += getString(R.string.bluetooth_bike_type);
        } else {
            bluetoothDetails += getString(R.string.bluetooth_other_resource_type);
        }

        String notificationContent = getString(R.string.bluetooth_connection_lost_base_text);
        String details = detailsLabel + " " + bluetoothDetails + " - " + addressLabel + bluetoothAddress;

        Intent notificationIntent = new Intent(this, ExerciseRecorderActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,notificationIntent,0);
        Log.d(TAG, "onCreate(): setting up notification");
        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.bluetooth_connection_lost_base_text))
                .setContentText(notificationContent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(details))
                .build();

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(BT_CONNECTION_LOST_NOTIFICATION_CODE, notification);
    }

    BroadcastReceiver mReceiverInput = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String input = intent.getStringExtra("data");
            BluetoothDevice device = intent.getExtras().getParcelable("device");
            Log.d(TAG, "mReceiverMessage: message: " + input);

            //TODO: se dispositivo de frequencia cardiaca esta disponivel fazer um novo arquivo somente com suas informacoes
            //TODO: se dispositivo de frequencia cardiaca esta disponivel colocar essas inforcaoes no objeto Result

            mResult.setResultString(input);

            //TODO: refatorar
            if (mResult.getAccidentStatus().equals(Result.ACCIDENT_HAPPENED) && !onAccidentMode) {
                onAccidentMode = true;
                Log.d(TAG, "acidente ocorreu");
                isPaused = true;
                pauseAllBluetoothCommunications();
                mExerciseChronometer.pause();
                sendChangedStatusAction();
                vibrate(600);
                vibrate(600);
                startCallHelpWarningEvent();
            }

            //escrevendo no arquivo
            if (mResultCompleteFile.isExternalStorageWritable()) {
                Log.d(TAG, "saveResultInFile isExternalStorageWritable true");

                mResultCompleteFile.createInfoLineArray(mResultCompleteFile.getFileHeader().length);

                for(int i = 0; i < mResult.getAccelerationInfo().length; i++) {
                    mResultCompleteFile.appendInfoInLineArray(String.valueOf(mResult.getAccelerationInfo()[i]));
                }

                for(int i = 0; i < mResult.getGyroscopeInfo().length; i++) {
                    mResultCompleteFile.appendInfoInLineArray(String.valueOf(mResult.getGyroscopeInfo()[i]));
                }

                mResultCompleteFile.appendInfoInLineArray((String.valueOf(mResult.getVelocityMetersPerSec())));
                mResultCompleteFile.appendInfoInLineArray(String.valueOf(mResult.getRotations()));
                mResultCompleteFile.appendInfoInLineArray(Helper.formatToDateTimeString(new Date()));
                mResultCompleteFile.appendInfoInLineArray(String.valueOf(isPaused));
                mResultCompleteFile.appendInfoInLineArray(String.valueOf(mResult.getPitch()));
                mResultCompleteFile.appendInfoInLineArray(String.valueOf(mResult.getRoll()));
                mResultCompleteFile.appendInfoInLineArray(String.valueOf(mResult.getLinearAccelerationAcc()));
                mResultCompleteFile.appendInfoInLineArray(String.valueOf(mResult.getAverageAccY()));
                mResultCompleteFile.appendInfoInLineArray(String.valueOf(mResult.getAverageAccZ()));
                mResultCompleteFile.appendInfoInLineArray(String.valueOf(mUserUUIDString));

                mResultCompleteFile.writeLine(mResultCompleteFile.getInfoLineArray());

                //testing creating other file
                //mTestCSVPartTimerFactory.appendInfo(new String[]{"testing"});

                Log.d(TAG, "mCSVPartTimerFactoryResult append info");
                mCSVPartTimerFactoryResult.appendInfo(mResultCompleteFile.getInfoLineArray());

                mResultCompleteFile.clearInfoLineArray();

            }

            if (!isPaused) {
                Intent intentToRecorderActivity = new Intent(ACTION_SEND_RESULT);
                intentToRecorderActivity.putExtra(EXTRA_RESULT, mResult);
                intentToRecorderActivity.putExtra("device", device);
                intentToRecorderActivity.putExtra("message", input);
                LocalBroadcastManager.getInstance(ExerciseRecorderService.this).sendBroadcast(intentToRecorderActivity);
            }

            /*for(BluetoothConnectionService btc : mBluetoothConnections) {
                if(btc.getConnectedDevice() == device) {
                    btc.write("SENDIT");
                    break;
                }
            }*/
        }
    };

    private void startCallHelpWarningEvent() {
        Intent intentCallHelpService = new Intent(this, CallHelpService.class);
        intentCallHelpService.setAction(CallHelpService.ACTION_START_WARNING_EVENT);
        startService(intentCallHelpService);
    }

    private void sendChangedStatusAction() {
        Intent intent = new Intent(ACTION_SEND_SERVICE_STATUS);
        intent.putExtra(EXTRA_PAUSE_SITUATION, isPaused);
        intent.putExtra(EXTRA_RUNNING_SITUATION, isRunning);
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(ACTION_SEND_SERVICE_STATUS));
    }

    private void pauseAllBluetoothCommunications() {
        for (BluetoothConnectionService btc : mBluetoothConnections) {
            btc.pauseCommunication();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnectionSucceed(BluetoothDevice bluetoothDevice) {
        Log.d(TAG, "onConnectionSucceed bluetooth: " + bluetoothDevice.getName());
    }

    @Override
    public void onConnectionFailed(BluetoothDevice bluetoothDevice) {
        Log.d(TAG, "onConnectionFailed called");

        if (mBikeDevice.getBluetoothDevice().getAddress().equals(bluetoothDevice.getAddress())) {
            Log.d(TAG, "connection failed with bike bluetooth");
            vibrate(200);

            Intent dialogIntent = new Intent(this, WarningDialogActivity.class);
            dialogIntent.putExtra(EXTRA_CONTENT, this.getString(R.string.error_while_connecting_to__bike_bluetooth));
            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_USER_ACTION);
            startActivity(dialogIntent);

            LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(ACTION_SEND_LOST_CONNECTION_BIKE_DEVICE));
            stopSelf();
        }
    }
}
