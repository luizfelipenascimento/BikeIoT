package android.scopus.com.bikeiot.model.location;

import android.annotation.SuppressLint;

import android.location.Location;
import android.os.Looper;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

/**
 *
 */

public class LocationUpdater {

    private static final String TAG = LocationUpdater.class.getSimpleName();
    private final SettingsClient mSettingsClient;
    private final FusedLocationProviderClient mFusedLocationProviderClient;
    private Callback.OnLastKnownLocation mOnLastKnownLocationCallback;
    private Location mCurrentLocation;
    private LocationHandler mLocationHandler;
    private Callback.OnLocationReceived mOnLocationReceivedCallback;
    private LocationCallback mLocationCallback;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationRequest mLocationRequest;

    public LocationUpdater(FusedLocationProviderClient fusedLocationProviderClient,
                           SettingsClient settingsClient,
                           Callback.OnLocationReceived onLocationReceivedCallback,
                           Callback.OnLastKnownLocation onLastKnownLocationCallback,
                           LocationRequest locationRequest) {

        mFusedLocationProviderClient = fusedLocationProviderClient;
        mSettingsClient = settingsClient;
        mLocationHandler = new LocationHandler();
        mOnLocationReceivedCallback = onLocationReceivedCallback;
        mOnLastKnownLocationCallback = onLastKnownLocationCallback;
        mLocationRequest = locationRequest;
        createLocationCallback();
        buildLocationRequest(mLocationRequest);
    }

    public LocationUpdater(FusedLocationProviderClient fusedLocationProviderClient,
                           SettingsClient settingsClient, LocationRequest locationRequest) {

        mFusedLocationProviderClient = fusedLocationProviderClient;
        mSettingsClient = settingsClient;
        mLocationHandler = new LocationHandler();
        mLocationRequest = locationRequest;
        buildLocationRequest(mLocationRequest);
    }

    public LocationUpdater(FusedLocationProviderClient fusedLocationProviderClient,
                           SettingsClient settingsClient,
                           Callback.OnLocationReceived onLocationReceivedCallback,
                           LocationRequest locationRequest) {

        mFusedLocationProviderClient = fusedLocationProviderClient;
        mSettingsClient = settingsClient;
        mLocationHandler = new LocationHandler();
        mOnLocationReceivedCallback = onLocationReceivedCallback;
        mLocationRequest = locationRequest;
        createLocationCallback();
        buildLocationRequest(mLocationRequest);
    }

    public interface Callback {
        interface OnLocationReceived {
            void locationReceived(Location location);

        }
        interface OnLastKnownLocation {
            void lastKnownLocationReceived(Location location);
        }
    }

    public void setupLocationCallback(LocationCallback locationCallback) {
        mLocationCallback = locationCallback;
    }

    public void setupLocationUpdateCallback(Callback.OnLocationReceived onLocationReceived) {
        mOnLocationReceivedCallback = onLocationReceived;
        createLocationCallback();
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location location = locationResult.getLastLocation();
                if (mLocationHandler.isLocationBetter(location, mCurrentLocation)) {
                    mCurrentLocation = location;
                    mOnLocationReceivedCallback.locationReceived(mCurrentLocation);
                }
            }
        };
    }

    private void buildLocationRequest(LocationRequest locationRequest) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        mLocationSettingsRequest = builder.build();
    }

    public void stopLocationUpdates() {
        Log.d(TAG, "stopLocationUpdates");
        if (mLocationCallback != null) {
            mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        }
    }

    @SuppressLint("MissingPermission")
    public void startLocationUpdates() {
        Log.d(TAG, "startLocationUpdates called");
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.d(TAG, "startLocationUpdates onSuccess called");
                        mFusedLocationProviderClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());
                    }
                });
    }

    @SuppressLint("MissingPermission")
    public void getLastKnownLocation() {
        mFusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                Log.d(TAG, "getLastKnownLocation called");
                Log.d(TAG, "onComplete called");
                if(task.isComplete() && task.getResult() != null) {
                    Location lastLocation = task.getResult();
                    mOnLastKnownLocationCallback.lastKnownLocationReceived(lastLocation);
                }
            }
        });
    }

    @SuppressLint("MissingPermission")
    public void getLastKnownLocation(final Callback.OnLastKnownLocation onLastKnownLocationCallback) {
        mFusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
            @Override
            public void onComplete(@NonNull Task<Location> task) {
                Log.d(TAG, "getLastKnownLocation called");
                Log.d(TAG, "onComplete called");
                if(task.isComplete() && task.getResult() != null) {
                    Location lastLocation = task.getResult();
                    onLastKnownLocationCallback.lastKnownLocationReceived(lastLocation);
                }
            }
        });
    }

    public static LocationRequest locationRequestHighAccuracy(long fastestInterval, long interval,
                                                              int displacement) {
        LocationRequest locationReq = new LocationRequest();
        locationReq.setFastestInterval(fastestInterval);
        locationReq.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationReq.setInterval(interval);
        locationReq.setSmallestDisplacement(displacement);
        return locationReq;
    }

}
