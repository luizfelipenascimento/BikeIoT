package android.scopus.com.bikeiot.exercisedetails;

import android.net.Uri;
import android.scopus.com.bikeiot.model.beans.BikeExercise;
import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.beans.User;

import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;

/**
 *
 */

public interface ExerciseDetailsContract {

    interface View {
        void showExerciseDetails(BikeExercise bikeExercise);
        void showBikeExerciseEditUi(int bikeExerciseId);
        void showRouteDetailsUi(int routeId);
        void showUserProfileUi(int userId);
        void showExercisesListUi();
        void showExerciseDetailsLoadError(int errorMsgId);
        void showRouteInfo(Route route);
        void showUserInfo(User user);
        void showNoRouteInfo();
        void showNoUserInfo();
        void showDeleteStatusMessage(int messageId);
        void showGeneralError(int errorMessageId);
        void showRouteImage(Uri uri);
        void showNoRouteImage();
        void showErrorToReadResultFile();
        void showVelocityChart(ArrayList<Entry> velocityEntries);
        void showPitchChart(ArrayList<Entry> pitchEntries);
    }

    interface Presenter {
        void initialize();
        void loadBikeExercise(int bikeExerciseId);
        void openUserProfile();
        void openRouteDetails();
        void openBikeExerciseEdit();
        void removeBikeExercise();
    }

}
