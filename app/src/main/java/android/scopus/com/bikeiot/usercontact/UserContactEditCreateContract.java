package android.scopus.com.bikeiot.usercontact;

import android.scopus.com.bikeiot.model.beans.Contact;

/**
 *
 */

interface UserContactEditCreateContract {

    interface View {
        void showNameError(int msgError);
        void showPhoneError(int msgError);
        void showContactInfo(Contact contact);
        void showError(int general_process_error_msg);
        void showContactsFromResponseUi();
        void showSuccessMsg(int msgId);
        void forceMenuShowDeleteOption();
    }

    interface Presenter {
        void persist(String name, String phone);
        void removeContact();
        void initialize();
    }


}
