package android.scopus.com.bikeiot.model.data.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.scopus.com.bikeiot.model.beans.Contact;
import android.scopus.com.bikeiot.model.beans.User;
import android.scopus.com.bikeiot.model.data.ContactDataSource;
import android.scopus.com.bikeiot.model.data.local.BikeIoTContract.ContactsEntry;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */

public class ContactDAO implements ContactDataSource {

    private static final String TAG = "ContactDAO";
    private BikeIotDbHelper mDbHelper;

    public ContactDAO(Context context) {
        mDbHelper = new BikeIotDbHelper(context);
    }

    private String[] projection = {
            ContactsEntry._ID,
            ContactsEntry.COLUMN_PHONE,
            ContactsEntry.COLUMN_NAME,
            ContactsEntry.COLUMN_FK_USER
    };


    @Override
    public List<Contact> getContactsFromUser(int userId) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String whereClause = ContactsEntry.COLUMN_FK_USER + "=?";
        String[] whereArgs = {String.valueOf(userId)};

        Cursor cursor = db.query(ContactsEntry.TABLE_NAME,
                projection,
                whereClause,
                whereArgs,
                null,
                null,
                null);

        ArrayList<Contact> contacts = new ArrayList<>();

        try {
            while (cursor.moveToNext()) {
                Contact contact = getData(cursor);
                contacts.add(contact);
            }
        } catch (Exception e) {
            Log.d(TAG, "exception " + e);
        } finally {
            cursor.close();
            db.close();
        }

        return contacts;
    }

    @Override
    public Contact getContact(int id) {
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String whereClause = ContactsEntry._ID + "=?";
        String[] whereArgs = {String.valueOf(id)};

        Cursor cursor = db.query(ContactsEntry.TABLE_NAME,
                projection,
                whereClause,
                whereArgs,
                null,
                null,
                null);

        Contact contact = null;

        try {
            cursor.moveToNext();
            contact = getData(cursor);
        } catch (Exception e) {
            Log.e(TAG, "exception : " + e);
        } finally {
            cursor.close();
            db.close();
        }

        return contact;
    }

    @Override
    public boolean save(Contact contact) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = createBaseContentValues(contact);

        long generatedId = db.insert(ContactsEntry.TABLE_NAME, null, values);
        db.close();
        if (generatedId != -1) {
            contact.setId((int)generatedId);
            return true;
        }

        return false;
    }

    @Override
    public boolean edit(Contact contact) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        ContentValues values = createBaseContentValues(contact);

        String whereClause = ContactsEntry._ID + "=?";
        String whereArgs[] = {String.valueOf(contact.getId())};

        int affected = db.update(ContactsEntry.TABLE_NAME, values, whereClause, whereArgs);
        db.close();
        return affected == 1;
    }

    @Override
    public boolean remove(int id) {
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String whereClause = ContactsEntry._ID + "=?";
        String[] whereArgs = {String.valueOf(id)};

        int affected = db.delete(ContactsEntry.TABLE_NAME, whereClause, whereArgs);
        db.close();
        return affected == 1;
    }

    private ContentValues createBaseContentValues(Contact contact) {
        ContentValues values = new ContentValues();

        values.put(ContactsEntry.COLUMN_NAME, contact.getName());
        values.put(ContactsEntry.COLUMN_PHONE, contact.getPhoneNumber());

        if (contact.getUser() != null && contact.getUser().getId() != 0) {
            values.put(ContactsEntry.COLUMN_FK_USER, contact.getUser().getId());
        }

        return values;
    }

    private Contact getData(Cursor c) {
        String name = c.getString(c.getColumnIndex(ContactsEntry.COLUMN_NAME));
        String phone = c.getString(c.getColumnIndex(ContactsEntry.COLUMN_PHONE));
        int fkUserId = c.getInt(c.getColumnIndex(ContactsEntry.COLUMN_FK_USER));
        int id = c.getInt(c.getColumnIndex(ContactsEntry._ID));

        Contact contact = new Contact();
        contact.setName(name);
        contact.setPhoneNumber(phone);
        contact.setId(id);
        User user = new User();
        user.setId(fkUserId);
        contact.setUser(user);

        return contact;
    }
}
