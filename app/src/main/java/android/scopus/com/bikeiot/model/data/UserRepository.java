package android.scopus.com.bikeiot.model.data;

import android.scopus.com.bikeiot.model.beans.Route;
import android.scopus.com.bikeiot.model.beans.User;

/**
 * Created by Luiz Felipe on 10/04/2018.
 */

public class UserRepository implements UserDataSource {

    private UserDataSource mUserDataSource;

    private User mCachedUser;

    private boolean cacheBroken = true;


    public UserRepository(UserDataSource userDataSource) {
        mUserDataSource = userDataSource;
    }

    @Override
    public User getUser(int userId) {
        if (!cacheBroken && mCachedUser != null) {
            if (mCachedUser.getId() == userId)
                return mCachedUser;
        }

        User user = mUserDataSource.getUser(userId);
        refreshUserCache(user);
        return user;
    }

    @Override
    public User getUserByEmail(String email) {
        if (!cacheBroken && mCachedUser != null) {
            if (mCachedUser.getEmail().equals(email)) {
                return mCachedUser;
            }
        }

        User user = mUserDataSource.getUserByEmail(email);
        refreshUserCache(user);
        return user;
    }

    @Override
    public boolean saveUser(User user) {
        forceBreakCache();
        return mUserDataSource.saveUser(user);
    }

    @Override
    public boolean update(User user) {
        forceBreakCache();
        return mUserDataSource.update(user);
    }

    public void forceBreakCache() {
        cacheBroken = true;
    }

    private void refreshUserCache(User user) {
         mCachedUser = user;
        cacheBroken = false;
    }

}
