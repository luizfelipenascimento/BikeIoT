package android.scopus.com.bikeiot.model.data.local;

import android.provider.BaseColumns;

/**
 * Created by Luiz Felipe on 08/02/2018.
 */

public final class BikeIoTContract {

    private interface MyBaseColumns extends BaseColumns {
        public static final String _ON_CREATE = "on_create";
        public static final String _ON_UPDATE = "on_update";
    }

    private BikeIoTContract() {}

    public static class UserEntry implements MyBaseColumns {

        //TODO: Definir se o usuario tera altura e peso

        public static final String TABLE_NAME = "user";

        public static final String _ID = MyBaseColumns._ID;
        public static final String COLUMN_USER_NAME = "name";
        public static final String COLUMN_USER_BIRTHDAY = "birthday";
        public static final String COLUMN_USER_EMAIL = "email";
        public static final String COLUMN_USER_WEIGHT = "weight";
        public static final String COLUMN_USER_PASSWORD = "password";
        public static final String COLUMN_USER_HEIGHT = "height";
        public static final String COLUMN_USER_UUID = "uuid";
        public static final String _ON_CREATE = MyBaseColumns._ON_CREATE;
        public static final String _ON_UPDATE = MyBaseColumns._ON_UPDATE;
    }

    /*
    * É necessário colocar as duas tabelas tanto Result quando BikeExercise juntas
    * devido a cardinalidade 1,1 */
    public static class BikeExerciseAndResultEntry implements MyBaseColumns {

        public static final String TABLE_NAME = "bikeExerciseAndResult";

        /*colunas referentes ao exercicio de bicicleta*/
        public static final String COLUMN_EXERCISE_DATE = "date";

        public static final String COLUMN_DESCRIPTION = "description";

        /* colunas referentes ao resultado:*/
        /*datatype: TEXT*/
        public static final String COLUMN_RESULT_FILE_PATH = "result_filepath";

        /*datatype: DATETIME*/
        public static final String COLUMN_RESULT_TOTAL_TIME_CHRONOMETER = "total_time_chronometer";

        /*datatype: real*/
        public static final String COLUMN_RESULT_MEAN_VELOCITY = "mean_velocity";

        /*datatype: real*/
        public static final String COLUMN_RESULT_MAX_VELOCITY = "max_velocity";

        /*datatype: integer  - informacao do tamanho do aro da bicicleta utilizada*/
        public static final String COLUMN_USED_BIKE_RIM = "used_bike_rim";

        public static final String COLUMN_RESULT_MEAN_HEART_RATE = "mean_heart_rate";

        public static final String COLUMN_RESULT_CALORIES_BURNED = "calories_burned";

        /*Foreign key da tabela user:*/
        public static final String COLUMN_FK_USER_ID = "fk_user_id";

        /*Foreign key da tabela route:*/
        public static final String COLUMN_FK_ROUTE_ID = "fk_route_id";

    }


    public static class RouteEntry implements MyBaseColumns {

        public static final String TABLE_NAME = "route";

        public static final String _ID = MyBaseColumns._ID;
        public static final String COLUMN_ROUTE_DISTANCE = "distance";
        public static final String COLUMN_ROUTE_DESCRIPTION = "description";
        public static final String COLUMN_ROUTE_START_LATITUDE = "start_latitude";
        public static final String COLUMN_ROUTE_START_LONGITUDE = "start_longitude";
        public static final String COLUMN_ROUTE_END_LATITUDE = "end_latitude";
        public static final String COLUMN_ROUTE_END_LONGITUDE = "end_longitude";
        public static final String COLUMN_ROUTE_FILE_PATH = "route_filepath";
        public static final String COLUMN_IMAGE_PATH = "image_path";
        public static final String COLUMN_ROUTE_DATE = "date";
        public static final String COLUMN_ROUTE_SHARED = "shared";
        public static final String _ON_CREATE = MyBaseColumns._ON_CREATE;
        public static final String _ON_UPDATE = MyBaseColumns._ON_UPDATE;

        public static final String COLUMN_ROUTE_FK_USER_ID = "fk_user_id";

        public static final int TO_SHARE = 1;
        public static final int NOT_TO_SHARE = 0;
    }

    public static class ContactsEntry implements  MyBaseColumns {
        public static final String TABLE_NAME = "contacts";

        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_PHONE = "phone";

        public static final String COLUMN_FK_USER = "fk_user_id";
    }

    public static class Achievement implements MyBaseColumns {
        public static final String TABLE_NAME = "achievement";

        public static final String COLUMN_ID = BaseColumns._ID;
        public static final String COLUMN_CONDITION = "condition";
        public static final String COLUMN_POINTS = "points";
        public static final String COLUMN_DESCRIPTION = "description";
    }

    public static class CompletedAchievement implements MyBaseColumns {

        public static final String TABLE_NAME = "completedAchievement";

        /*Foreign key da tabela achievement*/
        public static final String COLUMN_ID_ACHIEVEMENT = "achievement_id";
        /*Foreign key da tabela achievement user*/
        public static final String COLUMN_ID_USER = "user_id";

        public static final String COLUMN_DATE = "date";
    }

    public static class Friend implements MyBaseColumns {

    }


    /*Definição das tabelas separadas:*/
    /*public static class BikeExerciseEntry implements MyBaseColumns {

        public static final String TABLE_NAME = "bikeExercise";

        public static final String _ID = MyBaseColumns._ID;
        public static final String COLUMN_FK_USER_ID = "user_id";
        public static final String COLUMN_RESULT_ID = "result_id";
        public static final String COLUMN_ROUTE_ID = "route_id";
        public static final String COLUMN_EXERCISE_DATE = "date";
        public static final String _ON_CREATE = MyBaseColumns._ON_CREATE;
        public static final String _ON_UPDATE = MyBaseColumns._ON_UPDATE;
    }

    public static class ResultEntry implements MyBaseColumns {

        public static final String TABLE_NAME = "result";

        public static final String _ID = MyBaseColumns._ID;
        public static final String COLUMN_RESULT_FILE_PATH = "result_filepath";
        public static final String _ON_CREATE = MyBaseColumns._ON_CREATE;
        public static final String _ON_UPDATE = MyBaseColumns._ON_UPDATE;
    }*/
}
