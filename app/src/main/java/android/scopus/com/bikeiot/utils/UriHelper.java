package android.scopus.com.bikeiot.utils;

import android.net.Uri;

import java.io.File;

/**
 *
 */

public class UriHelper {

    public static Uri getUriFromFile(String stringUri) {
        Uri uri = null;
        File imageFile = new File(stringUri);
        if (imageFile.exists() && !imageFile.isDirectory()) {
            uri = Uri.fromFile(imageFile);
        }
        return uri;
    }
}
