package android.scopus.com.bikeiot.model.network;

/**
 * Created by Luiz Felipe on 18/04/2018.
 */

public interface ApiListenerBasic {
    void onNetworkProblem();
}
